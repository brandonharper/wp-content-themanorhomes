<?php
/*
 Template Name: Property Results
*/
?>
<?php
include_once( get_template_directory() . '/MLS-DB-Atlanta.php' );

header('Cache-Control: max-age=900');
?>

<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1444229760055{margin-bottom: 0px !important;}.vc_custom_1443104683429{margin-top: 0px !important;margin-bottom: 0px !important;background-image: url(http://manor.stradigys.com/wp-content/uploads/2015/09/lot-map-hero.jpg?id=1494) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1443189133927{margin-bottom: 0px !important;background-color: #0f6748 !important;}.vc_custom_1443197011586{margin-top: 0px !important;padding-top: 5% !important;padding-bottom: 5% !important;background-color: #efefef !important;}.vc_custom_1443194978576{margin-bottom: 0px !important;}.vc_custom_1444229767474{margin-bottom: 0px !important;}.vc_custom_1443128418176{padding-top: 10% !important;padding-bottom: 10% !important;}.vc_custom_1444148222353{background-color: rgba(255,255,255,0.48) !important;*background-color: rgb(255,255,255) !important;}.vc_custom_1443104692327{padding-top: 10% !important;padding-bottom: 10% !important;}.vc_custom_1443128426952{padding-top: 2.5% !important;padding-bottom: 2.5% !important;}.vc_custom_1443128434929{padding-top: 2.5% !important;padding-bottom: 2.5% !important;}.vc_custom_1443128442005{padding-top: 2.5% !important;padding-bottom: 2.5% !important;}.vc_custom_1443625783131{margin-top: 35px !important;background-image: url(http://manor.stradigys.com/wp-content/uploads/2015/09/explore-milton.png?id=1537) !important;background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1443625787822{margin-top: 35px !important;background-image: url(http://manor.stradigys.com/wp-content/uploads/2015/09/buildyourlegacy.jpg?id=1535) !important;background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1443625772218{margin-top: 35px !important;background-image: url(http://manor.stradigys.com/wp-content/uploads/2015/09/jointheclub.jpg?id=1536) !important;background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1443625793189{margin-top: 35px !important;background-image: url(http://manor.stradigys.com/wp-content/uploads/2015/09/searchourlistings.jpg?id=1534) !important;background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1443194972730{margin-bottom: 0px !important;}

}

</style><noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>		


<?php get_header(); ?>

<?php

	$data 			=   new db();
	$_SESSION['viewtype']	= 		$viewtype		= 	($_REQUEST ["view_type"]== '') ? $_SESSION['viewtype'] : $_REQUEST ["view_type"] ;

	//$result = "SELECT * FROM mls_properties_atlanta WHERE MLNUMBER IN ('5304286','5353867','5273176','5277513','5350085','5317474','5367598','5324830','5338426','5325506','5242115','5360576','5359859','5274458','5274448','5359854','5274445','527443') ORDER BY LISTPRICE DESC";
	//$result = "SELECT * from mls_properties_atlanta WHERE SUBDIVISION LIKE 'The Manor Golf and Country Clu' AND PROPERTYTYPE LIKE 'Residential Detached'";
	//$result = "SELECT * from mls_properties_atlanta WHERE SubdComplex LIKE 'The Manor%' AND PropertyType LIKE 'Residential Detached'";
	//$result = "SELECT * from mls_properties_atlanta WHERE SubdComplex LIKE 'The Manor%' ";
$result = "SELECT * from mls_properties_atlanta ORDER BY ListPrice";
	
	$current_page 	= 	site_url()."/".get_page_uri($page_id);

	$tbl_name="mls_properties_atlanta";		//your table name
	// How many adjacent pages should be shown on each side?
	$adjacents = 2;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	
	
	
	
	global $user_ID; if( $user_ID ) : 
		if( current_user_can('level_10') ) : 
			//Shows SQL Query 
			//echo "<br />---------------- PRINTING QUERY RESULT------------------<Br />";
			//echo "Result is: ".$result;
 	//echo "<br />---------------- PRINTING QUERY RESULT------------------<Br />";
	//echo "Result is: ".$result;
	//Print_r ($_SESSION);
  endif;
	endif;
 
 		$query = $result;		
	$total_pages = mysql_num_rows(mysql_query($query));
	//echo $total_pages;
	
	/* Setup vars for query. */
	$targetpage = $current_page; 	//your file name  (the name of this file)
	
	if (!$viewtype || $viewtype == 'list') {
		$limit =	8;
	} else if ($viewtype == 'grid') {
		$limit = 8;
	}	  
						//how many items to show per page
	
	
	$page = (get_query_var('page')) ? get_query_var('page') : 1; 

	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
		
		
	/* Get data. */
	$sql = $result." LIMIT $start, $limit";
	$execute = mysql_query ($sql);

	
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage/$prev\">&lt;&nbsp;Previous Page&nbsp;</a>";
		else
			//$pagination.= "<span class=\"disabled\">&lt;&nbsp;PREVIOUS&nbsp;</span>";	
			$pagination.= "";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
				else
					$pagination.= "<a href=\"$targetpage/$counter\" class='next'>&nbsp;$counter&nbsp;</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
					else
						$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage/$lpm1\">&nbsp;$lpm1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/$lastpage\">&nbsp;$lastpage&nbsp;</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage/1\">&nbsp;1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/2\">&nbsp;2&nbsp;</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
					else
						$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage/$lpm1\">&nbsp;$lpm1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/$lastpage\">&nbsp;$lastpage&nbsp;</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage/1\">&nbsp;1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/2\">&nbsp;2&nbsp;</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
					else
						$pagination.= "<a href=\"$targetpage/$counter\"> &nbsp;$counter &nbsp;</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
		{
			$pagination.= "<a href=\"$targetpage/$next\">&nbsp;Next Page&nbsp;></a>";
			}
		else
		{
				$pagination.= "<span class=\"disabled\">&nbsp;Next Page&nbsp;></span>";
		}
		$pagination.= "</div>\n";		
		
		
		
	}

	
?>



			<div id="content">

				<div id="inner-content" class="wrap cf defaultwrapper">

						<div id="main" class="m-all t-all d-all cf defaultpages" role="main">
							
							<!--*<div class="side-contact">
								<div class="side-title">Contact The Manor <br />Brokerage Team</div>
								<?php echo do_shortcode('[contact-form-7 id="980" title="Side Contact" html_class="use-floating-validation-tip"]'); ?>
							</div>

							<h1>Choose a Manor home today and begin living life to the fullest</h1>---->
							
							<div class="style-selector cf" style="display:none;">
								<a href="/find-your-home/1/?view_type=list" class="list-view">List View</a> <a href="/find-your-home/1/?view_type=grid" class="grid-view">Grid View</a> <a href="/explore" class="explore-button">Build Your Dream Home</a>
							</div>
							<div class="results-info cf">
								<div class="leftside"><?php echo number_format($total_pages);?> Properties</div>
								<div class="rightside"><?php echo $pagination;?></div>
							</div>
							
							<?php if (!$viewtype || $viewtype == 'list') { ?><ul class="display-grid-view cf"><?php } else if ($viewtype == 'grid') {?><ul id="gridlist" class="display-grid-view cf"><?php } ?>
								<?php while ($mlsData = mysql_fetch_array($execute)) { ?>
								<?php 

									$new_address = $mlsData["StreetNumber"] . " " . $mlsData["StreetName"] . " " . $mlsData["StreetSuffix"];


									$title 		= $new_address.", <Br />".substr($mlsData["City"].", ".$mlsData['StateOrProvince']." ".$mlsData['PostalCode'], 0, 40);	
				                  	
				                	$site_title = str_replace(",","",$title);
				                	$site_title = str_replace("#","",$site_title);
				                	$site_title = str_replace(".","",$site_title);
				                 	$site_title	= preg_replace ('/\s+/','-',$site_title);
				                 	
				                 	$URLtitle 		= $new_address.", ".substr($mlsData["City"].", ".$mlsData['StateOrProvince'].", ".$mlsData['PostalCode'], 0, 40);	
				                 	$urlsite_title = str_replace(",","",$URLtitle);
				                	$urlsite_title = str_replace("#","",$urlsite_title);
				                	$urlsite_title = str_replace(".","",$urlsite_title);
				                 	$urlsite_title	= preg_replace ('/\s+/','-',$urlsite_title);
								?>
								
								<li>
									<div class="property">
										<div class="property-image" style="position: relative; width: 504px; height: 346px; display: block;">
												<a href="<?php echo site_url(); ?>/property/<?php echo $mlsData['MLSNumber']; ?>/<?php echo trim($urlsite_title); ?>">
													<img src="<?php echo $data->get_MLSimage($mlsData['Matrix_Unique_ID']);?>"/>
												</a>
												<?php if ($mlsData["PropertyType"] == "Res Developed Lots") {  ?><!--<img src="<?php echo get_template_directory_uri(); ?>/library/images/home-site-lot.png" style="position: absolute; width: 237px; height: 122px; top: 0px; right: 0px;"/>--><img src="<?php echo get_template_directory_uri(); ?>/library/images/home-site-lot-2.png" style="position: absolute; width: 190px; height: 33px !important; padding: 0; top: 25px; right: 1px;"/><?php } ?>
										</div>
													
													

										<div class="propertyid">ID: <?php echo $mlsData["MLSNumber"];?> 
									<?php 
										
										$mapinfo = pods( 'listingstatus' ); 
									    $params = array('limit' => 400, 'where' => 'mls_id.meta_value = "' . $mlsData["MLSNumber"] . '"');  
										$mapinfo->find($params);
										while ( $mapinfo->fetch() ) { 
										
											if ($mapinfo->field('lot_number')   ) { 	?>
											
												 &nbsp;&nbsp;Lot: <?php echo $mapinfo->field('lot_number');
												  ?> 
												
											

											<?php	
											}
												
										}
						
									?>

									<?php 
										
										$mapinfo = pods( 'listingstatus' ); 
									    $params = array('limit' => 400, 'where' => 'mls_id.meta_value = "' . $mlsData["MLSNumber"] . '"');  
										$mapinfo->find($params);
										while ( $mapinfo->fetch() ) { 
										
											if ($mapinfo->field('lot_status')   ) { 	?>
											
												 &nbsp;&nbsp;Status: <?php echo $mapinfo->field('lot_status');
												  ?> 
												
											

											<?php	
											}
												
										}
						
									?>


								</div>
										<div class="m-1of2 t-1of2 d-1of2 last-col cf">
										<div class="address"><a href="<?php echo site_url(); ?>/property/<?php echo $mlsData['MLSNumber']; ?>/<?php echo trim($urlsite_title); ?>"><?php if (!$viewtype || $viewtype == 'list') { ?><?php echo $new_address.", <br />".$mlsData["City"].", ".$mlsData['StateOrProvince']." ".$mlsData['PostalCode'];  ?><?php } else if ($viewtype == 'grid') {?><?php echo $mlsData["ADDRESS"]."<br /> ".$mlsData["CITY"].", ".$mlsData["STATE"]." ".$mlsData["ZIPCODE"];  ?><?php } ?></a></div>
										
										</div>


										<div class="cf maxheight">
											
											<div class="m-1of2 t-1of2 d-1of2 last-col cf">
												
											

											<div class="property-details">
												
													<div class="price">
													$<?php echo number_format($mlsData["ListPrice"])?></div>
													
													<div class="detail-info"><?php if ($mlsData["PropertyType"] != "Res Developed Lots") { ?> 
													<?php echo ($mlsData["BedsTotal"] ? $mlsData["BedsTotal"] : "0")?> Bedrooms |
													<?php echo ($mlsData["BathsFull"] ? $mlsData["BathsFull"] : "0")?> Full Baths |
													<?php echo ($mlsData["BathsHalf"] ? $mlsData["BathsHalf"] : "0")?> Half Baths</div>
													<?php } ?>
													<?php// echo $mlsData["PROPERTYTYPE"]?>
													
													<!--<a href="<?php echo site_url(); ?>/property/<?php echo $mlsData['Matrix_Unique_ID']; ?>/<?php echo trim($urlsite_title); ?>" class="see-more">See More</a>---->
													<div class="sotheby">Atlanta Fine Homes<Br />Sotheby's International Realty</div>
											</div>
										</div>




										</div>

										<div class="description"><?php// echo $mlsData["REMARKS"];?>
										</div>
									</div>
								
								</li>
								<?php } ?>
							</ul>
							<div class="results-info cf">
								<div class="leftside"><?php echo number_format($total_pages);?> Properties</div>
								<div class="rightside"><?php echo $pagination;?></div>
							</div>
							
						</div>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?><?php the_content(); ?><?php endwhile; endif; ?>
				</div>
<!--<div class="vc_row wpb_row vc_row-fluid spring-board-container vc_custom_1443197011586 " data-vc-full-width="true" data-vc-full-width-init="true" style="margin-bottom:0px !important;margin-right: 0px !important;">
    
    <div class="wrap cf ">

    <div class="cta-bottom-box-spacing wpb_column vc_column_container vc_col-sm-6">
        <div class="wpb_wrapper">
            <div class="vc_row wpb_row vc_inner vc_row-fluid cta-text-box-w-overlay-container vc_custom_1443625783131">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element cta-text-box-w-overlay">
                            <div class="wpb_wrapper">
                                <div class="home-bottom-cta-bg">
                                    <p class="home-bottom-cta-title"><a class="homectalink" href="/premier-neighborhoods/">Explore Milton</a></p>
                                    <p class="home-bottom-cta-text"><a href="/premier-neighborhoods/">The Manor is set in the heart of Milton – one of the best townships in Greater Alpharetta and close to shopping, dining and more.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid cta-text-box-w-overlay-container vc_custom_1443625787822">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element cta-text-box-w-overlay">
                            <div class="wpb_wrapper">
                                <div class="home-bottom-cta-bg">
                                    <p class="home-bottom-cta-title"><a href="/premier-builders/">Build Your Legacy</a></p>
                                    <p class="home-bottom-cta-text"><a href="/premier-builders/">Partner with one of our premier builders to create your custom dream estate. Our home lots are large, private and ready to build.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cta-bottom-box-spacing wpb_column vc_column_container vc_col-sm-6">
        <div class="wpb_wrapper">
            <div class="vc_row wpb_row vc_inner vc_row-fluid cta-text-box-w-overlay-container vc_custom_1443625772218">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element cta-text-box-w-overlay">
                            <div class="wpb_wrapper">
                                <div class="home-bottom-cta-bg">
                                    <p class="home-bottom-cta-title"><a href="/premier-amenities/">Join the Club</a></p>
                                    <p class="home-bottom-cta-text"><a href="/premier-amenities/">The Manor Golf &amp; Country Club is the newest private social club in Alpharetta, offering world-class golf, tennis, swimming, dining and events.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid cta-text-box-w-overlay-container vc_custom_1443625793189">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element cta-text-box-w-overlay">
                            <div class="wpb_wrapper">
                                <div class="home-bottom-cta-bg">
                                    <p class="home-bottom-cta-title"><a href="/explore/">Search Our Listings</a></p>
                                    <p class="home-bottom-cta-text"><a href="/explore/">In addition to spacious ready-to-build lots, The Manor offers an inventory of finished custom homes overlooking the golf course.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
				
</div> <!--end wrap cf-->
				
			</div>

<?php get_footer(); ?>