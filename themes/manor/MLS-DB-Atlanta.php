<?php

//header('Cache-Control: no cache'); //no cache
//session_cache_limiter('private_no_expire'); // works
//session_cache_limiter('public'); // works too

@session_start();
// 20 mins in seconds

$site_url = site_url()."/".get_page_uri($page_id)."/".basename($_SERVER['REQUEST_URI']);

define('table_name','mls_properties_atlanta');
define ('serch_imge_dir',WP_CONTENT_DIR."/property_image/search"); //delete?
define ('serch_imge_url',WP_CONTENT_URL."/property_image/search"); //delete?
define ('imge_url',WP_CONTENT_URL."/property_image"); 			   //delete?
define('MLSImages',"http://mls.virsocom.com/mls-atlanta/images");

class db
	{
		function __construct() 
		{	
			$MLSDBSQL = mysql_connect("64.207.177.75","mls_manorhomes","pr0l1f1k");
			mysql_select_db("mls_manorhomes");
		}
			
		function query($TABLE,$FIELD,$WHERE) 
		{
			$query = "SELECT DISTINCT `$FIELD`  FROM  `$TABLE`  WHERE  $WHERE and list_137 >= 10000  and `$FIELD` != '' ORDER BY `$FIELD` ";
		
			$query_exec		= mysql_query($query);
			
			while($result=mysql_fetch_assoc($query_exec)) {				
				$query_result[] = $result[$FIELD];
			}
			
			return $query_result;
		}

		function select_box($TABLE,$FIELD,$WHERE)
		{
			$result = $this-> query($TABLE,$FIELD,$WHERE);
			foreach ($result as $item) 
			{
				echo  "<option value='$item'>$item</option>";
			}				
		}
			
		function peek_value($value)
		{
			if(($value=="1903 -  ". date('Y'))||($value=="500 - 10000"))
			{
				return '';
			}
			else
				{
				$result	= explode(' - ',$value);
				$return['low'] 	= $result[0];
				$return['high'] = $result[1];
				return $return;
			}
		}
				
		function search($value)
		{
			$_SESSION['uniqueId']	= 		$uniqueId 		= 	($_REQUEST ["search_MLS"] == '') ? $_SESSION['uniqueId'] : $_REQUEST ["search_MLS"] ;
			$_SESSION['address']	= 		$address 		= 	($_REQUEST ["search_Address"]== '') ? $_SESSION['address'] : $_REQUEST ["search_Address"] ;
			$_SESSION['city']		= 		$city 			= 	($_REQUEST ["search_City"] == '') ? $_SESSION['city'] : $_REQUEST ["search_City"] ;
			$_SESSION['area']		= 		$area 			= 	($_REQUEST ["search_Area"]== '') ? $_SESSION['area'] : $_REQUEST ["search_Area"] ;
			$_SESSION['zip']		= 		$zip 			= 	($_REQUEST ["search_Zip"]== '') ? $_SESSION['zip'] : $_REQUEST ["search_Zip"] ;
			$_SESSION['yearBuilt']	= 		$yearBuilt 		= 	($_REQUEST ["search_year"]== '') ? $_SESSION['yearBuilt'] : $this->peek_value($_REQUEST ["search_year"]);
			$_SESSION['sqFoot']		= 		$sqFoot 		= 	($_REQUEST ["search_sqftArea"]== '') ? $_SESSION['sqFoot'] : $this->peek_value($_REQUEST ["search_sqftArea"]);
			$_SESSION['keywords']	= 		$keywords 		= 	($_REQUEST ["search_Keywords"]== '') ? $_SESSION['keywords'] : $_REQUEST ["search_Keywords"] ;
			
			/*$_SESSION['filter_area']	= 		$filter_area 		= 	($_REQUEST ["filter_area"]== '') ? $_SESSION['filter_area'] : $_REQUEST ["filter_area"] ;*/
			
			
					
			
			if (isset($_REQUEST ["search_property"])) {
			
				$_SESSION['property']	= 	 $_REQUEST['search_property'];
			}
			
			
			if ( $_REQUEST["filter_type"] != '' ) {
				$_SESSION['property'] = array($_REQUEST["filter_type"]);
			}
			
			
			if (isset($_POST['search_type'])) {
				$_SESSION['type'] = $_POST ['search_type'];
				unset ($_POST ['search_type']);
			}
			
			
			$_SESSION['bedMin']		= 		$bedMin 		= 	($_REQUEST["search_BedroomsTotal_l"]== '') ? $_SESSION['bedMin'] : $_REQUEST ["search_BedroomsTotal_l"] ;
			$_SESSION['bedMax']		= 		$bedMax 		= 	($_REQUEST["search_BedroomsTotal_m"]== '') ? $_SESSION['bedMax'] : $_REQUEST ["search_BedroomsTotal_m"] ;
			$_SESSION['bathMin']	= 		$bathMin 		= 	($_REQUEST["search_BathsTotal_l"]== '') ? $_SESSION['bathMin'] : $_REQUEST ["search_BathsTotal_l"] ;
			$_SESSION['bathMax']	= 		$bathMax 		= 	($_REQUEST["search_BathsTotal_m"]== '') ? $_SESSION['bathMax'] : $_REQUEST ["search_BathsTotal_m"] ;
			$_SESSION['priceMin']	= 		$priceMin 		= 	($_REQUEST["search_ListPrice_l"]== '') ? $_SESSION['priceMin'] : $_REQUEST ["search_ListPrice_l"] ;
			$_SESSION['priceMax']	= 		$priceMax 		= 	($_REQUEST["search_ListPrice_m"]== '') ? $_SESSION['priceMax'] : $_REQUEST ["search_ListPrice_m"] ;
					
				$query = "select * from ".table_name." where 1 ";
				
				if ($uniqueId) {
					$query .= " and list_157 = '".$_SESSION['uniqueId']."'";
				}
				if ($address) {
					$query .= " and (list_881 like '".$_SESSION['address']."%'";
					
					//#### SMART SEARCH for Address Alias
					$smartSearch = array( 
						"Place" => "PL",
						"Street" => "ST",
						"Avenue" => array("Av","Ave"),
						"Av" => array("Avenue","Ave"),
						"Ave" => array("Avenue","Av"),
						"Road" => "RD",
						"RD" => "Road",
						"Way" => "WY",
						"WY" => "Way",
						"Drive" => "DR",
						"Court" => "CT",
						"CT" => "Court",
						"Terrace" => array("TE","TER"),
						"Ter" => array("Terrace","TE"),
						"Te" => array("Terrace","TER"),
						"Lane" => "LN",
						"LN" => "Lane",
						"Boulevard" => array("BL","BVLD"),
						"BL" => array("Boulevard","BVLD"),
						"BLVD" => array("Boulevard","BL"),
						"Penthouse" => "PH",
						"PH" => "Penthouse",
						"Highway" => array("HY","HWY"),
						"HY" => array("Highway","HWY"),
						"HWY" => array("Highway","HY"),
						"Parkway" => array("Parkway PW","PW"),
						"PW" => array("Parkway PW","Parkway"),
						"Parkway PW" => array("Parkway","PW"),
						"Townhouse" => array("TH"),
						"TH" => "Townhouse",
						"E" => "East",
						"East" => "E",
						"W" => "West",
						"West" => "W",
						"S" => "South",
						"South" => "S",
						"N" => "North",
						"North" => "N",
						"SW" => "Southwest",
						"Southwest" => "SW",
						"NW" => "Northwest",
						"Northwest" => "NW",
						"SE" => "Southest",
						"Southeast" => "SE",
						"NE" => "Northeast",
						"Northeast" => "NW"
					);
					
					foreach($smartSearch as $key=>$value)
					{
						if(stripos($_SESSION['address'],$key)!==false)
						{
						
							if (is_array($value)) {
								//If multidimensional array alias values
								foreach($value as $alias) {
									$query .= " or list_881 like '".str_ireplace($key, $alias, $_SESSION['address'])."%'";
								}
							} else {
								//If single alias value
								//echo " _ _ _ ".$value."\n";;
								$query .= " or list_881 like '".str_ireplace($key, $value, $_SESSION['address'])."%'";
							}
				
						}
					}	
					
					$query .= " ) ";	
					// END Smart Search		
					
					
				}
				if ($city && ($city != "Select City")) {
					if ($city == "Coconut Grove") {
						$query .= " and ( list_922 = '".$_SESSION['city']."' or (list_922 ='Miami' and list_10 ='33133') ) ";
					}
					else {
						$query .= " and list_922 = '".$_SESSION['city']."' ";
					}
				}
				if ($zip) {
					$query .= " and list_10 = ".$_SESSION['zip'];
				}
				if ($area && ($area != "Select Location")) {
				//	$query .= " and list_352 = '".$_SESSION['area']."'";
					$query .= " and (list_235 LIKE  '".$_SESSION['area']."' OR list_922 LIKE  '".$_SESSION['area']."' ) ";
				}
				/*if (isset($_SESSION['type'])) {
					if (in_array($_SESSION['type'],$_SESSION['property'])) {
						$query .= " and list_1 like '%".$_SESSION['type']."%'";
					}
				}*/
				
//Keywords
				if($keywords!='')
		    	{
		    		$query .= " and ( (list_922 like '%$keywords%' or list_235 like '%$keywords%' or list_1339 like '%$keywords%' or list_115 like '%$keywords' or list_214 like '%$keywords%' or list_326 like '%$keywords%' OR list_69 like '%$keywords%' OR list_157 like '%$keywords%' OR list_881 like '%$keywords%' OR list_10 like '%$keywords%') ";
		    		
		    		//#### SMART SEARCH for Address Alias
					$smartSearch = array( 
						"Place" => "PL",
						"Street" => "ST",
						"Avenue" => array("Av","Ave"),
						"Av" => array("Avenue","Ave"),
						"Ave" => array("Avenue","Av"),
						"Road" => "RD",
						"RD" => "Road",
						"Way" => "WY",
						"WY" => "Way",
						"Drive" => "DR",
						"Court" => "CT",
						"CT" => "Court",
						"Terrace" => array("TE","TER"),
						"Ter" => array("Terrace","TE"),
						"Te" => array("Terrace","TER"),
						"Lane" => "LN",
						"LN" => "Lane",
						"Boulevard" => array("BL","BVLD"),
						"BL" => array("Boulevard","BVLD"),
						"BLVD" => array("Boulevard","BL"),
						"Penthouse" => "PH",
						"PH" => "Penthouse",
						"Highway" => array("HY","HWY"),
						"HY" => array("Highway","HWY"),
						"HWY" => array("Highway","HY"),
						"Parkway" => array("Parkway PW","PW"),
						"PW" => array("Parkway PW","Parkway"),
						"Parkway PW" => array("Parkway","PW"),
						"Townhouse" => array("TH"),
						"TH" => "Townhouse",
						"E" => "East",
						"East" => "E",
						"W" => "West",
						"West" => "W",
						"S" => "South",
						"South" => "S",
						"N" => "North",
						"North" => "N",
						"SW" => "Southwest",
						"Southwest" => "SW",
						"NW" => "Northwest",
						"Northwest" => "NW",
						"SE" => "Southest",
						"Southeast" => "SE",
						"NE" => "Northeast",
						"Northeast" => "NW"
					);
		    		
					
					foreach($smartSearch as $key=>$value)
					{
						if(stripos($keywords,$key)!==false)
						{
						
							if (is_array($value)) {
								//If multidimensional array alias values
								foreach($value as $alias) {
								$newKeyword = str_ireplace($key, $alias, $keywords);
								$query .= " or (list_922 like '%$newKeyword%' or list_235 like '%$newKeyword%' or list_1339 like '%$newKeyword%' or list_115 like '%$newKeyword%' or list_214 like '%$newKeyword%' or list_326 like '%$newKeyword%' OR list_69 like '%$newKeyword%' OR list_157 like '%$newKeyword%' OR list_881 like '%$newKeyword%' OR list_10 like '%$newKeyword%') ";
								}
							} else {
								//If single alias value
								$newKeyword = str_ireplace($key, $value, $keywords);
								$query .= " or (list_922 like '%$newKeyword%' or list_235 like '%$newKeyword%' or list_1339 like '%$newKeyword%' or list_115 like '%$newKeyword%' or list_214 like '%$newKeyword%' or list_326 like '%$newKeyword%' OR list_69 like '%$newKeyword%' OR list_157 like '%$newKeyword%' OR list_881 like '%$newKeyword%' OR list_10 like '%$newKeyword%') ";
							}
				
						}
					}	
					
					$query .= ") ";	
					// END Smart Search	
		    		
		    		
		    	}				
				//Property Types Queries
				if (isset($_SESSION['property'])) {
					
					$query .= " and (";
					
					
					//- - - - - - - - Exclusive searches for property types - - - - - - - -
					if ( in_array('Open Houses', $_SESSION['property']) || in_array('New Listings', $_SESSION['property']) || in_array('Waterfront', $_SESSION['property']) || in_array('Short Sale', $_SESSION['property']) || in_array('Rental', $_SESSION['property']) || in_array('Bank Owned', $_SESSION['property']) || in_array('Penthouse', $_SESSION['property']) ) {
					
						foreach ($_SESSION['property'] as $value) {
						
						
							//Open Houses
						    if ($value=='Open Houses') 
							{
								$query .= " list_1155 <> '' AND ";
							}
							//New Listings
						    else if ($value=='New Listings') 
							{
								$query .= " list_74 < '8' AND list_74 > '0' AND ";
							}
							//Waterfront
							else if($value=='Waterfront')
							{
								$query .= " list_295 = 'Yes' AND ";
							}
							//Short Sales
							else if ($value=='Short Sale')
							{
								$query .= " list_1465 = 'Yes' AND ";
							}
							//Rental
							else if ($value=='Rental')
							{
								$query .= " list_1 like '%Rental%' AND ";
							}
							//Bank Owned
							else if ($value=='Bank Owned') 
							{
								$query .= "  list_1473 = 'Yes' AND ";
							}
							//Penthouses
							else if ($value=='Penthouse')
							{
								$query .= "  list_17 LIKE '%PH%' AND ";
							}
						}
						
					}
					
					//- - - - - - - - Inclusive searches for property types - - - - - - - -
					foreach ($_SESSION['property'] as $value) {

												
						//Single Family
						if ( !in_array('Rental', $_SESSION['property'])) {
						
							if ($value=='Single Family')
							{
								$query .= " list_1 like '%Single Family%' or  ";
							}
							//Condo
							else if ($value=='Condo')
							{
								$query .= " list_370 like '%Condo%' or ";
							}
							//Commercial
							else if ($value=='Commercial')
							{
								$query .= " list_1 like '%Commercial%' or ";
							}
							//Town House
							else if ($value=='Townhouse')
							{
								$query .= " list_370 like '%Townhouse%' or ";
							}
							//Vacant Land
							else if ($value=='Vacant Land')
							{
								$query .= " list_1 like '%Residential Land/Boat Docks%' or ";
							}
							//Multi Family
							else if ($value=='Multi Family')
							{
								$query .= " list_1 like '%Residential Income%' or ";
							}
						}
						
						
						

						
					}
					
					$query = substr($query,0,-4);
					$query .= ")";
					
														
						
					
									
				}
					/* query for water front end */
				
				if ($yearBuilt && ($yearBuilt['low'] != "1903" || $yearBuilt['high'] != date('Y'))&& ($yearBuilt['low'] != "") && ($yearBuilt['high'] != "")) {
			
					$query .=  " AND list_314 >= ".$_SESSION['yearBuilt']['low']."
								AND list_314 <= ".$_SESSION['yearBuilt']['high'];				
				}
				if (isset($sqFoot['low']) && isset($sqFoot['high'])) {
						if ($sqFoot['low'] == 500 && $sqFoot['high'] != 10000) {
							$query .= " AND list_232 <= ".$_SESSION['sqFoot']['high'];
						}
						elseif ($sqFoot['low'] != 500 && $sqFoot['high'] == 10000) {
							$query .= " AND list_232 >= ".$_SESSION['sqFoot']['low'];
						}
						elseif ($sqFoot['low'] != 500 && $sqFoot['high'] != 10000) {
							$query .=  " AND list_232 >= ".$_SESSION['sqFoot']['low']."
									AND list_232 <= ".$_SESSION['sqFoot']['high'];	
						}
				}
				
				/*if ($sqFoot && ($sqFoot['low'] != "500" || $sqFoot['high'] != "10000")&& ($sqFoot['low'] != "") && ($sqFoot['high'] != "")) {				
					$query .=  " AND list_232 >= ".$_SESSION['sqFoot']['low']."
								AND list_232 <= ".$_SESSION['sqFoot']['high'];	
							
				}*/
				
				if ($bedMin && ($bedMin!="Minimum")) {
					$query .= " and list_25 >= ".$_SESSION['bedMin'];
				}
				if ($bedMax && ($bedMax!="Maximum")) {
					if ($bedMax == "5" && $bedMin != "5") {
						$query .= " and list_25 <= 5";
					}
					elseif ($bedMax != "5") {
						$query .= " and list_25 <= ".$_SESSION['bedMax'];
					}
				}
				if ($bathMin && ($bathMin!="Minimum")) {
					$query .= " and list_92 >= ".$_SESSION['bathMin'];
				}
				if ($bathMax && ($bathMax!="Maximum")) {
					if ($bathMax == "5" && $bathMin != "5") {
						$query .= " and list_92 <= 5";
					}
					elseif ($bathMax != "5") {
						$query .= " and list_92 <= ".$_SESSION['bathMax'];
					}
				}
				if ($priceMin && ($priceMin!="Minimum")) {
					$query .= " and list_137 >= ".$_SESSION['priceMin'];
				}
				if ($priceMax && ($priceMax!="Maximum")) {
					$query .= " and list_137 <= ".$_SESSION['priceMax'];
				}
				
				/*
				if ($filter_area && $filter_area!= "") {
					$query .= "and list_922 = ".$_SESSION['filter_area'];
				}
				*/
				return $query;
				
			
		}
		
							
			
	
	//	function get_image($sysid,$Address,$City,$State ,$Zip)
//		
//		 {	
//		 
//		 	$mlsData['sysid']		= 	$sysid;
//
//			$mlsData['list_881']	=	$Address;
//			
//			$mlsData['list_922']	=	$City;
//			
//			$mlsData['list_924']	=	$State;
//			
//			$mlsData['list_10']		=	$Zip;		
//			
//			
//			$rets = new phRETS;
//			$rets->AddHeader("Accept", "*/*"); 
//			$rets->AddHeader("RETS-Version", RETS_VERSION); 
//			$rets->AddHeader("User-Agent", RETS_USERAGENT); 
//			$rets->SetParam("cookie_file", "phrets_cookies.txt"); 
//			$rets->SetParam("debug_mode", FALSE); // ends up in rets_debug.txt 
//			$connect = $rets->Connect(RETS_LOGIN_URL, RETS_USERNAME, RETS_PASSWORD,RETS_USERAGENT_PASSWORD);
//			
//
//			
//			$id = $mlsData['sysid'];
//		
//   		
//   			
//   			$address = str_replace(" ","-",$mlsData['list_881']);
//			
//			$url = $address."-".$mlsData['list_922']."-".$mlsData['list_924']."-".$mlsData['list_10'];
//		
//	   		$photos = $rets->GetObject("Property", "Photo", $mlsData['sysid']);
//			foreach($photos as $photo)
//			{
//					$listing = $photo['Content-ID'];
//					$number = $photo['Object-ID'];
//							
//					if ($photo['Success'] == true)
//					{
//			if(!file_exists(serch_imge_dir."/image-{$listing}-{$number}.jpg"))
//						{
//	file_put_contents(serch_imge_dir."/image-{$listing}-{$number}.jpg", $photo['Data']);
//							@chmod(serch_imge_dir."/image-{$listing}-{$number}.jpg",0777);
//					 $imagePath = "/image-{$listing}-{$number}.jpg";
//						
//						}
//						else 
//						{	
//						 $imagePath = "/image-{$listing}-{$number}.jpg";
//																					
//						}
//					}
//					
//					if(trim($imagePath)<>'')
//						{
//							echo "<img src=".serch_imge_url."".$imagePath." alt='' data-transition='slideInLeft'  title='' width='275px' height='155px'/>";
//						}
//					else
//						{
//							echo "<img src=".imge_url."/no_image.jpeg alt='' data-transition='slideInLeft'  title='' width='275px' height='155px'/>";	
//						}
//									
//				}	
//									  
//		
//			}
//			
//################################## GET MLS IMAGES	
function get_MLSimage($sysid)
{	

	$id = $sysid;
	
	$SQL = "SELECT photos FROM mls_properties_atlanta WHERE Matrix_Unique_ID = {$id}";
		
	$result = mysql_query($SQL);
	if (!$result) {
    	//echo 'Could not run query: ' . mysql_error();
    	exit;
	} 
	$NumberOfPhotos = mysql_fetch_row($result);
	
	if ($NumberOfPhotos[0]) 
	{
		//echo $NumberOfPhotos[0];
		
		
			$imagePath = "/image-{$id}-1.jpg";
			echo MLSImages.$imagePath;
		
		
		
	}
	else //0 photos returned
	{
		echo "/no-image.jpg";
	}
}		

//################################## GET MLS IMAGES FOR PROPERTY DETAIL PAGE
function retImage2 ($sysid) 
{
	$id = $sysid;
	
	$SQL = "SELECT photos FROM mls_properties_atlanta WHERE Matrix_Unique_ID = {$id}";
		
	$result = mysql_query($SQL);
	if (!$result) {
    	//echo 'Could not run query: ' . mysql_error();
    	exit;
	}
	$NumberOfPhotos = mysql_fetch_row($result);
	
	if ($NumberOfPhotos[0]) 
	{
		//echo $NumberOfPhotos[0];
		
		for ($i=1; $i<=$NumberOfPhotos[0]; $i++)
		{
			$imagePath = "/image-{$id}-{$i}.jpg";
			
			echo "<a href='".MLSImages."/image-{$id}-{$i}.jpg'>
			  <img data-title=' '
				 data-description='".$mlsData['list_214']."'
				 src='".MLSImages."/image-{$id}-{$i}.jpg' > </a>";
			
			
		}
		
		
	}
	else //0 photos returned
	{
		echo "<img src='/no_image.jpg' alt='' data-transition='slideInLeft'  title='' width='275px' height='155px'/>";
		echo "<a href='/no_image.jpg'>
			  <img data-title=' '
				 data-description='".$mlsData['list_214']."'
				 src='/no_image.jpg'> </a>";
	}
	
	
	
}
	
//################################## GET MLS IMAGES FOR PROPERTY DETAIL PAGE
function retImage3 ($sysid) 
{
	$id = $sysid;
	
	$SQL = "SELECT photos FROM mls_properties_atlanta WHERE Matrix_Unique_ID = {$id}";
		
	$result = mysql_query($SQL);
	if (!$result) {
    	//echo 'Could not run query: ' . mysql_error();
    	exit;
	}
	$NumberOfPhotos = mysql_fetch_row($result);
	
	if ($NumberOfPhotos[0]) 
	{
		//echo $NumberOfPhotos[0];
		
		for ($i=1; $i<=$NumberOfPhotos[0]; $i++)
		{
			$imagePath = "/image-{$id}-{$i}.jpg";
			
			echo "
			  <li><img data-title=' '
				 data-description='".$mlsData['list_214']."'
				 src='".MLSImages."/image-{$id}-{$i}.jpg' ></li>";
			
			
		}
		
		
	}
	else //0 photos returned
	{
		//echo "<img src='/no_image.jpg' alt='' data-transition='slideInLeft'  title='' width='275px' height='155px'/>";
		echo "<li>
			  <img data-title=' '
				 data-description='".$mlsData['list_214']."'
				 src='/no_image.jpg'></li>";
	}
	
	
	
}

function retImage4 ($sysid) 
{
	$id = $sysid;
	
	$SQL = "SELECT photos FROM mls_properties_atlanta WHERE sysid = {$id}";
		
	$result = mysql_query($SQL);
	if (!$result) {
    	//echo 'Could not run query: ' . mysql_error();
    	exit;
	}
	$NumberOfPhotos = mysql_fetch_row($result);
	
	if ($NumberOfPhotos[0]) 
	{
		//echo $NumberOfPhotos[0];
		
		for ($i=1; $i<=$NumberOfPhotos[0]; $i++)
		{
			$imagePath = "/image-{$id}-{$i}.jpg";
			
			echo "
			  <li><img 
				 src='".MLSImages."/image-{$id}-{$i}.jpg' ></li>";
			
			
		}
		
		
	}
	else //0 photos returned
	{
		//echo "<img src='/no_image.jpg' alt='' data-transition='slideInLeft'  title='' width='275px' height='155px'/>";
		echo "<li>
			  <img data-title=' '
				 data-description='".$mlsData['list_214']."'
				 src='/no_image.jpg'></li>";
	}
	
	
	
}	
	
	
	
	//function retImage ($sysid,$Address,$City,$State ,$Zip,$Description) {
//		$image = array ();
//		$mlsData['sysid']		= 	$sysid;
//
//			$mlsData['list_881']	=	$Address;
//			
//			$mlsData['list_922']	=	$City;
//			
//			$mlsData['list_924']	=	$State;
//			
//			$mlsData['list_10']		=	$Zip;
//			
//			$mlsData['list_214'] = $Description;
//			
//			$rets = new phRETS;
//			$rets->AddHeader("Accept", "*/*"); 
//			$rets->AddHeader("RETS-Version", RETS_VERSION); 
//			$rets->AddHeader("User-Agent", RETS_USERAGENT); 
//			$rets->SetParam("cookie_file", "phrets_cookies.txt"); 
//			$rets->SetParam("debug_mode", FALSE); // ends up in rets_debug.txt 
//			$connect = $rets->Connect(RETS_LOGIN_URL, RETS_USERNAME, RETS_PASSWORD,RETS_USERAGENT_PASSWORD);
//
//			
//			$id = $mlsData['sysid'];
//		
//   		
//   			
//   			$address = str_replace(" ","-",$mlsData['list_881']);
//			
//			$url = $address."-".$mlsData['list_922']."-".$mlsData['list_924']."-".$mlsData['list_10'];
//		
//	   		$photos = $rets->GetObject("Property", "Photo", $mlsData['sysid']);
//			foreach($photos as $photo)
//			{
//					$listing = $photo['Content-ID'];
//					$number = $photo['Object-ID'];
//							
//					if ($photo['Success'] == true)
//					{
//		if(!file_exists(serch_imge_dir."/image-{$listing}-{$number}.jpg"))
//						{
//	file_put_contents(serch_imge_dir."/image-{$listing}-{$number}.jpg", $photo['Data']);
//		@chmod(serch_imge_dir."/image-{$listing}-{$number}.jpg",0777);
//
//						 echo "<a href='".serch_imge_url."/image-{$listing}-{$number}.jpg'>
//                  <img data-title=' '
//                     data-description='".$mlsData['list_214']."'
//                     src='".serch_imge_url."/image-{$listing}-{$number}.jpg'> </a>";
//						}
//						else 
//						{
//							
//							 echo "<a href='".serch_imge_url."/image-{$listing}-{$number}.jpg'>
//                  <img data-title=' ' 
//                     data-description='".$mlsData['list_214']."'
//                     src='".serch_imge_url."/image-{$listing}-{$number}.jpg'> </a>";
//						}
//					}
//			  }	
//						  
//	}
	//function lightBoxImage ($sysid,$Address,$City,$State ,$Zip,$Description) {
//		$image = array ();
//		$mlsData['sysid']		= 	$sysid;
//
//			$mlsData['list_881']	=	$Address;
//			
//			$mlsData['list_922']	=	$City;
//			
//			$mlsData['list_924']	=	$State;
//			
//			$mlsData['list_10']		=	$Zip;
//			
//			$mlsData['list_214'] = $Description;
//			
//			$rets = new phRETS;
//			$rets->AddHeader("Accept", "*/*"); 
//			$rets->AddHeader("RETS-Version", RETS_VERSION); 
//			$rets->AddHeader("User-Agent", RETS_USERAGENT); 
//			$rets->SetParam("cookie_file", "phrets_cookies.txt"); 
//			$rets->SetParam("debug_mode", FALSE); // ends up in rets_debug.txt 
//			$connect = $rets->Connect(RETS_LOGIN_URL, RETS_USERNAME, RETS_PASSWORD,RETS_USERAGENT_PASSWORD);
//
//			
//			$id = $mlsData['sysid'];
//		
//   		
//   			
//   			$address = str_replace(" ","-",$mlsData['list_881']);
//			
//			$url = $address."-".$mlsData['list_922']."-".$mlsData['list_924']."-".$mlsData['list_10'];
//		
//	   		$photos = $rets->GetObject("Property", "Photo", $mlsData['sysid']);
//	   		$listing1 = $photos[0]['Content-ID'];
//	   		$number1 = $photos[0]['Object-ID'];
//	   		echo "<a href='".serch_imge_url."/image-{$listing1}-{$number1}.jpg' rel='lightbox[box]'><img src='images/light_box.png' alt='''></a>";
//	   		
//			foreach($photos as $photo)
//			{
//					$listing = $photo['Content-ID'];
//					$number = $photo['Object-ID'];
//							
//					if ($photo['Success'] == true)
//					{
//						if(!file_exists(serch_imge_dir."/image-{$listing}-{$number}.jpg"))
//						{
//	file_put_contents(serch_imge_dir."/image-{$listing}-{$number}.jpg", $photo['Data']);
//							@chmod(serch_imge_dir."/image-{$listing}-{$number}.jpg",0777);
//					 echo "<a href='".serch_imge_url."/image-{$listing}-{$number}.jpg' rel='lightbox[box]'></a>";
//						}
//						else 
//						{
//					echo "<a href='".serch_imge_url."/image-{$listing}-{$number}.jpg' rel='lightbox[box]'></a>";
//						}
//					}
//			  }	
//						  
//	}
//	
	
	
	
function firstImage2 ($id) {
		
		
	
	$SQL = "SELECT photos FROM mls_properties WHERE sysid = {$id}";
		
	$result = mysql_query($SQL);
	if (!$result) {
    	//echo 'Could not run query: ' . mysql_error();
    	exit;
	}
	$NumberOfPhotos = mysql_fetch_row($result);
	
	if ($NumberOfPhotos[0]) 
	{
		//echo $NumberOfPhotos[0];
		
		
			$imagePath = "/image-{$id}-1.jpg";
			
			return MLSImages."/image-{$id}-1.jpg";
			  
			
			
		
		
	}
	else //0 photos returned
	{
		return "no_image.jpg";
		
	}
	
		
		
		
		
		
		
		
		
		
		
}
	
	
	//
//	function firstImage ($id) {
//			$rets = new phRETS;
//			$rets->AddHeader("Accept", "*/*"); 
//			$rets->AddHeader("RETS-Version", RETS_VERSION); 
//			$rets->AddHeader("User-Agent", RETS_USERAGENT); 
//			$rets->SetParam("cookie_file", "phrets_cookies.txt"); 
//			$rets->SetParam("debug_mode", FALSE); // ends up in rets_debug.txt 
//			$connect = $rets->Connect(RETS_LOGIN_URL, RETS_USERNAME, RETS_PASSWORD,RETS_USERAGENT_PASSWORD);
//			
//			$photos = $rets->GetObject("Property", "Photo", $id);
//			
//				$listing1 = $photos[0]['Content-ID'];
//	   		$number1 = $photos[0]['Object-ID'];
//	   		if ($photos[0]['Success'] == true) {
//	   			if(!file_exists(serch_imge_dir."/image-{$listing1}-{$number1}.jpg"))
//						{
//							file_put_contents(serch_imge_dir."/image-{$listing1}-{$number1}.jpg", $photos['Data']);
//							@chmod(serch_imge_dir."/image-{$listing1}-{$number1}.jpg",0777);
//						 //$imagePath[] = "<img src='".$this->path()."images/search/image-{$listing}-{$number}.jpg' alt='' data-transition='slideInLeft'  title='#htmlcaption2' width='275px' height='155px'/>";
//						return  $path = serch_imge_url."/image-{$listing1}-{$number1}.jpg";
//						// echo "<img width='193' height='110' alt='no image' src='".$path."' />";
//						}
//						else 
//						{
//							 //$imagePath[] = "<img src='".$this->path()."images/search/image-{$listing}-{$number}.jpg' alt='' data-transition='slideInLeft'  title='#htmlcaption2' width='275px' height='155px'/>";
//							return   $path = serch_imge_url."/image-{$listing1}-{$number1}.jpg";
//							// echo "<img width='193' height='110' alt='no image' src='".$path."' />";
//						}
//	   		}		
//	}
//	
//	
		
	function paging($result,$current_page,$lmt)
		{
		$adjacents = 2;
		$targetpage = $current_page; 	//your file name  (the name of this file)
		
				
				/* this is just for shorting the  search result you can remove it later Start*/
				/*sorting by page*/
				if (isset($_POST['sortPage'])) {
					$_SESSION['sortPage']=$_POST['sortPage'];
					unset($_POST['sortPage']);
				}
				if ($_SESSION['sortPage'])
					$limit = $_SESSION['sortPage'];
				else
					$limit =	$lmt;							//how many items to show per page
					
				/*sort by type*/
				
				if (isset($_POST['type'])) {
					if($_REQUEST['type']=='Select one')
						unset($_SESSION['type']);
					else	
					$_SESSION['type'] = $_POST['type'];
				}
				if ($_SESSION['type']) {
					$result .= " and property_type_en like '%".$_SESSION['type']."%'";
				}	
				
				/*sort by price */
				if (isset($_POST['sortPrice']) && ($_POST['sortPrice']!="Sort By")) {
					if (isset($_SESSION['sortSqFt'])) {
						unset ($_SESSION['sortSqFt']);
					}
					$_SESSION['sortPrice'] = $_POST['sortPrice'];
					unset ($_POST['sortPrice']);
				}
				
				/*sort by square foot */
				if (isset($_POST['sortSqFt']) && ($_POST['sortSqFt']!="Sort By")) {
					if (isset($_SESSION['sortPrice'])) {
						unset ($_SESSION['sortPrice']);
					}
					$_SESSION['sortSqFt'] = $_POST['sortSqFt'];
					unset ($_POST['sortSqFt']);
				}
				
				if (isset($_SESSION['sortPrice'])) {
					if ($_SESSION['sortPrice'] == "Low") {
						$result .= " order by price_en asc";
					}
					elseif ($_SESSION['sortPrice'] == "High")
						$result .= " order by price_en desc";
				}
				
				
				if (isset($_SESSION['sortSqFt'])) {
					if ($_SESSION['sortSqFt'] == "Min Sq.Ft") {
						$result .= " order by sqft_en asc";
					}
					elseif ($_SESSION['sortSqFt'] == "Max Sq.Ft")
						$result .= " order by sqft_en desc";
				}
				
				if (!isset($_SESSION['sortPrice']) && !isset($_SESSION['sortSqFt'])) {
					$result .= " order by price_en desc";
				}
				
				
				/* this is just for shorting the  search result you can remove it later End*/
				
				
				
				
				$query = $result;
				//echo $query;
					
				$total_pages = mysql_num_rows(mysql_query($query));
				
				
				$page = (get_query_var('page')) ? get_query_var('page') : 1; 
				if($page) 
					$start = ($page - 1) * $limit; 			//first item to display on this page
				else
					$start = 0;								//if no page var is given, set start to 0
					
				
				/* Get data. */
				$sql = $result." LIMIT $start, $limit";
				
				$execute = mysql_query ($sql);
			
				
				/* Setup page vars for display. */
				if ($page == 0) $page = 1;					//if no page var is given, default to 1.
				$prev = $page - 1;							//previous page is page - 1
				$next = $page + 1;							//next page is page + 1
				$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
				$lpm1 = $lastpage - 1;						//last page minus 1
				
				/* 
					Now we apply our rules and draw the pagination object. 
					We're actually saving the code to a variable in case we want to draw it more than once.
				*/
				$pagination = "";
				if($lastpage > 1)
				{	
					$pagination .= "<div class=\"pagination\">";
					//previous button
					if ($page > 1) 
						$pagination.= "<a href=\"$targetpage/$prev\">&lt;&nbsp;PREVIOUS&nbsp;</a>";
					else
						$pagination.= "<span class=\"disabled\">&lt;&nbsp;PREVIOUS&nbsp;</span>";	
					
					//pages	
					if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
					{	
						for ($counter = 1; $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
							else
								$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
						}
					}
					elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
					{
						//close to beginning; only hide later pages
						if($page < 1 + ($adjacents * 2))		
						{
							for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
							{
								if ($counter == $page)
									$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
								else
									$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
							}
							$pagination.= "...";
							$pagination.= "<a href=\"$targetpage/$lpm1\">&nbsp;$lpm1&nbsp;</a>";
							$pagination.= "<a href=\"$targetpage/$lastpage\">&nbsp;$lastpage&nbsp;</a>";		
						}
						//in middle; hide some front and some back
						elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
						{
							$pagination.= "<a href=\"$targetpage/1\">&nbsp;1&nbsp;</a>";
							$pagination.= "<a href=\"$targetpage/2\">&nbsp;2&nbsp;</a>";
							$pagination.= "...";
							for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
							{
								if ($counter == $page)
									$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
								else
									$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
							}
							$pagination.= "...";
							$pagination.= "<a href=\"$targetpage/$lpm1\">&nbsp;$lpm1&nbsp;</a>";
							$pagination.= "<a href=\"$targetpage/$lastpage\">&nbsp;$lastpage&nbsp;</a>";		
						}
						//close to end; only hide early pages
						else
						{
							$pagination.= "<a href=\"$targetpage/1\">&nbsp;1&nbsp;</a>";
							$pagination.= "<a href=\"$targetpage/2\">&nbsp;2&nbsp;</a>";
							$pagination.= "...";
							for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
							{
								if ($counter == $page)
									$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
								else
									$pagination.= "<a href=\"$targetpage/$counter\"> &nbsp;$counter &nbsp;</a>";					
							}
						}
					}
					
					//next button
					if ($page < $counter - 1) 
					{
						$pagination.= "<a href=\"$targetpage/$next\">&nbsp;NEXT&nbsp;></a>";
						}
					else
					{
							$pagination.= "<span class=\"disabled\">&nbsp;NEXT&nbsp;></span>";
					}
					$pagination.= "</div>\n";		
					
					
					
				}
				$result	=	array();
				$result['paging'] = $pagination;
				$result['page']	  = $targetpage;
				$result['query']  = $execute;
				$result['total']  = $total_pages;
				return $result;
				
					
		}
	
	function get_saved_properties () {
		
	
		
		$sqlconnection = new MySQLi(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
		
		
		if ($sqlconnection>connect_errno) {
			//echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		}
				
		


	   $user_id = get_current_user_id ();
	   $query = mysqli_query ($sqlconnection, "SELECT * FROM ddobl_user_property WHERE user_id = '$user_id'");
		$result = "(";
		while ($data = mysqli_fetch_array ($query)) {
		   $result .= "'".$data['property_id']."'".',';
		}
		$result = substr ($result,0,-1);
		$result .= ")";
		return $result;
	}
	
	}
?>