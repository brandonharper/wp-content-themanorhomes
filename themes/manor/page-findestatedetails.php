<?php
/*
 Template Name: Find an estate
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
<?php

include_once( get_template_directory() . '/MLS-DB-Atlanta.php' );
$data 		= 	new db();

?>
<?php

	$mlsid = substr($_GET["id"],0,10);
	$query = "SELECT * FROM ".table_name." WHERE Matrix_Unique_ID = '".$mlsid."'" ;
	if (!mysql_num_rows(mysql_query($query))) {
		//Sysid does not exist, redirect to 404 using javascript
		echo '<script>window.location="/404"</script>';
		exit();
	} else {
		$runQuery = mysql_query ($query);
		$mlsData = mysql_fetch_array ($runQuery);
	}
	
		
	 
	
?>
<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<meta name="viewport" content="width=device-width,initial-scale=1" />

		
<script type="text/javascript" src="/wp-content/themes/manor/vimeoEmbedder/js/jquery-1.10.2.min.js"></script>
		<!-- Flexslider -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/js/libs/flexslider/flexslider.css" type="text/css">
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/flexslider/jquery.flexslider-min.js"></script>

		<script>
			jQuery(document).ready(function($) {
	
	
	
					/* ------ Flexslider ---------*/
					$('#carouselslider').flexslider({
				    animation: "slide",
				    controlNav: false,
				    animationLoop: false,
				    slideshow: false,
				    itemWidth: 260,
				    itemMargin: 20,
				    touch: true, 
				    asNavFor: '#imgslider',
				  
				  });
				 
				  $('#imgslider').flexslider({
				    animation: "slide",
				    controlNav: false,
				    animationLoop: false,
				    slideshow: false,
				    smoothHeight: true, 
				    touch: true, 
				    sync: "#carouselslider"
				  });
  
				});
			</script>

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
		
		<!-- Galleria -->
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/galleria/galleria-1.4.2.min.js"></script>
 <style>
	 
	 .entry-content {
		 padding: 0 !important;
	 }
	 body.page-template-page-findestatedetails-php {
		background: none;
		background-color: white;
	}
	 .find-an-estate-details {
		 width: 1692px;
		 margin: 40px auto;
	 }
	 
	 .find-an-estate-details .property-left {
		 width: 860px;
		 float: left;
	 }
	 .find-an-estate-details .property-left #imgslider {
		 width: 811px;
	 }
	 .find-an-estate-details .property-right {
		 width: 770px;
		 float: left;
		 font-family: sans-serif;
		font-size: 30px;
		line-height: 1.9;
		font-weight: 300;
		color: #636466;
		margin-bottom: 10px;
	 }
	
	.find-an-estate-details .property-right .address {
		margin-top: 30px;
		font-family: Georgia, serif;
		font-size: 40px;
		line-height: 1.2;
		font-weight: 600;
		color: #0f6748;
		margin-bottom: 10px;
	}
	.property-thumbnails {
		background-color: #d9d9d9;
		padding: 20px 50px 0px 50px;
		position: fixed;
		bottom: 0;
		width: 100%;
	}
	
  </style>


	</head>
	
	<body <?php body_class(); ?>>
	
		


						

							
		<section class="entry-content cf" itemprop="articleBody">
			
			<div class="find-an-estate-details cf">
				<div class="property-left">
					<div id="imgslider" class="flexslider">
					  <ul class="slides">
					    <?php 
							           		//Get MLS Image
										   $data->retImage3($mlsData['Matrix_Unique_ID']); 
							           ?>
					  </ul>
					</div>
					
				</div>
				<div class="property-right">
					<div class="address"><?php echo $mlsData["StreetNumber"] . " " . $mlsData["StreetName"] . " " . $mlsData["StreetSuffix"];  ?><br />$<?php echo number_format($mlsData["ListPrice"]); ?></div>
					<div class="details">
						Bedrooms: <?php echo $mlsData["BedsTotal"]; ?><br />
						Full Baths: <?php echo $mlsData["BathsFull"]; ?> <br />
						Half Baths: <?php echo $mlsData["BathsHalf"]; ?> <br />
						Property Type: <?php echo $mlsData["PropertyType"]; ?> <br />
						Year Built: <?php echo $mlsData["YearBuilt"]; ?><br />
						MLS ID: <?php echo $mlsData["MLSNumber"]; ?> <br />
						Subdivision: The Manor Golf and Country Club<br />
						Schools: Summit Hill, Cambridge
					</div>
				</div>
			</div>
		    <div class="property-thumbnails">
			    <div id="carouselslider" class="flexslider">
					  <ul class="slides">
					    <?php 
						           		//Get MLS Image
									   $data->retImage3($mlsData['Matrix_Unique_ID']); 
						           ?>
				  </ul>
				</div>
		    </div>

		</section>


								

							

									

				

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>



	</body>

</html> <!-- end of site. what a ride! -->