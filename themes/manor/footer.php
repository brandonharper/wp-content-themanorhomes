<?php if (!is_page_template('page-property-print.php')) { ?><footer class="footer">
    <div class="green-footer-block cf" >
        <div class="wrap cf" id="inner-footer">
            <div class="d-1of3 t-1of2 m-all">
                <div class="block alignc"><img src="/wp-content/uploads/2015/09/footer-logo-manor.png"></div>

                <div class="block alignc"><img src="<?php echo get_template_directory_uri(); ?>/library/images/evwhite.png" style="margin-top: 10px;"></div>

                <div class="alignc"><img src="/wp-content/themes/manor/images/line-footer.png"></div>

                <p class="footer-title-text">Connect with us</p>

                <div class="block alignc social-icons">
                    <a href="https://www.facebook.com/themanorestates" target="_blank">
                    <div class="facebook2"></div></a> <a href="https://twitter.com/ManorGCC" target="_blank">
                    <div class="twitter2"></div></a><a href="https://plus.google.com/116654318555287157180/about?gl=us&amp;hl=en" target="_blank">
                    <div class="google2"></div></a><a href="https://foursquare.com/v/the-manor-golf-and-country-club/4b5ef17ef964a520f19e29e3" target="_blank">
                    <div class="foursquare2"></div></a> <a class="alignc footer-url" href="http://manorgcc.com">manorgcc.com</a>
                </div>
            </div>

            <div class="d-1of3 t-1of2 m-all footer-links-menu cf">
                <p class="footer-title-text-left block2">Quick Links</p>

                <div class="d-all t-all m-all">
                    <ul class="footer-nav cf" id="menu-footer-menu block2">
                        <li class="footerlinks menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1500" id="menu-item-1500">
                            <a class="" href="/find-your-home/">Find Your Home</a>
                        </li>

                        <li class="footerlinks menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1501" id="menu-item-1501">
                            <a class="" href="/premier-neighborhoods/">Discover Milton</a>

                        </li>

                        <li class="footerlinks menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1502" id="menu-item-1502">
                            <a class="" href="/premier-builders/">Custom Homebuilders</a>

                            
                
                   
                        <li class="footerlinks menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1503" id="menu-item-1503" style="list-style-type:none;color:#fff;text-decoration:none;">
                            <a class="" href="/premier-amenities/" style="color:#fff;text-decoration:none;">Country Club</a>

                            
                        </li>

                        <li class="footerlinks menu-item menu-item-type-post_type menu-item-object-page menu-item-1504" id="menu-item-1504" style="list-style-type:none;color:#fff;">
                            <a href="/gallery/" style="list-style-type:none;color:#fff;text-decoration:none;padding-top:5px !important;">Gallery</a>
                        </li>

                        <li class="footerlinks menu-item menu-item-type-post_type menu-item-object-page menu-item-1505" id="menu-item-1505" style="list-style-type:none;color:#fff;">
                            <a href="/contact/" style="list-style-type:none;color:#fff;text-decoration:none;padding-top:5px !important;">Contact</a>
                        </li>

                        <li class="footerlinks menu-item menu-item-type-post_type menu-item-object-page menu-item-1506" id="menu-item-1506" style="list-style-type:none;color:#fff;">
                            <a href="/blog/" style="list-style-type:none;color:#fff;text-decoration:none;padding-top:5px !important;">Blog</a>
                        </li>
                    </ul>
                </div>
            </div>

            <?php if ( !is_page(2849) ) { ?> 
            <div class="d-1of3 t-1of2 m-all last-col" id="footer-contact">
                <p class="footer-title-text-left">Contact Us for more info</p><?php echo do_shortcode('[contact-form-7 id="1579" title="footer-form" html_id="footer-form" html_class="form footer-form"]'); ?>
            </div>
            <?php } ?>
        </div>
    </div>

    <div class="gold-footer-block cf" id="bottom-footer">
        <p class="source-org copyright">© 2016 Engel & Völkers. All Rights Reserved. Prices, conditions and availability subject to change. Property Ownership requires obligatory homeowners association membership and country club membership. <br />See sales office for full details. Engel & Völkers North Point® is a registered trademark licensed to Engel & Völkers IT-Services GmbH, an Equal Opportunity Company. </p>
        <p class="source-org copyright">Equal Housing Opportunity. If your property is listed with a real estate broker, this is not an attempt to solicit your offering.  | Ph: 770-521-6000 | <a href="/privacy-policy/" style="color:#0f6748;text-decoration:none;">Privacy Policy</a>| Designed &amp; Developed by <a href="http://stradigys.com">Stradigys</a></p>
    </div>
    

</footer><?php } ?><?php // all js scripts are loaded in library/bones.php ?>




<?php wp_footer(); ?>
<div style="display:none">
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
---------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 961084775;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/961084775/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</div>
<!-- AdRoll Smart Pixel -->
<script type="text/javascript">
adroll_adv_id = "J37XASLV2NEIXB2K3I4AXV";
adroll_pix_id = "RRUGRF56YVCZRKFYYQG6OS";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
</script>

</body>

</html> <!-- end of site. what a ride! -->