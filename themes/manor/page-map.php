<?php
/*
 Template Name: Map
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<meta name="viewport" content="width=device-width,initial-scale=1" />

		<link rel="stylesheet" href="/wp-content/themes/manor/vimeoEmbedder/css/vimeoEmbedder.css">
		<script type="text/javascript" src="/wp-content/themes/manor/vimeoEmbedder/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="/wp-content/themes/manor/vimeoEmbedder/js/jquery.vimeoEmbedder.js"></script>




		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
		
		<!-- Galleria -->
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/galleria/galleria-1.4.2.min.js"></script>
 <style>
	  body.page-template-page-map-php {
		  background: none;
		  background-color: white;
	  }
	  body {
  -webkit-user-select: none;
     -moz-user-select: -moz-none;
      -ms-user-select: none;
          user-select: none;
}
	.st0{fill:#FFFFFF;}
	.st1{fill:none;stroke:#C8B783;stroke-miterlimit:10;}
	.st2{fill:#C8B783;}
	.st3{fill:#C1D7CD;}
	.st4{fill:#F1F2F2;}
	.st5{fill:#ADCDEC;}
	.st6{fill:#9CC0B0;}
	.st7{fill:#CDC1AC;}
	.st8{fill:#5D9A83;}
	.st9{fill:#F1F2F2;stroke:#C1D7CD;stroke-miterlimit:10;}
	.st10{display:none;}
	.st11{display:inline;fill:#873333;}
	.st12{display:inline;fill:#F1F2F2;stroke:#C1D7CD;stroke-width:0.25;stroke-miterlimit:10;}
	.st13{fill:#6D6E71;}
	.st14{display:none;fill:none;stroke:#000000;stroke-width:0.15;stroke-miterlimit:10;}
	.st15{display:none;fill:none;stroke:#A7A9AC;stroke-miterlimit:10;}
	.st16{fill:#E6E7E8;}
	.st17{display:none;fill:none;stroke:#E6E7E8;stroke-width:2;stroke-miterlimit:10;}
	.st18{fill:#D1D3D4;}
	.st19{fill:#6AA28C;}
	.st20{fill:#FFFFFF;stroke:#7B6C2B;stroke-width:3;stroke-miterlimit:10;}
	.st21{fill:#C5A82E;stroke:#7B6C2B;stroke-width:3;stroke-miterlimit:10;}
	.st22{font-family:'TimesNewRomanPSMT',serif;}
	.st23{font-size:11.3376px;}
	.st24{font-family:'TimesNewRomanPS-BoldMT',serif;}
	.st25{font-size:18px;}
	.st26{fill:#855129;}
	.st27{font-family:'HelveticaNeue',sans-serif;}
	.st28{font-size:13px;}
	.st29{letter-spacing:-1;}
	.st30{fill:#C5A82E;}
	.st31{font-family:'HelveticaNeue',sans-serif;}
	.st32{font-size:13px;}
	.st33{font-size:11px;}
	.builder-color{fill:#7a6b2b;}
	
	
  .lotbg{fill:#F1F2F2;stroke:#C1D7CD;stroke-miterlimit:10; cursor: pointer;}
  .lot-text{fill:#ffffff;cursor: pointer;}
  /* .lot-text{fill:#6D6E71;cursor: pointer;} */
  
  .whitetxt{fill:#ffffff; cursor: auto;}
  .soldtxt{fill:#ffffff; display:none; !important; cursor: auto;}
  .selectedtxt{fill:#ffffff; cursor: auto;}
  
  .lot-sold{ fill:#f1f2f2; cursor: auto;  }
  .lot-builder{ fill:#ead8bf;}
  .lot-selected{fill:#c3a62e;}
  .lot-hover{fill:#c3a62e;}
  .Loudermilk{fill:#863335;}
  .Edward{fill:#c3a62e;}
  .Sales{fill:#ff0000;}

  .spec{fill:rgb(57, 47, 117);}
  
  .lot-noinfo{fill:#F1F2F2; cursor: auto;}
  .txt-noinfo{fill:#6D6E71; cursor: auto;}

  
  
  #popup-box,#popup-circle,#popup-label,#popup-lotnumber,#popup-lotsize,#popup-lottype,#popup-price,#popup-contact-bttn,#popup-contact-link, #popup-address1, #popup-address2, #popup-lotstatus, #popup-builder {
	  display: none;
  }
  
  .territory:hover{
  	fill:           #22aa44;
  }
  .compass{
  	fill:			#fff;
  	stroke:			#000;
  	stroke-width:	1.5;
  }
  .button{
  	fill:           	#225EA8;
  	stroke:   			#0C2C84;
  	stroke-miterlimit:	6;
  	stroke-linecap:		round;
  }
  .button:hover{
  	stroke-width:   	2;
  }
  .plus-minus{
  	fill:	#fff;
  	pointer-events: none;
  }
  #burj { position: relative;

width: 100%; padding-bottom: 137%;

vertical-align: middle; margin: 0; overflow: hidden; }

#burj svg { display: inline-block;

position: absolute; top: 0; left: 0; }

.map-container {
	width: 100%;
	height: auto;
	overflow: hidden;
	position: relative;
}
.map {
	width: 100%;
	height: auto;
	position: relative;
	margin: 0 auto;
	#toggle-fox {
		/*position: absolute;
		top: 1118px;
		left: 305px;
		width: 17px;
		height: 19px;*/
		
		cursor: pointer;
	}
}
  </style>
  <script src="<?php echo get_template_directory_uri(); ?>/jquery.panzoom.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/jquery.mousewheel.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/svg.min.js"></script>

  <script>
	  // ######## Add addClass/removeClass support to SVG ############
	  (function($){
	  	var addClass = $.fn.addClass;
	  	$.fn.addClass = function(value) {
	  		var orig = addClass.apply(this, arguments);
	  		
	  		var elem,
	  		i = 0,
	  		len = this.length;
	  		
	  		for (; i < len; i++ ) {
	  			elem = this[ i ];
	  			if ( elem instanceof SVGElement ) {
	  				var classes = $(elem).attr('class');
	  				if ( classes ) {
	  					var index = classes.search(value);
	  					if (index === -1) {
	  						classes = classes + " " + value;
	  						$(elem).attr('class', classes);
	  					}
	  				} else {
	  					$(elem).attr('class', value);
	  				}
	  			}
	  		}
	  		return orig;
	  	};
	  	
	  	var removeClass = $.fn.removeClass;
	  	$.fn.removeClass = function(value) {
	  		var orig = removeClass.apply(this, arguments);
	  		
	  		var elem,
	  		i = 0,
	  		len = this.length;
	  		
	  		for (; i < len; i++ ) {
	  			elem = this[ i ];
	  			if ( elem instanceof SVGElement ) {
	  				var classes = $(elem).attr('class');
	  				if ( classes ) {
	  					var index = classes.search(value);
	  					if (index !== -1) {
	  						classes = classes.substring(0, index) + classes.substring((index + value.length), classes.length);
	  						$(elem).attr('class', classes);
	  					}
	  				}
	  			}
	  		}
	  		return orig;
	  	};
	  })(jQuery);	
	  
	  
	  
	  jQuery(document).ready(function($) {
	  	
			// ######## Highlight lots ############
			
			$('.lotbg').hover(function () {
				
	
			    
			    
			    id =  $(this).attr('id').substr(3);
			    lotid =  "lot"+id;
			    var selectedLot = SVG.get(lotid)
			    
	
			   
			    if ( !selectedLot.hasClass("lot-sold") && !selectedLot.hasClass("lot-noinfo") ) {
				 
				 
				 	//change background coor
				 	$('.lotbg').removeClass("lot-hover");
			    	$(this).addClass("lot-hover");
					//change font color
				    textid = "#txt"+id;
				    $('.lot-text').removeClass("whitetxt");
				    $(textid).addClass("whitetxt");
				    
				}
			    
			    
			});
			
			
			//  ########## SOLD LOTS  ##########
			<?php 
				$disabled_lots = array();
				$map = pods( 'map' ); 
			    $params = array('limit' => 400,);  
				$map->find($params);
				while ( $map->fetch() ) { 
				
					if ($map->field('disabled_property')) { 	
						array_push($disabled_lots, $map->display('lot_number'));
						
					}
						
				}
					
					
			?>
			var soldLots = <?php echo json_encode($disabled_lots); ?>;
			
			// Mark sold lots
			for ( lot in soldLots) {
				$('#lot'+soldLots[lot]).addClass("lot-sold");
				$('#txt'+soldLots[lot]).addClass("soldtxt");
				
			}
			
			
			//  ########## REGULAR LOTS  ##########
			//   - Not in MLS
			/*
			<?php 
				$regular_lots = array();
				$map = pods( 'map' ); 
			    $params = array('limit' => 400,);  
				$map->find($params);
				while ( $map->fetch() ) { 
				
					if (!$map->field('disabled_property') && !$map->field('mls_id')) { 	
						//array_push($regular_lots, $map->display('lot_number'));
						$regular_lots[$map->display('lot_number')]['acreage'] = $map->display( 'acreage' );
						$regular_lots[$map->display('lot_number')]['property_type'] = $map->display( 'property_type' );
						$regular_lots[$map->display('lot_number')]['price'] = $map->display( 'price' );
					}
						
				}

			?>
			*/
			
			//  ########## NO INFO AVIALABLE  ##########
			// 
			<?php 
				$noinfo = array();
				$map = pods( 'map' ); 
			    $params = array('limit' => 400,);  
				$map->find($params);
				while ( $map->fetch() ) { 
				
					if (!$map->field('disabled_property')  && $map->field('no_info_available')  ) { 	
						
						$noinfo[$map->display('lot_number')]['lotnum'] = $map->display( 'lot_number' );
					}
						
				}

			?>
			var noinfo = <?php echo json_encode($noinfo); ?>;
			// Mark noinfo lots
			for ( lot in noinfo) {
				$('#lot'+noinfo[lot]['lotnum']).addClass("lot-noinfo");
				$('#txt'+noinfo[lot]['lotnum']).addClass("txt-noinfo");
			}
			
			
			//  ########## REGULAR LOTS  ##########
			// 
			<?php 
				$regular_lots = array();
				$map = pods( 'map' ); 
			    $params = array('limit' => 400,);  
				$map->find($params);
				while ( $map->fetch() ) { 
				
					if (!$map->field('disabled_property') && !$map->field('builder_name') && !$map->field('no_info_available') && !$map->field('house_built') ) { 	
						
						$regular_lots[$map->display('lot_number')]['address_line_1'] = $map->display( 'address_line_1' );
						$regular_lots[$map->display('lot_number')]['address_line_2'] = $map->display( 'address_line_2' );
						$regular_lots[$map->display('lot_number')]['status'] = $map->display( 'lot_status' );
						$regular_lots[$map->display('lot_number')]['acreage'] = $map->display( 'acreage' );
						$regular_lots[$map->display('lot_number')]['property_type'] = $map->display( 'property_type' );
						$regular_lots[$map->display('lot_number')]['price'] = $map->display( 'price' );
					}
						
				}

			?>
			var regularLots = <?php echo json_encode($regular_lots); ?>;
			// Mark regular lots
			for ( lot in regularLots) {
				$('#lot'+regularLots[lot]['lotnum']).addClass("lot-regularlot");
				$('#txt'+regularLots[lot]['lotnum']).addClass("txt-regularlot");
			}
			
			
			//  ########## REGULAR HOUSES  ##########
			// 
			<?php 
				$regular_houses = array();
				$map = pods( 'map' ); 
			    $params = array('limit' => 400,);  
				$map->find($params);
				while ( $map->fetch() ) { 
				
					if (!$map->field('disabled_property') && !$map->field('builder_name') && !$map->field('no_info_available') && $map->field('house_built') ) { 	
						

						$regular_houses[$map->display('lot_number')]['address_line_1'] = $map->display( 'address_line_1' );
						$regular_houses[$map->display('lot_number')]['address_line_2'] = $map->display( 'address_line_2' );
						$regular_houses[$map->display('lot_number')]['status'] = $map->display( 'lot_status' );
						$regular_houses[$map->display('lot_number')]['acreage'] = $map->display( 'acreage' );
						$regular_houses[$map->display('lot_number')]['property_type'] = $map->display( 'property_type' );
						$regular_houses[$map->display('lot_number')]['price'] = $map->display( 'price' );
					}
						
				}

			?>
			var regularHouses = <?php echo json_encode($regular_houses); ?>;
			

			
			//  ########## BUILDER LOTS  ##########
			// 
			<?php 
				$builder_lots = array();
				$map = pods( 'map' ); 
			    $params = array('limit' => 400,);  
				$map->find($params);
				while ( $map->fetch() ) { 
				
					if (!$map->field('disabled_property') && $map->field('builder_name') && !$map->field('no_info_available') && !$map->field('house_built') ) { 	
												
						$builder_lots[$map->display('lot_number')]['lotnum'] = $map->display( 'lot_number' );
						$builder_lots[$map->display('lot_number')]['address_line_1'] = $map->display( 'address_line_1' );
						$builder_lots[$map->display('lot_number')]['address_line_2'] = $map->display( 'address_line_2' );
						$builder_lots[$map->display('lot_number')]['builder_name'] = $map->display( 'builder_name' );
						$builder_lots[$map->display('lot_number')]['status'] = $map->display( 'lot_status' );
						$builder_lots[$map->display('lot_number')]['acreage'] = $map->display( 'acreage' );
						$builder_lots[$map->display('lot_number')]['property_type'] = $map->display( 'property_type' );
						$builder_lots[$map->display('lot_number')]['price'] = $map->display( 'price' );
					}
						
				}

			?>
			var builderLots = <?php echo json_encode($builder_lots); ?>;
			// Mark builder lots
			for ( lot in builderLots) {
				$('#lot'+builderLots[lot]['lotnum']).addClass('lot-builder '+builderLots[lot]['builder_name']);
				
			}
			
			
			//  ########## BUILDER HOUSES ##########
			// 
			<?php 
				$builder_houses = array();
				$map = pods( 'map' ); 
			    $params = array('limit' => 400,);  
				$map->find($params);
				while ( $map->fetch() ) { 
				
					if (!$map->field('disabled_property') && $map->field('builder_name') && !$map->field('no_info_available') && $map->field('house_built') ) { 	
												
						$builder_houses[$map->display('lot_number')]['lotnum'] = $map->display( 'lot_number' );
						$builder_houses[$map->display('lot_number')]['address_line_1'] = $map->display( 'address_line_1' );
						$builder_houses[$map->display('lot_number')]['address_line_2'] = $map->display( 'address_line_2' );
						$builder_houses[$map->display('lot_number')]['builder_name'] = $map->display( 'builder_name' );
						$builder_houses[$map->display('lot_number')]['status'] = $map->display( 'lot_status' );
						$builder_houses[$map->display('lot_number')]['acreage'] = $map->display( 'acreage' );
						$builder_houses[$map->display('lot_number')]['property_type'] = $map->display( 'property_type' );
						$builder_houses[$map->display('lot_number')]['price'] = $map->display( 'price' );
					}
						
				}

			?>
			var builderHouses = <?php echo json_encode($builder_houses); ?>;
			// Mark builder lots
			for ( lot in builderHouses) {
				$('#lot'+builderHouses[lot]['lotnum']).addClass('lot-builder spec '+builderHouses[lot]['builder_name']);
				
			}
			
			
			
			
			
			
			
			// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CLICK REGULAR LOT @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			$('.lotbg,.lot-text').click(function () {
							
				id = $(this).attr('id').substr(3);
				lotid =  "lot"+id;
			    txtid =  "txt"+id;
				
				var selectedLot = SVG.get(lotid)
				x = selectedLot.x();
				y = selectedLot.y();
					
				//CLICK ON REGULAR LOT
				if ( !selectedLot.hasClass("lot-sold") && !selectedLot.hasClass("lot-noinfo") ) {

					//Show popup
					// $("#popup-box,#popup-circle,#popup-label,#popup-lotnumber,#popup-lotsize,#popup-lottype,#popup-price,#popup-contact-bttn,#popup-contact-link,#popup-address1,#popup-address2, #popup-lotstatus, #popup-builder").show(); 
					$("#popup-box,#popup-circle,#popup-label,#popup-lotnumber,#popup-lotsize,#popup-lottype,#popup-price,#popup-address1,#popup-address2, #popup-lotstatus, #popup-builder").show(); 

					//Setup lot data
					$("#popup-lotnumber").text(id);
					if (id in regularLots) { // - - - - - SHOW REGULAR LOTS - - - - - - -
						$("#popup-builder").hide(); 
						$("#popup-address1").text(regularLots[id]['address_line_1']);
						$("#popup-address2").text(regularLots[id]['address_line_2']);
						$("#popup-lotstatus").text(regularLots[id]['status']);
						$("#popup-lotsize").text("Lot Size: "+regularLots[id]['acreage']+" Acres");
						$("#popup-lottype").text("Lot Type: "+regularLots[id]['property_type']);
						$("#popup-price").text("Price: "+regularLots[id]['price']);
					} else if (id in builderLots) {
						$("#popup-address1").text(builderLots[id]['address_line_1']);
						$("#popup-address2").text(builderLots[id]['address_line_2']);
						$("#popup-lotstatus").text(builderLots[id]['status']);
						$("#popup-builder").text(builderLots[id]['builder_name']);
						$("#popup-lotsize").text("Lot Size: "+builderLots[id]['acreage']+" Acres");
						$("#popup-lottype").text("Lot Type: "+builderLots[id]['property_type']);
						$("#popup-price").text("Price: "+builderLots[id]['price']);
					} else if (id in regularHouses) {
						$("#popup-builder").hide(); 
						$("#popup-address1").text(regularHouses[id]['address_line_1']);
						$("#popup-address2").text(regularHouses[id]['address_line_2']);
						$("#popup-lotstatus").text(regularHouses[id]['status']);
						$("#popup-lotsize").text("Lot Size: "+regularHouses[id]['acreage']+" Acres");
						$("#popup-lottype").text("Lot Type: "+regularHouses[id]['property_type']);
						$("#popup-price").text("Price: "+regularHouses[id]['price']);
					} else if (id in builderHouses) {
						$("#popup-address1").text(builderHouses[id]['address_line_1']);
						$("#popup-address2").text(builderHouses[id]['address_line_2']);
						$("#popup-lotstatus").text(builderHouses[id]['status']);
						$("#popup-builder").text(builderHouses[id]['builder_name']);
						$("#popup-lotsize").text("Lot Size: "+builderHouses[id]['acreage']+" Acres");
						$("#popup-lottype").text("Lot Type: "+builderHouses[id]['property_type']);
						$("#popup-price").text("Price: "+builderHouses[id]['price']);
					}
					
					else if (id in mlsLots){ //MLS lots display
						/*$("#popup-lotsize").hide(); 
						$("#popup-address1").text(mlsLots[id]['address1']);
						$("#popup-address2").text(mlsLots[id]['address2']);
						
						$("#popup-lottype").text(mlsLots[id]['acreage'] ); //lot type becomes lot size for MLS popups
						
						$("#popup-price").text("Price: $"+mlsLots[id]['price']);
						*/
					} else if (!(id in regularLots) && !( id in mlsLots)) { //Lot not found
						
							$("#popup-box,#popup-circle,#popup-label,#popup-lotnumber,#popup-lotsize,#popup-lottype,#popup-price,#popup-contact-bttn,#popup-contact-link,#popup-address1, #popup-address2, #popup-lotstatus, #popup-builder").hide(); 
							
					}
		
					var popup_box = SVG.get("popup-box");
					var popup_circle = SVG.get("popup-circle");
					var popup_label  = SVG.get("popup-label");
					var popup_lotstatus  = SVG.get("popup-lotstatus");
					var popup_builder  = SVG.get("popup-builder");
					var popup_lotnumber  = SVG.get("popup-lotnumber");
					var popup_lotsize  = SVG.get("popup-lotsize");
					var popup_lottype  = SVG.get("popup-lottype");
					var popup_address1  = SVG.get("popup-address1");
					var popup_address2 = SVG.get("popup-address2");
					var popup_price  = SVG.get("popup-price");
					//var popup_contact_bttn = SVG.get("popup-contact-bttn");
					//var popup_contact_link = SVG.get("popup-contact-link");
				
					// Move popup box to position
					var position_multiplier = 50;
					popup_box.move(x+position_multiplier,y+position_multiplier);
					popup_circle.center(x+position_multiplier+1.5,y+position_multiplier+2.1);
					popup_label.move(x+position_multiplier-10.7309,y+position_multiplier-10.9928);
					popup_lotnumber.move(x+position_multiplier+0.9873,y+position_multiplier+1.5);
					popup_address1.move(x+position_multiplier+102.2028,y+position_multiplier+7);
					popup_address2.move(x+position_multiplier+102.2028,y+position_multiplier+23);
					
					popup_lotstatus.move(x+position_multiplier+102,y+position_multiplier+38);
					popup_builder.move(x+position_multiplier+102,y+position_multiplier+55);
					
					popup_lotsize.move(x+position_multiplier+32.2028,y+position_multiplier+74);
					popup_lottype.move(x+position_multiplier+29.363,y+position_multiplier+91);
					popup_price.move(x+position_multiplier+50,y+position_multiplier+109);
					//popup_contact_bttn.move(x+position_multiplier+1.4,y+position_multiplier+88.9);
					//popup_contact_link.move(x+position_multiplier+98.3707,y+position_multiplier+99);
					
					//Highlight selected lot
					$('.lotbg').removeClass('lot-selected');
					$('#'+lotid).addClass('lot-selected');
					$('.lot-text').removeClass('selectedtxt');
					$('#'+txtid).addClass('selectedtxt');

				} //end regular lot click
				


			});
			

		});
	  </script>
	</head>
	
	<body <?php body_class(); ?>>
	
		
			
							
							

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						

							
								<section class="entry-content cf" itemprop="articleBody">
									<div class="map-container">
										<div class="map">
												
												<figure id=burj>
													
													<svg  version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1081.9 1482.3" preserveAspectRatio="xMinYMin meet" >
														<image id="map-img" width="1081.9" height="1482.3" xlink:href="<?php echo get_template_directory_uri(); ?>/lot_map.svg"></image>
														
														<!--<ellipse opacity="1" id="lot_1" cx="74.469" cy="976.508" ry="5.841" rx="5.841" stroke-width="0" stroke="#000000" fill="#863435" class="lot-dot"/>-->
														
														<g id="Lots">
															<path id="lot165" class="lotbg" d="M732.8,201.5l-0.9,16.3l8.1,16.7c0,0,2.2-1.6,4.9-1.6c2.7,0,5.2,1.7,5.2,1.7l7.8-8.9l26.3-14
															l-14.8-25L732.8,201.5z"/>
															<path id="lot166" class="lotbg" d="M740,234.5c-1.5,1.1-4.2,3.7-3.4,8.8c-11,0.5-43.8,2.7-43.8,2.7l15.4-45.3l24.6,0.7l-0.9,16.3
															L740,234.5z"/>
															<path id="lot172" class="lotbg" d="M708.2,200.8l-37.2-0.4l-2,35.5c0,0,3.6,0,6,2.6c2.3,2.4,2.5,6.1,2.4,6.9
															c2.3,0.2,15.4,0.8,15.4,0.8L708.2,200.8z"/>
															<path id="lot173" class="lotbg" d="M623.2,202.8l9.8,22.6l27.6,16.4c0,0,1.1-2.7,2.8-4.1c2.3-1.7,4.1-1.9,5.6-1.9
															c0.3-3.3,2-35.5,2-35.5l-31.3-2.1L623.2,202.8z"/>
															<path id="lot174" class="lotbg" d="M633,225.3l-2,24.8l-7.2,13.5l19.5,12.3l18.4-26.6c0,0-2.5-3.5-1.1-7.6
															C656.2,239.2,633,225.3,633,225.3z"/>
															<path id="lot175" class="lotbg" d="M623.2,202.8l-22.9,6.1c0,0,10.1,17.8,9.7,17.6s-1.5,31.9-1.5,31.9l15.3,5.2l7.2-13.5l2-24.8
															L623.2,202.8z"/>
															<path id="lot176" class="lotbg" d="M600.3,208.9l-24.6,20.8l16.8,31.7c0,0,3-2.8,8.5-3.4c4-0.4,7.5,0.4,7.5,0.4l1.5-31.9L600.3,208.9
															z"/>
															<polygon id="lot177" class="lotbg" points="592.5,261.4 571.4,278.4 547.5,250.7 575.7,229.7 	"/>
															<polygon id="lot178" class="lotbg" points="571.4,278.4 550.1,297.4 525.4,268.7 547.5,250.7 	"/>
															<polygon id="lot179" class="lotbg" points="550.1,297.4 533.1,311.3 515.5,291 493.6,285 504.7,273.3 522,272.7 525.4,268.7 	"/>
															<path id="lot180" class="lotbg" d="M533.1,311.3L515.9,326c0,0-12.6-11.1-17.4-14c-4.5-2.7-14.8-6.7-14.8-6.7l10-20.3l21.9,6.1
															L533.1,311.3z"/>
															<path id="lot170" class="lotbg" d="M668.9,253.2l-17,27l23.3,14.8l11.9-18.8l5.8-30.2l-15.4-0.8c0,0-0.4,3.4-2.6,5.6
															C672.5,253.2,668.9,253.2,668.9,253.2z"/>
															<polygon id="lot169" class="lotbg" points="675.1,295 700.5,310.8 712.2,292.7 702.7,245.4 692.8,246 687,276.2 	"/>
															<path id="lot167" class="lotbg" d="M700.5,310.8l22.3,13.9h3c0,0,9.5-14.1,10.1-15c3.9-6.1,7.1-15.7,7.1-23.4
															c0-9.4-1.5-36.8-1.5-36.8s-4.1-2-4.8-6.3c-2.3,0-33.9,2.1-33.9,2.1l9.6,47.2L700.5,310.8z"/>
															<path id="lot164" class="lotbg" d="M750.1,234.6l7.8-8.9l26.3-14l3.2,36.8l-37.6,1.7l0-1.5c0,0,4.1-2.5,4-7.3
															C753.6,237.1,750.7,235.1,750.1,234.6z"/>
															<polygon id="lot163" class="lotbg" points="749.8,250.3 750.4,275.6 787.4,274.3 787.4,248.6 	"/>
															<path id="lot162" class="lotbg" d="M750.4,275.6c0,0,1,8,0.5,14.5c-0.2,2.6-1,8.2-1,8.2l34.6,8.5l2.8-32.6L750.4,275.6z"/>
															<path id="lot161" class="lotbg" d="M750,298.3c0,0-5,14.8-9.1,19c7.6,5.4,24.6,17.6,24.6,17.6l19.1-28.1L750,298.3z"/>
															<path id="lot160" class="lotbg" d="M765.5,334.9L751,362.5l-19.7-24.8c0,0,5-3.1,2.5-10.4c3.5-5,7.1-10.1,7.1-10.1L765.5,334.9z"/>
															<path id="lot159" class="lotbg" d="M751,362.5l-12.5,25.3l-36.3-19.3l18.6-30.7c0,0,2.3,2,5.7,1.8s4.9-1.9,4.9-1.9L751,362.5z"/>
															<path id="lot158" class="lotbg" d="M701.1,321.5L677,360l25.1,8.5l18.6-30.7c0,0-2.9-1.8-3.2-5.9C711.2,328,701.1,321.5,701.1,321.5z
															"/>
															<polygon id="lot157" class="lotbg" points="701.1,321.5 682.6,310 655.8,351.7 677,360 	"/>
															<polygon id="lot156" class="lotbg" points="682.6,310 663.4,297.9 634.5,345.1 655.8,351.7 	"/>
															<polygon id="lot155" class="lotbg" points="663.4,297.9 644.8,286.5 612.4,338.7 634.5,345.1 	"/>
															<path id="lot154" class="lotbg" d="M644.8,286.5c0,0-18.5-11.4-18.7-11.4c-0.2,0-20.1,34.9-20.1,34.9l-0.5,28.5l7,0.2L644.8,286.5z"
															/>
															<path id="lot152" class="lotbg" d="M580.5,281.2l-18.4,16.4l23,25.6l20.4,6.1c0,0,0.6-18,0.5-19.3
															C603.7,307.5,580.5,281.2,580.5,281.2z"/>
															<path id="lot153" class="lotbg" d="M580.5,281.2c0.9-0.8,13.4-14.3,23.9-14.7s21.7,8.6,21.7,8.6L605.9,310L580.5,281.2z"/>
															<polygon id="lot151" class="lotbg" points="562.1,297.5 545.5,312 571.1,340.3 585,323.2 	"/>
															<path id="lot150" class="lotbg" d="M545.5,312l-23.6,20.3c0,0,8.7,12.7,11.2,20.6c8.1-2.6,38-12.5,38-12.5L545.5,312z"/>
															<path id="lot148" class="lotbg" d="M533.1,352.8c0,0,5.1,16.6,6.1,20.1c4.2-0.4,32.5-4.2,32.5-4.2l-0.5-28.4L533.1,352.8z"/>
															<polygon id="lot147" class="lotbg" points="539.2,372.9 539.5,396.6 573.1,396.6 571.6,368.7 	"/>
															<polygon id="lot146" class="lotbg" points="539.5,396.6 539.5,421.4 573.7,421.6 573.1,396.6 	"/>
															<polygon id="lot145" class="lotbg" points="539.5,421.4 539.5,447.1 574.2,447.1 573.7,421.6 	"/>
															<polygon id="lot144" class="lotbg" points="539.5,447.1 539.5,474.3 575.1,472.3 574.2,447.1 	"/>
															<path id="lot143" class="lotbg" d="M539.5,474.3c0,2.5,2.1,19.3,4.4,26c7.5-1.8,38.4-9.4,38.4-9.4l-7.2-18.5L539.5,474.3z"/>
															<path id="lot142" class="lotbg" d="M543.9,500.2c0.3,2.1,7.1,21,8.4,24.4c4.4-1.9,36.1-15.4,36.1-15.4s-5.8-18.6-6.2-18.5
															S543.9,500.2,543.9,500.2z"/>
															<polygon id="lot141" class="lotbg" points="552.4,524.7 563.4,547.3 596.1,530.5 588.5,509.3 	"/>
															<polygon id="lot140" class="lotbg" points="563.4,547.3 584.1,594.3 618.4,578 618.5,564.1 603.4,553.9 596.1,530.5 	"/>
															<path id="lot264" class="lotbg" d="M551.1,541.8c0,0,11,22.4,14.4,30.1c3.8,8.7,9.1,22.1,9,25.7c-0.1,1.8-14.2,3.2-20.8,2.9
															c-17-0.8-38.9-8.3-48.1-15.7c3.2-11.2,7.8-24,7.8-24L551.1,541.8z"/>
															<polygon id="lot263" class="lotbg" points="551.1,541.8 541.1,519.1 505,532.1 513.3,560.8 	"/>
															<polygon id="lot262" class="lotbg" points="541.1,519.1 534.4,496.3 497.5,503.7 505,532.1 	"/>
															<polygon id="lot261" class="lotbg" points="534.4,496.3 531.5,472.8 495.3,474.7 497.5,503.7 	"/>
															<polygon id="lot260" class="lotbg" points="531.5,472.8 531.5,447 494.2,447.1 495.3,474.7 	"/>
															<polygon id="lot259" class="lotbg" points="531.5,447 531.4,422.9 493.6,422.7 494.2,447.1 	"/>
															<polygon id="lot258" class="lotbg" points="531.4,422.9 531.7,397.8 493.2,397.8 493.6,422.7 	"/>
															<path id="lot257" class="lotbg" d="M531.7,397.8c0,0,0-12.1-0.3-17c-0.4-7.4-2.8-14.5-2.8-14.5l-35.7,8.8l0.3,22.7H531.7z"/>
															<path id="lot256" class="lotbg" d="M528.6,366.3c-0.9-2.6-5.7-20-16-31.5c-8.1,6.1-20,15.9-20,15.9l0.3,24.3L528.6,366.3z"/>
															<path id="lot267" class="lotbg" d="M577.5,608.3c0,0,1.6,4.8,2.4,8.3c-15.6,10.9-32.7,31.7-32.7,31.7l-10.2-10l-48.9-22.9L489,599
															l8-4.8c0,0,24,8.7,30.3,10.8c6.3,2.2,18,6.3,27.5,6.1C564.3,610.9,577.5,608.3,577.5,608.3z"/>
															<path id="lot268" class="lotbg" d="M488.1,615.4l-0.9,23.7l48.2,23.9c0,0,4.3-9.4,11.7-14.8c-6.5-6.3-10.2-10-10.2-10L488.1,615.4z"
															/>
															<path id="lot269" class="lotbg" d="M487.3,639.2l-1.3,36.8l44,5.3c0,0,1.1-10,5.5-18.1C524.6,657.6,487.3,639.2,487.3,639.2z"/>
															<!-- <polygon id="lot270" class="lotbg" points="529.9,681.2 530.2,702.3 484.9,702.1 486,675.9 	"/> -->
															<polygon id="lot270" class="lotbg" points="526.6,682.8 526.9,701.4 481.8,700.4 483,677.6 "/>
															<!-- <polygon id="lot271" class="lotbg" points="530.2,702.3 531.7,724.1 450.7,729.5 451.6,708.2 484.7,708.1 484.9,702.1 	"/> -->
															<polygon id="lot271" class="lotbg" points="526.3,708.2 528.4,725.3 448.1,730.6 449,713.3 485.3,712.7 485.3,707.7 "/>
															<polygon id="lot272" class="lotbg" points="531.7,724.1 533.7,746.1 484.3,749.4 484.3,742.9 450.2,745.1 450.7,729.5 	"/>
															<polygon id="lot273" class="lotbg" points="533.7,746.1 535.4,771.3 484.8,774.3 484.3,749.4 	"/>
															<path id="lot274" class="lotbg" d="M535.9,784.6l-0.5-13.4l-50.6,3.1l0.8,31.8c0,0,32.2,1.4,42.1-2.9
															C537.7,798.9,535.9,784.6,535.9,784.6z"/>
															<path id="lot276" class="lotbg" d="M450.2,745.1l-1.6,35.2l20.4,19.5c0,0,3.1-3.9,8.3-2.8c3.9,0.8,6.7,4.6,6.4,8c0.7,0,1.5,0,1.5,0
															l-0.7-62.2L450.2,745.1z"/>
															<path id="lot277" class="lotbg" d="M468.9,799.8l-20.4-19.5l-32.3,0.2l0.1,43.9l50.5-16.3C466.9,808.1,466.1,802.4,468.9,799.8z"/>
															<path id="lot279" class="lotbg" d="M416.3,824.4l0.2,37.2l2.5,5l46.4,0.6l6.5-53.9c0,0-3.8-2-5-5.4
															C462.7,809.5,416.3,824.4,416.3,824.4z"/>
															<polygon id="lot283" class="lotbg" points="471.9,813.4 494.8,813.4 494.9,870.6 465.4,867.3 	"/>
															<path id="lot284" class="lotbg" d="M494.8,813.4l24.9,0.2l5.8,68.8l-16.5-1.1c0,0-3.7-9.4-14.1-10.8
															C495,862.8,494.8,813.4,494.8,813.4z"/>
															<path id="lot285" class="lotbg" d="M519.7,813.6c2.9-0.2,11.9-3.8,18.8-10.2c8.7,5.6,38.8,25,38.8,25l-18.6,38.9l-33.3,15.1
															L519.7,813.6z"/>
															<path id="lot286" class="lotbg" d="M544.3,776.6c0,0,0.9,9.3,0.2,13.4c-0.7,4.2-2,9.3-6,13.4c6.9,4.4,38.8,25,38.8,25l9.1-54
															L544.3,776.6z"/>
															<polygon id="lot287" class="lotbg" points="544.3,776.6 542.6,751.5 585.5,748.9 586.4,774.3 	"/>
															<polygon id="lot288" class="lotbg" points="542.6,751.5 540.6,725.7 588.9,726.3 585.5,748.9 	"/>
															<polygon id="lot289" class="lotbg" points="540.6,725.7 538.4,699.9 590.9,701.3 588.9,726.3 	"/>
															<path id="lot290" class="lotbg" d="M590.9,701.3c0.2-0.1-4-14.7-4-14.7l-41.5-24.1c0,0-6.9,6.8-7.1,17.7c-0.2,10.9,0.2,19.6,0.2,19.6
															L590.9,701.3z"/>
															<path id="lot291" class="lotbg" d="M545.3,662.5c0,0,25-27.1,26.4-28.6c1.5-1.5,9.7-10.3,15.9-11.9c3.2,8.8,9.3,27.7,9.3,27.7
															s-7.3,9.6-8.9,18.3c-1.6,8.7-1.2,18.7-1.2,18.7L545.3,662.5z"/>
															<path id="lot138" class="lotbg" d="M588.3,604.5l3.5,8.3c0,0,10.9-1,18.5-0.1c7.9,1,34.6,6.5,34.6,6.5l7.8-35c0,0-9.3-1.7-22.1,0.9
															C622,586.8,600.5,596.4,588.3,604.5z"/>
															<path id="lot137" class="lotbg" d="M644.9,619.2c0,0,6.8,1.3,11.1,3c4.3,1.7,11,5.1,11,5.1l16.4-32.3c0,0-16.9-11.4-30.8-10.8
															C651.2,590.4,644.9,619.2,644.9,619.2z"/>
															<polygon id="lot136" class="lotbg" points="667,627.3 687,642 712.9,615.3 683.5,595 	"/>
															<polygon id="lot135" class="lotbg" points="712.9,615.3 727.5,625 732.8,639.1 701.9,659.8 687,642 	"/>
															<polygon id="lot134" class="lotbg" points="701.9,659.8 711.9,681 749.3,669.6 732.8,639.1 	"/>
															<polygon id="lot133" class="lotbg" points="711.9,681 716.8,703.8 759.8,696.3 749.3,669.6 	"/>
															<polygon id="lot132" class="lotbg" points="716.8,703.8 723,744.4 732.8,744.2 755.9,724.7 759.8,696.3 	"/>
															<path id="lot295" class="lotbg" d="M627.1,623.9c1.8,0,24.3,5.2,28.9,7.7c-2.5,5.9-13.7,33.3-13.7,33.3L621.4,651L627.1,623.9z"/>
															<path id="lot296" class="lotbg" d="M656,631.6c2.8,0.9,19.4,7.8,29.9,20.4c-10.7,10.3-28,26.4-28,26.4l-15.6-13.5L656,631.6z"/>
															<path id="lot297" class="lotbg" d="M685.9,652c1.6,2.1,12.7,14.7,17.7,30c-14.6,5.3-39,13.7-39,13.7l-6.7-17.3L685.9,652z"/>
															<path id="lot298" class="lotbg" d="M703.6,682c0.5,1.7,5.3,25.6,5.8,30c-8.4,1.1-42,5.4-42,5.4l-2.9-21.6L703.6,682z"/>
															<polygon id="lot299" class="lotbg" points="709.4,711.9 712.7,737.1 669.1,743 667.5,717.3 	"/>
															<polygon id="lot300" class="lotbg" points="712.7,737.1 715.9,761.4 673,792.6 666.4,786.6 670.2,782.8 669.1,743 	"/>
															<path id="lot314" class="lotbg" d="M690.4,826.9l7.7,27.3c0,0,9.8-2.2,13.3-2.2s17.2,1.2,25.6-9.1c8.2-10,6.2-18.8,5.7-21.4
															c-0.8-5-10.7-19.6-10.7-19.6L690.4,826.9z"/>
															<path id="lot130" class="lotbg" d="M764.3,754l11,12.5l-5,5.6l4.3,14.4l0.6,14.9l6.1,11.2c0,0-1.9,1.1-2.6,2
															c-0.7,0-28.5,1.8-28.5,1.8s-1.9-4.3-3.5-7.5s-15-23.7-15-23.7l18-24.1L764.3,754z"/>
															<path id="lot129" class="lotbg" d="M775.3,766.5c0,0,22.9,17.8,29.7,22.4c-3.2,4.8-15.3,24.2-15.3,24.2s-4.7-2.7-8.4-0.4
															c-1.8-3.4-6.1-11.2-6.1-11.2l-0.6-14.9l-4.3-14.4L775.3,766.5z"/>
															<path id="lot128" class="lotbg" d="M789.8,813.2c0,0,5,2.7,3.9,9c7.5,1.6,31.7,6.5,31.7,6.5s-0.7-14.1-3-20.3
															c-4.3-11.5-12.4-17.7-17.3-19.4C801.5,794.6,789.8,813.2,789.8,813.2z"/>
															<path id="lot127" class="lotbg" d="M793.7,822.2c-0.1,1.5-1.9,4.7-3.7,5.6c1.6,2.9,8.7,16.1,8.7,16.1l0.5,27.7l-4.8,5.4L804,893
															l16.2,4.4l10.7-5.6l-5.5-63.2L793.7,822.2z"/>
															<path id="lot125" class="lotbg" d="M752.2,824.9l24.7-1.6c0,0,0.6,1.9,2.2,3.4c2.4,2.2,6.5,4,11,1c3.2,5.9,8.7,16.1,8.7,16.1
															l0.5,27.7l-4.8,5.4l-7.8-12.2L746.9,843c0,0,3-5.4,3.9-9C751.6,830.5,752.2,824.9,752.2,824.9z"/>
															<path id="lot315" class="lotbg" d="M669.8,869.6l-38.3-40.2L593,877.9l60.1,19.3c0,0,2.2-7.7,6.1-14.2
															C663.1,876.4,669.8,869.6,669.8,869.6z"/>
															<path id="lot316" class="lotbg" d="M653.2,897.1L593,877.9l-6.5,7.8l7.3,7.5l-5.5,20l18.3,1.6l3.9,14.8l39.9-4.9
															C650.5,924.6,649.9,908.3,653.2,897.1z"/>
															<path id="lot11" class="lotbg" d="M619.7,948l33.7-8.9c0,0,7.6,15.1,8.6,16.1c1,1-39.3,28.9-39.3,28.9L619.7,948z"/>
															<path id="lot12" class="lotbg" d="M622.7,984l0.3,5.4l32.9,6.2l20.4-25.5c0,0-11.8-10.1-14.4-14.9C659.1,959.1,622.7,984,622.7,984z"
															/>
															<polygon id="lot13" class="lotbg" points="676.4,970 656,995.5 676,1009.1 694.6,982.2 	"/>
															<path id="lot14" class="lotbg" d="M694.6,982.2l-18.7,26.9l11.8,12.7l28.4-18.3c0,0-2.6-5.7-9.7-12.5
															C700,984.8,694.6,982.2,694.6,982.2z"/>
															<path id="lot15" class="lotbg" d="M716.2,1003.4c0.8,1.6,9.2,11.7,11.4,30c-16.7,1.9-38.1,2.9-38.1,2.9l-1.6-14.6L716.2,1003.4z"/>
															<path id="lot16" class="lotbg" d="M689.4,1036.4l-12.2,17.4v2.4l23.5,0.5l21.1,8.8c0,0,4.5-10.5,5.5-19.3c1-8.8,0.2-12.7,0.2-12.7
															L689.4,1036.4z"/>
															<polygon id="lot17" class="lotbg" points="721.9,1065.4 712.9,1085.5 691.9,1077.6 677.4,1077.6 677.3,1056.2 700.8,1056.7 	"/>
															<path id="lot18" class="lotbg" d="M712.9,1085.5c0,0-2.9,6.8-3.2,10.9c-0.3,4.1-0.3,7.5-0.3,7.5l-34.7,0.8l2.8-27.1h14.4
															L712.9,1085.5z"/>
															<path id="lot19" class="lotbg" d="M709.4,1103.9c0,0,3.4,18,6,22.9c-10.4,5.8-15.3,13.1-15.3,13.1l-28.6-26.3l3.1-8.9L709.4,1103.9z"
															/>
															<path id="lot20" class="lotbg" d="M700.1,1139.9c0,0-8.3,6.7-10.5,15.1c-6.2-2.4-29.7-12.8-29.7-12.8s11-28.9,11.7-28.6
															C672.2,1113.9,700.1,1139.9,700.1,1139.9z"/>
															<polygon id="lot21" class="lotbg" points="689.6,1155 685.2,1174.9 653.2,1168.6 659.9,1142.2 	"/>
															<polygon id="lot22" class="lotbg" points="685.2,1174.9 680.3,1196.9 649,1190.8 653.2,1168.6 	"/>
															<polygon id="lot23" class="lotbg" points="680.3,1196.9 676,1218.3 645,1211.3 649,1190.8 	"/>
															<polygon id="lot24" class="lotbg" points="676,1218.3 671.6,1241.3 637.2,1232.4 645,1211.3 	"/>
															<polygon id="lot25" class="lotbg" points="671.6,1241.3 661,1265 623.9,1248 637.2,1232.4 	"/>
															<path id="lot26" class="lotbg" d="M661,1265c-1.1,3.2-9.2,19.3-18.3,25.5c-9.6-11.6-30.5-37.3-30.5-37.3l11.7-5.2L661,1265z"/>
															<path id="lot27" class="lotbg" d="M642.7,1290.5c-2.8,2.3-9.9,7.5-19.3,10.5s-18.8,2.6-18.8,2.6l1-47.7l6.7-2.8L642.7,1290.5z"/>
															<path id="lot28" class="lotbg" d="M604.6,1303.6c0,0-7.6,0-15.6-1.9c-8-1.9-13-3.9-13-3.9l11.8-39.4l17.7-2.4L604.6,1303.6z"/>
															<polygon id="lot29" class="lotbg" points="576,1297.8 555.4,1291.3 565.9,1255.9 587.8,1258.3 	"/>
															<polygon id="lot30" class="lotbg" points="555.4,1291.3 529.8,1284.5 534.8,1247.8 565.9,1255.9 	"/>
															<path id="lot32" class="lotbg" d="M529.8,1284.5l5-36.7l-35.2-1.9l4.2,38.6c0,0,8.8-0.6,14.1-0.6
															C523.3,1283.8,529.8,1284.5,529.8,1284.5z"/>
															<path id="lot33" class="lotbg" d="M503.8,1284.5c-2.9,0.8-6.3,1.8-14,2.4c-4.7,0.4-13.8,0.2-13.8,0.2l4.1-40.1l19.5-1.1L503.8,1284.5
															z"/>
															<path id="lot34" class="lotbg" d="M476.1,1287.1c0,0-7.8-1.5-13.5-2.6c-5.6-1.1-11.7-3.1-11.7-3.1l8.8-35.4l20.4,1L476.1,1287.1z"/>
															<polygon id="lot35" class="lotbg" points="450.9,1281.4 429.3,1276.8 438.1,1240.5 459.7,1246 	"/>
															<polygon id="lot36" class="lotbg" points="429.3,1276.8 407.4,1271.5 415.7,1237.7 438.1,1240.5 	"/>
															<path id="lot37" class="lotbg" d="M407.4,1271.5c0,0-6.8-1.3-9.7-1.3s-9.2,0.2-9.2,0.2l-2.9-41.2l30.2,8.6L407.4,1271.5z"/>
															<path id="lot38" class="lotbg" d="M388.4,1270.4c0,0-5.5,0-9.7,1.1c-3,0.8-8.8,3.4-8.8,3.4l-19.6-47.4l35.2,1.6L388.4,1270.4z"/>
															<polygon id="lot39" class="lotbg" points="369.9,1274.9 353.2,1284.5 326.6,1244.9 350.3,1227.5 	"/>
															<path id="lot124" class="lotbg" d="M700,975.2c0,0,14.9,12.3,17.2,14.6c6-4.9,28.2-23.7,28.2-23.7l-8.8-13.6l-22.1,0.2L700,975.2z"/>
															<path id="lot123" class="lotbg" d="M717.2,989.8c0,0,6.7,8.6,8.1,11c1.5,2.4,4.1,7.5,4.1,7.5l33.7-15.1l-17.7-27.1L717.2,989.8z"/>
															<path id="lot122" class="lotbg" d="M729.3,1008.3c0,0,4.7,10.9,5.8,18.5c8.8-1.1,36.8-4.7,40.2-5.5c0.2-0.8,2.4-14.1,2.4-14.1
															l-14.8-14L729.3,1008.3z"/>
															<path id="lot121" class="lotbg" d="M735.2,1026.8c0.3,2.6,1.3,15.4,0.6,20.9c6.3,0.8,41.1,3.7,41.1,3.7l-1.5-30.2L735.2,1026.8z"/>
															<path id="lot120" class="lotbg" d="M735.8,1047.7c0,0-2.6,16.1-4.1,18.7c6.5,2.3,34.2,11.8,34.2,11.8l10.9-26.8L735.8,1047.7z"/>
															<path id="lot119" class="lotbg" d="M731.8,1066.4c0,0-8.1,14.1-10.1,19c-1.6,3.9-3.1,10.7-3.1,10.7l46.7,2.4l0.6-20.3L731.8,1066.4z"
															/>
															<path id="lot118" class="lotbg" d="M718.6,1096.1c-0.3,2.3-1.3,8.3,0.3,15.3s4.2,12.5,4.2,12.5s7.8-1.8,18.5-1.8
															c10.7,0,22.9,0.8,22.9,0.8l0.8-24.3L718.6,1096.1z"/>
															<polygon id="lot117" class="lotbg" points="764.5,1122.9 786.6,1124.2 787.4,1080.7 766,1078.2 	"/>
															<polygon id="lot116" class="lotbg" points="786.6,1124.2 809,1125.5 811.1,1085.7 787.4,1080.7 	"/>
															<polygon id="lot115" class="lotbg" points="809,1125.5 828.6,1127.4 832.2,1092.5 811.1,1085.7 	"/>
															<polygon id="lot82" class="lotbg" points="872.9,1142 870,1161.2 835.5,1156.9 838,1136.7 	"/>
															<polygon id="lot81" class="lotbg" points="870,1161.2 870,1179.2 832.9,1175.8 835.5,1156.9 	"/>
															<path id="lot80" class="lotbg" d="M870,1179.2v16.9l-37.2,2.8c0,0-0.8-20.8,0-23C838.5,1176.2,870,1179.2,870,1179.2z"/>
															<polygon id="lot79" class="lotbg" points="832.9,1198.8 834,1217.8 884.3,1213.7 884.6,1194.7 	"/>
															<path id="lot78" class="lotbg" d="M834,1217.8l1.6,15.5c0,0,2.4,0.6,4.3,3.4c5.2-0.5,45.1-4.6,45.1-4.6s0-18.5-0.6-18.3
															C883.6,1213.9,834,1217.8,834,1217.8z"/>
															<path id="lot77" class="lotbg" d="M839.8,1236.7c0,0,2.8,3.5,0.6,8.5c7,4.2,36.7,21.7,36.7,21.7l7.6,0.2l0.2-35L839.8,1236.7z"/>
															<path id="lot76" class="lotbg" d="M833.3,1250.1l4.4,31.6l39.4-14.8l-36.7-21.7C840.5,1245.2,838.2,1249.9,833.3,1250.1z"/>
															<path id="lot75" class="lotbg" d="M825.4,1246.4l-37.4,25.9l31.5,16.2l18.2-6.8l-4.4-31.6C833.3,1250.1,828.2,1250.6,825.4,1246.4z"
															/>
															<path id="lot73" class="lotbg" d="M788.1,1272.3c0-0.6-7.3-26.3-7.3-26.3l7.1-15.4l38.3-5.7l1.5,9.4c0,0-3.7,2.8-3.7,6.5
															s1.5,5.6,1.5,5.6L788.1,1272.3z"/>
															<path id="lot72" class="lotbg" d="M826.2,1224.9l-1.9-23.7l-38.8,4.9c0,0,1.9,24.7,2.4,24.5C788.4,1230.4,826.2,1224.9,826.2,1224.9z
															"/>
															<polygon id="lot71" class="lotbg" points="824.3,1201.2 824.6,1176.4 775.3,1175.4 767.8,1199.4 785.5,1206.1 	"/>
															<polygon id="lot70" class="lotbg" points="824.6,1176.4 829.8,1135.7 803.3,1133.2 800.6,1175.9 	"/>
															<path id="lot69" class="lotbg" d="M803.3,1133.2c-1.6,0-25.5-1.1-25.5-1.1l-2.6,43.3l25.3,0.5L803.3,1133.2z"/>
															<polygon id="lot68" class="lotbg" points="777.9,1132.1 756.1,1130.7 757.1,1172.8 775.3,1175.4 	"/>
															<path id="lot67" class="lotbg" d="M756.1,1130.7c0,0-15.1-0.5-20.4-0.5c-2.6,0-8.1,1.6-8.1,1.6l9.2,28.1l13.1,13.8l7.1-0.8
															L756.1,1130.7z"/>
															<path id="lot66" class="lotbg" d="M727.6,1131.8c-0.3,0.3-8.4,1.6-17.8,10.2c-9.4,8.6-10.4,13.8-10.4,13.8l33.1,18.5l17.5-0.6
															l-13.1-13.8L727.6,1131.8z"/>
															<path id="lot65" class="lotbg" d="M732.4,1174.3l-5.4,15.9l-35-6.8c0,0,1.1-9.2,2.8-15.1c1.6-5.8,4.5-12.5,4.5-12.5L732.4,1174.3z"/>
															<polygon id="lot64" class="lotbg" points="692,1183.4 687.3,1204.6 721.2,1211.8 727.1,1190.2 	"/>
															<polygon id="lot63" class="lotbg" points="687.3,1204.6 683.1,1226.7 717.2,1233.8 721.2,1211.8 	"/>
															<polygon id="lot62" class="lotbg" points="683.1,1226.7 678.7,1247.5 712.9,1258.7 717.2,1233.8 	"/>
															<path id="lot61" class="lotbg" d="M678.7,1247.5c0,0-4.9,10.5-8.3,18.5c4.9,2.4,36.3,18.7,36.3,18.7l6.2-26L678.7,1247.5z"/>
															<path id="lot60" class="lotbg" d="M670.4,1266l-9.1,16.6l33.6,24.7c0,0,12.5-22.6,11.8-22.6S670.4,1266,670.4,1266z"/>
															<path id="lot59" class="lotbg" d="M661.4,1282.5c0,0-7.8,10.2-14,14.8c1.6,2.3,33.9,42.7,33.9,42.7l13.6-32.8L661.4,1282.5z"/>
															<path id="lot58" class="lotbg" d="M647.4,1297.3c-5,4.5-15.9,10.2-15.9,10.2l25.1,60.7l24.7-28.2C681.3,1340,650,1300.5,647.4,1297.3
															z"/>
															<path id="lot57" class="lotbg" d="M631.5,1307.5c0,0-10.2,3.1-19.1,4.5c0,3.9,5,54.4,5,54.4l39.3,1.8L631.5,1307.5z"/>
															<path id="lot56" class="lotbg" d="M612.4,1312.1c-2.4,0-13.3,0.3-19.1-0.8c-1.6,8.6-8.6,42.3-8.1,42.5s32.3,12.7,32.3,12.7
															L612.4,1312.1z"/>
															<path id="lot55" class="lotbg" d="M593.2,1311.2c-0.5,0.3-21.3-6.2-21.3-6.2l-11.2,38.6l24.3,10.1L593.2,1311.2z"/>
															<polygon id="lot54" class="lotbg" points="571.9,1305.1 550.7,1298.8 539.2,1336.7 560.8,1343.7 	"/>
															<path id="lot53" class="lotbg" d="M550.7,1298.8c-1.6-1-17.5-6.3-25.8-6c-0.6,9.1-2.9,40.2-2.9,40.2s17,4.2,17.2,3.7
															C539.3,1336.2,550.7,1298.8,550.7,1298.8z"/>
															<path id="lot51" class="lotbg" d="M524.9,1292.7c-2.4-0.2-10.4-1.1-19.8,0.8c-9.4,1.9-15.6,1.6-15.6,1.6l2.8,37.5l29.7,0.3
															L524.9,1292.7z"/>
															<path id="lot50" class="lotbg" d="M489.5,1295.2c0,0-4.4,0.6-10.7,0.5c-6.3-0.2-9.7-1-9.7-1l-5.5,35.5l28.7,2.4L489.5,1295.2z"/>
															<polygon id="lot49" class="lotbg" points="469.1,1294.7 447.8,1290.3 438.6,1328.9 463.6,1330.2 	"/>
															<polygon id="lot48" class="lotbg" points="447.8,1290.3 426.6,1285.1 419.1,1314.5 418.8,1328.6 425.4,1335.1 438.6,1328.9 	"/>
															<polygon id="lot47" class="lotbg" points="426.6,1285.1 403.9,1279.8 394.8,1328.8 418.8,1328.6 419.1,1314.5 	"/>
															<path id="lot40" class="lotbg" d="M403.9,1279.8c0,0-3.6-1.8-12-1.3s-20.3,3.7-25.1,7.5c-4.9,3.7,4.9,4.1,9.7,20.8
															c4.9,16.7,4.2,22.6,4.2,22.6l14.1-0.5L403.9,1279.8z"/>
															<path id="lot46" class="lotbg" d="M380.6,1329.3c0,0,0,10.3,0,11.3c0.5,0.2,5.6,1.9,6,7.9c2.8,0.3,7.8,0.8,7.8,0.8l27.4,7.5l3.6-21.6
															l-6.7-6.5L380.6,1329.3z"/>
															<path id="lot45" class="lotbg" d="M386.7,1348.4l7.8,0.8l27.4,7.5c0,0,6.7,39.3,5.4,39.3s-26,0.5-26,0.2s-19.3-39.5-19.3-39.5
															s2.8-1.6,3.7-3.7S386.7,1348.4,386.7,1348.4z"/>
															<path id="lot44" class="lotbg" d="M372.6,1355.7l-26.4,35l5.8,5.8l49.3-0.4l-19.3-39.5c0,0-1.8,1.2-5.2,0.8
															C373.9,1357.2,372.6,1355.7,372.6,1355.7z"/>
															<path id="lot43" class="lotbg" d="M369.8,1345.9l-43.8,3.7c0,0,1.9,10.7,7,21.4c5,10.7,13.2,19.7,13.2,19.7l26.4-35
															c0,0-2.6-2.3-3.2-4.8C368.9,1348.4,369.8,1345.9,369.8,1345.9z"/>
															<path id="lot42" class="lotbg" d="M326,1349.6l-5-21.7l50.5-4.9l1.9,18.3c0,0-1,0.7-2.1,1.9s-1.5,2.7-1.5,2.7L326,1349.6z"/>
															<path id="lot41" class="lotbg" d="M371.5,1322.9c0,0-0.8-7.3-2.1-11.7c-2-6.9-11.8-19.1-11.8-19.1s-10.8,6.2-19.3,10.4
															c-8.4,4.2-21.3,8.8-21.3,8.8l4,16.6L371.5,1322.9z"/>
															<polygon id="lot325" class="lotbg" points="273.5,879.2 273.5,887.4 275.7,887.4 275.7,904 232.8,904 232.8,878.7 	"/>
															<polygon id="lot326" class="lotbg" points="232.8,904 233,928.5 275.9,927.8 275.7,904 	"/>
															<polygon id="lot327" class="lotbg" points="211.4,878.9 211.4,928.5 233,928.5 232.8,878.7 	"/>
															<polygon id="lot328" class="lotbg" points="211.4,878.9 190,878.9 190.2,928.9 211.4,928.5 	"/>
															<polygon id="lot329" class="lotbg" points="190,878.9 186.5,878.9 165.5,897.3 164.9,928.5 190.2,928.9 	"/>
															<path id="lot331" class="lotbg" d="M186.5,878.9l0.4-4.8l-47.4-3.5c0,0-2.2,4.1-5.2,5.4c-0.4,4.1-1.7,18.2-1.7,18.2l32.9,3
															L186.5,878.9z"/>
															<polygon id="lot330" class="lotbg" points="132.7,894.3 130.9,927.4 164.9,928.5 165.5,897.3 	"/>
															<path id="lot332" class="lotbg" d="M187,874.2l-1.1-42.2H179l-41.3,31.2c0,0,1.7,1.1,1.9,3.5c0.2,2.4,0,4.1,0,4.1L187,874.2z"/>
															<path id="lot333" class="lotbg" d="M179,832l-60.1-1.1l8.4,28.8c0,0,3.2-0.6,5.8,0.2c2.6,0.9,4.5,3.2,4.5,3.2L179,832z"/>
															<path id="lot334" class="lotbg" d="M118.8,830.9l-38.1-0.2l-3,22.7l44.8,12.1c0,0,1.1-4.1,4.8-5.8
															C125.3,853.4,118.8,830.9,118.8,830.9z"/>
															<path id="lot335" class="lotbg" d="M122.5,865.5l-44.8-12.1l-3.5,26.2l50.4,4.3l0.9-8.4c0,0-2.8-1.9-3-4.8
															C122.3,867.9,122.5,865.5,122.5,865.5z"/>
															<polygon id="lot336" class="lotbg" points="124.7,883.9 123.1,905.1 71.4,900.4 74.3,879.6 	"/>
															<polygon id="lot337" class="lotbg" points="123.1,905.1 121.8,927.6 68.6,925.2 71.4,900.4 	"/>
															<polygon id="lot1" class="lotbg" points="68,945.8 61.1,985.8 103.9,986.5 102.4,946.7 	"/>
															<polygon id="lot2" class="lotbg" points="103.9,986.5 133.5,986.7 128.1,941.2 102.4,946.7 	"/>
															<path id="lot3" class="lotbg" d="M133.5,986.7c0.4,0.2,27.9,0,27.9,0l0.2-49.8h-10.8l-22.7,4.3L133.5,986.7z"/>
															<polygon id="lot4" class="lotbg" points="161.4,986.7 187.6,986.7 187.6,936.7 161.7,936.9 	"/>
															<polygon id="lot5" class="lotbg" points="187.6,986.7 214.9,986.7 215.3,936.7 187.6,936.7 	"/>
															<polygon id="lot6" class="lotbg" points="214.9,986.7 241.7,986.9 241.9,936.9 215.3,936.9 	"/>
															<polygon id="lot7" class="lotbg" points="241.7,986.9 268.5,986.9 268.7,937.8 241.9,936.9 	"/>
															<polygon id="lot8" class="lotbg" points="268.5,986.9 292.8,986.9 292.5,938.2 268.7,937.8 	"/>
															<polygon id="lot9" class="lotbg" points="292.8,986.9 316.6,986.5 314.2,938.4 292.5,938.2 	"/>
															<polygon id="lot324" class="lotbg" points="286.5,879.4 286.5,887.8 284.3,887.8 284.3,903.8 351,903.8 350.7,878.9 	"/>
															<polygon id="lot323" class="lotbg" points="284.3,903.8 284.3,926.1 335.4,926.1 372.4,934.3 351,903.8 	"/>
															<path id="lot322" class="lotbg" d="M461.9,922.6l-0.2,47.8c0,0-10.5,1.2-15.1-0.2c-11.7-3.5-18.8-11.2-20.6-13.2
															c5.4-6.9,11.5-14.9,11.5-14.9l0.9-14.7L461.9,922.6z"/>
															<polygon id="lot321" class="lotbg" points="461.9,922.6 485.1,925.2 484.2,971.1 461.7,970.5 	"/>
															<polygon id="lot320" class="lotbg" points="485.1,925.2 507.2,928.7 506.3,971.3 484.2,971.1 	"/>
															<polygon id="lot319" class="lotbg" points="507.2,928.7 531.2,932.6 530.1,972.2 506.3,971.3 	"/>
															<path id="lot318" class="lotbg" d="M531.2,932.6c0,0,9.7,2.6,12.5,2.2s6.9-1.9,6.9-1.9l7.6,36.8c0,0-4.3,2.6-11,3s-17.1-0.4-17.1-0.4
															L531.2,932.6z"/>
															<path id="lot317" class="lotbg" d="M550.6,932.8c0,0,14.1-7.4,22.5-6.7c8.4,0.6,17.3,2.4,17.3,2.4l9.1,16.4c0,0-11.2,6.9-19.3,11.9
															c-8,5-12.8,10.8-22.1,12.8C557.3,965.9,550.6,932.8,550.6,932.8z"/>
															<polygon id="lot441" class="lotbg" points="504.1,980.8 534,980.8 534.8,1046 531.6,1068.2 509.1,1068.2 503.7,1062.2 	"/>
															<polygon id="lot440" class="lotbg" points="504.1,980.8 473.6,980.2 473.8,1004.4 468.4,1046.4 470.6,1052 495.3,1055.9 
															503.7,1062.2 	"/>
															<polygon id="lot439" class="lotbg" points="473.6,980.2 449.6,979.5 450.7,999.4 395.7,1046.8 468.4,1046.4 473.8,1004.4 	"/>
															<path id="lot438" class="lotbg" d="M449.6,979.5c0,0-5.4,0.4-13.6-3.7c-8.2-4.1-21.4-16.2-21.4-16.2l-12.5,8.9l-2.6,13.4l-19.9,10.6
															l-12.1-1.5l-7.4,31.8l5,24.4l30.7-0.4l55-47.4L449.6,979.5z"/>
															<!-- <polygon id="lot442" class="lotbg" points="489.2,601.3 496.8,594.1 485.5,589.7 481.2,642.7 441.2,642.8 439.5,654.4 428,667 
																422,686 416.8,694.7 452.2,707.6 484.7,707.6 "/> -->
															<polygon id="lot442" class="lotbg" points="489.2,600.8 496.8,593.6 485.5,589.2 481.2,642.2 441.2,642.3 439.5,653.9 428.1,666.5 
	422.1,685.5 416.8,694.2 452.2,707.1 478.8,707.2 480.8,675.7 486.1,675.7 "/>
															<polygon id="lot443" class="lotbg" points="452.2,707.6 417,694.8 422,685.8 428.5,666.8 439.2,654.3 441.2,642.8 445.7,616.1 
																463.5,585 441.7,578.3 409,670.8 369.2,736.3 380.5,762.4 450.5,731.7 "/>
															<polygon id="lot444" class="lotbg" points="380.8,762.7 369.2,736.4 409,671.1 441.7,578.3 424,575.3 359.5,722.8 382.3,780.6 
																447.5,779.4 450.5,731.6 "/>
															<path id="lotea1" class="lotbg" d="M535.5,981.1l-0.3,37.7l41-15l-10-22l-7-3.3c0,0-7.3,3-12.3,3S535.5,981.1,535.5,981.1z"/>
															<path id="lotea2" class="lotbg" d="M576.2,1003.8c0,0,3.1,5.1,6,13.7c2,5.8,3.1,14.6,3.1,14.6l-50.8,0.1l0.7-13.3L576.2,1003.8z"/>
															<path id="lotea3" class="lotbg" d="M534.5,1032.1l-0.3,15.7l48,12.7c0,0,2.3-5.5,3-14.5s0-13.8,0-13.8h-50.7V1032.1z"/>
															<path id="lotea4" class="lotbg" d="M534.2,1047.8l-4,22.3l43.7,11.5c0,0,2.1-6.7,3.3-10.2c1.9-5.3,4.9-11,4.9-11L534.2,1047.8z"/>
															<path id="lotea5" class="lotbg" d="M573.8,1081.6c0,0-1.5,5.1-1.6,9.5s-0.1,5.8,0.1,9.4c-0.6,0-49.2,3.2-49.2,3.2l-1.6-34.5
																l8.7,0.9L573.8,1081.6z"/>
															<path id="lotea6" class="lotbg" d="M572.3,1100.5c0,0,3.9,18.2,4.9,21.8c-2.4,0-19.2,1.6-19.2,1.6l-30.4-0.9l-4.5-19.2
																L572.3,1100.5z"/>
															<path id="lotea7" class="lotbg" d="M577.2,1122.2c0,0,4.1,9,5,14.5c0.3,2,0.5,7.8,0.5,7.8l-23.9,1.6l-45.1-16.8l13.9-6.4l30.4,0.9
																L577.2,1122.2z"/>
															<path id="lotea8" class="lotbg" d="M541.7,1140l-0.8,30.2c0,0,11.1,0.2,13.2,0.1c2.1-0.1,13,0,19.9-7.9s8.5-15,8.5-18.1
																c-7.5,0.5-23.8,1.8-23.8,1.8L541.7,1140z"/>
															<polygon id="lotea9" class="lotbg" points="541,1170.2 512,1169.2 513.7,1129.3 541.7,1140 		"/>
															<path id="lotea10" class="lotbg" d="M512,1169.2l-10.1-0.3c0,0-5.2-2.5-7.2-2.5s-7.8,2.2-7.8,2.2l-3.8-0.4l0.9-30.1l29.6-8.9
																L512,1169.2z"/>
															<polygon id="lotea11" class="lotbg" points="483.2,1168.3 460,1167.8 448.2,1166.3 452.2,1136.5 484,1138.2 		"/>
															<polygon id="lotea12" class="lotbg" points="448.2,1166.3 418,1162.6 422.5,1127.2 452.2,1136.5 		"/>
															<path id="lotea13" class="lotbg" d="M418,1162.6c0,0-12.4-1.4-15.6-2.2c-3.2-0.8-9.9-4.1-10.2-7s3.1-6,3.6-7.9s9.1-24.8,9.1-24.8
																l17.6,6.5L418,1162.6z"/>
															<path id="lotea14" class="lotbg" d="M405,1120.7c0,0,3.1-14.4,13.2-20.5s18.8-5.4,27.9-1.8c-3,8-12.2,32.2-12.2,32.2L405,1120.7z"
																/>
															<path id="lotea15" class="lotbg" d="M446,1098.5c0,0,9.8,3.6,10.5,4.1s1.5,1,1.8,3.2c0.3,2.2,3.1,5.1,5,6s5.4,0.9,5.4,0.9l4,24.9
																l-20.5-1.1l-18.4-5.8L446,1098.5z"/>
															<path id="lotea16" class="lotbg" d="M468.7,1112.7c0,0,3.2-1,5.2-2.5c1.6-1.2,2.9-4.1,2.9-4.1l50.8,16.9l-13.9,6.4l-29.6,8.9
																l-11.5-0.8L468.7,1112.7z"/>
															<path id="lotea17" class="lotbg" d="M476.8,1106.1c0.5-1.9,0.4-6.5-1.9-9.2c7.2-6.1,33-28.9,33-28.9l13.5,1.2l1.6,34.5l4.5,19.2
																L476.8,1106.1z"/>
															<path id="lotea18" class="lotbg" d="M475,1096.8c0,0-2.8-3.5-7-3.6c-3.6-0.1-6.4,2.8-7.2,2.8s-1.9,0-1.9,0l16.5-41.4l20.2,2.2
																l12.4,11.1L475,1096.8z"/>
															<polygon id="lotea19" class="lotbg" points="458.8,1096 440.2,1089.3 447.2,1051 475.3,1054.6 		"/>
															<path id="lotea20" class="lotbg" d="M440.2,1089.3c0,0-5.4-0.8-7.9-0.6c-2.5,0.2-8,1.2-8,1.2l-11.5-40.9l34.4,1.9L440.2,1089.3z"/>
															<path id="lotea21" class="lotbg" d="M424.3,1090c0,0-5.9,1.9-8.2,3.4s-5.2,3.9-5.2,3.9l-40.8-47.9l42.8-0.2L424.3,1090z"/>
															<polygon id="lotea22" class="lotbg" points="399.7,1114.1 391.8,1135.1 378.2,1129.8 339,1081.7 343,1072.6 371.5,1103.8 		"/>
															<path id="lotea23" class="lotbg" d="M391.8,1135.1c0,0-2,6.4-3,8.5s-2.8,6.1-7,8.8c-9.2,5.9-20.1,2.8-20.1,2.8l0.2-28.2l-21.8-43.4
																l0.1-0.4l37.9,46.8L391.8,1135.1z"/>
															<polygon id="lotea24" class="lotbg" points="361.6,1155.1 345.6,1153.1 339,1152.8 339.9,1083.1 362,1126.8 		"/>
															<path id="lotea25" class="lotbg" d="M340,1083.3c0,0-1.1-2.2-6.8-0.8s-4,3.1-7.9,3.1c-2.1,0-7.8-1.2-7.8-1.2l-1,69.1l22.5-0.8
																L340,1083.3z"/>
															<path id="lotea26" class="lotbg" d="M316.5,1153.6c0,0-5.8,1.5-10.1,0.6s-12-7.4-12-7.4l0.4-67.1l3.8,2.5l15.8-0.6l3.2,2.9
																L316.5,1153.6z"/>
															<path id="lotea27" class="lotbg" d="M294.5,1146.8c0,0-6.9-4.8-10.8-6.1c-3.9-1.3-11.9-3.1-11.9-3.1l1.2-57.6l4.9,0.9l6.1-4.1
																l3.5,1.6l2.4-1.2l4.9,2.6L294.5,1146.8z"/>
															<polygon id="lotea28" class="lotbg" points="271.9,1137.6 249.5,1137.1 251.4,1081.6 259.5,1080.1 265.6,1079.2 273,1080 		"/>
															<path id="lotea29" class="lotbg" d="M249.5,1137.1c0,0-3-0.1-4.9,0.9c-1.4,0.8-3.4,3.4-3.4,3.4l-40.8-25.9l5.1-9.6l13.5-12.4
																l14.5-5l8.2-5.8l9.5-1.1L249.5,1137.1z"/>
															<path id="lotea30" class="lotbg" d="M241.1,1141.3c0,0-1.8,2.9-1.4,5.4s1,4.1,1,4.1l-41.9,21.8l-25.2-22.4l7.6-4.8l8.1,1l4.9-5.1
																l6.1-25.9L241.1,1141.3z"/>
															<path id="lotea31" class="lotbg" d="M240.9,1150.8c0,0,1.6,3,3.8,4.1s4.1,1.2,4.1,1.2l-3.1,46.9l-13.1-0.2l-33.5-30.2L240.9,1150.8
																z"/>
															<path id="lotea32" class="lotbg" d="M248.6,1156.2c0,0,3.6-0.4,6.9-2s10.8-7.1,18.2-7c0,4.9-2.9,55.6-2.9,55.6l-25.4,0.2
																L248.6,1156.2z"/>
															<path id="lotea33" class="lotbg" d="M273.9,1147.2c0,0,5.5-1,9.9,1.4s12.8,8.5,12.8,8.5l-0.1,45.6l-25.4,0.1L273.9,1147.2z"/>
															<path id="lotea34" class="lotbg" d="M296.5,1157.1c0,0,3.9,3.1,7.9,3.8c4,0.7,16.8-0.2,16.8-0.2s2.8,38.6,3.1,41.9
																c-0.4,0-27.9,0.2-27.9,0.2L296.5,1157.1z"/>
															<path id="lotea35" class="lotbg" d="M321,1160.6c0,0,12.1-1,14.8-1s13.8,1,13.8,1l-5,41.6l-20.4,0.2L321,1160.6z"/>
															<path id="lotea36" class="lotbg" d="M349.5,1160.6c0,0,12.8,2,15.1,2.1c5.2,0.4,9.8-0.2,9.8-0.2l-3.2,39.4l-26.6,0.4L349.5,1160.6z
																"/>
															<path id="lotea37" class="lotbg" d="M374.5,1162.5c0,0,2.5-0.4,5.4-1.4s4.2-4.1,8.4-0.9s10.6,7.1,13.6,7.6
																c-0.4,2.4-4.4,34.1-4.4,34.1l-26.2-0.1L374.5,1162.5z"/>
															<polygon id="lotea38" class="lotbg" points="401.8,1167.8 432.3,1171.6 428.5,1202.5 397.5,1202 		"/>
															<polygon id="lotea39" class="lotbg" points="432.3,1171.6 454.8,1174.3 464.2,1174.7 463.5,1203.3 428.5,1202.5 		"/>
															<path id="lotea40" class="lotbg" d="M464.2,1174.7c0,0,24.3,2.2,24.8,2.2s4.2,1,5.8,1s3.8-1.6,3.8-1.6l-0.9,27.8l-34.2-0.8
																L464.2,1174.7z"/>
															<polygon id="lotea41" class="lotbg" points="498.5,1176.3 532.7,1176.8 531.8,1206 497.7,1204.1 		"/>
															<path id="lotea42" class="lotbg" d="M532.7,1176.8c0,0,21.8,1.1,23.6,1c3.4-0.2,7.4-1.5,7.4-1.5l6.9,30.2l-38.8-0.6L532.7,1176.8z"
																/>
															<path id="lotea43" class="lotbg" d="M563.7,1176.3c0,0,6.4-2.3,8.6-3.6c4.5-2.6,7.8-6.5,7.8-6.5l40,42.1l-49.5-1.8L563.7,1176.3z"
																/>
															<path id="99944" class="lotbg" d="M580,1166.2c0,0,4.1-4.6,5.5-7.5s3.1-7.6,3.1-7.6l32.6,8.9l-1.2,48.4L580,1166.2z"/>
															<path id="lotea45" class="lotbg" d="M588.7,1151.1c0,0,1.5-6.6,1.1-11.2c-0.4-4.6-4-14.4-4-14.4l36.1-6.1l-0.6,40.6L588.7,1151.1z"
																/>
															<path id="lotea46" class="lotbg" d="M585.8,1125.5c0,0-5.5-16.5-6.1-19.8c-0.6-3.3-0.8-13.2-0.8-13.2l43.1,2l-0.1,24.9
																L585.8,1125.5z"/>
															<path id="lotea47" class="lotbg" d="M579,1092.5c0,0,0.4-5.9,2-11s7.1-19.4,7.1-19.4l34.5,4.4l-0.5,28L579,1092.5z"/>
															<path id="lotea48" class="lotbg" d="M588,1062.1c0,0,2.9-10.9,3-15s0.2-14,0.2-14l31.5,0.4l-0.2,33L588,1062.1z"/>
															<path id="lotea49" class="lotbg" d="M591.3,1033.1c0,0-0.8-10.1-1.6-13.2c-0.8-3.1-3-9.2-3-9.2l36.6-16.4l-0.5,39.2L591.3,1033.1z"
																/>
															
														</g>
														
														
														
														<g id="Layer_8">
															<path id="txt180" class="lot-text" d="M509.9,307.3L509.9,307.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V307.3z M512.7,311
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C513.5,312.9,512.7,312.1,512.7,311z M515.3,310.9c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C515.1,311.9,515.3,311.5,515.3,310.9z M514.1,307.7c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S514.1,307.3,514.1,307.7z M521.1,309.4c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4
															c0-1.9,0.6-3.5,2-3.5C520.7,305.9,521.1,307.8,521.1,309.4z M518.5,309.4c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3
															c0-1.4-0.1-2.3-0.6-2.3C518.7,307.1,518.5,307.8,518.5,309.4z"/>
															<path id="txt179" class="lot-text" d="M529.5,294L529.5,294l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V294z M536.1,292.7v0.9l-2.1,5.9
															h-1.3l2.1-5.5v0h-2.3v-1.2H536.1z M537.2,298.4c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0
															c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3
															c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V298.4z M538,294.9c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4
															c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C538.3,293.6,538,294.1,538,294.9z"/>
															<path id="txt178" class="lot-text" d="M548.3,278.1L548.3,278.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V278.1z M554.9,276.8v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H554.9z M555.5,281.8c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C556.3,283.6,555.5,282.9,555.5,281.8z M558.2,281.7
															c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C557.9,282.7,558.2,282.3,558.2,281.7z M556.9,278.5
															c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S556.9,278,556.9,278.5z"/>
															<path id="txt177" class="lot-text" d="M567.4,260L567.4,260l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V260z M574.1,258.7v0.9l-2.1,5.9
															h-1.3l2.1-5.5v0h-2.3v-1.2H574.1z M578.5,258.7v0.9l-2.1,5.9H575l2.1-5.5v0h-2.3v-1.2H578.5z"/>
															<path id="txt176" class="lot-text" d="M593.3,243L593.3,243l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V243z M599.9,241.8v0.9l-2.1,5.9
															h-1.3l2.1-5.5v0h-2.3v-1.2H599.9z M604,242.8c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V242.8z M603.1,246.2c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C603,247.6,603.1,247,603.1,246.2z"/>
															<path id="txt175" class="lot-text" d="M616.1,238.8L616.1,238.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V238.8z M622.8,237.5v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H622.8z M626.9,238.7h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5
															c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2
															c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V238.7z"/>
															<path id="txt174" class="lot-text" d="M634.6,257.5L634.6,257.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V257.5z M641.2,256.3v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H641.2z M644,263v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H644z M644,260.3v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H644z"/>
															<path id="txt173" class="lot-text" d="M650.6,223.8L650.6,223.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V223.8z M657.3,222.5v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H657.3z M658,228c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L658,228z"/>
															<path id="txt172" class="lot-text" d="M679.4,223.8L679.4,223.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V223.8z M686,222.5v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H686z M686.6,229.3v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9
															c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H686.6z"/>
															<path id="txt170" class="lot-text" d="M665.5,275.5L665.5,275.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V275.5z M672.2,274.2v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H672.2z M676.7,277.6c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5
															C676.3,274.1,676.7,275.9,676.7,277.6z M674.1,277.6c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3
															C674.3,275.3,674.1,276,674.1,277.6z"/>
															<path id="txt166" class="lot-text" d="M710.5,230L710.5,230l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V230z M716.7,229.7
		c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
		c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V229.7z M715.9,233.1
		c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C715.8,234.5,715.9,233.9,715.9,233.1z
		 M721.7,229.7c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1
		c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V229.7z
		 M720.9,233.1c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5
		C720.7,234.5,720.9,233.9,720.9,233.1z"/>
															<path id="txt167" class="lot-text" d="M715.9,303.3L715.9,303.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V303.3z M722.2,303
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V303z M721.4,306.5
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C721.2,307.9,721.4,307.3,721.4,306.5z
															M727,302v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3V302H727z"/>
															<path id="txt169" class="lot-text" d="M691.7,287.5L691.7,287.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V287.5z M698,287.2
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V287.2z M697.2,290.7
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C697,292.1,697.2,291.5,697.2,290.7z
															M699.5,291.9c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4
															c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9
															c-0.2,0-0.4,0-0.6,0V291.9z M700.3,288.4c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7
															c0-0.9-0.2-1.4-0.6-1.4C700.5,287.1,700.3,287.6,700.3,288.4z"/>
															<path id="txt165" class="lot-text" d="M743.3,216.4L743.3,216.4l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V216.4z M749.6,216.2
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V216.2z M748.8,219.6
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C748.6,221,748.8,220.4,748.8,219.6z
															M754.1,216.3h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4
															c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8
															V216.3z"/>
															<path id="txt164" class="lot-text" d="M759.3,238.5L759.3,238.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V238.5z M765.6,238.2
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V238.2z M764.8,241.7
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C764.6,243.1,764.8,242.5,764.8,241.7z
															M768.7,244v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H768.7z M768.7,241.3v-1.4c0-0.4,0-0.8,0.1-1.1h0
															c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H768.7z"/>
															<path id="txt163" class="lot-text" d="M757.6,262.7L757.6,262.7l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V262.7z M763.8,262.5
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V262.5z M763,265.9
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C762.9,267.3,763,266.7,763,265.9z
															M765,266.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9
															c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0
															c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L765,266.9z"/>
															<path id="txt162" class="lot-text" d="M759.3,282.5L759.3,282.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V282.5z M765.6,282.3
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V282.3z M764.8,285.7
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C764.6,287.1,764.8,286.5,764.8,285.7z
															M766.6,288v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4
															c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H766.6z"/>
															<path id="txt161" class="lot-text" d="M750.2,313.9L750.2,313.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V313.9z M756.5,313.6
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V313.6z M755.7,317.1
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C755.5,318.5,755.7,317.9,755.7,317.1z
															M759.1,313.9L759.1,313.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V313.9z"/>
															<path id="txt160" class="lot-text" d="M741,333.1L741,333.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8H741V333.1z M747.3,332.9
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V332.9z M746.5,336.3
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C746.3,337.7,746.5,337.1,746.5,336.3z
															M752.2,335.2c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C751.9,331.8,752.2,333.6,752.2,335.2z M749.6,335.2
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C749.8,332.9,749.6,333.7,749.6,335.2z"/>
															<path id="txt159" class="lot-text" d="M722.9,351.2L722.9,351.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V351.2z M729.2,351.1h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V351.1z M730.6,355.6
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V355.6z M731.4,352.1
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C731.7,350.8,731.4,351.3,731.4,352.1
															z"/>
															<path id="txt158" class="lot-text" d="M697.6,340L697.6,340l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V340z M703.9,339.9h-1.8l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V339.9z M704.8,343.7
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C705.6,345.5,704.8,344.8,704.8,343.7z M707.4,343.6c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C707.2,344.6,707.4,344.2,707.4,343.6z M706.2,340.3c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S706.2,339.9,706.2,340.3z"/>
															<path id="txt157" class="lot-text" d="M678.1,326.8L678.1,326.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V326.8z M684.4,326.7h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V326.7z M689.2,325.5v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H689.2z"/>
															<path id="txt156" class="lot-text" d="M660.2,316.4L660.2,316.4l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V316.4z M666.6,316.3h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V316.3z M670.9,316.1
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V316.1z M670.1,319.5
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C670,321,670.1,320.4,670.1,319.5z"/>
															<path id="txt155" class="lot-text" d="M641.2,304.3L641.2,304.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V304.3z M647.6,304.2h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V304.2z M652,304.2h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V304.2z"/>
															<path id="txt154" class="lot-text" d="M622.9,293.3L622.9,293.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V293.3z M629.3,293.2h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V293.2z M632.3,298.8v-1.6H630
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H632.3z M632.3,296.1v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H632.3z
															"/>
															<path id="txt153" class="lot-text" d="M599.6,278.7L599.6,278.7l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V278.7z M605.9,278.6h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V278.6z M607,282.9
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L607,282.9z"/>
															<path id="txt152" class="lot-text" d="M572.1,298.5L572.1,298.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V298.5z M578.4,298.4h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V298.4z M579.3,304v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H579.3z"/>
															<path id="txt151" class="lot-text" d="M556.3,313.5L556.3,313.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V313.5z M562.6,313.4h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V313.4z M565.1,313.5
															L565.1,313.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V313.5z"/>
															<path id="txt150" class="lot-text" d="M536.3,332.9L536.3,332.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V332.9z M542.6,332.8h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V332.8z M547.4,335
															c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C547.1,331.6,547.4,333.4,547.4,335z M544.9,335
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C545.1,332.7,544.9,333.5,544.9,335z"/>
															<path id="txt148" class="lot-text" d="M544.5,360L544.5,360l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V360z M549.5,365.4v-1.6h-2.3v-0.9
															l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H549.5z M549.5,362.7v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H549.5z
															M551.7,363.7c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C552.5,365.5,551.7,364.8,551.7,363.7z M554.4,363.6c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C554.1,364.6,554.4,364.2,554.4,363.6z M553.2,360.3c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S553.2,359.9,553.2,360.3z"/>
															<path id="txt147" class="lot-text" d="M545.3,384.7L545.3,384.7l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V384.7z M550.2,390.2v-1.6h-2.3
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H550.2z M550.2,387.5v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H550.2z
															M556.3,383.5v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H556.3z"/>
															<path id="txt146" class="lot-text" d="M544.7,408.8L544.7,408.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V408.8z M549.7,414.2v-1.6h-2.3
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H549.7z M549.7,411.5v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H549.7z
															M555.4,408.5c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1
															c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V408.5z
															M554.6,411.9c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5
															C554.4,413.3,554.6,412.7,554.6,411.9z"/>
															<path id="txt145" class="lot-text" d="M546.3,433.2L546.3,433.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V433.2z M551.3,438.7v-1.6H549
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H551.3z M551.3,436v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H551.3z
															M557.1,433.1h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4
															c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8
															V433.1z"/>
															<path id="txt144" class="lot-text" d="M545.5,459.5L545.5,459.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V459.5z M550.5,465v-1.6h-2.3
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H550.5z M550.5,462.3v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H550.5z
															M554.9,465v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H554.9z M554.9,462.3v-1.4c0-0.4,0-0.8,0.1-1.1h0
															c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H554.9z"/>
															<path id="txt143" class="lot-text" d="M547.3,485L547.3,485l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V485z M552.3,490.5v-1.6H550v-0.9
															l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H552.3z M552.3,487.8v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H552.3z
															M554.7,489.2c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9
															c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0
															c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L554.7,489.2z"/>
															<path id="txt142" class="lot-text" d="M556.5,506.9L556.5,506.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V506.9z M561.5,512.3v-1.6h-2.3
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H561.5z M561.5,509.6v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H561.5z
															M563.7,512.3v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4
															c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H563.7z"/>
															<path id="txt141" class="lot-text" d="M563.5,529.6L563.5,529.6l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V529.6z M568.5,535.1v-1.6h-2.3
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H568.5z M568.5,532.4V531c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H568.5z
															M572.4,529.6L572.4,529.6l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V529.6z"/>
															<path id="txt140" class="lot-text" d="M574.6,550.4L574.6,550.4l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V550.4z M579.6,555.8v-1.6h-2.3
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H579.6z M579.6,553.1v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H579.6z
															M585.7,552.4c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C585.4,549,585.7,550.8,585.7,552.4z M583.2,552.5
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C583.4,550.2,583.2,550.9,583.2,552.5z"/>
															<path id="txt138" class="lot-text" d="M627.6,604.4L627.6,604.4l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V604.4z M630.6,608.6
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L630.6,608.6z M634.8,608.2c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5
															c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C635.6,610,634.8,609.3,634.8,608.2z M637.4,608.1c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C637.2,609.1,637.4,608.6,637.4,608.1z M636.2,604.8c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S636.2,604.4,636.2,604.8z"/>
															<path id="txt137" class="lot-text" d="M654.6,610.9L654.6,610.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V610.9z M657.6,615.1
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L657.6,615.1z M665.7,609.7v0.9l-2.1,5.9h-1.3l2.1-5.5v0H662v-1.2H665.7z"/>
															<path id="txt136" class="lot-text" d="M679.3,624.3L679.3,624.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V624.3z M682.3,628.5
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L682.3,628.5z M689.9,624.1c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V624.1z M689.1,627.5c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C689,628.9,689.1,628.3,689.1,627.5z"/>
															<path id="txt135" class="lot-text" d="M698.7,642.3L698.7,642.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V642.3z M701.8,646.5
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L701.8,646.5z M709.5,642.2h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5
															c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2
															c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V642.2z"/>
															<path id="txt134" class="lot-text" d="M715.1,663.3L715.1,663.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V663.3z M718.1,667.5
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L718.1,667.5z M724.5,668.7v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H724.5z
															M724.5,666v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H724.5z"/>
															<path id="txt133" class="lot-text" d="M725.1,688.8L725.1,688.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V688.8z M728.2,693
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L728.2,693z M732.6,693c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2
															v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4
															c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L732.6,693z"/>
															<path id="txt132" class="lot-text" d="M729.7,721.3L729.7,721.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V721.3z M732.7,725.5
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L732.7,725.5z M736.9,726.7v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9
															c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H736.9z"/>
															<path id="txt130" class="lot-text" d="M755.4,801.1L755.4,801.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V801.1z M758.4,805.3
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H759v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L758.4,805.3z M766.6,803.2c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5
															C766.2,799.7,766.6,801.6,766.6,803.2z M764,803.2c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3
															C764.2,800.9,764,801.6,764,803.2z"/>
															<path id="txt129" class="lot-text" d="M780.3,796.8L780.3,796.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V796.8z M783.1,802.2v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H783.1z M788.1,801.2c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4
															c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8
															c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V801.2z M788.9,797.7c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C789.1,796.4,788.9,796.8,788.9,797.7z"/>
															<path id="txt128" class="lot-text" d="M801.5,814.7L801.5,814.7l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V814.7z M804.3,820.2v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2L806,819v0h2.1v1.2H804.3z M808.7,818.4c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5
															c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C809.5,820.3,808.7,819.5,808.7,818.4z M811.4,818.4c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C811.2,819.4,811.4,818.9,811.4,818.4z M810.2,815.1c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S810.2,814.7,810.2,815.1z"/>
															<path id="txt127" class="lot-text" d="M806.3,841.1L806.3,841.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V841.1z M809.1,846.6v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H809.1z M817.3,839.8v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H817.3z"/>
															<path id="txt125" class="lot-text" d="M759.3,835.5L759.3,835.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V835.5z M762.1,841v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H762.1z M770.1,835.4h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5
															c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2
															c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V835.4z"/>
															<path id="txt314" class="lot-text" d="M719.8,835.1c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L719.8,835.1z M725.7,831L725.7,831l-1.2,0.6
															l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V831z M730.7,836.4v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H730.7z M730.7,833.7v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H730.7z"/>
															<path id="txt315" class="lot-text" d="M638.3,879.5c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L638.3,879.5z M644.1,875.3L644.1,875.3
															l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V875.3z M650.5,875.2h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5
															c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2
															c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V875.2z"/>
															<path id="txt316" class="lot-text" d="M628.5,910.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L628.5,910.9z M634.4,906.7L634.4,906.7
															l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V906.7z M640.6,906.4c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V906.4z M639.8,909.8c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C639.7,911.2,639.8,910.6,639.8,909.8z"/>
															<path id="txt317" class="lot-text" d="M562.3,953.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L562.3,953.9z M568.2,949.7L568.2,949.7
															l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V949.7z M574.8,948.4v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H574.8z"/>
															<path id="txt318" class="lot-text" d="M536.8,960c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L536.8,960z M542.6,955.8L542.6,955.8
															l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V955.8z M545.4,959.5c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C546.2,961.3,545.4,960.6,545.4,959.5z M548.1,959.4
															c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C547.9,960.4,548.1,960,548.1,959.4z M546.9,956.2
															c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S546.9,955.7,546.9,956.2z"/>
															<path id="txt319" class="lot-text" d="M512.6,960c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L512.6,960z M518.4,955.8L518.4,955.8
															l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V955.8z M521.7,960.2c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4
															c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8
															c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V960.2z M522.6,956.7c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C522.8,955.4,522.6,955.9,522.6,956.7z"/>
															<path id="txt320" class="lot-text" d="M488.3,960c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L488.3,960z M492.6,961.3v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H492.6z M500.9,957.8c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5
															C500.6,954.4,500.9,956.2,500.9,957.8z M498.4,957.9c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3
															C498.6,955.6,498.3,956.3,498.4,957.9z"/>
															<path id="txt321" class="lot-text" d="M467.4,960c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L467.4,960z M471.6,961.3v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H471.6z M477.6,955.8L477.6,955.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V955.8z"/>
															<path id="txt322" class="lot-text" d="M443.8,960c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L443.8,960z M448,961.3v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H448z M452.4,961.3v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9
															c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H452.4z"/>
															<path id="txt438" class="lot-text" d="M418.7,998.9v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H418.7z M418.7,996.2v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H418.7z M421.2,997.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1
															c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1
															c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2
															c-0.6,0-1-0.1-1.3-0.3L421.2,997.6z M425.3,997.1c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C426.2,999,425.3,998.2,425.3,997.1z M428,997.1
															c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C427.8,998.1,428,997.6,428,997.1z M426.8,993.8
															c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S426.8,993.4,426.8,993.8z"/>
															<path id="txt323" class="lot-text" d="M297.5,918.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L297.5,918.6z M301.8,919.9V919l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H301.8z M306.4,918.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H307
															v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4
															c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L306.4,918.6z"/>
															<path id="txt324" class="lot-text" d="M297.5,895.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L297.5,895.9z M301.8,897.2v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H301.8z M308.4,897.2v-1.6H306v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H308.4z
															M308.4,894.5v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H308.4z"/>
															<path id="txt326" class="lot-text" d="M253.6,918.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L253.6,918.6z M257.8,919.9V919l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H257.8z M265.7,914.2c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V914.2z M264.9,917.6c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C264.7,919,264.9,918.4,264.9,917.6z"/>
															<path id="txt327" class="lot-text" d="M215.5,918.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L215.5,918.6z M219.8,919.9V919l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H219.8z M228,913.2v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H228z"/>
															<path id="txt328" class="lot-text" d="M195.4,918.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H196v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L195.4,918.6z M199.6,919.9V919l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H199.6z M204,918.2c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C204.8,920,204,919.3,204,918.2z M206.7,918.1
															c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C206.5,919.1,206.7,918.7,206.7,918.1z M205.5,914.8
															c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S205.5,914.4,205.5,914.8z"/>
															<path id="txt329" class="lot-text" d="M171.8,918.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L171.8,918.6z M176.1,919.9V919l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H176.1z M181,918.9c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4
															c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8
															c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V918.9z M181.8,915.4c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C182.1,914.1,181.8,914.5,181.8,915.4z"/>
															<path id="txt330" class="lot-text" d="M139.8,908.3c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L139.8,908.3z M144.2,908.3
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L144.2,908.3z M152.4,906.1c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5
															C152,902.7,152.4,904.5,152.4,906.1z M149.8,906.2c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3
															C150,903.9,149.8,904.6,149.8,906.2z"/>
															<path id="txt331" class="lot-text" d="M142,886.8c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L142,886.8z M146.4,886.8
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H147v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L146.4,886.8z M152.2,882.7L152.2,882.7l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V882.7z"/>
															<path id="txt332" class="lot-text" d="M156,864.3c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L156,864.3z M160.5,864.3
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H161v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L160.5,864.3z M164.7,865.6v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9
															c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H164.7z"/>
															<path id="txt333" class="lot-text" d="M132.5,850.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H133v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L132.5,850.9z M136.9,850.9
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L136.9,850.9z M141.3,850.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1
															h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4
															c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L141.3,850.9z"/>
															<path id="txt334" class="lot-text" d="M101.5,850.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L101.5,850.9z M105.9,850.9
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L105.9,850.9z M112.4,852.2v-1.6H110v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H112.4z
															M112.4,849.5v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H112.4z"/>
															<path id="txt335" class="lot-text" d="M96.3,874.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1H97
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L96.3,874.9z M100.8,874.9
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L100.8,874.9z M108.5,870.7h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5
															c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2
															c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V870.7z"/>
															<path id="txt336" class="lot-text" d="M94,896.1c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L94,896.1z M98.4,896.1
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L98.4,896.1z M106,891.7c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V891.7z M105.2,895.1c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C105.1,896.5,105.2,895.9,105.2,895.1z"/>
															<path id="txt337" class="lot-text" d="M91.6,918.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L91.6,918.6z M96,918.6
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L96,918.6z M104.1,913.2v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H104.1z"/>
															<path id="txt1" class="lot-text" d="M84.3,955.3L84.3,955.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V955.3z"/>
															<path id="txt2" class="lot-text" d="M115.6,960.8v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H115.6z"/>
															<path id="txt3" class="lot-text" d="M145.4,954.3c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H146v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L145.4,954.3z"/>
															<path id="txt4" class="lot-text" d="M175.1,955.6V954h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H175.1z M175.1,952.9v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H175.1z"/>
															<path id="txt5" class="lot-text" d="M203.5,950.1h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V950.1z"/>
															<path id="txt6" class="lot-text" d="M230.5,949.9c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V949.9z M229.7,953.3c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C229.5,954.7,229.7,954.1,229.7,953.3z"/>
															<path id="txt7" class="lot-text" d="M256.2,948.9v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H256.2z"/>
															<path id="txt8" class="lot-text" d="M278.3,953.9c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C279.1,955.7,278.3,955,278.3,953.9z M280.9,953.8
															c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C280.7,954.8,280.9,954.4,280.9,953.8z M279.7,950.5
															c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S279.7,950.1,279.7,950.5z"/>
															<path id="txt9" class="lot-text" d="M303.3,954.6c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0
															c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3
															c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V954.6z M304.1,951c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4
															c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C304.3,949.8,304.1,950.2,304.1,951z"/>
															<path id="txt11" class="lot-text" d="M643.6,951.2L643.6,951.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V951.2z M648,951.2L648,951.2
															l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8H648V951.2z"/>
															<path id="txt12" class="lot-text" d="M656.6,970.3L656.6,970.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V970.3z M659.4,975.8v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H659.4z"/>
															<path id="txt13" class="lot-text" d="M676.7,984.3L676.7,984.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V984.3z M679.7,988.5
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L679.7,988.5z"/>
															<path id="txt14" class="lot-text" d="M695.9,997.8L695.9,997.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V997.8z M700.9,1003.2v-1.6h-2.3
															v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H700.9z M700.9,1000.5v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H700.9
															z"/>
															<path id="txt15" class="lot-text" d="M710.2,1020.7L710.2,1020.7l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1020.7z M716.6,1020.6h-1.8
															l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1020.6z"/>
															<path id="txt16" class="lot-text" d="M711.9,1047.3L711.9,1047.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1047.3z M718.2,1047
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V1047z M717.4,1050.5
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5
															C717.2,1051.9,717.4,1051.3,717.4,1050.5z"/>
															<path id="txt17" class="lot-text" d="M703.5,1069.6L703.5,1069.6l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1069.6z M710.2,1068.3v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H710.2z"/>
															<path id="txt18" class="lot-text" d="M695.3,1092.1L695.3,1092.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1092.1z M698.1,1095.8
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C698.9,1097.6,698.1,1096.9,698.1,1095.8z M700.7,1095.7c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C700.5,1096.7,700.7,1096.3,700.7,1095.7z M699.5,1092.4c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S699.5,1092,699.5,1092.4z"/>
															<path id="txt19" class="lot-text" d="M697,1119.3L697,1119.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8H697V1119.3z M700.3,1123.8
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1123.8z
															M701.2,1120.2c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4
															C701.4,1119,701.2,1119.4,701.2,1120.2z"/>
															<path id="txt20" class="lot-text" d="M679.2,1144.5v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H679.2z M687.5,1141.1
															c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C687.2,1137.7,687.5,1139.5,687.5,1141.1z M685,1141.1
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C685.2,1138.8,685,1139.6,685,1141.1z"/>
															<path id="txt21" class="lot-text" d="M671.4,1167v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H671.4z M677.4,1161.5L677.4,1161.5
															l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1161.5z"/>
															<path id="txt22" class="lot-text" d="M668.1,1188.4v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H668.1z M672.6,1188.4v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H672.6z"/>
															<path id="txt23" class="lot-text" d="M663.6,1209.8v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H663.6z M668.2,1208.5
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L668.2,1208.5z"/>
															<path id="txt24" class="lot-text" d="M658.8,1232.1v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H658.8z M665.4,1232.1v-1.6h-2.3v-0.9
															l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H665.4z M665.4,1229.4v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H665.4z"/>
															<path id="txt25" class="lot-text" d="M650,1253.8v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H650z M657.9,1248.2h-1.8l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1248.2z"/>
															<path id="txt26" class="lot-text" d="M641.8,1276.7v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H641.8z M649.6,1270.9
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V1270.9z M648.8,1274.4
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5
															C648.7,1275.8,648.8,1275.2,648.8,1274.4z"/>
															<path id="txt28" class="lot-text" d="M586.8,1292.7v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H586.8z M591.2,1290.9
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C592,1292.8,591.2,1292,591.2,1290.9z M593.9,1290.9c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C593.6,1291.9,593.9,1291.4,593.9,1290.9z M592.6,1287.6c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S592.6,1287.2,592.6,1287.6z"/>
															<path id="txt29" class="lot-text" d="M564.3,1285.8v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H564.3z M569.2,1284.7
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1284.7z
															M570.1,1281.2c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4
															C570.3,1279.9,570.1,1280.4,570.1,1281.2z"/>
															<path id="txt30" class="lot-text" d="M541.3,1278.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L541.3,1278.9z M549.5,1276.7
															c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C549.2,1273.3,549.5,1275.1,549.5,1276.7z M546.9,1276.8
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C547.1,1274.5,546.9,1275.2,546.9,1276.8z"/>
															<path id="txt32" class="lot-text" d="M513.2,1275c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L513.2,1275z M517.5,1276.3v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H517.5z"/>
															<path id="txt33" class="lot-text" d="M486.2,1276.7c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L486.2,1276.7z M490.6,1276.7
															c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8
															c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7
															c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L490.6,1276.7z"/>
															<path id="txt34" class="lot-text" d="M462,1275.4c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L462,1275.4z M468.4,1276.7v-1.6H466v-0.9
															l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H468.4z M468.4,1274v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H468.4z"/>
															<path id="txt35" class="lot-text" d="M439.2,1271.1c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L439.2,1271.1z M447,1266.8h-1.8l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1266.8z"/>
															<path id="txt36" class="lot-text" d="M417.4,1265.4c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H418v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L417.4,1265.4z M425.1,1261
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V1261z M424.2,1264.4
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5
															C424.1,1265.8,424.2,1265.2,424.2,1264.4z"/>
															<path id="txt38" class="lot-text" d="M372.4,1263.1c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H373v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L372.4,1263.1z M376.6,1262.6
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C377.4,1264.4,376.6,1263.7,376.6,1262.6z M379.3,1262.5c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C379,1263.5,379.3,1263.1,379.3,1262.5z M378,1259.2c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S378,1258.8,378,1259.2z"/>
															<path id="txt37" class="lot-text" d="M395.3,1261.1c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L395.3,1261.1z M403.4,1255.7v0.9l-2.1,5.9
															h-1.3l2.1-5.5v0h-2.3v-1.2H403.4z"/>
															<path id="txt39" class="lot-text" d="M351.4,1270c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H352v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L351.4,1270z M356.1,1270.2
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1270.2z M357,1266.7
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C357.2,1265.4,357,1265.9,357,1266.7z
															"/>
															<path id="txt41" class="lot-text" d="M353.4,1315.8v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H353.4z M353.4,1313.1v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H353.4z M357.2,1310.4L357.2,1310.4l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8
															h-1.4V1310.4z"/>
															<path id="txt42" class="lot-text" d="M356.6,1339.9v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H356.6z M356.6,1337.2v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H356.6z M358.9,1339.9v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3
															c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0
															h2.1v1.2H358.9z"/>
															<path id="txt43" class="lot-text" d="M354.5,1362.6v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H354.5z M354.5,1359.9v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H354.5z M356.9,1361.3c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1
															c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1
															c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2
															c-0.6,0-1-0.1-1.3-0.3L356.9,1361.3z"/>
															<path id="txt44" class="lot-text" d="M372.9,1375.1v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H372.9z M372.9,1372.4v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H372.9z M377.3,1375.1v-1.6H375v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6
															H377.3z M377.3,1372.4v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H377.3z"/>
															<path id="txt45" class="lot-text" d="M396.2,1367.8v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H396.2z M396.2,1365.1v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H396.2z M402,1362.2h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0
															c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2
															c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1362.2z"/>
															<path id="txt46" class="lot-text" d="M394.1,1342.9v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H394.1z M394.1,1340.2v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H394.1z M399.7,1337.1c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V1337.1z M398.9,1340.6c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C398.8,1342,398.9,1341.4,398.9,1340.6z"/>
															<path id="txt40" class="lot-text" d="M384.8,1295.1v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1H386v1.6H384.8z M384.8,1292.4v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H384.8z M390.9,1291.7c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4
															c0-1.9,0.6-3.5,2-3.5C390.6,1288.2,390.9,1290.1,390.9,1291.7z M388.4,1291.7c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3
															c0-1.4-0.1-2.3-0.6-2.3C388.6,1289.4,388.3,1290.1,388.4,1291.7z"/>
															<path id="txt47" class="lot-text" d="M410.7,1298.5v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H410.7z M410.7,1295.8v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H410.7z M416.8,1291.7v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H416.8z"/>
															<path id="txt48" class="lot-text" d="M431.7,1304.8v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H431.7z M431.7,1302.1v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H431.7z M433.9,1303.1c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5
															c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C434.7,1304.9,433.9,1304.2,433.9,1303.1z M436.6,1303c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C436.4,1304,436.6,1303.5,436.6,1303z M435.4,1299.7c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S435.4,1299.3,435.4,1299.7z"/>
															<path id="txt49" class="lot-text" d="M454,1309.1v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H454z M454,1306.4v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H454z M456.7,1308.1c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4
															c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8
															c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1308.1z M457.6,1304.6c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C457.8,1303.3,457.6,1303.7,457.6,1304.6z"/>
															<path id="txt50" class="lot-text" d="M479.1,1306.4h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1306.4z M484,1308.5c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5
															C483.6,1305.1,484,1306.9,484,1308.5z M481.4,1308.6c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3
															C481.6,1306.3,481.4,1307,481.4,1308.6z"/>
															<path id="txt51" class="lot-text" d="M506.2,1302.7h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1302.7z M508.7,1302.8L508.7,1302.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1302.8z"/>
															<path id="txt53" class="lot-text" d="M535.4,1304.2h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1304.2z M536.5,1308.5c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1H537v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L536.5,1308.5z"/>
															<path id="txt54" class="lot-text" d="M557.9,1312.4h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1312.4z M561,1318v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H561z M561,1315.3v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H561z"/>
															<path id="txt55" class="lot-text" d="M579.5,1318.5h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1318.5z M584,1318.5h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5
															c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2
															c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1318.5z"/>
															<path id="txt56" class="lot-text" d="M602,1322.4h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1322.4z M606.3,1322.2c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1322.2z M605.5,1325.6c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C605.4,1327.1,605.5,1326.5,605.5,1325.6z"/>
															<path id="txt57" class="lot-text" d="M624.5,1319.4h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1319.4z M629.2,1318.2v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H629.2z"/>
															<path id="txt58" class="lot-text" d="M645.1,1310.5h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1310.5z M645.9,1314.3c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C646.7,1316.1,645.9,1315.4,645.9,1314.3z
															M648.6,1314.2c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C648.4,1315.2,648.6,1314.8,648.6,1314.2z
															M647.4,1311c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S647.4,1310.5,647.4,1311z"/>
															<path id="txt59" class="lot-text" d="M662.4,1296.2h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1296.2z M663.8,1300.7c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0
															c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3
															c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1300.7z M664.6,1297.2c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4
															c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C664.9,1295.9,664.6,1296.4,664.6,1297.2z"/>
															<path id="txt60" class="lot-text" d="M674.8,1277.9c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1277.9z M674,1281.3c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C673.9,1282.7,674,1282.1,674,1281.3z M679.8,1280.2c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4
															c0-1.9,0.6-3.5,2-3.5C679.4,1276.8,679.8,1278.6,679.8,1280.2z M677.2,1280.2c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3
															c0-1.4-0.1-2.3-0.6-2.3C677.4,1277.9,677.2,1278.7,677.2,1280.2z"/>
															<path id="txt61" class="lot-text" d="M685.4,1259.3c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1259.3z M684.6,1262.7c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C684.5,1264.1,684.6,1263.5,684.6,1262.7z M688,1259.5L688,1259.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8H688
															V1259.5z"/>
															<path id="txt62" class="lot-text" d="M690.6,1238.5c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1238.5z M689.8,1241.9c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C689.7,1243.3,689.8,1242.7,689.8,1241.9z M691.6,1244.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3
															c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0
															h2.1v1.2H691.6z"/>
															<path id="txt63" class="lot-text" d="M695.4,1217.5c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1217.5z M694.6,1220.9c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C694.4,1222.3,694.6,1221.7,694.6,1220.9z M696.6,1222c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1
															c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1
															c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2
															c-0.6,0-1-0.1-1.3-0.3L696.6,1222z"/>
															<path id="txt64" class="lot-text" d="M699.5,1195.2c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1195.2z M698.7,1198.6c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C698.5,1200.1,698.7,1199.5,698.7,1198.6z M702.7,1201v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6
															H702.7z M702.7,1198.3v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H702.7z"/>
															<path id="txt66" class="lot-text" d="M716.8,1148.9c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1148.9z M716,1152.3c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C715.8,1153.8,716,1153.2,716,1152.3z M721.2,1148.9c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V1148.9z M720.4,1152.3c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C720.3,1153.8,720.4,1153.2,720.4,1152.3z"/>
															<path id="txt65" class="lot-text" d="M703.4,1171.9c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1171.9z M702.6,1175.3c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C702.4,1176.7,702.6,1176.1,702.6,1175.3z M707.9,1172h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0
															c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2
															c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1172z"/>
															<path id="txt67" class="lot-text" d="M744.1,1139.4c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1139.4z M743.2,1142.8c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C743.1,1144.2,743.2,1143.6,743.2,1142.8z M748.9,1138.4v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H748.9z"/>
															<path id="txt68" class="lot-text" d="M766.1,1140.3c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1140.3z M765.3,1143.7c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C765.2,1145.1,765.3,1144.5,765.3,1143.7z M767.1,1144.3c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5
															c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C767.9,1146.1,767.1,1145.4,767.1,1144.3z M769.7,1144.2c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C769.5,1145.2,769.7,1144.7,769.7,1144.2z M768.5,1140.9c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S768.5,1140.5,768.5,1140.9z"/>
															<path id="txt69" class="lot-text" d="M789.3,1142.9c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V1142.9z M788.5,1146.3c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C788.3,1147.7,788.5,1147.1,788.5,1146.3z M790.8,1147.6c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4
															c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8
															c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1147.6z M791.6,1144c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C791.8,1142.8,791.6,1143.2,791.6,1144z"/>
															<path id="txt70" class="lot-text" d="M815.4,1142.7v0.9l-2.1,5.9H812l2.1-5.5v0h-2.3v-1.2H815.4z M819.9,1146.1c0,2.4-0.9,3.5-2,3.5
															c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C819.6,1142.6,819.9,1144.5,819.9,1146.1z M817.4,1146.1c0,1.6,0.2,2.3,0.6,2.3
															c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C817.6,1143.8,817.4,1144.5,817.4,1146.1z"/>
															<path id="txt71" class="lot-text" d="M810.9,1187.1v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H810.9z M813.1,1188.3L813.1,1188.3l-1.2,0.6
															l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1188.3z"/>
															<path id="txt72" class="lot-text" d="M812.2,1212v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H812.2z M812.8,1218.7v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H812.8z"/>
															<path id="txt73" class="lot-text" d="M811.5,1236.6v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H811.5z M812.3,1242.1c0.3,0.2,0.6,0.3,1,0.3
															c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1
															c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2
															c-0.6,0-1-0.1-1.3-0.3L812.3,1242.1z"/>
															<path id="txt75" class="lot-text" d="M824.7,1258.7v0.9l-2.1,5.9h-1.3l2.1-5.5v0H821v-1.2H824.7z M828.8,1259.9h-1.8l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1259.9z"/>
															<path id="txt76" class="lot-text" d="M843.5,1254.6v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H843.5z M847.6,1255.6c-0.2,0-0.4,0-0.6,0.1
															c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8
															c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V1255.6z M846.8,1259c0-0.6-0.2-1.1-0.6-1.1
															c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C846.6,1260.4,846.8,1259.8,846.8,1259z"/>
															<path id="txt77" class="lot-text" d="M859.1,1242.5v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H859.1z M863.5,1242.5v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0h-2.3v-1.2H863.5z"/>
															<path id="txt78" class="lot-text" d="M851.8,1223v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H851.8z M852.3,1228c0-0.8,0.5-1.4,1-1.8v0
															c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C853.1,1229.8,852.3,1229.1,852.3,1228z M855,1227.9c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C854.8,1228.9,855,1228.5,855,1227.9z M853.8,1224.6c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S853.8,1224.2,853.8,1224.6z"/>
															<path id="txt79" class="lot-text" d="M846.6,1204.4v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H846.6z M847.7,1210.1c0.3,0,0.4,0,0.6-0.1
															c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4
															c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1210.1z M848.5,1206.5
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4
															C848.7,1205.3,848.5,1205.7,848.5,1206.5z"/>
															<path id="txt80" class="lot-text" d="M839.7,1189.5c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C840.5,1191.3,839.7,1190.6,839.7,1189.5z
															M842.3,1189.4c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C842.1,1190.4,842.3,1190,842.3,1189.4z
															M841.1,1186.1c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S841.1,1185.7,841.1,1186.1z M848.1,1187.8
															c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C847.7,1184.4,848.1,1186.2,848.1,1187.8z M845.5,1187.9
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C845.7,1185.6,845.5,1186.3,845.5,1187.9z"/>
															<path id="txt81" class="lot-text" d="M843.1,1170.9c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C843.9,1172.7,843.1,1172,843.1,1170.9z
															M845.8,1170.8c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C845.6,1171.8,845.8,1171.4,845.8,1170.8z
															M844.6,1167.5c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S844.6,1167.1,844.6,1167.5z M849.2,1167.1
															L849.2,1167.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1167.1z"/>
															<path id="txt82" class="lot-text" d="M844.4,1150.3c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8
															c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C845.2,1152.2,844.4,1151.4,844.4,1150.3z
															M847.1,1150.2c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C846.9,1151.2,847.1,1150.8,847.1,1150.2z
															M845.9,1147c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S845.9,1146.6,845.9,1147z M848.9,1152.1v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H848.9z"/>
															<path id="txt115" class="lot-text" d="M814.5,1112.8L814.5,1112.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1112.8z M818.9,1112.8
															L818.9,1112.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1112.8z M825.3,1112.8h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0
															c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2
															c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V1112.8z"/>
															<path id="txt116" class="lot-text" d="M792.9,1111.5L792.9,1111.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1111.5z M797.3,1111.5
															L797.3,1111.5l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1111.5z M803.5,1111.3c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V1111.3z M802.7,1114.7c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C802.6,1116.1,802.7,1115.5,802.7,1114.7z"/>
															<path id="txt117" class="lot-text" d="M771.2,1110.2L771.2,1110.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1110.2z M775.6,1110.2
															L775.6,1110.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1110.2z M782.3,1109v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H782.3z"/>
															<path id="txt118" class="lot-text" d="M727.3,1107.2L727.3,1107.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1107.2z M731.7,1107.2
															L731.7,1107.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1107.2z M734.5,1110.9c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5
															c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C735.3,1112.8,734.5,1112,734.5,1110.9z M737.2,1110.9c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C736.9,1111.9,737.2,1111.4,737.2,1110.9z M736,1107.6c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S736,1107.2,736,1107.6z"/>
															<path id="txt119" class="lot-text" d="M730.8,1084.1L730.8,1084.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1084.1z M735.2,1084.1
															L735.2,1084.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1084.1z M738.5,1088.5c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4
															c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8
															c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1088.5z M739.3,1085c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C739.6,1083.7,739.3,1084.1,739.3,1085z"/>
															<path id="txt120" class="lot-text" d="M741.8,1058.3L741.8,1058.3l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1058.3z M744.6,1063.8v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H744.6z M753,1060.4c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5
															C752.6,1057,753,1058.8,753,1060.4z M750.4,1060.4c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3
															C750.6,1058.1,750.4,1058.9,750.4,1060.4z"/>
															<path id="txt121" class="lot-text" d="M744.2,1036.9L744.2,1036.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1036.9z M747,1042.4v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H747z M753,1036.9L753,1036.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8H753V1036.9z"/>
															<path id="txt122" class="lot-text" d="M741.1,1013.1L741.1,1013.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V1013.1z M744,1018.6v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H744z M748.4,1018.6v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9
															c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H748.4z"/>
															<path id="txt123" class="lot-text" d="M730.8,990.8L730.8,990.8l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V990.8z M733.6,996.3v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H733.6z M738.2,995c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2
															v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4
															c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L738.2,995z"/>
															<path id="txt124" class="lot-text" d="M712.8,970.9L712.8,970.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V970.9z M715.6,976.4v-0.9
															l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H715.6z M722.2,976.4v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H722.2z
															M722.2,973.7v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H722.2z"/>
															<path id="txt27" class="lot-text" d="M616.9,1290.7v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H616.9z M625.1,1284v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0h-2.3v-1.2H625.1z"/>
															<path id="txt325" class="lot-text" d="M253.6,895.9c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L253.6,895.9z M257.8,897.2v-0.9l0.7-0.9
															c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2
															c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H257.8z M265.8,891.6H264l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5
															c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2
															c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V891.6z"/>
															<path id="txt439" class="lot-text" d="M453.3,1011.9v-1.6H451v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H453.3z M453.3,1009.2v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H453.3z M455.8,1010.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1
															c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1
															c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2
															c-0.6,0-1-0.1-1.3-0.3L455.8,1010.6z M460.5,1010.8c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0
															c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3
															c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V1010.8z M461.3,1007.3c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4
															c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C461.6,1006,461.3,1006.5,461.3,1007.3z"/>
															<path id="txt440" class="lot-text" d="M484.3,998.9v-1.6H482v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H484.3z M484.3,996.2v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H484.3z M488.7,998.9v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6
															H488.7z M488.7,996.2v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H488.7z M494.9,995.5c0,2.4-0.9,3.5-2,3.5
															c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C494.5,992.1,494.9,993.9,494.9,995.5z M492.3,995.5c0,1.6,0.2,2.3,0.6,2.3
															c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C492.5,993.2,492.3,994,492.3,995.5z"/>
															<path id="txt441" class="lot-text" d="M515,998.9v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H515z M515,996.2v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H515z M519.4,998.9v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6
															H519.4z M519.4,996.2v-1.4c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H519.4z M523.3,993.4L523.3,993.4l-1.2,0.6
															l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V993.4z"/>
															<path id="txt300" class="lot-text" d="M693.9,755.3c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L693.9,755.3z M702,753.2
															c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C701.7,749.8,702,751.6,702,753.2z M699.5,753.2
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C699.7,750.9,699.4,751.7,699.5,753.2z M706.5,753.2
															c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C706.1,749.8,706.5,751.6,706.5,753.2z M703.9,753.2
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C704.1,750.9,703.9,751.7,703.9,753.2z"/>
															<path id="txt299" class="lot-text" d="M689.8,731.5v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H689.8z M694.7,730.5
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V730.5z M695.5,726.9
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C695.8,725.7,695.5,726.1,695.5,726.9
															z M699.1,730.5c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4
															c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9
															c-0.2,0-0.4,0-0.6,0V730.5z M700,726.9c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7
															c0-0.9-0.2-1.4-0.6-1.4C700.2,725.7,700,726.1,700,726.9z"/>
															<path id="txt298" class="lot-text" d="M688.5,704.5v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H688.5z M693.4,703.4
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V703.4z M694.2,699.9
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C694.5,698.6,694.2,699.1,694.2,699.9
															z M697.3,702.7c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C698.1,704.5,697.3,703.8,697.3,702.7z M700,702.6c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C699.7,703.6,700,703.2,700,702.6z M698.8,699.4c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S698.8,698.9,698.8,699.4z"/>
															<path id="txt297" class="lot-text" d="M680.7,678.1v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H680.7z M685.6,677c0.3,0,0.4,0,0.6-0.1
															c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4
															c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V677z M686.5,673.5c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C686.7,672.2,686.4,672.7,686.5,673.5z M693.4,671.3v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H693.4z"/>
															<path id="txt296" class="lot-text" d="M660.4,653.6v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H660.4z M665.3,652.6
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V652.6z M666.1,649
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C666.4,647.8,666.1,648.2,666.1,649z
															M672.6,647.9c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1
															c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V647.9z
															M671.8,651.3c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5
															C671.7,652.7,671.8,652.1,671.8,651.3z"/>
															<path id="txt295" class="lot-text" d="M632.7,640.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H632.7z M637.6,639.2
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V639.2z M638.4,635.6
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C638.7,634.4,638.4,634.8,638.4,635.6
															z M645.1,634.6h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4
															c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8
															V634.6z"/>
															<path id="txt291" class="lot-text" d="M558.2,663.6v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H558.2z M563.2,662.5
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V662.5z M564,659
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C564.3,657.7,564,658.2,564,659z
															M568.7,658.1L568.7,658.1l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V658.1z"/>
															<path id="txt290" class="lot-text" d="M546.3,687.6v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H546.3z M551.3,686.5
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V686.5z M552.1,683
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C552.4,681.7,552.1,682.2,552.1,683z
															M559.1,684.2c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C558.8,680.7,559.1,682.6,559.1,684.2z M556.6,684.2
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C556.8,681.9,556.5,682.6,556.6,684.2z"/>
															<path id="txt289" class="lot-text" d="M548.5,717.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H548.5z M552.9,715.5
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C553.7,717.3,552.9,716.6,552.9,715.5z M555.6,715.4c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C555.3,716.4,555.6,716,555.6,715.4z M554.4,712.1c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S554.4,711.7,554.4,712.1z M557.9,716.2c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4
															c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8
															c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V716.2z M558.7,712.6c0,0.7,0.2,1.2,0.6,1.2
															c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C558.9,711.4,558.7,711.8,558.7,712.6z"/>
															<path id="txt288" class="lot-text" d="M549.6,743.8v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H549.6z M554,742.1c0-0.8,0.5-1.4,1-1.8
															v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C554.8,743.9,554,743.2,554,742.1z M556.7,742c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C556.4,743,556.7,742.6,556.7,742z M555.4,738.7c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S555.4,738.3,555.4,738.7z M558.4,742.1c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7
															c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C559.2,743.9,558.4,743.2,558.4,742.1z M561.1,742c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C560.8,743,561.1,742.6,561.1,742z M559.9,738.7c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S559.9,738.3,559.9,738.7z"/>
															<path id="txt287" class="lot-text" d="M551.3,768.7v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H551.3z M555.7,767c0-0.8,0.5-1.4,1-1.8
															v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C556.5,768.8,555.7,768.1,555.7,767z M558.4,766.9c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C558.2,767.9,558.4,767.4,558.4,766.9z M557.2,763.6c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S557.2,763.2,557.2,763.6z M564,762v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3V762H564z"/>
															<path id="txt286" class="lot-text" d="M551.1,796.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H551.1z M555.5,794.4
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C556.3,796.3,555.5,795.5,555.5,794.4z M558.2,794.3c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C557.9,795.4,558.2,794.9,558.2,794.3z M557,791.1c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S557,790.7,557,791.1z M563.4,790.5c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0
															c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1
															c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V790.5z M562.6,793.9c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5
															c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C562.4,795.3,562.6,794.7,562.6,793.9z"/>
															<path id="txt285" class="lot-text" d="M533.6,831v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H533.6z M538,829.3c0-0.8,0.5-1.4,1-1.8
															v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C538.8,831.1,538,830.4,538,829.3z M540.6,829.2c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C540.4,830.2,540.6,829.8,540.6,829.2z M539.4,825.9c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S539.4,825.5,539.4,825.9z M546,825.4h-1.8l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5
															c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2
															c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V825.4z"/>
															<path id="txt284" class="lot-text" d="M502.6,831v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H502.6z M507,829.3c0-0.8,0.5-1.4,1-1.8
															v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C507.8,831.1,507,830.4,507,829.3z M509.7,829.2c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C509.5,830.2,509.7,829.8,509.7,829.2z M508.5,825.9c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S508.5,825.5,508.5,825.9z M513.7,831v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H513.7z M513.7,828.3v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H513.7z"/>
															<path id="txt283" class="lot-text" d="M475.6,831v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H475.6z M480,829.3c0-0.8,0.5-1.4,1-1.8
															v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2
															C480.8,831.1,480,830.4,480,829.3z M482.7,829.2c0-0.6-0.4-1-0.8-1.2c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1
															C482.4,830.2,482.7,829.8,482.7,829.2z M481.4,825.9c0,0.5,0.3,0.8,0.7,1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
															S481.4,825.5,481.4,825.9z M484.6,829.7c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L484.6,829.7z"/>
															<path id="txt279" class="lot-text" d="M448.1,831.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H448.1z M456.4,824.5v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0h-2.3v-1.2H456.4z M457.5,830.2c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0
															c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3
															c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V830.2z M458.3,826.7c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4
															c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C458.6,825.4,458.3,825.8,458.3,826.7z"/>
															<path id="txt277" class="lot-text" d="M441.9,804v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H441.9z M450.1,797.2v0.9L448,804h-1.3
															l2.1-5.5v0h-2.3v-1.2H450.1z M454.5,797.2v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H454.5z"/>
															<path id="txt276" class="lot-text" d="M460.7,776.5v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H460.7z M468.9,769.7v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0h-2.3v-1.2H468.9z M473,770.8c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5
															c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9
															c0.3-0.1,0.5-0.1,0.7-0.1V770.8z M472.1,774.2c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
															c0,0.9,0.2,1.5,0.7,1.5C472,775.6,472.1,775,472.1,774.2z"/>
															<path id="txt274" class="lot-text" d="M510.4,791.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H510.4z M518.7,784.5v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0H515v-1.2H518.7z M521.5,791.2v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H521.5z M521.5,788.5v-1.4
															c0-0.4,0-0.8,0.1-1.1h0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H521.5z"/>
															<path id="txt273" class="lot-text" d="M510.2,764.4v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H510.2z M518.5,757.6v0.9l-2.1,5.9H515
															l2.1-5.5v0h-2.3v-1.2H518.5z M519.3,763.1c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1
															c0.5,0,1.1-0.2,1.1-0.9c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6
															c0,0.7-0.4,1.2-1,1.5v0c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L519.3,763.1z"/>
															<path id="txt272" class="lot-text" d="M510.6,741.5v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H510.6z M518.9,734.7v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0h-2.3v-1.2H518.9z M519.5,741.5v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1
															c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H519.5z"/>
															<path id="txt271" class="lot-text" d="M507.8,719.4v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															L508,713c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H507.8z M516.1,712.6v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0h-2.3v-1.2H516.1z M518.3,713.9L518.3,713.9l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V713.9z"/>
															<path id="txt270" class="lot-text" d="M504.8,696.5v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															L505,690c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H504.8z M513.1,689.7v0.9l-2.1,5.9h-1.3
															l2.1-5.5v0h-2.3v-1.2H513.1z M517.6,693c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5
															C517.2,689.6,517.6,691.4,517.6,693z M515,693.1c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3
															C515.2,690.8,515,691.5,515,693.1z"/>
															<path id="txt269" class="lot-text" d="M509.8,672.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H509.8z M517.6,666.5
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V666.5z M516.8,669.9
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C516.7,671.3,516.8,670.7,516.8,669.9z
															M519.1,671.2c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4
															c-0.9,0-1.6-0.8-1.6-2c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9
															c-0.2,0-0.4,0-0.6,0V671.2z M520,667.6c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7
															c0-0.9-0.2-1.4-0.6-1.4C520.2,666.4,520,666.8,520,667.6z"/>
															<path id="txt268" class="lot-text" d="M521.9,650.4v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															L522,644c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H521.9z M529.8,644.6
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V644.6z M529,648.1
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C528.8,649.5,529,648.9,529,648.1z
															M530.7,648.6c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C531.5,650.5,530.7,649.7,530.7,648.6z M533.4,648.5c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C533.2,649.5,533.4,649.1,533.4,648.5z M532.2,645.3c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S532.2,644.9,532.2,645.3z"/>
															<path id="txt267" class="lot-text" d="M541.2,630v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H541.2z M549,624.3
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V624.3z M548.2,627.7
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C548.1,629.1,548.2,628.5,548.2,627.7z
															M553.8,623.3v0.9l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H553.8z"/>
															<path id="txt264" class="lot-text" d="M540.5,565.9V565l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H540.5z M548.4,560.2
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V560.2z M547.6,563.6
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C547.4,565,547.6,564.4,547.6,563.6z
															M551.5,565.9v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6H551.5z M551.5,563.2v-1.4c0-0.4,0-0.8,0.1-1.1h0
															c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4v0H551.5z"/>
															<path id="txt263" class="lot-text" d="M526.6,539.9V539l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H526.6z M534.4,534.2
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V534.2z M533.6,537.6
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C533.5,539,533.6,538.4,533.6,537.6z
															M535.6,538.6c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9
															c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5v0
															c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L535.6,538.6z"/>
															<path id="txt262" class="lot-text" d="M520.7,516.9V516l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H520.7z M528.6,511.2
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V511.2z M527.8,514.6
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C527.6,516,527.8,515.4,527.8,514.6z
															M529.6,516.9V516l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4
															c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H529.6z"/>
															<path id="txt261" class="lot-text" d="M514.9,489.6v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H514.9z M522.7,483.9
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V483.9z M521.9,487.3
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C521.8,488.7,521.9,488.1,521.9,487.3z
															M525.3,484.2L525.3,484.2l-1.2,0.6l-0.2-1.1l1.5-0.8h1.2v6.8h-1.4V484.2z"/>
															<path id="txt260" class="lot-text" d="M513.1,465.8v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H513.1z M521,460.1
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V460.1z M520.2,463.5
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C520,464.9,520.2,464.3,520.2,463.5z
															M525.9,462.4c0,2.4-0.9,3.5-2,3.5c-1.5,0-2-1.7-2-3.4c0-1.9,0.6-3.5,2-3.5C525.6,459,525.9,460.8,525.9,462.4z M523.3,462.5
															c0,1.6,0.2,2.3,0.6,2.3c0.4,0,0.6-0.7,0.6-2.3c0-1.4-0.1-2.3-0.6-2.3C523.6,460.2,523.3,460.9,523.3,462.5z"/>
															<path id="txt259" class="lot-text" d="M512.7,440.1v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H512.7z M520.7,434.5h-1.8l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V434.5z M522.1,439
															c0.3,0,0.4,0,0.6-0.1c0.3-0.1,0.6-0.2,0.8-0.4c0.3-0.3,0.5-0.8,0.7-1.4h0c-0.2,0.2-0.5,0.4-1,0.4c-0.9,0-1.6-0.8-1.6-2
															c0-1.2,0.7-2.4,2-2.4c1.3,0,1.9,1.1,1.9,2.8c0,1.5-0.5,2.6-1.1,3.3c-0.4,0.4-1,0.7-1.7,0.9c-0.2,0-0.4,0-0.6,0V439z M522.9,435.5
															c0,0.7,0.2,1.2,0.6,1.2c0.2,0,0.4-0.2,0.5-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.2-1.4-0.6-1.4C523.1,434.2,522.9,434.7,522.9,435.5
															z"/>
															<path id="txt258" class="lot-text" d="M513.8,415.2v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H513.8z M521.8,409.6H520l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V409.6z M522.6,413.5
															c0-0.8,0.5-1.4,1-1.8v0c-0.6-0.4-0.8-1-0.8-1.5c0-1.1,0.9-1.8,1.9-1.8c1,0,1.7,0.7,1.7,1.7c0,0.4-0.2,1-0.9,1.5v0
															c0.6,0.3,1,1,1,1.8c0,1.2-0.9,2-2,2C523.4,415.3,522.6,414.6,522.6,413.5z M525.3,413.4c0-0.6-0.4-1-0.8-1.2
															c-0.3,0.2-0.6,0.7-0.6,1.2c0,0.6,0.2,1.1,0.7,1.1C525,414.4,525.3,413.9,525.3,413.4z M524.1,410.1c0,0.5,0.3,0.8,0.7,1
															c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9S524.1,409.7,524.1,410.1z"/>
															<path id="txt257" class="lot-text" d="M513.4,390.1v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H513.4z M521.3,384.5h-1.8l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V384.5z M526,383.4v0.9
															l-2.1,5.9h-1.3l2.1-5.5v0h-2.3v-1.2H526z"/>
															<path id="txt256" class="lot-text" d="M508.6,364.4v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
															l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5v0h2.1v1.2H508.6z M516.6,358.8h-1.8l-0.2,1.2
															c0.1,0,0.1,0,0.2,0c0.3,0,0.9,0.1,1.3,0.5c0.4,0.4,0.6,0.9,0.6,1.5c0,1.3-0.9,2.4-2.3,2.4c-0.5,0-1-0.1-1.3-0.3l0.2-1.1
															c0.2,0.1,0.6,0.2,1,0.2c0.7,0,1.1-0.4,1.1-1.2c0-0.9-0.7-1.2-1.5-1.2c-0.2,0-0.3,0-0.5,0l0.4-3.4h2.8V358.8z M520.9,358.6
															c-0.2,0-0.4,0-0.6,0.1c-0.9,0.2-1.3,0.9-1.5,1.8h0c0.2-0.3,0.6-0.5,1-0.5c0.9,0,1.5,0.8,1.5,2.1c0,1.2-0.6,2.4-1.9,2.4
															c-1.3,0-2.1-1.1-2.1-2.8c0-1.5,0.5-2.5,1.1-3.1c0.4-0.5,1.1-0.8,1.7-0.9c0.3-0.1,0.5-0.1,0.7-0.1V358.6z M520.1,362.1
															c0-0.6-0.2-1.1-0.6-1.1c-0.2,0-0.4,0.1-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.2,1.5,0.7,1.5C519.9,363.5,520.1,362.9,520.1,362.1z
															"/>
															<path id="txt444" class="lot-text" d="M421.7,765.7v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6L421.7,765.7L421.7,765.7z
																 M421.7,763v-1.4c0-0.4,0-0.8,0.1-1.1l0,0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4l0,0L421.7,763L421.7,763L421.7,763z M426.5,765.7v-1.6
																h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6L426.5,765.7L426.5,765.7z M426.5,763v-1.4c0-0.4,0-0.8,0.1-1.1l0,0
																c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4l0,0L426.5,763L426.5,763L426.5,763z M431.5,765.7v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7
																v1.6L431.5,765.7L431.5,765.7z M431.5,763v-1.4c0-0.4,0-0.8,0.1-1.1l0,0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4l0,0L431.5,763L431.5,763
																L431.5,763z"/>
															<path id="txt442" class="lot-text" d="M452,676.7v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6L452,676.7L452,676.7z M452,674v-1.4
																c0-0.4,0-0.8,0.1-1.1l0,0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4l0,0L452,674L452,674L452,674z M456.5,676.7v-1.6h-2.3v-0.9l2.2-4.2h1.3
																v4.1h0.7v1.1h-0.7v1.6L456.5,676.7L456.5,676.7z M456.5,674v-1.4c0-0.4,0-0.8,0.1-1.1l0,0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4l0,0
																L456.5,674L456.5,674L456.5,674z M458.5,676.7v-0.9l0.7-0.9c0.9-1.1,1.6-2,1.6-3c0-0.5-0.3-0.9-0.9-0.9c-0.4,0-0.8,0.2-1,0.3
																l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.5,0,2,1,2,2c0,1.3-0.8,2.3-1.6,3.2l-0.4,0.5l0,0h2.1v1.2C462.2,676.7,458.5,676.7,458.5,676.7z
																"/>
															<path id="txt443" class="lot-text" d="M406.5,725.7v-1.6h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6L406.5,725.7L406.5,725.7z
																 M406.5,723v-1.4c0-0.4,0-0.8,0.1-1.1l0,0c-0.2,0.4-0.3,0.7-0.5,1l-0.7,1.4l0,0L406.5,723L406.5,723L406.5,723z M411,725.7v-1.6
																h-2.3v-0.9l2.2-4.2h1.3v4.1h0.7v1.1h-0.7v1.6L411,725.7L411,725.7z M411,723v-1.4c0-0.4,0-0.8,0.1-1.1l0,0c-0.2,0.4-0.3,0.7-0.5,1
																l-0.8,1.5l0,0H411z M413.3,724.4c0.3,0.2,0.6,0.3,1,0.3c0.7,0,1-0.4,1-1c0-0.7-0.6-1.1-1.3-1.1h-0.2v-1h0.1c0.5,0,1.1-0.2,1.1-0.9
																c0-0.5-0.3-0.8-0.8-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1c0.3-0.2,0.8-0.4,1.4-0.4c1.2,0,1.8,0.8,1.8,1.6c0,0.7-0.4,1.2-1,1.5l0,0
																c0.7,0.2,1.2,0.8,1.2,1.7c0,1-0.8,2-2.3,2c-0.6,0-1-0.1-1.3-0.3L413.3,724.4z"/>
															<path id="txtea1" class="lot-text" d="M548.6,998.2h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V998.2z M551.1,999.7l-0.3,1.7h-1.4
																l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H551.1z M552.3,998.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7l-0.3,1.5
																H552.3z M558.2,995.7L558.2,995.7l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V995.7z"/>
															<path id="txtea2" class="lot-text" d="M559.9,1021.2h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1021.2z M562.4,1022.7l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H562.4z M563.6,1021.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H563.6z M567.8,1024.4v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H567.8z"/>
															<path id="txtea3" class="lot-text" d="M561.4,1044.2h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1044.2z M563.9,1045.7l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H563.9z M565.1,1044.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H565.1z M569.5,1046c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L569.5,1046z"/>
															<path id="txtea4" class="lot-text" d="M556.8,1066.5H555v2h1.9v1.2h-3.4v-7.3h3.3v1.2H555v1.7h1.7V1066.5z M559.2,1068l-0.3,1.7h-1.4
																l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H559.2z M560.4,1066.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7l-0.3,1.5
																H560.4z M567,1069.7v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1h-0.7v1.7H567z M567,1066.9v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H567z"/>
															<path id="txtea5" class="lot-text" d="M548.1,1089.5h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1089.5z M550.6,1091l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H550.6z M551.8,1089.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H551.8z M559.8,1086.9h-1.9l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,1,0.2,1.4,0.5c0.4,0.4,0.6,0.9,0.6,1.6
																c0,1.4-0.9,2.5-2.5,2.5c-0.5,0-1.1-0.1-1.4-0.3l0.2-1.1c0.3,0.1,0.6,0.2,1,0.2c0.7,0,1.2-0.4,1.2-1.2c0-0.9-0.7-1.2-1.6-1.2
																c-0.2,0-0.4,0-0.5,0l0.4-3.5h3V1086.9z"/>
															<path id="txtea6" class="lot-text" d="M553.8,1112.8H552v2h1.9v1.2h-3.4v-7.3h3.3v1.2H552v1.7h1.7V1112.8z M556.2,1114.4l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H556.2z M557.4,1113.3l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H557.4z M565.4,1110.1c-0.2,0-0.4,0-0.6,0.1c-1,0.2-1.4,1-1.6,1.9h0c0.3-0.3,0.6-0.6,1.1-0.6c1,0,1.6,0.9,1.6,2.2
																c0,1.2-0.6,2.5-2.1,2.5c-1.4,0-2.2-1.2-2.2-2.9c0-1.5,0.5-2.6,1.2-3.3c0.5-0.5,1.2-0.8,1.8-0.9c0.3-0.1,0.6-0.1,0.7-0.1V1110.1z
																 M564.5,1113.6c0-0.6-0.2-1.2-0.7-1.2c-0.2,0-0.5,0.2-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.3,1.5,0.7,1.5
																C564.3,1115.1,564.5,1114.5,564.5,1113.6z"/>
															<path id="txtea7" class="lot-text" d="M563.8,1134.5H562v2h1.9v1.2h-3.4v-7.3h3.3v1.2H562v1.7h1.7V1134.5z M566.2,1136l-0.3,1.7h-1.4
																l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H566.2z M567.4,1134.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7l-0.3,1.5
																H567.4z M575.8,1130.7v0.9l-2.3,6.1h-1.4l2.2-5.7v0h-2.5v-1.2H575.8z"/>
															<path id="txtea8" class="lot-text" d="M557.1,1156.8h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1156.8z M559.6,1158.4l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H559.6z M560.8,1157.3l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H560.8z M564.9,1158.2c0-0.9,0.5-1.5,1.1-1.9v0c-0.6-0.4-0.9-1-0.9-1.5c0-1.1,0.9-1.8,2.1-1.8c1,0,1.9,0.7,1.9,1.7
																c0,0.5-0.2,1.1-1,1.5v0c0.7,0.4,1.1,1,1.1,1.8c0,1.3-1,2-2.2,2C565.8,1160.1,564.9,1159.4,564.9,1158.2z M567.8,1158.1
																c0-0.7-0.4-1.1-0.8-1.3c-0.4,0.2-0.6,0.7-0.6,1.2c0,0.6,0.3,1.1,0.8,1.1C567.6,1159.2,567.8,1158.7,567.8,1158.1z M566.5,1154.8
																c0,0.5,0.3,0.8,0.7,1.1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9C566.7,1153.9,566.5,1154.3,566.5,1154.8z"/>
															<path id="txtea9" class="lot-text" d="M523.7,1154.1h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1154.1z M526.1,1155.6l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H526.1z M527.4,1154.5l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H527.4z M532.1,1156.2c0.3,0,0.4,0,0.7-0.1c0.3-0.1,0.6-0.2,0.9-0.4c0.3-0.3,0.6-0.8,0.7-1.4h0c-0.2,0.2-0.6,0.4-1,0.4
																c-0.9,0-1.7-0.8-1.7-2.1c0-1.2,0.8-2.5,2.2-2.5c1.4,0,2.1,1.2,2.1,2.9c0,1.6-0.5,2.7-1.2,3.4c-0.4,0.5-1.1,0.8-1.8,0.9
																c-0.2,0-0.5,0-0.7,0V1156.2z M533,1152.5c0,0.7,0.2,1.2,0.7,1.2c0.3,0,0.5-0.2,0.6-0.4c0.1-0.1,0.1-0.3,0.1-0.7
																c0-0.9-0.3-1.4-0.7-1.4C533.3,1151.2,533,1151.7,533,1152.5z"/>
															<path id="txtea10" class="lot-text" d="M492.5,1152.8h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1152.8z M495,1154.3l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H495z M496.2,1153.2l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H496.2z M502.1,1150.3L502.1,1150.3l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1150.3z M509.5,1152.4c0,2.5-1,3.6-2.1,3.6
																c-1.6,0-2.1-1.8-2.1-3.5c0-2,0.7-3.6,2.2-3.6C509.1,1148.9,509.5,1150.8,509.5,1152.4z M506.7,1152.5c0,1.6,0.2,2.4,0.7,2.4
																c0.5,0,0.6-0.8,0.6-2.4c0-1.5-0.1-2.4-0.6-2.4C506.9,1150.1,506.7,1150.8,506.7,1152.5z"/>
															<path id="txtea11" class="lot-text" d="M461.3,1152.5h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1152.5z M463.8,1154l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H463.8z M465,1152.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H465z M471,1150L471,1150l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7H471V1150z M475.8,1150L475.8,1150l-1.3,0.6l-0.2-1.1l1.7-0.8
																h1.3v7h-1.5V1150z"/>
															<path id="txtea12" class="lot-text" d="M429.2,1149.9h-1.7v2h1.9v1.2H426v-7.3h3.3v1.2h-1.8v1.7h1.7V1149.9z M431.6,1151.5l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H431.6z M432.9,1150.4l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H432.9z M438.8,1147.4L438.8,1147.4l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1147.4z M441.9,1153.1v-0.9l0.7-0.9
																c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2
																c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H441.9z"/>
															<path id="txtea13" class="lot-text" d="M407.3,1140.2h-1.7v2h1.9v1.2h-3.4v-7.2h3.3v1.2h-1.8v1.7h1.7V1140.2z M409.7,1141.7l-0.3,1.7
																H408l1.6-7.2h1.8l1.5,7.2h-1.4l-0.3-1.7H409.7z M410.9,1140.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H410.9z M405.4,1147.7L405.4,1147.7l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1147.7z M408.7,1152c0.3,0.2,0.7,0.3,1.1,0.3
																c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3
																l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1
																c-0.6,0-1.1-0.1-1.4-0.3L408.7,1152z"/>
															<path id="txtea14" class="lot-text" d="M417.8,1112.7h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1112.7z M420.3,1114.2l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3H422l-0.3-1.7H420.3z M421.5,1113.1l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H421.5z M427.5,1110.2L427.5,1110.2l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1110.2z M432.9,1115.9v-1.7h-2.5v-1l2.4-4.4
																h1.4v4.2h0.7v1.1h-0.7v1.7H432.9z M432.9,1113.1v-1.4c0-0.4,0-0.8,0.1-1.2h0c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H432.9z"/>
															<path id="txtea15" class="lot-text" d="M447.5,1122.5h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1122.5z M450,1124l-0.3,1.7h-1.4
																l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H450z M451.2,1122.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7l-0.3,1.5
																H451.2z M457.1,1120L457.1,1120l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1120z M464,1119.9h-1.9l-0.2,1.2c0.1,0,0.1,0,0.2,0
																c0.3,0,1,0.2,1.4,0.5c0.4,0.4,0.6,0.9,0.6,1.6c0,1.4-0.9,2.5-2.5,2.5c-0.5,0-1.1-0.1-1.4-0.3l0.2-1.1c0.3,0.1,0.6,0.2,1,0.2
																c0.7,0,1.2-0.4,1.2-1.2c0-0.9-0.7-1.2-1.6-1.2c-0.2,0-0.4,0-0.5,0l0.4-3.5h3V1119.9z"/>
															<path id="txtea16" class="lot-text" d="M484.8,1124.1H483v2h1.9v1.2h-3.4v-7.3h3.3v1.2H483v1.7h1.7V1124.1z M487.2,1125.6l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H487.2z M488.4,1124.5l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H488.4z M494.4,1121.6L494.4,1121.6l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1121.6z M501.2,1121.3c-0.2,0-0.4,0-0.6,0.1
																c-1,0.2-1.4,1-1.6,1.9h0c0.3-0.3,0.6-0.6,1.1-0.6c1,0,1.6,0.9,1.6,2.2c0,1.2-0.6,2.5-2.1,2.5c-1.4,0-2.2-1.2-2.2-2.9
																c0-1.5,0.5-2.6,1.2-3.3c0.5-0.5,1.2-0.8,1.8-0.9c0.3-0.1,0.6-0.1,0.7-0.1V1121.3z M500.3,1124.9c0-0.6-0.2-1.2-0.7-1.2
																c-0.2,0-0.5,0.2-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.3,1.5,0.7,1.5C500.2,1126.3,500.3,1125.7,500.3,1124.9z"/>
															<path id="txtea17" class="lot-text" d="M498.8,1096.4h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1096.4z M501.3,1098l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3H503l-0.3-1.7H501.3z M502.5,1096.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H502.5z M508.5,1093.9L508.5,1093.9l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1093.9z M515.7,1092.6v0.9l-2.3,6.1H512
																l2.2-5.7v0h-2.5v-1.2H515.7z"/>
															<path id="txtea18" class="lot-text" d="M478.3,1071.7h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1071.7z M480.7,1073.2l-0.3,1.7
																H479l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H480.7z M481.9,1072.1l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H481.9z M476.4,1079.2L476.4,1079.2l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1079.2z M479.5,1083c0-0.9,0.5-1.5,1.1-1.9v0
																c-0.6-0.4-0.9-1-0.9-1.5c0-1.1,0.9-1.8,2.1-1.8c1,0,1.9,0.7,1.9,1.7c0,0.5-0.2,1.1-1,1.5v0c0.7,0.4,1.1,1,1.1,1.8c0,1.3-1,2-2.2,2
																C480.3,1085,479.5,1084.2,479.5,1083z M482.3,1083c0-0.7-0.4-1.1-0.8-1.3c-0.4,0.2-0.6,0.7-0.6,1.2c0,0.6,0.3,1.1,0.8,1.1
																C482.1,1084,482.3,1083.6,482.3,1083z M481,1079.6c0,0.5,0.3,0.8,0.7,1.1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
																C481.2,1078.7,481,1079.2,481,1079.6z"/>
															<path id="txtea19" class="lot-text" d="M454.3,1064.4h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1064.4z M456.7,1066l-0.3,1.7
																H455l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H456.7z M457.9,1064.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H457.9z M452.4,1071.9L452.4,1071.9l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1071.9z M456.1,1076.5c0.3,0,0.4,0,0.7-0.1
																c0.3-0.1,0.6-0.2,0.9-0.4c0.3-0.3,0.6-0.8,0.7-1.4h0c-0.2,0.2-0.6,0.4-1,0.4c-0.9,0-1.7-0.8-1.7-2.1c0-1.2,0.8-2.5,2.2-2.5
																c1.4,0,2.1,1.2,2.1,2.9c0,1.6-0.5,2.7-1.2,3.4c-0.4,0.5-1.1,0.8-1.8,0.9c-0.2,0-0.5,0-0.7,0V1076.5z M457,1072.9
																c0,0.7,0.2,1.2,0.7,1.2c0.3,0,0.5-0.2,0.6-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.3-1.4-0.7-1.4C457.2,1071.6,456.9,1072,457,1072.9
																z"/>
															<path id="txtea20" class="lot-text" d="M430.8,1064.4H429v2h1.9v1.2h-3.4v-7.3h3.3v1.2H429v1.7h1.7V1064.4z M433.2,1066l-0.3,1.7h-1.4
																l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H433.2z M434.4,1064.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7l-0.3,1.5
																H434.4z M427.2,1077.6v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H427.2z M436.2,1074.1c0,2.5-1,3.6-2.1,3.6
																c-1.6,0-2.1-1.8-2.1-3.5c0-2,0.7-3.6,2.2-3.6C435.9,1070.5,436.2,1072.4,436.2,1074.1z M433.5,1074.1c0,1.6,0.2,2.4,0.7,2.4
																c0.5,0,0.6-0.8,0.6-2.4c0-1.5-0.1-2.4-0.6-2.4C433.7,1071.7,433.5,1072.5,433.5,1074.1z"/>
															<path id="txtea21" class="lot-text" d="M404.3,1064.4h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1064.4z M406.7,1066l-0.3,1.7
																H405l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H406.7z M407.9,1064.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H407.9z M400.7,1077.6v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H400.7z M407.2,1071.9L407.2,1071.9l-1.3,0.6
																l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1071.9z"/>
															<path id="txtea22" class="lot-text" d="M377.9,1119.5h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1119.5z M380.4,1121l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H380.4z M381.6,1119.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H381.6z M385.8,1122.7v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H385.8z M390.6,1122.7v-0.9l0.7-0.9
																c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2
																c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H390.6z"/>
															<path id="txtea23" class="lot-text" d="M371.6,1136.7h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1136.7z M374.1,1138.2l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H374.1z M375.3,1137.1l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H375.3z M368,1149.9v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H368z M373.1,1148.5c0.3,0.2,0.7,0.3,1.1,0.3
																c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3
																l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1
																c-0.6,0-1.1-0.1-1.4-0.3L373.1,1148.5z"/>
															<path id="txtea24" class="lot-text" d="M349.6,1131.4h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1131.4z M352.1,1133l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H352.1z M353.3,1131.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H353.3z M346,1144.6v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H346z M353.2,1144.6v-1.7h-2.5v-1l2.4-4.4h1.4
																v4.2h0.7v1.1h-0.7v1.7H353.2z M353.2,1141.8v-1.4c0-0.4,0-0.8,0.1-1.2h0c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H353.2z"/>
															<path id="txtea25" class="lot-text" d="M326.6,1115.6h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1115.6z M329.1,1117.1l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H329.1z M330.3,1116l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H330.3z M323,1128.8v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H323z M331.7,1123h-1.9l-0.2,1.2
																c0.1,0,0.1,0,0.2,0c0.3,0,1,0.2,1.4,0.5c0.4,0.4,0.6,0.9,0.6,1.6c0,1.4-0.9,2.5-2.5,2.5c-0.5,0-1.1-0.1-1.4-0.3l0.2-1.1
																c0.3,0.1,0.6,0.2,1,0.2c0.7,0,1.2-0.4,1.2-1.2c0-0.9-0.7-1.2-1.6-1.2c-0.2,0-0.4,0-0.5,0l0.4-3.5h3V1123z"/>
															<path id="txtea26" class="lot-text" d="M305,1115.3h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1115.3z M307.5,1116.9l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H307.5z M308.7,1115.8l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H308.7z M301.4,1128.5v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H301.4z M310,1122.6c-0.2,0-0.4,0-0.6,0.1
																c-1,0.2-1.4,1-1.6,1.9h0c0.3-0.3,0.6-0.6,1.1-0.6c1,0,1.6,0.9,1.6,2.2c0,1.2-0.6,2.5-2.1,2.5c-1.4,0-2.2-1.2-2.2-2.9
																c0-1.5,0.5-2.6,1.2-3.3c0.5-0.5,1.2-0.8,1.8-0.9c0.3-0.1,0.6-0.1,0.7-0.1V1122.6z M309.1,1126.1c0-0.6-0.2-1.2-0.7-1.2
																c-0.2,0-0.5,0.2-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.3,1.5,0.7,1.5C308.9,1127.6,309.1,1127,309.1,1126.1z"/>
															<path id="txtea27" class="lot-text" d="M283,1108.1h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1108.1z M285.5,1109.6l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H285.5z M286.7,1108.5l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H286.7z M279.4,1121.3v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H279.4z M288.4,1114.3v0.9l-2.3,6.1h-1.4
																l2.2-5.7v0h-2.5v-1.2H288.4z"/>
															<path id="txtea28" class="lot-text" d="M260.8,1107.6h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1107.6z M263.3,1109.1l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3H265l-0.3-1.7H263.3z M264.5,1108l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H264.5z M257.3,1121.8v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H257.3z M262,1120c0-0.9,0.5-1.5,1.1-1.9v0
																c-0.6-0.4-0.9-1-0.9-1.5c0-1.1,0.9-1.8,2.1-1.8c1,0,1.9,0.7,1.9,1.7c0,0.5-0.2,1.1-1,1.5v0c0.7,0.4,1.1,1,1.1,1.8c0,1.3-1,2-2.2,2
																C262.9,1121.9,262,1121.1,262,1120z M264.9,1119.9c0-0.7-0.4-1.1-0.8-1.3c-0.4,0.2-0.6,0.7-0.6,1.2c0,0.6,0.3,1.1,0.8,1.1
																C264.7,1120.9,264.9,1120.5,264.9,1119.9z M263.6,1116.5c0,0.5,0.3,0.8,0.7,1.1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
																C263.8,1115.7,263.6,1116.1,263.6,1116.5z"/>
															<path id="txtea29" class="lot-text" d="M226.8,1114.8H225v2h1.9v1.2h-3.4v-7.3h3.3v1.2H225v1.7h1.7V1114.8z M229.2,1116.4l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H229.2z M230.4,1115.3l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H230.4z M234.7,1118v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1
																c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H234.7z M240,1116.9c0.3,0,0.4,0,0.7-0.1
																c0.3-0.1,0.6-0.2,0.9-0.4c0.3-0.3,0.6-0.8,0.7-1.4h0c-0.2,0.2-0.6,0.4-1,0.4c-0.9,0-1.7-0.8-1.7-2.1c0-1.2,0.8-2.5,2.2-2.5
																c1.4,0,2.1,1.2,2.1,2.9c0,1.6-0.5,2.7-1.2,3.4c-0.4,0.5-1.1,0.8-1.8,0.9c-0.2,0-0.5,0-0.7,0V1116.9z M240.9,1113.3
																c0,0.7,0.2,1.2,0.7,1.2c0.3,0,0.5-0.2,0.6-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.3-1.4-0.7-1.4
																C241.2,1112,240.9,1112.4,240.9,1113.3z"/>
															<path id="txtea30" class="lot-text" d="M209.4,1145.5h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1145.5z M211.9,1147l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H211.9z M213.1,1145.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H213.1z M217.5,1147.4c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L217.5,1147.4z M226.4,1145.2
																c0,2.5-1,3.6-2.1,3.6c-1.6,0-2.1-1.8-2.1-3.5c0-2,0.7-3.6,2.2-3.6C226,1141.6,226.4,1143.5,226.4,1145.2z M223.6,1145.2
																c0,1.6,0.2,2.4,0.7,2.4c0.5,0,0.6-0.8,0.6-2.4c0-1.5-0.1-2.4-0.6-2.4C223.8,1142.8,223.6,1143.6,223.6,1145.2z"/>
															<path id="txtea31" class="lot-text" d="M231.2,1173.3h-1.7v2h1.9v1.2H228v-7.3h3.3v1.2h-1.8v1.7h1.7V1173.3z M233.6,1174.9l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H233.6z M234.9,1173.8l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H234.9z M227.8,1185.2c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L227.8,1185.2z M234.2,1180.9
																L234.2,1180.9l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1180.9z"/>
															<path id="txtea32" class="lot-text" d="M257.8,1173.3h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1173.3z M260.3,1174.9l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3H262l-0.3-1.7H260.3z M261.5,1173.8l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H261.5z M254.5,1185.2c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L254.5,1185.2z M259.1,1186.5v-0.9
																l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2
																c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H259.1z"/>
															<path id="txtea33" class="lot-text" d="M283.1,1173.3h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1173.3z M285.6,1174.9l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H285.6z M286.8,1173.8l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H286.8z M279.7,1185.2c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L279.7,1185.2z M284.6,1185.2
																c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8
																c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8
																c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L284.6,1185.2z"/>
															<path id="txtea34" class="lot-text" d="M307.7,1173.3H306v2h1.9v1.2h-3.4v-7.3h3.3v1.2H306v1.7h1.7V1173.3z M310.2,1174.9l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H310.2z M311.4,1173.8l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H311.4z M304.4,1185.2c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1H305v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L304.4,1185.2z M311.3,1186.5v-1.7
																h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1h-0.7v1.7H311.3z M311.3,1183.7v-1.4c0-0.4,0-0.8,0.1-1.2h0c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0
																H311.3z"/>
															<path id="txtea35" class="lot-text" d="M333.7,1173.3H332v2h1.9v1.2h-3.4v-7.3h3.3v1.2H332v1.7h1.7V1173.3z M336.2,1174.9l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H336.2z M337.4,1173.8l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H337.4z M330.4,1185.2c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1H331v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L330.4,1185.2z M338.8,1180.8h-1.9
																l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,1,0.2,1.4,0.5c0.4,0.4,0.6,0.9,0.6,1.6c0,1.4-0.9,2.5-2.5,2.5c-0.5,0-1.1-0.1-1.4-0.3l0.2-1.1
																c0.3,0.1,0.6,0.2,1,0.2c0.7,0,1.2-0.4,1.2-1.2c0-0.9-0.7-1.2-1.6-1.2c-0.2,0-0.4,0-0.5,0l0.4-3.5h3V1180.8z"/>
															<path id="txtea36" class="lot-text" d="M359.4,1178.2h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1178.2z M361.9,1179.7l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H361.9z M363.1,1178.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H363.1z M356.1,1190c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L356.1,1190z M364.4,1185.4
																c-0.2,0-0.4,0-0.6,0.1c-1,0.2-1.4,1-1.6,1.9h0c0.3-0.3,0.6-0.6,1.1-0.6c1,0,1.6,0.9,1.6,2.2c0,1.2-0.6,2.5-2.1,2.5
																c-1.4,0-2.2-1.2-2.2-2.9c0-1.5,0.5-2.6,1.2-3.3c0.5-0.5,1.2-0.8,1.8-0.9c0.3-0.1,0.6-0.1,0.7-0.1V1185.4z M363.5,1189
																c0-0.6-0.2-1.2-0.7-1.2c-0.2,0-0.5,0.2-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6c0,0.9,0.3,1.5,0.7,1.5
																C363.3,1190.4,363.5,1189.8,363.5,1189z"/>
															<path id="txtea37" class="lot-text" d="M385.8,1179.2H384v2h1.9v1.2h-3.4v-7.3h3.3v1.2H384v1.7h1.7V1179.2z M388.2,1180.7l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H388.2z M389.4,1179.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H389.4z M382.4,1191c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1H383v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L382.4,1191z M391.1,1185.4v0.9
																l-2.3,6.1h-1.4l2.2-5.7v0h-2.5v-1.2H391.1z"/>
															<path id="txtea38" class="lot-text" d="M413.5,1182.5h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1182.5z M416,1184l-0.3,1.7h-1.4
																l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H416z M417.2,1182.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7l-0.3,1.5
																H417.2z M410.1,1195.4c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1h-0.2v-1h0.1c0.5,0,1.2-0.2,1.2-0.9
																c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7c0,0.7-0.4,1.3-1,1.6v0
																c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L410.1,1195.4z M414.7,1194.9c0-0.9,0.5-1.5,1.1-1.9v0
																c-0.6-0.4-0.9-1-0.9-1.5c0-1.1,0.9-1.8,2.1-1.8c1,0,1.9,0.7,1.9,1.7c0,0.5-0.2,1.1-1,1.5v0c0.7,0.4,1.1,1,1.1,1.8c0,1.3-1,2-2.2,2
																C415.6,1196.8,414.7,1196,414.7,1194.9z M417.6,1194.8c0-0.7-0.4-1.1-0.8-1.3c-0.4,0.2-0.6,0.7-0.6,1.2c0,0.6,0.3,1.1,0.8,1.1
																C417.4,1195.8,417.6,1195.4,417.6,1194.8z M416.3,1191.4c0,0.5,0.3,0.8,0.7,1.1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
																C416.5,1190.6,416.3,1191,416.3,1191.4z"/>
															<path id="txtea39" class="lot-text" d="M445.8,1185.2H444v2h1.9v1.2h-3.4v-7.3h3.3v1.2H444v1.7h1.7V1185.2z M448.2,1186.7l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H448.2z M449.4,1185.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H449.4z M442.4,1197c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1H443v-1h0.1
																c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4c1.3,0,1.9,0.8,1.9,1.7
																c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3L442.4,1197z M447.6,1197.3
																c0.3,0,0.4,0,0.7-0.1c0.3-0.1,0.6-0.2,0.9-0.4c0.3-0.3,0.6-0.8,0.7-1.4h0c-0.2,0.2-0.6,0.4-1,0.4c-0.9,0-1.7-0.8-1.7-2.1
																c0-1.2,0.8-2.5,2.2-2.5c1.4,0,2.1,1.2,2.1,2.9c0,1.6-0.5,2.7-1.2,3.4c-0.4,0.5-1.1,0.8-1.8,0.9c-0.2,0-0.5,0-0.7,0V1197.3z
																 M448.4,1193.6c0,0.7,0.2,1.2,0.7,1.2c0.3,0,0.5-0.2,0.6-0.4c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.3-1.4-0.7-1.4
																C448.7,1192.3,448.4,1192.8,448.4,1193.6z"/>
															<path id="txtea40" class="lot-text" d="M474.8,1190.7h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1190.7z M477.3,1192.2l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3H479l-0.3-1.7H477.3z M478.5,1191.1l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H478.5z M485.1,1193.9v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1h-0.7v1.7H485.1z M485.1,1191.1v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H485.1z M491.8,1190.3c0,2.5-1,3.6-2.1,3.6c-1.6,0-2.1-1.8-2.1-3.5c0-2,0.7-3.6,2.2-3.6
																C491.4,1186.8,491.8,1188.7,491.8,1190.3z M489,1190.4c0,1.6,0.2,2.4,0.7,2.4c0.5,0,0.6-0.8,0.6-2.4c0-1.5-0.1-2.4-0.6-2.4
																C489.3,1188,489,1188.7,489,1190.4z"/>
															<path id="txtea41" class="lot-text" d="M509.2,1190.7h-1.7v2h1.9v1.2H506v-7.3h3.3v1.2h-1.8v1.7h1.7V1190.7z M511.6,1192.2l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H511.6z M512.9,1191.1l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H512.9z M519.4,1193.9v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1h-0.7v1.7H519.4z M519.4,1191.1v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H519.4z M523.6,1188.2L523.6,1188.2l-1.3,0.6l-0.2-1.1l1.7-0.8h1.3v7h-1.5V1188.2z"/>
															<path id="txtea42" class="lot-text" d="M542.5,1190.7h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1190.7z M545,1192.2l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H545z M546.2,1191.1l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H546.2z M552.8,1193.9v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1h-0.7v1.7H552.8z M552.8,1191.1v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H552.8z M555.2,1193.9v-0.9l0.7-0.9c0.9-1.2,1.7-2.1,1.7-3.1c0-0.6-0.3-1-0.9-1
																c-0.5,0-0.8,0.2-1.1,0.4l-0.3-1.1c0.3-0.2,1-0.4,1.6-0.4c1.6,0,2.1,1,2.1,2c0,1.3-0.9,2.4-1.7,3.3l-0.4,0.5v0h2.2v1.2H555.2z"/>
															<path id="txtea43" class="lot-text" d="M579.6,1190.7h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1190.7z M582.1,1192.2l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H582.1z M583.3,1191.1l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H583.3z M589.8,1193.9v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1h-0.7v1.7H589.8z M589.8,1191.1v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H589.8z M592.5,1192.5c0.3,0.2,0.7,0.3,1.1,0.3c0.7,0,1.1-0.4,1.1-1c0-0.8-0.6-1.1-1.4-1.1
																h-0.2v-1h0.1c0.5,0,1.2-0.2,1.2-0.9c0-0.5-0.3-0.8-0.9-0.8c-0.3,0-0.6,0.1-0.9,0.3l-0.2-1.1c0.3-0.2,0.9-0.4,1.5-0.4
																c1.3,0,1.9,0.8,1.9,1.7c0,0.7-0.4,1.3-1,1.6v0c0.7,0.2,1.2,0.8,1.2,1.8c0,1.1-0.8,2.1-2.4,2.1c-0.6,0-1.1-0.1-1.4-0.3
																L592.5,1192.5z"/>
															<path id="txtea44" class="lot-text" d="M597.5,1168.1h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1168.1z M599.9,1169.6l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H599.9z M601.2,1168.5l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H601.2z M607.7,1171.3v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1H609v1.7H607.7z M607.7,1168.5v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H607.7z M612.5,1171.3v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1h-0.7v1.7H612.5z M612.5,1168.5
																v-1.4c0-0.4,0-0.8,0.1-1.2h0c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H612.5z"/>
															<path id="txtea45" class="lot-text" d="M601.5,1137.8h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1137.8z M603.9,1139.3l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H603.9z M605.2,1138.2l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H605.2z M611.7,1140.9v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1H613v1.7H611.7z M611.7,1138.2v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H611.7z M618,1135.2h-1.9l-0.2,1.2c0.1,0,0.1,0,0.2,0c0.3,0,1,0.2,1.4,0.5
																c0.4,0.4,0.6,0.9,0.6,1.6c0,1.4-0.9,2.5-2.5,2.5c-0.5,0-1.1-0.1-1.4-0.3l0.2-1.1c0.3,0.1,0.6,0.2,1,0.2c0.7,0,1.2-0.4,1.2-1.2
																c0-0.9-0.7-1.2-1.6-1.2c-0.2,0-0.4,0-0.5,0l0.4-3.5h3V1135.2z"/>
															<path id="txtea46" class="lot-text" d="M597.5,1107.2h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1107.2z M599.9,1108.7l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H599.9z M601.2,1107.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H601.2z M607.7,1110.4v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1H609v1.7H607.7z M607.7,1107.6v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H607.7z M613.9,1104.4c-0.2,0-0.4,0-0.6,0.1c-1,0.2-1.4,1-1.6,1.9h0c0.3-0.3,0.6-0.6,1.1-0.6
																c1,0,1.6,0.9,1.6,2.2c0,1.2-0.6,2.5-2.1,2.5c-1.4,0-2.2-1.2-2.2-2.9c0-1.5,0.5-2.6,1.2-3.3c0.5-0.5,1.2-0.8,1.8-0.9
																c0.3-0.1,0.6-0.1,0.7-0.1V1104.4z M613,1108c0-0.6-0.2-1.2-0.7-1.2c-0.2,0-0.5,0.2-0.6,0.5c0,0.1-0.1,0.3-0.1,0.6
																c0,0.9,0.3,1.5,0.7,1.5C612.9,1109.4,613,1108.8,613,1108z"/>
															<path id="txtea47" class="lot-text" d="M600.5,1078.5h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1078.5z M602.9,1080l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H602.9z M604.2,1078.9l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H604.2z M610.7,1081.7v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1H612v1.7H610.7z M610.7,1078.9v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H610.7z M617.3,1074.7v0.9l-2.3,6.1h-1.4l2.2-5.7v0h-2.5v-1.2H617.3z"/>
															<path id="txtea48" class="lot-text" d="M601.5,1050.9h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1050.9z M603.9,1052.4l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H603.9z M605.2,1051.3l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H605.2z M611.7,1054v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1H613v1.7H611.7z M611.7,1051.2v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H611.7z M614.2,1052.2c0-0.9,0.5-1.5,1.1-1.9v0c-0.6-0.4-0.9-1-0.9-1.5
																c0-1.1,0.9-1.8,2.1-1.8c1,0,1.9,0.7,1.9,1.7c0,0.5-0.2,1.1-1,1.5v0c0.7,0.4,1.1,1,1.1,1.8c0,1.3-1,2-2.2,2
																C615,1054.1,614.2,1053.4,614.2,1052.2z M617,1052.1c0-0.7-0.4-1.1-0.8-1.3c-0.4,0.2-0.6,0.7-0.6,1.2c0,0.6,0.3,1.1,0.8,1.1
																C616.8,1053.2,617,1052.7,617,1052.1z M615.7,1048.8c0,0.5,0.3,0.8,0.7,1.1c0.3-0.2,0.5-0.6,0.5-1c0-0.5-0.2-0.9-0.6-0.9
																C615.9,1047.9,615.7,1048.3,615.7,1048.8z"/>
															<path id="txtea49" class="lot-text" d="M601.5,1019.2h-1.7v2h1.9v1.2h-3.4v-7.3h3.3v1.2h-1.8v1.7h1.7V1019.2z M603.9,1020.7l-0.3,1.7
																h-1.4l1.6-7.3h1.8l1.5,7.3h-1.4l-0.3-1.7H603.9z M605.2,1019.6l-0.2-1.5c-0.1-0.4-0.2-1.2-0.2-1.6h0c-0.1,0.5-0.2,1.2-0.3,1.7
																l-0.3,1.5H605.2z M611.7,1022.4v-1.7h-2.5v-1l2.4-4.4h1.4v4.2h0.7v1.1H613v1.7H611.7z M611.7,1019.6v-1.4c0-0.4,0-0.8,0.1-1.2h0
																c-0.2,0.4-0.3,0.7-0.5,1.1l-0.8,1.5v0H611.7z M614.8,1021.3c0.3,0,0.4,0,0.7-0.1c0.3-0.1,0.6-0.2,0.9-0.4c0.3-0.3,0.6-0.8,0.7-1.4
																h0c-0.2,0.2-0.6,0.4-1,0.4c-0.9,0-1.7-0.8-1.7-2.1c0-1.2,0.8-2.5,2.2-2.5c1.4,0,2.1,1.2,2.1,2.9c0,1.6-0.5,2.7-1.2,3.4
																c-0.4,0.5-1.1,0.8-1.8,0.9c-0.2,0-0.5,0-0.7,0V1021.3z M615.6,1017.6c0,0.7,0.2,1.2,0.7,1.2c0.3,0,0.5-0.2,0.6-0.4
																c0.1-0.1,0.1-0.3,0.1-0.7c0-0.9-0.3-1.4-0.7-1.4C615.9,1016.3,615.6,1016.8,615.6,1017.6z"/>
															
														</g>
														
														
														<g id="popup">
															<rect id="popup-box" x="804.6" y="469.3" class="st20" width="196.4" height="130"/><!--130-->
															<circle id="popup-circle" class="st21" cx="806.1" cy="471.4" r="24.7"/>
															<text id="popup-label" x="793.8691" y="466.3072" class="st0 st22 st23">Lot #</text>
															<text id="popup-lotnumber" x="805.5573" y="484.2021" class="st0 st24 st25" text-anchor="middle"></text>
															<text id="popup-address1" x="909.8028" y="490.3461" class="st26 st27 st28"  text-anchor="middle"></text>
															<text id="popup-address2" x="916.8028" y="503.3461" class="st26 st27 st33"  text-anchor="middle"></text>
															<text id="popup-lotstatus" x="916.8028" y="503.3461" class="st26 st27 st28" text-anchor="middle"></text>
															<text id="popup-builder" x="916.8028" y="503.3461" class="st26 st27 st28 builder-color" text-anchor="middle"></text>
															
															<text id="popup-lotsize" x="836.8028" y="500.3461" class="st26 st27 st28"></text>
															<text id="popup-lottype" x="833.963" y="521.6699"class="st26 st27 st28" ></text>
															<text id="popup-price" x="856.8575" y="543.0936"  class="st26 st27 st28"></text>
															<rect id="popup-contact-bttn" x="806" y="557.9" class="st30" width="193.7" height="36.1"/>
															<text id="popup-contact-link" x="814.9707" y="581.1202" style="text-decoration: underline;" text-anchor="middle" class="st0 st31 st32">View Property</text>
														</g>
								
												
												
												
												
												
											
												
											</svg>
										
										</figure>
										
									</div>
									</div>
								</section>


								

							

							<?php endwhile; endif; ?>

									

				

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>



	</body>

</html> <!-- end of site. what a ride! -->
