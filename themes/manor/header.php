<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PVCV5KN');</script>
<!-- End Google Tag Manager -->

		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">



            <link rel="stylesheet" href="/wp-content/themes/manor/library/css/navigataur.css">
		

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<!-- VEMO EMBEDDER DELETED -->

		<!-- Vimeo embedder -->
	<link rel="stylesheet" href="/wp-content/themes/manor/library/js/libs/vimeo/css/vimeoEmbedder.css">
	<script type="text/javascript" src="/wp-content/themes/manor/library/js/libs/vimeo/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/wp-content/themes/manor/library/js/libs/vimeo/js/jquery.vimeoEmbedder.js"></script>


		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

		<!--MOBILE MENU DROPDOWNS STYLES AND FIXES ---->

		<!--<link rel="stylesheet" href="/wp-content/themes/manor/library/css/flaunt.css">---->
		<!--<script type="text/javascript" src="/wp-content/themes/manor/library/js/libs/jquery.slicknav.js"></script>---->

		<!---INIFIT SCROLL-
		<script type="text/javascript" src="/wp-content/themes/manor/library/js/libs/jquery.slicknav.js">	

		<!--- FONT AWESOME! ---->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

		<!-- Galleria -->
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/galleria/galleria-1.4.2.min.js"></script>

		<!-- Header -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


<!-- Galleria -->
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/galleria/galleria-1.4.2.min.js"></script>

		<!---VC-->
		<link rel="stylesheet" href="/wp-content/plugins/js_composer/assets/css/js_composer.css">

		<!-- Flexslider -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/js/libs/flexslider/flexslider.css" type="text/css">
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/flexslider/jquery.flexslider-min.js"></script>


		<?php if ( is_page(2849) ) { ?> 
			<script>
				jQuery(document).ready(function($) {
					$('.contact-link a').attr("href", "#ea-contact");
					
				});
			</script>
		<?php } ?>

		<script>
			jQuery(document).ready(function($) {
	
	
	
					/* ------ Flexslider ---------*/
					$('#carouselslider').flexslider({
				    animation: "slide",
				    controlNav: false,
				    animationLoop: false,
				    slideshow: false,
				    itemWidth: 260,
				    itemMargin: 20,
				    touch: true, 
				    asNavFor: '#imgslider',
				  
				  });
				 
				  $('#imgslider').flexslider({
				    animation: "slide",
				    controlNav: false,
				    animationLoop: false,
				    slideshow: false,
				    smoothHeight: true, 
				    touch: true, 
				    sync: "#carouselslider"
				  });
  
				});
			</script>

			<script>
			jQuery(document).ready(function($) {
	
	
	
					/* ------ Flexslider ---------*/
					$('#carouselslider2').flexslider({
				    animation: "slide",
				    controlNav: false,
				    animationLoop: false,
				    slideshow: false,
				    itemWidth: 188,
				    itemMargin: 20,
				    touch: true, 
				    asNavFor: '#imgslider2',
				  
				  });
				 
				  $('#imgslider2').flexslider({
				    animation: "slide",
				    controlNav: false,
				    animationLoop: false,
				    slideshow: false,
				    smoothHeight: true, 
				    touch: true, 
				    sync: "#carouselslider2"
				  });
  
				});
			</script>


		

		<script type="text/javascript">
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".header").addClass("scrolling");
    } else {
        $(".header").removeClass("scrolling");
    }
});


$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".toggle").addClass("scrolling");
    } else {
        $(".toggle").removeClass("scrolling");
    }
});





$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".toplink").addClass("toplinkcolor");
    } else {
        $(".toplink").removeClass("toplinkcolor");
    }
});

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {

        $(".toplink").addClass("toplinkcolor");
    } else {
        $(".toplink").removeClass("toplinkcolor");
    }



});




$(window).scroll(function () {
    if ($(document).scrollTop() >= 50) {
        
        $('.logoimg').attr('src', '/wp-content/themes/manor/images/logo-green.png');
    } else {
       
        $('.logoimg').attr('src', '/wp-content/themes/manor/images/logo-manor.png');
    }
}); 


		</script>






	</head>

	<?php if (is_page_template('page-property-print.php')) { ?><body <?php body_class(); ?> onLoad="window.print();window.close();"><?php } else {?>
	<body <?php body_class(); ?>>

	<?php } ?>
<!-- Google Tag Manager (PROLIFIK) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55ZZDTV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (OLD AGENCY)
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K8RRBJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K8RRBJ');</script>
End Google Tag Manager -->

		<div id="container">
<?php if (!is_page_template('page-property-print.php')) { ?>
			<header class="header" role="banner" id="topnavv" itemscope itemtype="http://schema.org/WPHeader">

				<div id="inner-header" class="wrap cf">

					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<div class="m-all t-all d-1of8"><div id="logo" class="" itemscope itemtype="http://schema.org/Organization"><a href="<?php echo home_url(); ?>" rel="nofollow"><span><img src="<?php echo home_url(); ?>/wp-content/themes/manor/images/logo-manor.png" class="logoimg"/></span><span><img src="<?php echo home_url(); ?>/wp-content/themes/manor/images/logo-manor-mobile.png" class="logomobile"/></span></a></div></div>

					<?php // if you'd like to use the site description you can un-comment it below ?>
					<?php // bloginfo('description'); ?>

					<div class="m-all t-all d-1of1 last-col">
					<?php if ( is_page(2849) ) { ?> 
					<div class="ea-number">Call (678) 400-6121</div>
					<?php } ?>
					<nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
						<?php // Responsive Menu Toggle ?>
						<input type="checkbox" id="toggle" />
						<label for="toggle" class="toggle" onclick></label>

						<?php wp_nav_menu(array(
    					         'container' => false,                           // remove nav container
    					         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
    					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
    					         'menu_class' => 'nav top-nav cf',               // adding custom nav class
    					         'theme_location' => 'main-nav',                 // where it's located in the theme
    					         'before' => '',                                 // before the menu
        			               'after' => '',                                  // after the menu
        			               'link_before' => '',                            // before each link
        			               'link_after' => '',                             // after each link
        			               'depth' => 0,                                   // limit the depth of the nav
    					         'fallback_cb' => ''                             // fallback function (if there is one)

						)); ?>

					</nav>
				</div>
				</div>

			</header>
			<?php } ?>