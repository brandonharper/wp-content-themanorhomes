<?php
/*
 Template Name: Area Map Touch
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<meta name="viewport" content="width=device-width,initial-scale=1" />

	



		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
		
		<!-- Galleria -->
		
 <style>
	  body.page-template-page-areamap-php {
		  background: none;
		  background-color: white;
	  }
	  body {
  -webkit-user-select: none;
     -moz-user-select: -moz-none;
      -ms-user-select: none;
          user-select: none;
}
	.st0{fill:#FFFFFF;stroke:#636466;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st1{fill:#CDDFD7;}
	.st2{fill:#82B19E;}
	.st3{fill:none;stroke:#CDDFD7;stroke-width:3;}
	.st4{fill:none;stroke:#CDDFD7;stroke-width:3;stroke-dasharray:10.1245,10.1245;}
	.st5{fill:none;stroke:#DCDDDE;stroke-linecap:round;stroke-linejoin:round;}
	.st6{fill:#000000;}
	.st7{fill:#F5F0E5;}
	.st8{fill:#C7D6EE;}
	.st9{fill:#AEC5E7;}
	.st10{fill:none;stroke:#DCDDDE;stroke-miterlimit:10;}
	.st11{fill:none;stroke:#DCDDDE;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;}
	.st12{fill:#FFFFFF;stroke:#DCDDDE;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;}
	.st13{fill:none;stroke:#DCDDDE;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;}
	.st14{fill:none;stroke:#DCDDDE;stroke-width:4.4517;stroke-linecap:round;stroke-linejoin:round;}
	.st15{fill:none;stroke:#DCDDDE;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;}
	.st16{fill:none;stroke:#C8B783;stroke-dasharray:6,3,2,1,2,3;}
	.st17{fill:none;stroke:#C8B783;}
	.st18{fill:none;stroke:#C8B783;stroke-dasharray:2.012,1.006,2.012,3.018;}
	.st19{fill:none;stroke:#C8B783;stroke-dasharray:6.036,3.018,2.012,1.006,2.012,3.018;}
	.st20{fill:none;stroke:#C8B783;stroke-dasharray:1.2072,0.6036,1.2072,1.8107;}
	.st21{fill:none;stroke:#C8B783;stroke-dasharray:1.9956,0.9978,1.9956,2.9934;}
	.st22{fill:none;stroke:#C8B783;stroke-dasharray:5.9868,2.9934,1.9956,0.9978,1.9956,2.9934;}
	.st23{fill:none;stroke:#C8B783;stroke-dasharray:1.9809,0.9905,1.9809,2.9714;}
	.st24{fill:none;stroke:#C8B783;stroke-dasharray:5.9427,2.9714,1.9809,0.9905,1.9809,2.9714;}
	.st25{fill:none;stroke:#C8B783;stroke-dasharray:2.0702,1.0351,2.0702,3.1053;}
	.st26{fill:none;stroke:#C8B783;stroke-dasharray:6.2107,3.1053,2.0702,1.0351,2.0702,3.1053;}
	.st27{fill:#939598;}
	.st28{display:none;}
	.st29{display:inline;fill:#BCBEC0;}
	.st30{font-family:'MyriadPro-Cond';}
	.st31{font-size:5.6437px;}
	.st32{fill:#FFFFFF;}
	.st33{fill:#BBA25A;}
	.st34{fill:#B76C31;}
	.st35{fill:#5D8FAE;}
	.st36{fill:#A788BE;}
	.st37{fill:#952E1F;}
	.st38{fill:#6AA28C;}
	.st39{fill:#DAE7E1;}
	.st40{fill:#9CC0B0;}
	.st41{fill:#6D6E71;}
	.st42{fill:#C8B783;}
	.st43{fill:#636466;}
	.st44{fill-rule:evenodd;clip-rule:evenodd;fill:#636466;stroke:#414042;stroke-linecap:round;stroke-linejoin:round;}
	.st45{fill:#636466;stroke:#414042;stroke-linecap:round;stroke-linejoin:round;}
	.st46{fill:#414042;stroke:#414042;stroke-linecap:round;stroke-linejoin:round;}
	.st47{fill:#77787B;stroke:#414042;stroke-linecap:round;stroke-linejoin:round;}
	.st48{fill:#905501;}
	.st49{fill:#C3956D;stroke:#905501;stroke-width:0.3674;stroke-miterlimit:10;}
	.st50{fill:#FFFFFF;stroke:#905501;stroke-width:0.3674;stroke-miterlimit:10;}
	.st51{fill:none;stroke:#C8B783;stroke-width:1.8881;stroke-miterlimit:10;}
	.st52{opacity:0.45;}
	.st53{fill:#F1F2F2;}
	.st54{fill:#952E1F;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;}
	.st55{fill:#A788BE;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;}
	.st56{fill:#5D8FAE;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;}
	.st57{fill:#B76C31;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;}
	.st58{fill:#BBA25A;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;}
	.st59{display:none;opacity:0.3;}
	.st60{display:inline;}
	.st61{clip-path:url(#SVGID_2_);}
	.st62{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;}
	.st63{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:1.986,0.993,1.986,2.979;}
	.st64{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:5.957,2.979,1.986,0.993,1.986,2.979;}
	.st65{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:1.493,0.746,1.493,2.239;}
	.st66{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:2.041,1.02,2.041,3.061;}
	.st67{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:6.123,3.061,2.041,1.02,2.041,3.061;}
	.st68{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:1.848,0.924,1.848,2.772;}
	.st69{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:5.544,2.772,1.848,0.924,1.848,2.772;}
	.st70{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:1.875,0.938,1.875,2.813;}
	.st71{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:5.626,2.813,1.875,0.938,1.875,2.813;}
	.st72{clip-path:url(#SVGID_4_);fill:none;stroke:#C8B783;stroke-dasharray:6,3,2,1,2,3;}
	.st73{clip-path:url(#SVGID_4_);fill:#C0D8CC;}
	.st74{clip-path:url(#SVGID_4_);fill:#C1D7CD;}
	.st75{clip-path:url(#SVGID_4_);}
	.st76{clip-path:url(#SVGID_6_);fill:#C0D8CC;}
	.st77{clip-path:url(#SVGID_6_);fill:#C1D7CD;}
	.st78{clip-path:url(#SVGID_8_);fill:#F3ECDE;}
	.st79{clip-path:url(#SVGID_4_);fill:#ABCCEB;}
	.st80{fill:#60A7CD;}
	.st81{font-size:4.4423px;}
	.st82{clip-path:url(#SVGID_10_);fill:#ABCCEB;}
	.st83{opacity:0.75;clip-path:url(#SVGID_10_);}
	.st84{clip-path:url(#SVGID_12_);fill:#8FB3DE;}
	.st85{clip-path:url(#SVGID_10_);fill:#8FB3DE;}
	.st86{clip-path:url(#SVGID_4_);fill:none;stroke:#D0D2D3;stroke-linecap:round;stroke-linejoin:round;}
	.st87{clip-path:url(#SVGID_14_);fill:none;stroke:#D0D2D3;stroke-linecap:round;stroke-linejoin:round;}
	.st88{clip-path:url(#SVGID_14_);fill:none;stroke:#D0D2D3;stroke-miterlimit:10;}
	.st89{clip-path:url(#SVGID_4_);fill:none;stroke:#DCDBDA;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;}
	.st90{clip-path:url(#SVGID_16_);fill:none;stroke:#DCDBDA;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;}
	.st91{clip-path:url(#SVGID_4_);fill:none;stroke:#DCDBDA;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;}
	.st92{clip-path:url(#SVGID_18_);fill:none;stroke:#DCDBDA;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;}
	.st93{clip-path:url(#SVGID_18_);fill:none;stroke:#DCDBDA;stroke-width:4.452;stroke-linecap:round;stroke-linejoin:round;}
	.st94{clip-path:url(#SVGID_4_);fill:none;stroke:#DBDBD9;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;}
	.st95{clip-path:url(#SVGID_20_);fill:none;stroke:#DBDBD9;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;}
	.st96{fill:#D1D1D1;}
	.st97{font-size:5.3308px;}
	.st98{font-size:5.3307px;}
	.st99{clip-path:url(#SVGID_4_);fill:#D1D1D1;}
	.st100{fill:#DBDBD9;}
	.st101{font-size:6.2192px;}
	.st102{font-size:6.2193px;}
	.st103{fill:#B4B4B5;}
	.st104{font-family:'MyriadPro-BoldCond';}
	.st105{font-size:6.2191px;}
	.st106{clip-path:url(#SVGID_4_);fill:#FFFFFF;}
	.st107{clip-path:url(#SVGID_22_);fill:#C8B783;}
	.st108{clip-path:url(#SVGID_24_);fill:#C8B783;}
	.st109{clip-path:url(#SVGID_26_);fill:#C8B783;}
	.st110{clip-path:url(#SVGID_28_);fill:#C8B783;}
	.st111{clip-path:url(#SVGID_30_);fill:#C8B783;}
	.st112{clip-path:url(#SVGID_32_);fill:#C8B783;}
	.st113{clip-path:url(#SVGID_34_);fill:#C8B783;}
	.st114{clip-path:url(#SVGID_36_);fill:#C8B783;}
	.st115{clip-path:url(#SVGID_38_);fill:#C8B783;}
	.st116{clip-path:url(#SVGID_40_);fill:#C8B783;}
	.st117{clip-path:url(#SVGID_42_);fill:#C8B783;}
	.st118{clip-path:url(#SVGID_44_);fill:#C8B783;}
	.st119{clip-path:url(#SVGID_46_);fill:#C8B783;}
	.st120{clip-path:url(#SVGID_48_);fill:#C8B783;}
	.st121{font-size:7.1077px;}
	.st122{clip-path:url(#SVGID_50_);fill:none;stroke:#C8B783;stroke-width:1.5;stroke-miterlimit:10;}
	.st123{clip-path:url(#SVGID_52_);fill:#B46628;}
	.st124{clip-path:url(#SVGID_54_);fill:#B46628;}
	.st125{clip-path:url(#SVGID_56_);fill:#B46628;}
	.st126{clip-path:url(#SVGID_58_);fill:#B46628;}
	.st127{clip-path:url(#SVGID_60_);fill:#B46628;}
	.st128{clip-path:url(#SVGID_62_);fill:#B46628;}
	.st129{clip-path:url(#SVGID_64_);fill:#B46628;}
	.st130{fill:#B46628;}
	.st131{clip-path:url(#SVGID_66_);fill:#558BAB;}
	.st132{clip-path:url(#SVGID_68_);fill:#558BAB;}
	.st133{clip-path:url(#SVGID_70_);fill:#558BAB;}
	.st134{clip-path:url(#SVGID_72_);fill:#558BAB;}
	.st135{clip-path:url(#SVGID_74_);fill:#558BAB;}
	.st136{clip-path:url(#SVGID_76_);fill:#558BAB;}
	.st137{clip-path:url(#SVGID_78_);fill:#558BAB;}
	.st138{clip-path:url(#SVGID_80_);fill:#558BAB;}
	.st139{clip-path:url(#SVGID_82_);fill:#558BAB;}
	.st140{clip-path:url(#SVGID_84_);fill:#558BAB;}
	.st141{clip-path:url(#SVGID_86_);fill:#558BAB;}
	.st142{clip-path:url(#SVGID_88_);fill:#558BAB;}
	.st143{fill:#558BAB;}
	.st144{clip-path:url(#SVGID_90_);fill:#A483BC;}
	.st145{clip-path:url(#SVGID_92_);fill:#A483BC;}
	.st146{clip-path:url(#SVGID_94_);fill:#A483BC;}
	.st147{clip-path:url(#SVGID_96_);fill:#A483BC;}
	.st148{clip-path:url(#SVGID_98_);fill:#A483BC;}
	.st149{fill:#A483BC;}
	.st150{clip-path:url(#SVGID_100_);fill:#901C1C;}
	.st151{clip-path:url(#SVGID_102_);fill:#901C1C;}
	.st152{clip-path:url(#SVGID_104_);fill:#901C1C;}
	.st153{clip-path:url(#SVGID_106_);fill:#901C1C;}
	.st154{clip-path:url(#SVGID_108_);fill:#901C1C;}
	.st155{clip-path:url(#SVGID_110_);fill:#901C1C;}
	.st156{clip-path:url(#SVGID_112_);fill:#901C1C;}
	.st157{clip-path:url(#SVGID_114_);fill:#901C1C;}
	.st158{clip-path:url(#SVGID_116_);fill:#901C1C;}
	.st159{clip-path:url(#SVGID_118_);fill:#901C1C;}
	.st160{clip-path:url(#SVGID_120_);fill:#901C1C;}
	.st161{clip-path:url(#SVGID_122_);fill:#901C1C;}
	.st162{clip-path:url(#SVGID_124_);fill:#901C1C;}
	.st163{clip-path:url(#SVGID_126_);fill:#901C1C;}
	.st164{clip-path:url(#SVGID_128_);fill:#901C1C;}
	.st165{clip-path:url(#SVGID_130_);fill:#901C1C;}
	.st166{clip-path:url(#SVGID_132_);fill:#901C1C;}
	.st167{clip-path:url(#SVGID_134_);fill:#901C1C;}
	.st168{clip-path:url(#SVGID_136_);fill:#901C1C;}
	.st169{clip-path:url(#SVGID_138_);fill:#901C1C;}
	.st170{clip-path:url(#SVGID_140_);fill:#901C1C;}
	.st171{clip-path:url(#SVGID_142_);fill:#901C1C;}
	.st172{clip-path:url(#SVGID_144_);fill:#901C1C;}
	.st173{clip-path:url(#SVGID_146_);fill:#901C1C;}
	.st174{clip-path:url(#SVGID_148_);fill:#901C1C;}
	.st175{clip-path:url(#SVGID_150_);fill:#901C1C;}
	.st176{clip-path:url(#SVGID_152_);fill:#901C1C;}
	.st177{clip-path:url(#SVGID_154_);fill:#901C1C;}
	.st178{clip-path:url(#SVGID_156_);fill:#901C1C;}
	.st179{clip-path:url(#SVGID_158_);fill:#901C1C;}
	.st180{clip-path:url(#SVGID_160_);fill:#901C1C;}
	.st181{clip-path:url(#SVGID_162_);fill:#901C1C;}
	.st182{clip-path:url(#SVGID_164_);fill:#901C1C;}
	.st183{clip-path:url(#SVGID_166_);fill:#901C1C;}
	.st184{clip-path:url(#SVGID_168_);fill:#901C1C;}
	.st185{fill:#901C1C;}
	.st186{clip-path:url(#SVGID_4_);fill:#901C1C;}
	.st187{font-size:19.5461px;}
	.st188{letter-spacing:1;}
	.st189{font-size:10.6615px;}
	.st190{letter-spacing:2;}
	.st191{fill:#67696B;}
	.st192{clip-path:url(#SVGID_170_);}
	.st193{clip-path:url(#SVGID_172_);fill:#FFFFFF;}
	.st194{clip-path:url(#SVGID_172_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st195{fill:#5C5D5F;}
	.st196{font-size:7.1075px;}
	.st197{clip-path:url(#SVGID_174_);}
	.st198{clip-path:url(#SVGID_176_);fill:#FFFFFF;}
	.st199{clip-path:url(#SVGID_176_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st200{clip-path:url(#SVGID_178_);}
	.st201{clip-path:url(#SVGID_180_);fill:#FFFFFF;}
	.st202{clip-path:url(#SVGID_180_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st203{clip-path:url(#SVGID_182_);}
	.st204{clip-path:url(#SVGID_184_);fill:#FFFFFF;}
	.st205{clip-path:url(#SVGID_184_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st206{clip-path:url(#SVGID_186_);}
	.st207{clip-path:url(#SVGID_188_);fill:#FFFFFF;}
	.st208{clip-path:url(#SVGID_188_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st209{clip-path:url(#SVGID_190_);}
	.st210{clip-path:url(#SVGID_192_);fill:#FFFFFF;}
	.st211{clip-path:url(#SVGID_192_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st212{clip-path:url(#SVGID_194_);}
	.st213{clip-path:url(#SVGID_196_);fill:#FFFFFF;}
	.st214{clip-path:url(#SVGID_196_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st215{clip-path:url(#SVGID_198_);}
	.st216{clip-path:url(#SVGID_200_);fill:#FFFFFF;}
	.st217{clip-path:url(#SVGID_200_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st218{clip-path:url(#SVGID_202_);}
	.st219{clip-path:url(#SVGID_204_);fill:#FFFFFF;}
	.st220{clip-path:url(#SVGID_204_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st221{clip-path:url(#SVGID_206_);}
	.st222{clip-path:url(#SVGID_208_);fill:#FFFFFF;}
	.st223{clip-path:url(#SVGID_208_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st224{clip-path:url(#SVGID_210_);}
	.st225{clip-path:url(#SVGID_212_);fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
	.st226{clip-path:url(#SVGID_212_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st227{clip-path:url(#SVGID_214_);}
	.st228{clip-path:url(#SVGID_216_);fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
	.st229{clip-path:url(#SVGID_216_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st230{clip-path:url(#SVGID_218_);}
	.st231{clip-path:url(#SVGID_220_);fill:#FFFFFF;}
	.st232{clip-path:url(#SVGID_220_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st233{clip-path:url(#SVGID_222_);}
	.st234{clip-path:url(#SVGID_224_);fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
	.st235{clip-path:url(#SVGID_224_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st236{clip-path:url(#SVGID_226_);}
	.st237{clip-path:url(#SVGID_228_);fill:#FFFFFF;}
	.st238{clip-path:url(#SVGID_228_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st239{clip-path:url(#SVGID_230_);}
	.st240{clip-path:url(#SVGID_232_);fill:#FFFFFF;}
	.st241{clip-path:url(#SVGID_232_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st242{clip-path:url(#SVGID_234_);}
	.st243{clip-path:url(#SVGID_236_);fill:#FFFFFF;}
	.st244{clip-path:url(#SVGID_236_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st245{clip-path:url(#SVGID_238_);}
	.st246{clip-path:url(#SVGID_240_);fill:#FFFFFF;}
	.st247{clip-path:url(#SVGID_240_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st248{clip-path:url(#SVGID_242_);}
	.st249{clip-path:url(#SVGID_244_);fill:#FFFFFF;}
	.st250{clip-path:url(#SVGID_244_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st251{clip-path:url(#SVGID_246_);}
	.st252{clip-path:url(#SVGID_248_);fill:#FFFFFF;}
	.st253{clip-path:url(#SVGID_248_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st254{clip-path:url(#SVGID_250_);}
	.st255{clip-path:url(#SVGID_252_);fill:#FFFFFF;}
	.st256{clip-path:url(#SVGID_252_);fill:none;stroke:#5C5D5F;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;}
	.st257{fill:#885121;}
	.st258{clip-path:url(#SVGID_4_);fill:#C19165;}
	.st259{clip-path:url(#SVGID_4_);fill:none;stroke:#885121;stroke-width:0.367;stroke-miterlimit:10;}
	.st260{clip-path:url(#SVGID_4_);fill:#C8B783;}
	.st261{clip-path:url(#SVGID_4_);fill:#B46628;}
	.st262{clip-path:url(#SVGID_4_);fill:#A483BC;}
	.st263{clip-path:url(#SVGID_4_);fill:#558BAB;}
	.st264{fill:#070204;}
	.st265{clip-path:url(#SVGID_254_);fill:none;stroke:#C8B783;stroke-width:1.888;stroke-miterlimit:10;}
	.st266{font-family:'Avenir-Black';}
	.st267{font-size:13.415px;}
	.st268{clip-path:url(#SVGID_256_);fill:#C8B783;}
	.st269{opacity:0.75;clip-path:url(#SVGID_256_);}
	.st270{clip-path:url(#SVGID_258_);fill:none;stroke:#D0D2D3;stroke-miterlimit:10;}
	.st271{clip-path:url(#SVGID_256_);fill:none;stroke:#D0D2D3;stroke-miterlimit:10;}
	.st272{clip-path:url(#SVGID_256_);fill:#8FB3DE;}
	
	
	#box-shopping, #box-dining, #box-hotel, #box-schools, #box-spa {
		cursor: pointer !important;
	}

	
  #burj { position: relative;

width: 100%; padding-bottom: 167%;

vertical-align: middle; margin: 0; overflow: hidden; }

#burj svg { display: inline-block;

position: absolute; top: 0; left: 0; }

.map-container {
	width: 100%;
	height: auto;
	overflow: hidden;
	position: relative;
}
.map {
	width: 100%;
	height: auto;
	position: relative;
	margin: 0 auto;
	#toggle-fox {
		/*position: absolute;
		top: 1118px;
		left: 305px;
		width: 17px;
		height: 19px;*/
		
		cursor: pointer;
	}
}
  </style>

  <script src="<?php echo get_template_directory_uri(); ?>/svg.min.js"></script>

  <script>
	  // ######## Add addClass/removeClass support to SVG ############
	  (function($){
	  	var addClass = $.fn.addClass;
	  	$.fn.addClass = function(value) {
	  		var orig = addClass.apply(this, arguments);
	  		
	  		var elem,
	  		i = 0,
	  		len = this.length;
	  		
	  		for (; i < len; i++ ) {
	  			elem = this[ i ];
	  			if ( elem instanceof SVGElement ) {
	  				var classes = $(elem).attr('class');
	  				if ( classes ) {
	  					var index = classes.search(value);
	  					if (index === -1) {
	  						classes = classes + " " + value;
	  						$(elem).attr('class', classes);
	  					}
	  				} else {
	  					$(elem).attr('class', value);
	  				}
	  			}
	  		}
	  		return orig;
	  	};
	  	
	  	var removeClass = $.fn.removeClass;
	  	$.fn.removeClass = function(value) {
	  		var orig = removeClass.apply(this, arguments);
	  		
	  		var elem,
	  		i = 0,
	  		len = this.length;
	  		
	  		for (; i < len; i++ ) {
	  			elem = this[ i ];
	  			if ( elem instanceof SVGElement ) {
	  				var classes = $(elem).attr('class');
	  				if ( classes ) {
	  					var index = classes.search(value);
	  					if (index !== -1) {
	  						classes = classes.substring(0, index) + classes.substring((index + value.length), classes.length);
	  						$(elem).attr('class', classes);
	  					}
	  				}
	  			}
	  		}
	  		return orig;
	  	};
	  })(jQuery);	
	  
	  
	  
	  	jQuery(document).ready(function($) {
	  					
		  					
	  		
	  		var shopping = 0;
	  		var dining = 0;
	  		var hotel = 0;
	  		var schools = 0;
	  		var spa = 0;
	  		
	  		
	  		$("#box-shopping").click(function () {
		  		
		  		var shopping_box = SVG.get("Shopping");
		  		var shopping_pin = SVG.get("shopping_pin");
		  		
		  		if (shopping == 0 ) {
			  		shopping = 1;
			  		shopping_box.opacity(0.3);
			  		shopping_pin.opacity(0.3);
			  		$("#Shopping_POI").hide();
		  		} else {
			  		shopping = 0;
			  		shopping_box.opacity(1);
			  		shopping_pin.opacity(1);
			  		$("#Shopping_POI").show();
		  		}

		  
		  	});
		  	
		  	$("#box-dining").click(function () {
			  	var dining_box = SVG.get("Dining");
		  		var dining_pin = SVG.get("dining_pin");
		  		
		  		if (dining== 0 ) {
			  		dining = 1;
			  		dining_box.opacity(0.3);
			  		dining_pin.opacity(0.3);
			  		$("#Dining_POI").hide();
		  		} else {
			  		dining = 0;
			  		dining_box.opacity(1);
			  		dining_pin.opacity(1);
			  		$("#Dining_POI").show();
		  		}
			});	
			
			$("#box-hotel").click(function () {
			  	var hotel_box = SVG.get("Hotel");
		  		var hotel_pin = SVG.get("hotel_pin");
		  		
		  		if (hotel == 0 ) {
			  		hotel = 1;
			  		hotel_box.opacity(0.3);
			  		hotel_pin.opacity(0.3);
			  		$("#Hotels_POI").hide();
		  		} else {
			  		hotel = 0;
			  		hotel_box.opacity(1);
			  		hotel_pin.opacity(1);
			  		$("#Hotels_POI").show();
		  		}
			});	
			
			$("#box-schools").click(function () {
			  	var schools_box = SVG.get("Schools");
		  		var schools_pin = SVG.get("schools_pin");
		  		
		  		if (schools == 0 ) {
			  		schools = 1;
			  		schools_box.opacity(0.3);
			  		schools_pin.opacity(0.3);
			  		$("#Schools_POI").hide();
		  		} else {
			  		schools = 0;
			  		schools_box.opacity(1);
			  		schools_pin.opacity(1);
			  		$("#Schools_POI").show();
		  		}
			});	
			
			$("#box-spa").click(function () {
			  	var spa_box = SVG.get("Spa_x2F_Salon");
		  		var spa_pin = SVG.get("spa_pin");
		  		
		  		if (spa == 0 ) {
			  		spa = 1;
			  		spa_box.opacity(0.3);
			  		spa_pin.opacity(0.3);
			  		$("#Salon_x2F_Spa_POI").hide();
		  		} else {
			  		spa = 0;
			  		spa_box.opacity(1);
			  		spa_pin.opacity(1);
			  		$("#Salon_x2F_Spa_POI").show();
		  		}
			});	

		});
	  </script>
	  
	  
	  
	</head>
	
	<body <?php body_class(); ?>>
	

	
			
			<div class="map-container">
				
				<div class="map">
						
					<figure id=burj>
							
						<svg  version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1080 1806.9" preserveAspectRatio="xMinYMin meet" >
							<image id="map-img" width="1080" height="1806.9" xlink:href="<?php echo get_template_directory_uri(); ?>/area_map.svg"></image>
							
							
							
							<!---- DOTS --->
							<g id="Shopping_POI">
								<polygon class="st32" points="229.8,1327.6 213.8,1325.4 215.1,1308.8 229.8,1308.8 	"/>
								<g id="Shop_Dots">
									<g id="shopping_1">
										<g>
											<circle class="st33" cx="690.2" cy="392" r="8.5"/>
											<g>
												<path class="st32" d="M689.8,389.7L689.8,389.7l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V389.7z"/>
											</g>
										</g>
									</g>
									<g id="shopping_2">
										<g>
											<circle class="st33" cx="625.1" cy="546.4" r="8.5"/>
											<g>
												<path class="st32" d="M623.1,551.1v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
													c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H623.1z"/>
											</g>
										</g>
									</g>
									<g id="shopping_3">
										<g>
											<circle class="st33" cx="711.7" cy="643.9" r="8.5"/>
											<g>
												<path class="st32" d="M709.6,646.9c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
													c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
													c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L709.6,646.9z"/>
											</g>
										</g>
									</g>
									<g id="shopping_4">
										<g>
											<circle class="st33" cx="449" cy="1064.9" r="8.5"/>
											<g>
												<path class="st32" d="M449.3,1069.6v-2h-2.9v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H449.3z M449.3,1066.2v-1.8
													c0-0.5,0.1-1,0.1-1.4h-0.1c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H449.3z"/>
											</g>
										</g>
									</g>
									<g id="shopping_5">
										<circle class="st33" cx="437.4" cy="1083.3" r="8.5"/>
										<g>
											<path class="st32" d="M439.5,1080.5h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1080.5z"/>
										</g>
									</g>
									<g id="shopping_6">
										<g>
											<circle class="st33" cx="363.4" cy="1099.2" r="8.5"/>
											<g>
												<path class="st32" d="M365.8,1096.7c-0.2,0-0.4,0-0.7,0.1c-1.1,0.2-1.7,1.2-1.9,2.3h0c0.3-0.4,0.8-0.7,1.3-0.7
													c1.2,0,1.9,1,1.9,2.7c0,1.5-0.8,3-2.4,3c-1.6,0-2.6-1.4-2.6-3.5c0-1.9,0.6-3.1,1.4-4c0.6-0.6,1.4-1,2.1-1.2
													c0.4-0.1,0.7-0.1,0.9-0.1V1096.7z M364.7,1101c0-0.7-0.2-1.4-0.8-1.4c-0.3,0-0.5,0.2-0.7,0.6c-0.1,0.1-0.1,0.4-0.1,0.8
													c0,1.1,0.3,1.9,0.8,1.9C364.5,1102.8,364.7,1102,364.7,1101z"/>
											</g>
										</g>
									</g>
									<g id="shopping_7">
										<circle class="st33" cx="338" cy="1115.7" r="8.5"/>
										<g>
											<path class="st32" d="M340.9,1111.3v1.1l-2.7,7.4h-1.7l2.6-7v0h-2.9v-1.5H340.9z"/>
										</g>
									</g>
									<g id="shopping_8">
										<circle class="st33" cx="405.2" cy="1110.5" r="8.5"/>
										<g>
											<path class="st32" d="M402.7,1113c0-1.1,0.6-1.8,1.3-2.3v0c-0.7-0.5-1.1-1.2-1.1-1.9c0-1.4,1.1-2.3,2.4-2.3
												c1.2,0,2.2,0.8,2.2,2.1c0,0.6-0.2,1.3-1.1,1.9v0c0.8,0.4,1.3,1.2,1.3,2.2c0,1.6-1.2,2.5-2.6,2.5
												C403.8,1115.3,402.7,1114.4,402.7,1113z M406.1,1112.9c0-0.8-0.5-1.3-1-1.6c-0.4,0.3-0.7,0.8-0.7,1.5c0,0.7,0.3,1.4,0.9,1.4
												C405.8,1114.2,406.1,1113.6,406.1,1112.9z M404.6,1108.7c0,0.6,0.3,1,0.8,1.3c0.4-0.2,0.6-0.7,0.6-1.2c0-0.6-0.2-1.1-0.7-1.1
												S404.6,1108.2,404.6,1108.7z"/>
										</g>
									</g>
									<g id="shopping_9">
										<circle class="st33" cx="402.4" cy="1138.8" r="8.5"/>
										<g>
											<path class="st32" d="M400.1,1141.6c0.3,0,0.5,0,0.8-0.1c0.3-0.1,0.7-0.2,1-0.5c0.4-0.4,0.7-1,0.8-1.7h0
												c-0.3,0.3-0.7,0.5-1.2,0.5c-1.1,0-2-1-2-2.6c0-1.5,0.9-3,2.6-3c1.6,0,2.4,1.4,2.4,3.5c0,1.9-0.6,3.3-1.4,4.1
												c-0.5,0.6-1.3,0.9-2.1,1.1c-0.3,0-0.5,0-0.8,0V1141.6z M401.2,1137.1c0,0.8,0.3,1.5,0.8,1.5c0.3,0,0.6-0.2,0.7-0.4
												c0.1-0.2,0.1-0.3,0.1-0.9c0-1.1-0.3-1.8-0.8-1.8C401.5,1135.5,401.2,1136.1,401.2,1137.1z"/>
										</g>
									</g>
									<g id="shopping_10">
										<circle class="st33" cx="360.6" cy="1150.5" r="8.5"/>
										<g>
											<path class="st32" d="M357.3,1148.2L357.3,1148.2l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1148.2z"/>
											<path class="st32" d="M365.9,1150.9c0,3.1-1.1,4.4-2.5,4.4c-1.9,0-2.5-2.1-2.5-4.3c0-2.5,0.8-4.4,2.6-4.4
												C365.4,1146.5,365.9,1148.8,365.9,1150.9z M362.6,1150.9c0,2,0.2,2.9,0.8,2.9c0.5,0,0.7-0.9,0.7-2.9c0-1.8-0.2-2.9-0.7-2.9
												C362.9,1148,362.6,1148.9,362.6,1150.9z"/>
										</g>
									</g>
									<g id="shopping_11">
										<circle class="st33" cx="490.8" cy="1153.8" r="8.5"/>
										<g>
											<path class="st32" d="M487.6,1151.5L487.6,1151.5l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1151.5z"/>
											<path class="st32" d="M493.2,1151.5L493.2,1151.5l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1151.5z"/>
										</g>
									</g>
									<g id="shopping_12">
										<circle class="st33" cx="466.9" cy="1185.8" r="8.5"/>
										<g>
											<path class="st32" d="M463.6,1183.5L463.6,1183.5l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1183.5z"/>
											<path class="st32" d="M467.2,1190.5v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H467.2z"/>
										</g>
									</g>
									<g id="shopping_13">
										<circle class="st33" cx="496.5" cy="1182" r="8.5"/>
										<g>
											<path class="st32" d="M493.3,1179.8L493.3,1179.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1179.8z"/>
											<path class="st32" d="M497.1,1185.1c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L497.1,1185.1z"/>
										</g>
									</g>
									<g id="shopping_14">
										<circle class="st33" cx="358.7" cy="1384.2" r="8.5"/>
										<g>
											<path class="st32" d="M355.5,1382L355.5,1382l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1382z"/>
											<path class="st32" d="M361.8,1388.9v-2h-2.9v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H361.8z M361.8,1385.5v-1.8
												c0-0.5,0.1-1,0.1-1.4h-0.1c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H361.8z"/>
										</g>
									</g>
									<g id="shopping_15">
										<circle class="st33" cx="381.7" cy="1395" r="8.5"/>
										<g>
											<path class="st32" d="M378.5,1392.8L378.5,1392.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1392.8z"/>
											<path class="st32" d="M386.6,1392.7h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1392.7z"/>
										</g>
									</g>
									<g id="shopping_16">
										<circle class="st33" cx="213.5" cy="1624.1" r="8.5"/>
										<g>
											<path class="st32" d="M210.3,1621.8L210.3,1621.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1621.8z"/>
											<path class="st32" d="M218.2,1621.5c-0.2,0-0.4,0-0.7,0.1c-1.1,0.2-1.7,1.2-1.9,2.3h0c0.3-0.4,0.8-0.7,1.3-0.7
												c1.2,0,1.9,1,1.9,2.7c0,1.5-0.8,3-2.4,3c-1.6,0-2.6-1.4-2.6-3.5c0-1.9,0.6-3.1,1.4-4c0.6-0.6,1.4-1,2.1-1.2
												c0.4-0.1,0.7-0.1,0.9-0.1V1621.5z M217.2,1625.8c0-0.7-0.2-1.4-0.8-1.4c-0.3,0-0.5,0.2-0.7,0.6c-0.1,0.1-0.1,0.4-0.1,0.8
												c0,1.1,0.3,1.9,0.8,1.9C217,1627.6,217.2,1626.9,217.2,1625.8z"/>
										</g>
									</g>
									<g id="shopping_17">
										<circle class="st33" cx="107.1" cy="1567.2" r="8.5"/>
										<g>
											<path class="st32" d="M103.8,1564.9L103.8,1564.9l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1564.9z"/>
											<path class="st32" d="M112.3,1563.3v1.1l-2.7,7.4h-1.7l2.6-7v0h-2.9v-1.5H112.3z"/>
										</g>
									</g>
									<g id="shopping_18">
										<circle class="st33" cx="96.2" cy="1637.2" r="8.5"/>
										<g>
											<path class="st32" d="M93,1635L93,1635l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6H93V1635z"/>
											<path class="st32" d="M96.5,1639.7c0-1.1,0.6-1.8,1.3-2.3v0c-0.7-0.5-1.1-1.2-1.1-1.9c0-1.4,1.1-2.3,2.4-2.3
												c1.2,0,2.2,0.8,2.2,2.1c0,0.6-0.2,1.3-1.1,1.9v0c0.8,0.4,1.3,1.2,1.3,2.2c0,1.6-1.2,2.5-2.6,2.5
												C97.6,1642.1,96.5,1641.1,96.5,1639.7z M99.9,1639.6c0-0.8-0.5-1.3-1-1.6c-0.4,0.3-0.7,0.8-0.7,1.5c0,0.7,0.3,1.4,0.9,1.4
												C99.6,1640.9,99.9,1640.3,99.9,1639.6z M98.4,1635.5c0,0.6,0.3,1,0.8,1.3c0.4-0.2,0.6-0.7,0.6-1.2c0-0.6-0.2-1.1-0.7-1.1
												S98.4,1635,98.4,1635.5z"/>
										</g>
									</g>
									<g id="shopping_19">
										<circle class="st33" cx="78.1" cy="1685.4" r="8.5"/>
										<g>
											<path class="st32" d="M73.9,1682.8L73.9,1682.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1682.8z"/>
											<path class="st32" d="M78.2,1688.4c0.3,0,0.5,0,0.8-0.1c0.3-0.1,0.7-0.2,1-0.5c0.4-0.4,0.7-1,0.8-1.7h0
												c-0.3,0.3-0.7,0.5-1.2,0.5c-1.1,0-2-1-2-2.6c0-1.5,0.9-3,2.6-3c1.6,0,2.4,1.4,2.4,3.5c0,1.9-0.6,3.3-1.4,4.1
												c-0.5,0.6-1.3,0.9-2.1,1.1c-0.3,0-0.5,0-0.8,0V1688.4z M79.2,1684c0,0.8,0.3,1.5,0.8,1.5c0.3,0,0.6-0.2,0.7-0.4
												c0.1-0.2,0.1-0.3,0.1-0.9c0-1.1-0.3-1.8-0.8-1.8C79.5,1682.3,79.2,1682.9,79.2,1684z"/>
										</g>
									</g>
									<g id="shopping_20">
										<g>
											<circle class="st33" cx="914" cy="124.1" r="8.5"/>
											<g>
												<path class="st32" d="M908.7,128.7v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
													c0.4-0.2,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H908.7z"/>
												<path class="st32" d="M919.3,124.4c0,3.1-1.1,4.4-2.5,4.4c-1.9,0-2.5-2.1-2.5-4.3c0-2.5,0.8-4.4,2.6-4.4
													C918.9,120.1,919.3,122.4,919.3,124.4z M916.1,124.5c0,2,0.2,2.9,0.8,2.9c0.5,0,0.7-0.9,0.7-2.9c0-1.8-0.2-2.9-0.7-2.9
													C916.3,121.5,916,122.5,916.1,124.5z"/>
											</g>
										</g>
									</g>
								</g>
							</g>
							<g id="Salon_x2F_Spa_POI">
								<g id="Spa_Dots">
									<g id="spa_1">
										<circle class="st34" cx="713.2" cy="389.3" r="8.5"/>
										<g>
											<path class="st32" d="M712.9,386.9L712.9,386.9l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V386.9z"/>
										</g>
									</g>
									<g id="spa_2">
										<circle class="st34" cx="440.5" cy="879.1" r="8.5"/>
										<g>
											<path class="st32" d="M438.2,883.5v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H438.2z"/>
										</g>
									</g>
									<g id="spa_3">
										<circle class="st34" cx="355.2" cy="1082.9" r="8.5"/>
										<g>
											<path class="st32" d="M353.2,1085.4c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L353.2,1085.4z"/>
										</g>
									</g>
									<g id="spa_4">
										<circle class="st34" cx="419.2" cy="1168.2" r="8.5"/>
										<g>
											<path class="st32" d="M419.2,1173.1v-2h-2.9v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H419.2z M419.2,1169.6v-1.8
												c0-0.5,0.1-1,0.1-1.4h-0.1c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H419.2z"/>
										</g>
									</g>
									<g id="spa_5">
										<circle class="st34" cx="571.6" cy="1173.2" r="8.5"/>
										<g>
											<path class="st32" d="M573.8,1170.2h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1170.2z"/>
										</g>
									</g>
								</g>
							</g>
							<g id="Schools_POI">
								<g id="Sch_Dots">
									<g id="school_1">
										<circle class="st35" cx="50.7" cy="869.1" r="8.5"/>
										<g>
											<path class="st32" d="M50.2,866.8L50.2,866.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V866.8z"/>
										</g>
									</g>
									<g id="school_2">
										<circle class="st35" cx="357.1" cy="848.4" r="8.5"/>
										<g>
											<path class="st32" d="M354.2,852.5v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H354.2z"/>
										</g>
									</g>
									<g id="school_3">
										<circle class="st35" cx="402.9" cy="838.4" r="8.5"/>
										<g>
											<path class="st32" d="M400.8,840.8c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L400.8,840.8z"/>
										</g>
									</g>
									<g id="school_4">
										<circle class="st35" cx="327.6" cy="1060.3" r="8.5"/>
										<g>
											<path class="st32" d="M328.1,1064.4v-2h-2.9v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H328.1z M328.1,1061v-1.8c0-0.5,0.1-1,0.1-1.4
												h-0.1c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H328.1z"/>
										</g>
									</g>
									<g id="school_5">
										<circle class="st35" cx="291.9" cy="1155" r="8.5"/>
										<g>
											<path class="st32" d="M294.1,1152h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1152z"/>
										</g>
									</g>
									<g id="school_6">
										<circle class="st35" cx="163.5" cy="1597.1" r="8.5"/>
										<g>
											<path class="st32" d="M164.8,1594.5c-0.2,0-0.4,0-0.7,0.1c-1.1,0.2-1.7,1.2-1.9,2.3h0c0.3-0.4,0.8-0.7,1.3-0.7
												c1.2,0,1.9,1,1.9,2.7c0,1.5-0.8,3-2.4,3c-1.6,0-2.6-1.4-2.6-3.5c0-1.9,0.6-3.1,1.4-4c0.6-0.6,1.4-1,2.1-1.2
												c0.4-0.1,0.7-0.1,0.9-0.1V1594.5z M163.7,1598.9c0-0.7-0.2-1.4-0.8-1.4c-0.3,0-0.5,0.2-0.7,0.6c-0.1,0.1-0.1,0.4-0.1,0.8
												c0,1.1,0.3,1.9,0.8,1.9C163.6,1600.7,163.7,1599.9,163.7,1598.9z"/>
										</g>
									</g>
									<g id="school_7">
										<circle class="st35" cx="826.4" cy="1521.9" r="8.5"/>
										<g>
											<path class="st32" d="M828.7,1518v1.1l-2.7,7.4h-1.7l2.6-7v0h-2.9v-1.5H828.7z"/>
										</g>
									</g>
									<g id="school_8">
										<circle class="st35" cx="800.7" cy="1570.2" r="8.5"/>
										<g>
											<path class="st32" d="M798.1,1572.6c0-1.1,0.6-1.8,1.3-2.3v0c-0.7-0.5-1.1-1.2-1.1-1.9c0-1.4,1.1-2.3,2.4-2.3
												c1.2,0,2.2,0.8,2.2,2.1c0,0.6-0.2,1.3-1.1,1.9v0c0.8,0.4,1.3,1.2,1.3,2.2c0,1.6-1.2,2.5-2.6,2.5
												C799.1,1575,798.1,1574,798.1,1572.6z M801.5,1572.5c0-0.8-0.5-1.3-1-1.6c-0.4,0.3-0.7,0.8-0.7,1.5c0,0.7,0.3,1.4,0.9,1.4
												C801.2,1573.8,801.5,1573.2,801.5,1572.5z M800,1568.4c0,0.6,0.3,1,0.8,1.3c0.4-0.2,0.6-0.7,0.6-1.2c0-0.6-0.2-1.1-0.7-1.1
												C800.2,1567.3,800,1567.9,800,1568.4z"/>
										</g>
									</g>
								</g>
							</g>
							<g id="Hotels_POI">
								<g id="Hotel_Dots">
									<g id="hotel_1">
										<circle class="st36" cx="422.7" cy="1134.9" r="8.5"/>
										<g>
											<path class="st32" d="M422.7,1131.8L422.7,1131.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1131.8z"/>
										</g>
									</g>
									<g id="hotel_2">
										<circle class="st36" cx="389.3" cy="1179.9" r="8.5"/>
										<g>
											<path class="st32" d="M386.8,1183.2v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H386.8z"/>
										</g>
									</g>
									<g id="hotel_3">
										<circle class="st36" cx="468.9" cy="1212.5" r="8.5"/>
										<g>
											<path class="st32" d="M466.6,1214.8c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L466.6,1214.8z"/>
										</g>
									</g>
									<g id="hotel_4">
										<circle class="st36" cx="549.6" cy="1173.9" r="8.5"/>
										<g>
											<path class="st32" d="M550,1178v-2h-2.9v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H550z M550,1174.6v-1.8c0-0.5,0.1-1,0.1-1.4h-0.1
												c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H550z"/>
										</g>
									</g>
									<g id="hotel_5">
										<circle class="st36" cx="258.4" cy="1588.1" r="8.5"/>
										<g>
											<path class="st32" d="M260.4,1585.7h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1585.7z"/>
										</g>
									</g>
								</g>
							</g>
							<g id="Dining_POI">
								<g id="Dining_Dots">
									<g id="dining_1">
										<circle class="st37" cx="581.1" cy="714" r="8.5"/>
										<g>
											<path class="st32" d="M580.2,711.3L580.2,711.3l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V711.3z"/>
										</g>
									</g>
									<g id="dining_2">
										<circle class="st37" cx="361.5" cy="1131.8" r="8.5"/>
										<g>
											<path class="st32" d="M358.5,1136.1v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H358.5z"/>
										</g>
									</g>
									<g id="dining_3">
										<circle class="st37" cx="388.9" cy="1123.2" r="8.5"/>
										<g>
											<path class="st32" d="M386.7,1126c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L386.7,1126z"/>
										</g>
									</g>
									<g id="dining_4">
										<circle class="st37" cx="401.8" cy="1159.8" r="8.5"/>
										<g>
											<path class="st32" d="M402,1164v-2H399v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H402z M402,1160.6v-1.8c0-0.5,0.1-1,0.1-1.4H402
												c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H402z"/>
										</g>
									</g>
									<g id="dining_5">
										<circle class="st37" cx="406.2" cy="1191.4" r="8.5"/>
										<g>
											<path class="st32" d="M408.2,1188.7h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1188.7z"/>
										</g>
									</g>
									<g id="dining_6">
										<circle class="st37" cx="436.8" cy="1182.9" r="8.5"/>
										<g>
											<path class="st32" d="M438.2,1180.5c-0.2,0-0.4,0-0.7,0.1c-1.1,0.2-1.7,1.2-1.9,2.3h0c0.3-0.4,0.8-0.7,1.3-0.7
												c1.2,0,1.9,1,1.9,2.7c0,1.5-0.8,3-2.4,3c-1.6,0-2.6-1.4-2.6-3.5c0-1.9,0.6-3.1,1.4-4c0.6-0.6,1.4-1,2.1-1.2
												c0.4-0.1,0.7-0.1,0.9-0.1V1180.5z M437.1,1184.9c0-0.7-0.2-1.4-0.8-1.4c-0.3,0-0.5,0.2-0.7,0.6c-0.1,0.1-0.1,0.4-0.1,0.8
												c0,1.1,0.3,1.9,0.8,1.9C437,1186.6,437.1,1185.9,437.1,1184.9z"/>
										</g>
									</g>
									<g id="dining_7">
										<circle class="st37" cx="488.2" cy="1207.5" r="8.5"/>
										<g>
											<path class="st32" d="M490.6,1203.1v1.1l-2.7,7.4h-1.7l2.6-7v0h-2.9v-1.5H490.6z"/>
										</g>
									</g>
									<g id="dining_8">
										<circle class="st37" cx="537.5" cy="1160.5" r="8.5"/>
										<g>
											<path class="st32" d="M534.9,1162.3c0-1.1,0.6-1.8,1.3-2.3v0c-0.7-0.5-1.1-1.2-1.1-1.9c0-1.4,1.1-2.3,2.4-2.3
												c1.2,0,2.2,0.8,2.2,2.1c0,0.6-0.2,1.3-1.1,1.9v0c0.8,0.4,1.3,1.2,1.3,2.2c0,1.6-1.2,2.5-2.6,2.5
												C535.9,1164.7,534.9,1163.7,534.9,1162.3z M538.3,1162.2c0-0.8-0.5-1.3-1-1.6c-0.4,0.3-0.7,0.8-0.7,1.5c0,0.7,0.3,1.4,0.9,1.4
												C538,1163.5,538.3,1162.9,538.3,1162.2z M536.7,1158.1c0,0.6,0.3,1,0.8,1.3c0.4-0.2,0.6-0.7,0.6-1.2c0-0.6-0.2-1.1-0.7-1.1
												S536.7,1157.6,536.7,1158.1z"/>
										</g>
									</g>
									<g id="dining_9">
										<circle class="st37" cx="572.4" cy="1192" r="8.5"/>
										<g>
											<path class="st32" d="M570.1,1195.6c0.3,0,0.5,0,0.8-0.1c0.3-0.1,0.7-0.2,1-0.5c0.4-0.4,0.7-1,0.8-1.7h0
												c-0.3,0.3-0.7,0.5-1.2,0.5c-1.1,0-2-1-2-2.6c0-1.5,0.9-3,2.6-3c1.6,0,2.4,1.4,2.4,3.5c0,1.9-0.6,3.3-1.4,4.1
												c-0.5,0.6-1.3,0.9-2.1,1.1c-0.3,0-0.5,0-0.8,0V1195.6z M571.1,1191.1c0,0.8,0.3,1.5,0.8,1.5c0.3,0,0.6-0.2,0.7-0.4
												c0.1-0.2,0.1-0.3,0.1-0.9c0-1.1-0.3-1.8-0.8-1.8C571.4,1189.5,571.1,1190,571.1,1191.1z"/>
										</g>
									</g>
									<g id="dining_10">
										<circle class="st37" cx="592.1" cy="1192" r="8.5"/>
										<g>
											<path class="st32" d="M588.3,1189.8L588.3,1189.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1189.8z"/>
											<path class="st32" d="M596.8,1192.4c0,3.1-1.1,4.4-2.5,4.4c-1.9,0-2.5-2.1-2.5-4.3c0-2.5,0.8-4.4,2.6-4.4
												C596.4,1188.1,596.8,1190.4,596.8,1192.4z M593.5,1192.4c0,2,0.2,2.9,0.8,2.9c0.5,0,0.7-0.9,0.7-2.9c0-1.8-0.2-2.9-0.7-2.9
												C593.8,1189.5,593.5,1190.5,593.5,1192.4z"/>
										</g>
									</g>
									<g id="dining_11">
										<circle class="st37" cx="573.3" cy="1210.4" r="8.5"/>
										<g>
											<path class="st32" d="M569.4,1208L569.4,1208l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1208z"/>
											<path class="st32" d="M575.1,1208L575.1,1208l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1208z"/>
										</g>
									</g>
									<g id="dining_12">
										<circle class="st37" cx="573" cy="1228.7" r="8.5"/>
										<g>
											<path class="st32" d="M569.1,1226.3L569.1,1226.3l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1226.3z"/>
											<path class="st32" d="M572.7,1233.3v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H572.7z"/>
										</g>
									</g>
									<g id="dining_13">
										<circle class="st37" cx="725.2" cy="1120.1" r="8.5"/>
										<g>
											<path class="st32" d="M722.1,1117.7L722.1,1117.7l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1117.7z"/>
											<path class="st32" d="M726,1123c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L726,1123z"/>
										</g>
									</g>
									<g id="dining_14">
										<circle class="st37" cx="710.5" cy="1132.1" r="8.5"/>
										<g>
											<path class="st32" d="M705.8,1129.6L705.8,1129.6l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1129.6z"/>
											<path class="st32" d="M712.2,1136.5v-2h-2.9v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H712.2z M712.2,1133.1v-1.8
												c0-0.5,0.1-1,0.1-1.4h-0.1c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H712.2z"/>
										</g>
									</g>
									<g id="dining_15">
										<circle class="st37" cx="693.1" cy="1144.1" r="8.5"/>
										<g>
											<path class="st32" d="M689.5,1141.5L689.5,1141.5l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1141.5z"/>
											<path class="st32" d="M697.6,1141.4h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1141.4z"/>
										</g>
									</g>
									<g id="dining_16">
										<circle class="st37" cx="758.3" cy="1153.6" r="8.5"/>
										<g>
											<path class="st32" d="M754.6,1151L754.6,1151l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1151z"/>
											<path class="st32" d="M762.5,1150.7c-0.2,0-0.4,0-0.7,0.1c-1.1,0.2-1.7,1.2-1.9,2.3h0c0.3-0.4,0.8-0.7,1.3-0.7
												c1.2,0,1.9,1,1.9,2.7c0,1.5-0.8,3-2.4,3c-1.6,0-2.6-1.4-2.6-3.5c0-1.9,0.6-3.1,1.4-4c0.6-0.6,1.4-1,2.1-1.2
												c0.4-0.1,0.7-0.1,0.9-0.1V1150.7z M761.5,1155c0-0.7-0.2-1.4-0.8-1.4c-0.3,0-0.5,0.2-0.7,0.6c-0.1,0.1-0.1,0.4-0.1,0.8
												c0,1.1,0.3,1.9,0.8,1.9C761.3,1156.8,761.5,1156.1,761.5,1155z"/>
										</g>
									</g>
									<g id="dining_17">
										<circle class="st37" cx="346.6" cy="1368.9" r="8.5"/>
										<g>
											<path class="st32" d="M343.1,1366.5L343.1,1366.5l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1366.5z"/>
											<path class="st32" d="M351.5,1364.8v1.1l-2.7,7.4h-1.7l2.6-7v0h-2.9v-1.5H351.5z"/>
										</g>
									</g>
									<g id="dining_18">
										<circle class="st37" cx="365.9" cy="1366.5" r="8.5"/>
										<g>
											<path class="st32" d="M362.5,1364L362.5,1364l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1364z"/>
											<path class="st32" d="M366,1368.7c0-1.1,0.6-1.8,1.3-2.3v0c-0.7-0.5-1.1-1.2-1.1-1.9c0-1.4,1.1-2.3,2.4-2.3
												c1.2,0,2.2,0.8,2.2,2.1c0,0.6-0.2,1.3-1.1,1.9v0c0.8,0.4,1.3,1.2,1.3,2.2c0,1.6-1.2,2.5-2.6,2.5
												C367.1,1371,366,1370.1,366,1368.7z M369.4,1368.6c0-0.8-0.5-1.3-1-1.6c-0.4,0.3-0.7,0.8-0.7,1.5c0,0.7,0.3,1.4,0.9,1.4
												C369.1,1369.8,369.4,1369.3,369.4,1368.6z M367.9,1364.4c0,0.6,0.3,1,0.8,1.3c0.4-0.2,0.6-0.7,0.6-1.2c0-0.6-0.2-1.1-0.7-1.1
												S367.9,1363.9,367.9,1364.4z"/>
										</g>
									</g>
									<g id="dining_19">
										<circle class="st37" cx="385.5" cy="1367.3" r="8.5"/>
										<g>
											<path class="st32" d="M381.3,1364.9L381.3,1364.9l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1364.9z"/>
											<path class="st32" d="M385.5,1370.5c0.3,0,0.5,0,0.8-0.1c0.3-0.1,0.7-0.2,1-0.5c0.4-0.4,0.7-1,0.8-1.7h0
												c-0.3,0.3-0.7,0.5-1.2,0.5c-1.1,0-2-1-2-2.6c0-1.5,0.9-3,2.6-3c1.6,0,2.4,1.4,2.4,3.5c0,1.9-0.6,3.3-1.4,4.1
												c-0.5,0.6-1.3,0.9-2.1,1.1c-0.3,0-0.5,0-0.8,0V1370.5z M386.6,1366c0,0.8,0.3,1.5,0.8,1.5c0.3,0,0.6-0.2,0.7-0.4
												c0.1-0.2,0.1-0.3,0.1-0.9c0-1.1-0.3-1.8-0.8-1.8C386.9,1364.4,386.6,1365,386.6,1366z"/>
										</g>
									</g>
									<g id="dining_20">
										<circle class="st37" cx="402.4" cy="1377.8" r="8.5"/>
										<g>
											<path class="st32" d="M396.8,1382.2v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H396.8z"/>
											<path class="st32" d="M407.4,1377.9c0,3.1-1.1,4.4-2.5,4.4c-1.9,0-2.5-2.1-2.5-4.3c0-2.5,0.8-4.4,2.6-4.4
												C407,1373.5,407.4,1375.8,407.4,1377.9z M404.2,1377.9c0,2,0.2,2.9,0.8,2.9c0.5,0,0.7-0.9,0.7-2.9c0-1.8-0.2-2.9-0.7-2.9
												C404.4,1375,404.2,1375.9,404.2,1377.9z"/>
										</g>
									</g>
									<g id="dining_21">
										<circle class="st37" cx="336.3" cy="1385.3" r="8.5"/>
										<g>
											<path class="st32" d="M331,1389.7v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H331z"/>
											<path class="st32" d="M338.6,1382.8L338.6,1382.8l-1.5,0.8l-0.3-1.4l2-1h1.5v8.6h-1.7V1382.8z"/>
										</g>
									</g>
									<g id="dining_22">
										<circle class="st37" cx="245.5" cy="1444.6" r="8.5"/>
										<g>
											<path class="st32" d="M240.1,1449.3v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H240.1z"/>
											<path class="st32" d="M245.7,1449.3v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H245.7z"/>
										</g>
									</g>
									<g id="dining_23">
										<circle class="st37" cx="276.4" cy="1497.1" r="8.5"/>
										<g>
											<path class="st32" d="M271.1,1501.3v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H271.1z"/>
											<path class="st32" d="M277,1499.7c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L277,1499.7z"/>
										</g>
									</g>
									<g id="dining_24">
										<circle class="st37" cx="254.4" cy="1632.2" r="8.5"/>
										<g>
											<path class="st32" d="M249.2,1636.1v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H249.2z"/>
											<path class="st32" d="M257.5,1636.1v-2h-2.9v-1.2l2.8-5.3h1.7v5.1h0.9v1.4h-0.9v2H257.5z M257.5,1632.7v-1.8
												c0-0.5,0.1-1,0.1-1.4h-0.1c-0.2,0.5-0.4,0.9-0.6,1.3l-0.9,1.8v0H257.5z"/>
										</g>
									</g>
									<g id="dining_25">
										<circle class="st37" cx="234" cy="1648.5" r="8.5"/>
										<g>
											<path class="st32" d="M229.1,1652.8v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H229.1z"/>
											<path class="st32" d="M239.2,1645.7h-2.3l-0.2,1.5c0.1,0,0.2,0,0.3,0c0.4,0,1.1,0.2,1.6,0.6c0.5,0.5,0.7,1.1,0.7,2
												c0,1.7-1.1,3-3,3c-0.6,0-1.3-0.2-1.6-0.3l0.3-1.4c0.3,0.2,0.8,0.3,1.2,0.3c0.8,0,1.4-0.5,1.4-1.5c0-1.1-0.9-1.5-1.9-1.5
												c-0.2,0-0.4,0-0.6,0l0.5-4.3h3.5V1645.7z"/>
										</g>
									</g>
									<g id="dining_26">
										<circle class="st37" cx="52.7" cy="1718.8" r="8.5"/>
										<g>
											<path class="st32" d="M47.6,1723v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H47.6z"/>
											<path class="st32" d="M57.5,1715.7c-0.2,0-0.4,0-0.7,0.1c-1.1,0.2-1.7,1.2-1.9,2.3h0c0.3-0.4,0.8-0.7,1.3-0.7
												c1.2,0,1.9,1,1.9,2.7c0,1.5-0.8,3-2.4,3c-1.6,0-2.6-1.4-2.6-3.5c0-1.9,0.6-3.1,1.4-4c0.6-0.6,1.4-1,2.1-1.2
												c0.4-0.1,0.7-0.1,0.9-0.1V1715.7z M56.5,1720.1c0-0.7-0.2-1.4-0.8-1.4c-0.3,0-0.5,0.2-0.7,0.6c-0.1,0.1-0.1,0.4-0.1,0.8
												c0,1.1,0.3,1.9,0.8,1.9C56.3,1721.9,56.5,1721.1,56.5,1720.1z"/>
										</g>
									</g>
									<g id="dining_27">
										<circle class="st37" cx="99.2" cy="1731" r="8.5"/>
										<g>
											<path class="st32" d="M94,1735.2v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H94z"/>
											<path class="st32" d="M104.4,1726.7v1.1l-2.7,7.4h-1.7l2.6-7v0h-2.9v-1.5H104.4z"/>
										</g>
									</g>
									<g id="dining_28">
										<circle class="st37" cx="85.7" cy="1752.3" r="8.5"/>
										<g>
											<path class="st32" d="M80.5,1756.9v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H80.5z"/>
											<path class="st32" d="M86,1754.6c0-1.1,0.6-1.8,1.3-2.3v0c-0.7-0.5-1.1-1.2-1.1-1.9c0-1.4,1.1-2.3,2.4-2.3
												c1.2,0,2.2,0.8,2.2,2.1c0,0.6-0.2,1.3-1.1,1.9v0c0.8,0.4,1.3,1.2,1.3,2.2c0,1.6-1.2,2.5-2.6,2.5C87.1,1757,86,1756,86,1754.6z
												 M89.4,1754.5c0-0.8-0.5-1.3-1-1.6c-0.4,0.3-0.7,0.8-0.7,1.5c0,0.7,0.3,1.4,0.9,1.4C89.1,1755.8,89.4,1755.3,89.4,1754.5z
												 M87.9,1750.4c0,0.6,0.3,1,0.8,1.3c0.4-0.2,0.6-0.7,0.6-1.2c0-0.6-0.2-1.1-0.7-1.1S87.9,1749.9,87.9,1750.4z"/>
										</g>
									</g>
									<g id="dining_29">
										<circle class="st37" cx="108.2" cy="1753.6" r="8.5"/>
										<g>
											<path class="st32" d="M103.1,1758.1v-1.2l0.9-1.1c1.1-1.4,2-2.6,2-3.8c0-0.7-0.4-1.2-1.1-1.2c-0.5,0-1,0.2-1.3,0.4l-0.3-1.3
												c0.4-0.3,1.1-0.5,1.9-0.5c1.9,0,2.5,1.3,2.5,2.5c0,1.6-1,2.9-2,4l-0.5,0.6v0h2.6v1.5H103.1z"/>
											<path class="st32" d="M109.3,1756.8c0.3,0,0.5,0,0.8-0.1c0.3-0.1,0.7-0.2,1-0.5c0.4-0.4,0.7-1,0.8-1.7h0
												c-0.3,0.3-0.7,0.5-1.2,0.5c-1.1,0-2-1-2-2.6c0-1.5,0.9-3,2.6-3c1.6,0,2.4,1.4,2.4,3.5c0,1.9-0.6,3.3-1.4,4.1
												c-0.5,0.6-1.3,0.9-2.1,1.1c-0.3,0-0.5,0-0.8,0V1756.8z M110.4,1752.3c0,0.8,0.3,1.5,0.8,1.5c0.3,0,0.6-0.2,0.7-0.4
												c0.1-0.2,0.1-0.3,0.1-0.9c0-1.1-0.3-1.8-0.8-1.8C110.7,1750.7,110.3,1751.3,110.4,1752.3z"/>
										</g>
									</g>
									<g id="dining_30">
										<circle class="st37" cx="708.6" cy="1505.6" r="8.5"/>
										<g>
											<path class="st32" d="M703.7,1508.5c0.3,0.2,0.8,0.3,1.3,0.3c0.8,0,1.3-0.5,1.3-1.2c0-0.9-0.7-1.4-1.6-1.4h-0.2v-1.3h0.2
												c0.6,0,1.4-0.3,1.4-1.1c0-0.6-0.4-1-1-1c-0.4,0-0.8,0.1-1.1,0.3l-0.2-1.3c0.4-0.2,1.1-0.4,1.8-0.4c1.5,0,2.3,1,2.3,2.1
												c0,0.9-0.5,1.6-1.2,1.9v0c0.8,0.3,1.5,1,1.5,2.2c0,1.3-1,2.6-2.9,2.6c-0.7,0-1.3-0.2-1.7-0.4L703.7,1508.5z"/>
											<path class="st32" d="M714.1,1505.8c0,3.1-1.1,4.4-2.5,4.4c-1.9,0-2.5-2.1-2.5-4.3c0-2.5,0.8-4.4,2.6-4.4
												C713.6,1501.4,714.1,1503.7,714.1,1505.8z M710.8,1505.8c0,2,0.2,2.9,0.8,2.9c0.5,0,0.7-0.9,0.7-2.9c0-1.8-0.2-2.9-0.7-2.9
												C711.1,1502.9,710.8,1503.8,710.8,1505.8z"/>
										</g>
									</g>
								</g>
								<polygon class="st32" points="229.8,1327.6 213.8,1325.4 215.1,1308.8 229.8,1308.8 	"/>
							</g>
							
							<!---------------------- LEGEND -------------------->
							<g id="Legend">
								<g>
									<g>
										<g>
											<g>
												<g>
													<path class="st48" d="M810,1769.8c0,1.6-0.5,2.2-1.1,2.2c-0.7,0-1.1-0.8-1.1-2.2c0-1.5,0.4-2.2,1.1-2.2
														C809.6,1767.6,810,1768.4,810,1769.8z M808.3,1769.8c0,1,0.2,1.8,0.6,1.8c0.4,0,0.6-0.7,0.6-1.8c0-1-0.2-1.8-0.6-1.8
														C808.5,1768,808.3,1768.8,808.3,1769.8z"/>
												</g>
											</g>
											<rect x="809.2" y="1753.3" class="st49" width="43.1" height="10.2"/>
											<g>
												<g>
													<path class="st48" d="M845.2,1769.8c0,1.6-0.5,2.2-1.1,2.2c-0.7,0-1.1-0.8-1.1-2.2c0-1.5,0.4-2.2,1.1-2.2
														C844.9,1767.6,845.2,1768.4,845.2,1769.8z M843.5,1769.8c0,1,0.2,1.8,0.6,1.8c0.4,0,0.6-0.7,0.6-1.8c0-1-0.2-1.8-0.6-1.8
														C843.7,1768,843.5,1768.8,843.5,1769.8z"/>
													<path class="st48" d="M845.7,1771.6c0-0.2,0.2-0.4,0.3-0.4c0.2,0,0.3,0.2,0.3,0.4s-0.1,0.4-0.3,0.4
														C845.9,1772,845.7,1771.9,845.7,1771.6z"/>
													<path class="st48" d="M846.9,1772v-0.3l0.4-0.5c0.5-0.7,1.1-1.5,1.1-2.3c0-0.5-0.2-0.8-0.6-0.8c-0.3,0-0.5,0.2-0.6,0.2
														l-0.1-0.4c0.2-0.1,0.5-0.3,0.8-0.3c0.7,0,1,0.5,1,1.2c0,0.8-0.5,1.6-1.1,2.3l-0.3,0.4v0h1.4v0.5H846.9z"/>
													<path class="st48" d="M851.4,1768.1h-1.2l-0.2,1.1c0.1,0,0.1,0,0.2,0c0.2,0,0.6,0.1,0.8,0.3c0.2,0.2,0.4,0.5,0.4,1
														c0,0.9-0.5,1.5-1.3,1.5c-0.3,0-0.6-0.1-0.7-0.2l0.1-0.4c0.1,0.1,0.4,0.2,0.6,0.2c0.4,0,0.8-0.3,0.8-1c0-0.7-0.4-1-1-1
														c-0.2,0-0.3,0-0.3,0l0.3-2h1.5V1768.1z"/>
												</g>
											</g>
											<g>
												<g>
													<path class="st48" d="M891.5,1769.8c0,1.6-0.5,2.2-1.1,2.2c-0.7,0-1.1-0.8-1.1-2.2c0-1.5,0.4-2.2,1.1-2.2
														C891.2,1767.6,891.5,1768.4,891.5,1769.8z M889.8,1769.8c0,1,0.2,1.8,0.6,1.8c0.4,0,0.6-0.7,0.6-1.8c0-1-0.2-1.8-0.6-1.8
														C890,1768,889.8,1768.8,889.8,1769.8z"/>
													<path class="st48" d="M892,1771.6c0-0.2,0.2-0.4,0.3-0.4c0.2,0,0.3,0.2,0.3,0.4s-0.1,0.4-0.3,0.4
														C892.2,1772,892,1771.9,892,1771.6z"/>
													<path class="st48" d="M895.2,1768.1H894l-0.2,1.1c0.1,0,0.1,0,0.2,0c0.2,0,0.6,0.1,0.8,0.3c0.2,0.2,0.4,0.5,0.4,1
														c0,0.9-0.5,1.5-1.3,1.5c-0.3,0-0.6-0.1-0.7-0.2l0.1-0.4c0.1,0.1,0.4,0.2,0.6,0.2c0.4,0,0.8-0.3,0.8-1c0-0.7-0.4-1-1-1
														c-0.2,0-0.3,0-0.3,0l0.3-2h1.5V1768.1z"/>
												</g>
											</g>
											<rect x="895.5" y="1753.3" class="st49" width="43.1" height="10.2"/>
											<g>
												<g>
													<path class="st48" d="M931.5,1769.8c0,1.6-0.5,2.2-1.1,2.2c-0.7,0-1.1-0.8-1.1-2.2c0-1.5,0.4-2.2,1.1-2.2
														C931.1,1767.6,931.5,1768.4,931.5,1769.8z M929.8,1769.8c0,1,0.2,1.8,0.6,1.8c0.4,0,0.6-0.7,0.6-1.8c0-1-0.2-1.8-0.6-1.8
														C930,1768,929.8,1768.8,929.8,1769.8z"/>
													<path class="st48" d="M932,1771.6c0-0.2,0.2-0.4,0.3-0.4c0.2,0,0.3,0.2,0.3,0.4s-0.1,0.4-0.3,0.4
														C932.2,1772,932,1771.9,932,1771.6z"/>
													<path class="st48" d="M935.3,1767.6v0.3l-1.4,4h-0.5l1.4-3.8v0h-1.5v-0.5H935.3z"/>
													<path class="st48" d="M937.7,1768.1h-1.2l-0.2,1.1c0.1,0,0.1,0,0.2,0c0.2,0,0.6,0.1,0.8,0.3c0.2,0.2,0.4,0.5,0.4,1
														c0,0.9-0.5,1.5-1.3,1.5c-0.3,0-0.6-0.1-0.7-0.2l0.1-0.4c0.1,0.1,0.4,0.2,0.6,0.2c0.4,0,0.8-0.3,0.8-1c0-0.7-0.4-1-1-1
														c-0.2,0-0.3,0-0.3,0l0.3-2h1.5V1768.1z"/>
												</g>
											</g>
											<g>
												<g>
													<path class="st48" d="M981.4,1768.2L981.4,1768.2l-0.7,0.4l-0.1-0.4l0.9-0.5h0.4v4.3h-0.5V1768.2z"/>
													<path class="st48" d="M986.9,1770c0-0.6-0.1-1.2,0-1.7h0c-0.1,0.5-0.2,1-0.4,1.5l-0.5,2.1h-0.3l-0.5-2
														c-0.1-0.6-0.3-1.1-0.4-1.6h0c0,0.5,0,1.1-0.1,1.8l-0.1,1.9H984l0.3-4.5h0.5l0.5,2c0.2,0.6,0.3,1,0.4,1.5h0
														c0.1-0.5,0.2-0.9,0.3-1.5l0.5-2h0.6l0.3,4.5h-0.5L986.9,1770z"/>
													<path class="st48" d="M988.6,1767.9c0,0.2-0.1,0.3-0.3,0.3c-0.2,0-0.3-0.1-0.3-0.3c0-0.2,0.1-0.3,0.3-0.3
														C988.5,1767.6,988.6,1767.7,988.6,1767.9z M988.1,1772v-3.2h0.5v3.2H988.1z"/>
													<path class="st48" d="M989.3,1767.4h0.5v4.6h-0.5V1767.4z"/>
													<path class="st48" d="M990.9,1770.4c0,1,0.4,1.2,0.8,1.2c0.2,0,0.4-0.1,0.5-0.1l0.1,0.4c-0.2,0.1-0.5,0.2-0.7,0.2
														c-0.8,0-1.2-0.6-1.2-1.6c0-1,0.4-1.7,1.1-1.7c0.7,0,0.9,0.7,0.9,1.4c0,0.1,0,0.2,0,0.3H990.9z M991.9,1770
														c0-0.6-0.2-0.9-0.5-0.9c-0.3,0-0.5,0.5-0.5,0.9H991.9z"/>
												</g>
											</g>
											<rect x="852.3" y="1753.3" class="st50" width="43.1" height="10.2"/>
											<rect x="938.6" y="1753.3" class="st50" width="43.1" height="10.2"/>
										</g>
									</g>
								</g>
								<g>
									<circle id="XMLID_56_" class="st51" cx="115.1" cy="236.6" r="17.1"/>
									<line id="XMLID_55_" class="st51" x1="115.1" y1="236.5" x2="115.1" y2="219.5"/>
									<g>
										<path class="st42" d="M110,201.5h3l4.3,7h0v-7h2.2v10.1h-2.9l-4.4-7.2h0v7.2H110V201.5z"/>
									</g>
								</g>
								<rect x="769.4" y="0.2" class="st52" width="310.6" height="1298.7"/>
								
								<g id="Touch__x26__Explore">
									<g>
										<path class="st32" d="M805.2,32.4h-1.8V18.3h-5v-1.6h11.8v1.6h-5V32.4z"/>
										<path class="st32" d="M822.5,26.5c0,1.9-0.5,3.4-1.5,4.5c-1,1.1-2.3,1.6-4,1.6c-1.1,0-2-0.2-2.8-0.7c-0.8-0.5-1.4-1.2-1.9-2.1
											s-0.7-2-0.7-3.2c0-1.9,0.5-3.4,1.4-4.5c1-1.1,2.3-1.6,4-1.6c1.6,0,3,0.5,3.9,1.6C822,23.1,822.5,24.6,822.5,26.5z M813.5,26.5
											c0,1.5,0.3,2.6,0.9,3.4c0.6,0.8,1.5,1.2,2.7,1.2s2.1-0.4,2.7-1.2c0.6-0.8,0.9-1.9,0.9-3.4c0-1.5-0.3-2.6-0.9-3.4
											c-0.6-0.8-1.5-1.2-2.7-1.2c-1.2,0-2,0.4-2.6,1.1C813.8,23.8,813.5,24.9,813.5,26.5z"/>
										<path class="st32" d="M827.3,20.6v7.6c0,1,0.2,1.7,0.7,2.1s1.1,0.7,2.1,0.7c1.2,0,2.1-0.3,2.7-1c0.6-0.7,0.9-1.8,0.9-3.3v-6.2
											h1.8v11.8h-1.5l-0.3-1.6h-0.1c-0.4,0.6-0.9,1-1.5,1.3c-0.6,0.3-1.4,0.5-2.2,0.5c-1.4,0-2.5-0.3-3.2-1c-0.7-0.7-1.1-1.8-1.1-3.3
											v-7.7H827.3z"/>
										<path class="st32" d="M843.8,32.6c-1.7,0-3-0.5-4-1.6c-0.9-1-1.4-2.5-1.4-4.5c0-2,0.5-3.5,1.4-4.6c0.9-1.1,2.3-1.6,4.1-1.6
											c0.6,0,1.1,0.1,1.7,0.2c0.6,0.1,1,0.3,1.3,0.4l-0.5,1.5c-0.4-0.2-0.8-0.3-1.3-0.4c-0.5-0.1-0.9-0.2-1.2-0.2
											c-2.4,0-3.6,1.5-3.6,4.6c0,1.4,0.3,2.6,0.9,3.3c0.6,0.8,1.4,1.2,2.6,1.2c1,0,2-0.2,3-0.6V32C846,32.4,845,32.6,843.8,32.6z"/>
										<path class="st32" d="M857.6,32.4v-7.6c0-1-0.2-1.7-0.7-2.1s-1.1-0.7-2.1-0.7c-1.2,0-2.1,0.3-2.7,1c-0.6,0.7-0.9,1.8-0.9,3.3v6.2
											h-1.8V15.6h1.8v5.1c0,0.6,0,1.1-0.1,1.5h0.1c0.4-0.6,0.9-1,1.5-1.3c0.6-0.3,1.4-0.5,2.2-0.5c1.4,0,2.5,0.3,3.2,1
											c0.7,0.7,1.1,1.8,1.1,3.3v7.7H857.6z"/>
										<path class="st32" d="M868.1,28.3c0-0.9,0.2-1.8,0.7-2.5c0.5-0.7,1.4-1.4,2.7-2.2c-0.6-0.7-1-1.2-1.2-1.5
											c-0.2-0.4-0.4-0.7-0.5-1.1c-0.1-0.4-0.2-0.8-0.2-1.2c0-1.1,0.4-1.9,1.1-2.5s1.7-0.9,2.9-0.9c1.2,0,2.1,0.3,2.7,0.9
											c0.7,0.6,1,1.4,1,2.5c0,0.8-0.2,1.5-0.7,2.1c-0.5,0.6-1.3,1.3-2.4,2l4.4,4.2c0.4-0.4,0.7-1,1-1.6c0.2-0.6,0.4-1.3,0.6-2h1.8
											c-0.5,2-1.2,3.6-2.2,4.7l3.2,3.1h-2.5l-2-1.9c-0.8,0.8-1.7,1.3-2.6,1.6c-0.9,0.3-1.8,0.5-2.9,0.5c-1.5,0-2.7-0.4-3.6-1.1
											C868.5,30.7,868.1,29.6,868.1,28.3z M873,31c1.7,0,3.2-0.6,4.3-1.7l-4.7-4.6c-0.8,0.5-1.4,0.9-1.7,1.2c-0.3,0.3-0.6,0.7-0.7,1
											s-0.2,0.8-0.2,1.2c0,0.8,0.3,1.5,0.8,2C871.3,30.7,872.1,31,873,31z M871.3,19.8c0,0.5,0.1,1,0.4,1.4c0.3,0.4,0.7,1,1.3,1.6
											c0.9-0.5,1.6-1,1.9-1.5c0.4-0.5,0.5-1,0.5-1.6c0-0.6-0.2-1-0.6-1.3c-0.4-0.3-0.9-0.5-1.5-0.5c-0.6,0-1.2,0.2-1.6,0.5
											C871.5,18.7,871.3,19.2,871.3,19.8z"/>
										<path class="st32" d="M899.6,32.4h-8.8V16.6h8.8v1.6h-6.9v5.1h6.5v1.6h-6.5v5.8h6.9V32.4z"/>
										<path class="st32" d="M905.6,26.3l-4.1-5.7h2l3.1,4.5l3.1-4.5h2l-4.1,5.7l4.3,6h-2l-3.3-4.8l-3.3,4.8h-2L905.6,26.3z"/>
										<path class="st32" d="M919.8,32.6c-0.8,0-1.5-0.1-2.1-0.4c-0.6-0.3-1.2-0.7-1.6-1.3H916c0.1,0.7,0.1,1.3,0.1,2v4.8h-1.8V20.6h1.5
											l0.2,1.6h0.1c0.5-0.6,1-1.1,1.6-1.4c0.6-0.3,1.3-0.4,2.1-0.4c1.6,0,2.8,0.5,3.6,1.6c0.8,1.1,1.3,2.6,1.3,4.5
											c0,1.9-0.4,3.4-1.3,4.5C922.5,32,921.3,32.6,919.8,32.6z M919.5,21.9c-1.2,0-2.1,0.3-2.6,1s-0.8,1.7-0.8,3.2v0.4
											c0,1.7,0.3,2.8,0.8,3.6c0.6,0.7,1.4,1.1,2.7,1.1c1,0,1.8-0.4,2.4-1.2c0.6-0.8,0.9-2,0.9-3.4c0-1.5-0.3-2.6-0.9-3.4
											C921.4,22.3,920.6,21.9,919.5,21.9z"/>
										<path class="st32" d="M929.6,32.4h-1.8V15.6h1.8V32.4z"/>
										<path class="st32" d="M943.5,26.5c0,1.9-0.5,3.4-1.5,4.5c-1,1.1-2.3,1.6-4,1.6c-1.1,0-2-0.2-2.8-0.7c-0.8-0.5-1.4-1.2-1.9-2.1
											s-0.7-2-0.7-3.2c0-1.9,0.5-3.4,1.4-4.5c1-1.1,2.3-1.6,4-1.6c1.6,0,3,0.5,3.9,1.6C943,23.1,943.5,24.6,943.5,26.5z M934.6,26.5
											c0,1.5,0.3,2.6,0.9,3.4c0.6,0.8,1.5,1.2,2.7,1.2s2.1-0.4,2.7-1.2c0.6-0.8,0.9-1.9,0.9-3.4c0-1.5-0.3-2.6-0.9-3.4
											c-0.6-0.8-1.5-1.2-2.7-1.2c-1.2,0-2,0.4-2.6,1.1C934.9,23.8,934.6,24.9,934.6,26.5z"/>
										<path class="st32" d="M952,20.4c0.5,0,1,0,1.4,0.1l-0.2,1.7c-0.5-0.1-0.9-0.2-1.3-0.2c-1,0-1.8,0.4-2.4,1.2c-0.7,0.8-1,1.7-1,2.9
											v6.3h-1.8V20.6h1.5l0.2,2.2h0.1c0.4-0.8,1-1.4,1.6-1.8C950.6,20.6,951.3,20.4,952,20.4z"/>
										<path class="st32" d="M960.6,32.6c-1.7,0-3.1-0.5-4.1-1.6c-1-1.1-1.5-2.5-1.5-4.4c0-1.9,0.5-3.4,1.4-4.5c0.9-1.1,2.2-1.7,3.8-1.7
											c1.5,0,2.6,0.5,3.5,1.5c0.9,1,1.3,2.3,1.3,3.8v1.1h-8.1c0,1.4,0.4,2.4,1,3.1c0.7,0.7,1.6,1.1,2.8,1.1c1.3,0,2.5-0.3,3.8-0.8v1.6
											c-0.6,0.3-1.2,0.5-1.8,0.6C962.1,32.5,961.4,32.6,960.6,32.6z M960.1,21.9c-0.9,0-1.7,0.3-2.3,0.9c-0.6,0.6-0.9,1.5-1,2.6h6.2
											c0-1.1-0.3-2-0.8-2.6C961.8,22.2,961.1,21.9,960.1,21.9z"/>
									</g>
								</g>
								
							</g>
							<!------------------------- key text -------------------------->
							<g id="dining_pin">
								<circle class="st54" cx="786.1" cy="57.2" r="6"/>
							</g>
							<g id="hotel_pin">
								<circle class="st55" cx="786.2" cy="551.2" r="6"/>
							</g>
							<g id="schools_pin">
								<circle class="st56" cx="786.1" cy="681.5" r="6"/>
							</g>
							<g id="spa_pin">
								<circle class="st57" cx="786.1" cy="842.9" r="6"/>
							</g>
							<g id="shopping_pin">
								<circle class="st58" cx="786.1" cy="957.9" r="6"/>
							</g>
							<g id="Shopping" class="shopping_text">
								<g id="shopping_txt_1" >
									<g>
										<path class="st32" d="M804,976.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V976.6z"/>
										<path class="st32" d="M807.5,976c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,976.3,807.5,976z"/>
										<path class="st32" d="M819.5,970.5c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2
											C818.5,968.1,819.5,968.9,819.5,970.5z M815.1,972.4h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3
											s-0.9-0.4-1.7-0.4h-1.1V972.4z"/>
										<path class="st32" d="M822.1,970.2v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H822.1z"/>
										<path class="st32" d="M831.5,970.1c0.8,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4s-0.2,1.9-0.7,2.5s-1.1,0.9-2,0.9
											c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1l-0.2,0.8h-0.7v-9.1h1v2.2c0,0.5,0,0.9,0,1.3h0
											C830,970.4,830.7,970.1,831.5,970.1z M831.4,970.9c-0.7,0-1.1,0.2-1.4,0.6s-0.4,1-0.4,1.9s0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.4-1.1,0.4-1.9c0-0.8-0.1-1.5-0.4-1.9S832,970.9,831.4,970.9z"/>
										<path class="st32" d="M836.9,976.6h-1v-9.1h1V976.6z"/>
										<path class="st32" d="M838.9,968.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S838.9,968.7,838.9,968.5z M839.9,976.6h-1v-6.4h1V976.6z"/>
										<path class="st32" d="M843.5,973.3l-2.2-3.1h1.1l1.7,2.5l1.7-2.5h1.1l-2.2,3.1l2.3,3.3h-1.1l-1.8-2.6l-1.8,2.6h-1.1L843.5,973.3
											z"/>
									</g>
								</g>
								<g id="shopping_txt_2">
									<g>
										<path class="st32" d="M806,991.6h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V991.6z"/>
										<path class="st32" d="M807.5,991c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,991.3,807.5,991z"/>
										<path class="st32" d="M820.3,991.6h-1.2l-3.1-4.2l-0.9,0.8v3.4h-1v-8.6h1v4.2l3.9-4.2h1.2l-3.4,3.7L820.3,991.6z"/>
										<path class="st32" d="M824.3,985.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S823.9,985.1,824.3,985.1z"/>
										<path class="st32" d="M831.8,988.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S831.8,987.4,831.8,988.4z M826.9,988.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S826.9,987.6,826.9,988.4z"/>
										<path class="st32" d="M838.7,985.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H838.7z M833.6,992.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2H835c-0.4,0-0.8,0.1-1,0.3
											S833.6,992.3,833.6,992.7z M834.1,987.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S834.1,986.8,834.1,987.3z"/>
										<path class="st32" d="M842.8,991.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S843.2,991.7,842.8,991.7z M842.5,985.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S843,985.9,842.5,985.9z"/>
										<path class="st32" d="M849.7,985.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S849.3,985.1,849.7,985.1z"/>
									</g>
								</g>
								<g id="shopping_txt_3">
									<g>
										<path class="st32" d="M805.7,1000.1c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6h-0.8v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S805.7,999.4,805.7,1000.1z"/>
										<path class="st32" d="M807.5,1006c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,1006.3,807.5,1006z"/>
										<path class="st32" d="M815.1,1003.1v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8H819
											l-2.1-3.6H815.1z M815.1,1002.2h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V1002.2z"
											/>
										<path class="st32" d="M821.5,998.1h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V998.1z M822.5,1001.7h1.6c0.7,0,1.2-0.1,1.5-0.3
											s0.5-0.6,0.5-1.1c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V1001.7z M822.5,1002.6v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4
											s0.5-0.7,0.5-1.3c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H822.5z"/>
										<path class="st32" d="M833.1,1006.6l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H833.1z"/>
										<path class="st32" d="M847.1,1006.6l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H847.1z"/>
										<path class="st32" d="M856.7,1006.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S857.1,1006.7,856.7,1006.7z M856.4,1000.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S856.9,1000.9,856.4,1000.9z"/>
										<path class="st32" d="M863.6,1000.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S863.2,1000.1,863.6,1000.1z"/>
										<path class="st32" d="M868.1,1006.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C869.3,1006.6,868.8,1006.7,868.1,1006.7z"/>
										<path class="st32" d="M874,1006.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S874.4,1006.7,874,1006.7z M873.7,1000.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S874.3,1000.9,873.7,1000.9z"/>
										<path class="st32" d="M882.4,1005.8L882.4,1005.8c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L882.4,1005.8z M880.5,1005.9c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S879.9,1005.9,880.5,1005.9z"/>
										<path class="st32" d="M888.1,1006.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6H886c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S888.5,1006.7,888.1,1006.7z M887.8,1000.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S888.3,1000.9,887.8,1000.9z"/>
										<path class="st32" d="M896.3,1004.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S896.3,1004.4,896.3,1004.9z"/>
									</g>
								</g>
								<g id="shopping_txt_4">
									<g>
										<path class="st32" d="M806.4,1019.6h-1.3v2h-0.9v-2H800v-0.8l4.1-5.8h1v5.8h1.3V1019.6z M804.2,1018.8v-2.8c0-0.6,0-1.2,0.1-1.9
											h0c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H804.2z"/>
										<path class="st32" d="M807.5,1021c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,1021.3,807.5,1021z"/>
										<path class="st32" d="M816.8,1021.6h-1v-7.7H813v-0.9h6.4v0.9h-2.7V1021.6z"/>
										<path class="st32" d="M824.6,1021.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H824.6z M822.4,1020.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S822,1020.9,822.4,1020.9z"/>
										<path class="st32" d="M830.2,1015.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S829.8,1015.1,830.2,1015.1z"/>
										<path class="st32" d="M837.4,1015.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5
											s-1,0.6-1.8,0.6c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H837.4z M832.3,1022.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S832.3,1022.3,832.3,1022.7z M832.8,1017.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S832.8,1016.8,832.8,1017.3z"/>
										<path class="st32" d="M841.5,1021.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S841.9,1021.7,841.5,1021.7z M841.2,1015.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S841.7,1015.9,841.2,1015.9z"/>
										<path class="st32" d="M847.6,1020.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S847.2,1020.9,847.6,1020.9z"/>
									</g>
								</g>
								<g id="shopping_txt_5">
									<g>
										<path class="st32" d="M803,1031.4c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C802.1,1031.4,802.6,1031.4,803,1031.4z"/>
										<path class="st32" d="M807.5,1036c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,1036.3,807.5,1036z"/>
										<path class="st32" d="M815.1,1036.6h-1v-8.6h4.8v0.9h-3.8v3.1h3.6v0.9h-3.6V1036.6z"/>
										<path class="st32" d="M823.1,1030.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S822.7,1030.1,823.1,1030.1z"/>
										<path class="st32" d="M824,1030.2h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L824,1030.2z"/>
										<path class="st32" d="M839.1,1036.6h-4.8v-8.6h4.8v0.9h-3.8v2.8h3.6v0.9h-3.6v3.2h3.8V1036.6z"/>
										<path class="st32" d="M841.9,1036.6h-1v-9.1h1V1036.6z"/>
										<path class="st32" d="M846.6,1036.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S847.1,1036.7,846.6,1036.7z M846.4,1030.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S846.9,1030.9,846.4,1030.9z"/>
										<path class="st32" d="M853.2,1036.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C854.4,1036.6,853.9,1036.7,853.2,1036.7z"/>
										<path class="st32" d="M858.5,1035.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S858.1,1035.9,858.5,1035.9z"/>
										<path class="st32" d="M863.5,1030.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S863.1,1030.1,863.5,1030.1z"/>
										<path class="st32" d="M871.1,1033.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S871.1,1032.4,871.1,1033.4z M866.2,1033.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S866.2,1032.6,866.2,1033.4z"/>
										<path class="st32" d="M877.2,1036.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H877.2z"/>
										<path class="st32" d="M880,1028.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S880,1028.7,880,1028.5z M881.1,1036.6h-1v-6.4h1V1036.6z"/>
										<path class="st32" d="M885.7,1036.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C886.9,1036.6,886.4,1036.7,885.7,1036.7z"/>
										<path class="st32" d="M893,1034.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S893,1034.4,893,1034.9z"/>
									</g>
								</g>
								<g id="shopping_txt_6">
									<g>
										<path class="st32" d="M800.5,1048c0-1.7,0.3-2.9,1-3.8s1.6-1.3,2.9-1.3c0.4,0,0.8,0,1,0.1v0.8c-0.3-0.1-0.6-0.1-1-0.1
											c-0.9,0-1.6,0.3-2.1,0.9s-0.8,1.5-0.8,2.7h0.1c0.4-0.7,1.1-1,2-1c0.8,0,1.4,0.2,1.8,0.7s0.7,1.1,0.7,1.9c0,0.9-0.2,1.6-0.7,2.1
											s-1.1,0.8-2,0.8c-0.9,0-1.6-0.3-2.1-1S800.5,1049.1,800.5,1048z M803.3,1050.9c0.6,0,1-0.2,1.3-0.5s0.5-0.9,0.5-1.5
											c0-0.6-0.1-1-0.4-1.3s-0.7-0.5-1.3-0.5c-0.4,0-0.7,0.1-1,0.2s-0.5,0.3-0.7,0.6s-0.3,0.5-0.3,0.8c0,0.4,0.1,0.8,0.2,1.1
											s0.4,0.6,0.7,0.8S803,1050.9,803.3,1050.9z"/>
										<path class="st32" d="M807.5,1051c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,1051.3,807.5,1051z"/>
										<path class="st32" d="M816,1053.9c-0.4,0-0.7-0.1-0.9-0.2v-0.8c0.3,0.1,0.6,0.1,0.9,0.1c0.4,0,0.7-0.1,0.9-0.4s0.3-0.6,0.3-1
											v-8.6h1v8.5c0,0.7-0.2,1.3-0.6,1.7S816.7,1053.9,816,1053.9z"/>
										<path class="st32" d="M824.2,1051.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H824.2z M822,1050.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S821.7,1050.9,822,1050.9z"/>
										<path class="st32" d="M831.4,1051.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H831.4z"/>
										<path class="st32" d="M835.1,1043.1l0.1,0.1c-0.1,0.4-0.2,0.8-0.4,1.4s-0.4,1-0.6,1.4h-0.7c0.3-1.1,0.5-2.1,0.6-2.9H835.1z"/>
										<path class="st32" d="M840.5,1049.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S840.5,1049.4,840.5,1049.9z"/>
										<path class="st32" d="M851.9,1051.6h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V1051.6z"/>
										<path class="st32" d="M858,1051.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H858z M855.8,1050.9c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S855.4,1050.9,855.8,1050.9z"/>
										<path class="st32" d="M861.7,1051.6h-1v-9.1h1V1051.6z"/>
										<path class="st32" d="M864.7,1051.6h-1v-9.1h1V1051.6z"/>
										<path class="st32" d="M875,1051.6v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H875z"/>
										<path class="st32" d="M881.9,1051.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H881.9z M879.7,1050.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S879.3,1050.9,879.7,1050.9z"/>
										<path class="st32" d="M887.6,1045.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S887.2,1045.1,887.6,1045.1z"/>
										<path class="st32" d="M890.5,1048.3c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C890.5,1047.5,890.5,1047.9,890.5,1048.3L890.5,1048.3z"/>
									</g>
								</g>
								<g id="shopping_txt_7">
									<g>
										<path class="st32" d="M801.4,1066.6l3.6-7.7h-4.7v-0.9h5.7v0.8l-3.5,7.8H801.4z"/>
										<path class="st32" d="M807.5,1066c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,1066.3,807.5,1066z"/>
										<path class="st32" d="M814.1,1066.6v-8.6h1v7.7h3.8v0.9H814.1z"/>
										<path class="st32" d="M825.7,1063.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S825.7,1062.4,825.7,1063.4z M820.9,1063.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S820.9,1062.6,820.9,1063.4z"/>
										<path class="st32" d="M832.7,1066.6l-1.2-3.8c-0.1-0.2-0.2-0.8-0.4-1.6h0c-0.2,0.7-0.3,1.2-0.4,1.6l-1.2,3.8h-1.1l-1.8-6.4h1
											c0.4,1.6,0.7,2.8,0.9,3.7s0.3,1.4,0.4,1.7h0c0-0.2,0.1-0.5,0.2-0.9s0.2-0.6,0.2-0.8l1.2-3.7h1.1l1.1,3.7
											c0.2,0.7,0.4,1.2,0.4,1.7h0c0-0.1,0.1-0.4,0.1-0.7s0.5-1.9,1.2-4.7h1l-1.8,6.4H832.7z"/>
										<path class="st32" d="M839.5,1066.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S839.9,1066.7,839.5,1066.7z M839.2,1060.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S839.7,1060.9,839.2,1060.9z"/>
										<path class="st32" d="M844.3,1058.1l0.1,0.1c-0.1,0.4-0.2,0.8-0.4,1.4s-0.4,1-0.6,1.4h-0.7c0.3-1.1,0.5-2.1,0.6-2.9H844.3z"/>
										<path class="st32" d="M849.7,1064.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S849.7,1064.4,849.7,1064.9z"/>
									</g>
								</g>
								<g id="shopping_txt_8">
									<g>
										<path class="st32" d="M803.2,1072.9c0.8,0,1.4,0.2,1.9,0.5s0.7,0.9,0.7,1.5c0,0.4-0.1,0.8-0.4,1.2s-0.7,0.7-1.3,0.9
											c0.7,0.3,1.2,0.7,1.5,1s0.4,0.8,0.4,1.3c0,0.7-0.2,1.3-0.7,1.7s-1.2,0.6-2,0.6c-0.9,0-1.6-0.2-2.1-0.6s-0.7-1-0.7-1.7
											c0-1,0.6-1.7,1.8-2.3c-0.5-0.3-0.9-0.6-1.2-1s-0.4-0.7-0.4-1.2c0-0.6,0.2-1.1,0.7-1.5S802.4,1072.9,803.2,1072.9z M801.3,1079.5
											c0,0.5,0.2,0.8,0.5,1.1s0.8,0.4,1.4,0.4c0.6,0,1-0.1,1.4-0.4s0.5-0.6,0.5-1.1c0-0.4-0.2-0.7-0.5-1s-0.8-0.6-1.6-0.9
											c-0.6,0.2-1,0.5-1.3,0.8S801.3,1079,801.3,1079.5z M803.2,1073.7c-0.5,0-0.9,0.1-1.1,0.4s-0.4,0.5-0.4,0.9
											c0,0.4,0.1,0.7,0.3,0.9s0.7,0.5,1.3,0.8c0.6-0.2,1-0.5,1.2-0.8s0.3-0.6,0.3-0.9c0-0.4-0.1-0.7-0.4-0.9
											S803.7,1073.7,803.2,1073.7z"/>
										<path class="st32" d="M807.5,1081c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,1081.3,807.5,1081z"/>
										<path class="st32" d="M821.6,1081.6h-1l-1.7-5.7c-0.1-0.3-0.2-0.6-0.3-1s-0.2-0.6-0.2-0.7c-0.1,0.5-0.2,1.1-0.4,1.7l-1.7,5.7h-1
											l-2.3-8.6h1.1l1.4,5.3c0.2,0.7,0.3,1.4,0.4,2c0.1-0.7,0.3-1.4,0.5-2.1l1.5-5.2h1.1l1.6,5.3c0.2,0.6,0.3,1.3,0.5,2.1
											c0.1-0.6,0.2-1.2,0.4-2l1.3-5.3h1.1L821.6,1081.6z"/>
										<path class="st32" d="M829,1081.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H829z M826.8,1080.9c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S826.4,1080.9,826.8,1080.9z"/>
										<path class="st32" d="M832.7,1081.6h-1v-9.1h1V1081.6z"/>
										<path class="st32" d="M843,1081.6v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H843z"/>
										<path class="st32" d="M849.9,1081.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H849.9z M847.7,1080.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S847.3,1080.9,847.7,1080.9z"/>
										<path class="st32" d="M855.6,1075.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S855.2,1075.1,855.6,1075.1z"/>
										<path class="st32" d="M859.6,1080.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S859.3,1080.9,859.6,1080.9z"/>
									</g>
								</g>
								<g id="shopping_txt_9">
									<g>
										<path class="st32" d="M806,1091.7c0,3.4-1.3,5-3.9,5c-0.5,0-0.8,0-1.1-0.1v-0.8c0.3,0.1,0.7,0.2,1.1,0.2c0.9,0,1.6-0.3,2.1-0.9
											s0.7-1.5,0.8-2.7h-0.1c-0.2,0.3-0.5,0.6-0.9,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.7s-0.7-1.1-0.7-1.9
											c0-0.9,0.2-1.6,0.7-2.1s1.2-0.8,2-0.8c0.6,0,1.1,0.1,1.5,0.4s0.8,0.7,1,1.3S806,1090.9,806,1091.7z M803.1,1088.8
											c-0.6,0-1,0.2-1.3,0.5s-0.5,0.9-0.5,1.5c0,0.6,0.1,1,0.4,1.3s0.7,0.5,1.3,0.5c0.4,0,0.7-0.1,1-0.2s0.5-0.3,0.7-0.6
											s0.3-0.5,0.3-0.8c0-0.4-0.1-0.8-0.2-1.1s-0.4-0.6-0.7-0.8S803.5,1088.8,803.1,1088.8z"/>
										<path class="st32" d="M807.5,1096c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807.5,1096.3,807.5,1096z"/>
										<path class="st32" d="M820.6,1096.6h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V1096.6z"/>
										<path class="st32" d="M828.4,1093.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S828.4,1092.4,828.4,1093.4z M823.5,1093.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S823.5,1092.6,823.5,1093.4z"/>
										<path class="st32" d="M838.3,1096.6v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H838.3z"/>
										<path class="st32" d="M843.9,1096.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S844.4,1096.7,843.9,1096.7z M843.7,1090.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S844.2,1090.9,843.7,1090.9z"/>
										<path class="st32" d="M858.1,1092.3c0,1.4-0.4,2.5-1.2,3.2s-1.9,1.1-3.3,1.1h-2.4v-8.6h2.6c1.3,0,2.4,0.4,3.1,1.1
											S858.1,1090.9,858.1,1092.3z M857,1092.3c0-1.1-0.3-2-0.8-2.5s-1.4-0.8-2.5-0.8h-1.4v6.8h1.2c1.2,0,2.1-0.3,2.7-0.9
											S857,1093.4,857,1092.3z"/>
										<path class="st32" d="M862.5,1096.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S863,1096.7,862.5,1096.7z M862.3,1090.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S862.8,1090.9,862.3,1090.9z"/>
										<path class="st32" d="M869.6,1096.7c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S870.4,1096.7,869.6,1096.7z M869.4,1090.9c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S870,1090.9,869.4,1090.9z"/>
										<path class="st32" d="M879.5,1093.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S879.5,1092.4,879.5,1093.4z M874.6,1093.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S874.6,1092.6,874.6,1093.4z"/>
										<path class="st32" d="M883.2,1095.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0.1-0.6,0.1
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S882.9,1095.9,883.2,1095.9z"/>
									</g>
								</g>
								<g id="shopping_txt_10">
									<g>
										<path class="st32" d="M804,1111.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1111.6z"/>
										<path class="st32" d="M812.9,1107.3c0,1.5-0.2,2.6-0.7,3.3s-1.2,1.1-2.1,1.1c-0.9,0-1.6-0.4-2.1-1.1s-0.7-1.8-0.7-3.3
											c0-1.5,0.2-2.6,0.7-3.3s1.2-1.1,2.1-1.1c0.9,0,1.6,0.4,2.1,1.1S812.9,1105.9,812.9,1107.3z M808.2,1107.3c0,1.2,0.1,2.2,0.4,2.7
											s0.8,0.9,1.4,0.9c0.6,0,1.1-0.3,1.4-0.9s0.4-1.5,0.4-2.7s-0.1-2.1-0.4-2.7s-0.8-0.9-1.4-0.9c-0.6,0-1.1,0.3-1.4,0.8
											S808.2,1106.1,808.2,1107.3z"/>
										<path class="st32" d="M814.4,1111c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1111.3,814.4,1111z"/>
										<path class="st32" d="M824.6,1103.8c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C825.9,1104,825.3,1103.8,824.6,1103.8z"/>
										<path class="st32" d="M833.9,1108.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S833.9,1107.4,833.9,1108.4z M829.1,1108.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S829.1,1107.6,829.1,1108.4z"/>
										<path class="st32" d="M839.8,1109.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S839.8,1109.4,839.8,1109.9z"/>
										<path class="st32" d="M843.4,1110.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S843.1,1110.9,843.4,1110.9z"/>
										<path class="st32" d="M848.2,1111.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C849.4,1111.6,848.8,1111.7,848.2,1111.7z"/>
										<path class="st32" d="M856.9,1108.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S856.9,1107.4,856.9,1108.4z M852,1108.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S852,1107.6,852,1108.4z"/>
									</g>
								</g>
								<g id="shopping_txt_11">
									<g>
										<path class="st32" d="M804,1126.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1126.6z"/>
										<path class="st32" d="M810.8,1126.6h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1126.6z"/>
										<path class="st32" d="M814.4,1126c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1126.3,814.4,1126z"/>
										<path class="st32" d="M824.8,1126.6l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4H821v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H824.8z"/>
										<path class="st32" d="M830.6,1120.2h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L830.6,1120.2z"/>
										<path class="st32" d="M848.4,1122.3c0,1.4-0.3,2.4-1,3.2s-1.7,1.2-2.9,1.2c-1.3,0-2.2-0.4-2.9-1.2s-1-1.9-1-3.3
											c0-1.4,0.3-2.5,1-3.2s1.7-1.2,2.9-1.2c1.2,0,2.2,0.4,2.9,1.2S848.4,1120.9,848.4,1122.3z M841.6,1122.3c0,1.2,0.2,2,0.7,2.6
											s1.2,0.9,2.2,0.9c0.9,0,1.7-0.3,2.2-0.9s0.7-1.5,0.7-2.6c0-1.2-0.2-2-0.7-2.6s-1.2-0.9-2.1-0.9c-0.9,0-1.7,0.3-2.2,0.9
											S841.6,1121.2,841.6,1122.3z"/>
										<path class="st32" d="M853.1,1120.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S852.7,1120.1,853.1,1120.1z"/>
										<path class="st32" d="M857.8,1126.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S858.2,1126.7,857.8,1126.7z M857.5,1120.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S858,1120.9,857.5,1120.9z"/>
										<path class="st32" d="M864.4,1126.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C865.6,1126.6,865,1126.7,864.4,1126.7z"/>
										<path class="st32" d="M868.5,1123.3c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C868.5,1122.5,868.5,1122.9,868.5,1123.3L868.5,1123.3z"/>
										<path class="st32" d="M881.9,1124.3c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S881.9,1123.8,881.9,1124.3z"/>
										<path class="st32" d="M885.6,1125.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S885.3,1125.9,885.6,1125.9z"/>
										<path class="st32" d="M893.3,1123.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S893.3,1122.4,893.3,1123.4z M888.4,1123.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S888.4,1122.6,888.4,1123.4z"/>
										<path class="st32" d="M897.9,1120.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S897.5,1120.1,897.9,1120.1z"/>
										<path class="st32" d="M902.6,1126.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S903.1,1126.7,902.6,1126.7z M902.4,1120.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S902.9,1120.9,902.4,1120.9z"/>
									</g>
								</g>
								<g id="shopping_txt_12">
									<g>
										<path class="st32" d="M804,1141.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1141.6z"/>
										<path class="st32" d="M812.8,1141.6h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V1141.6z"/>
										<path class="st32" d="M814.4,1141c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1141.3,814.4,1141z"/>
										<path class="st32" d="M821,1133.1h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V1133.1z M822,1136.7h1.6c0.7,0,1.2-0.1,1.5-0.3
											s0.5-0.6,0.5-1.1c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3H822V1136.7z M822,1137.6v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H822z"/>
										<path class="st32" d="M828.5,1133.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S828.5,1133.7,828.5,1133.5z M829.6,1141.6h-1v-6.4h1V1141.6z"/>
										<path class="st32" d="M836.9,1135.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5
											s-1,0.6-1.8,0.6c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H836.9z M831.8,1142.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S831.8,1142.3,831.8,1142.7z M832.3,1137.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S832.3,1136.8,832.3,1137.3z"/>
										<path class="st32" d="M846.9,1135.5c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2
											C845.8,1133.1,846.9,1133.9,846.9,1135.5z M842.5,1137.4h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3
											s-0.9-0.4-1.7-0.4h-1.1V1137.4z"/>
										<path class="st32" d="M851.3,1141.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S851.7,1141.7,851.3,1141.7z M851,1135.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S851.5,1135.9,851,1135.9z"/>
										<path class="st32" d="M859.2,1141.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H859.2z M857,1140.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S856.7,1140.9,857,1140.9z"/>
										<path class="st32" d="M864.5,1141.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C865.7,1141.6,865.2,1141.7,864.5,1141.7z"/>
										<path class="st32" d="M872.1,1141.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H872.1z"/>
										<path class="st32" d="M879.3,1138.1v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H879.3z M879.3,1137.2h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V1137.2z"
											/>
										<path class="st32" d="M886.5,1135.2v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H886.5z"/>
										<path class="st32" d="M897.3,1141.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H897.3z"/>
										<path class="st32" d="M904.7,1141.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H904.7z"/>
										<path class="st32" d="M907.6,1133.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S907.6,1133.7,907.6,1133.5z M908.6,1141.6h-1v-6.4h1V1141.6z"/>
										<path class="st32" d="M915.1,1141.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H915.1z"/>
										<path class="st32" d="M923.3,1135.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5
											s-1,0.6-1.8,0.6c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H923.3z M918.2,1142.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S918.2,1142.3,918.2,1142.7z M918.7,1137.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S918.7,1136.8,918.7,1137.3z"/>
									</g>
								</g>
								<g id="shopping_txt_13">
									<g>
										<path class="st32" d="M804,1156.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1156.6z"/>
										<path class="st32" d="M812.5,1150.1c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6h-0.8v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S812.5,1149.4,812.5,1150.1z"/>
										<path class="st32" d="M814.4,1156c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1156.3,814.4,1156z"/>
										<path class="st32" d="M827.5,1156.6h-1v-4H822v4h-1v-8.6h1v3.6h4.5v-3.6h1V1156.6z"/>
										<path class="st32" d="M836.3,1156.6h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V1156.6z"/>
										<path class="st32" d="M845.6,1152.1h2.9v4.2c-0.5,0.1-0.9,0.3-1.4,0.3s-1,0.1-1.6,0.1c-1.3,0-2.3-0.4-3-1.2s-1.1-1.9-1.1-3.2
											c0-0.9,0.2-1.7,0.5-2.3s0.9-1.2,1.5-1.5s1.5-0.5,2.4-0.5c0.9,0,1.8,0.2,2.6,0.5l-0.4,0.9c-0.8-0.3-1.5-0.5-2.2-0.5
											c-1,0-1.9,0.3-2.4,0.9s-0.9,1.5-0.9,2.6c0,1.2,0.3,2,0.8,2.6s1.4,0.9,2.5,0.9c0.6,0,1.2-0.1,1.7-0.2v-2.6h-1.9V1152.1z"/>
										<path class="st32" d="M853.3,1150.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S852.9,1150.1,853.3,1150.1z"/>
										<path class="st32" d="M858,1156.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S858.4,1156.7,858,1156.7z M857.7,1150.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S858.2,1150.9,857.7,1150.9z"/>
										<path class="st32" d="M867.3,1150.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5
											s-1,0.6-1.8,0.6c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H867.3z M862.2,1157.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S862.2,1157.3,862.2,1157.7z M862.7,1152.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S862.7,1151.8,862.7,1152.3z"/>
										<path class="st32" d="M873.9,1150.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5
											s-1,0.6-1.8,0.6c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H873.9z M868.7,1157.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S868.7,1157.3,868.7,1157.7z M869.3,1152.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S869.3,1151.8,869.3,1152.3z"/>
									</g>
								</g>
								<g id="shopping_txt_14">
									<g>
										<path class="st32" d="M804,1171.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1171.6z"/>
										<path class="st32" d="M813.3,1169.6H812v2H811v-2h-4.2v-0.8l4.1-5.8h1v5.8h1.3V1169.6z M811,1168.8v-2.8c0-0.6,0-1.2,0.1-1.9h0
											c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H811z"/>
										<path class="st32" d="M814.4,1171c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1171.3,814.4,1171z"/>
										<path class="st32" d="M826.4,1171.6l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H826.4z M825,1168l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H825z"/>
										<path class="st32" d="M829.8,1171.6l-2.4-6.4h1l1.4,3.8c0.3,0.9,0.5,1.5,0.6,1.7h0c0-0.2,0.2-0.6,0.4-1.3s0.7-2.1,1.5-4.3h1
											l-2.4,6.4H829.8z"/>
										<path class="st32" d="M838.4,1171.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H838.4z M836.2,1170.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S835.8,1170.9,836.2,1170.9z"/>
										<path class="st32" d="M842.1,1171.6h-1v-9.1h1V1171.6z"/>
										<path class="st32" d="M849.7,1168.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S849.7,1167.4,849.7,1168.4z M844.8,1168.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S844.8,1167.6,844.8,1168.4z"/>
										<path class="st32" d="M855.8,1171.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H855.8z"/>
									</g>
								</g>
								<g id="shopping_txt_15">
									<g>
										<path class="st32" d="M804,1186.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1186.6z"/>
										<path class="st32" d="M809.9,1181.4c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C809,1181.4,809.4,1181.4,809.9,1181.4z"/>
										<path class="st32" d="M814.4,1186c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1186.3,814.4,1186z"/>
										<path class="st32" d="M828.5,1186.6h-1l-1.7-5.7c-0.1-0.3-0.2-0.6-0.3-1s-0.2-0.6-0.2-0.7c-0.1,0.5-0.2,1.1-0.4,1.7l-1.7,5.7h-1
											l-2.3-8.6h1.1l1.4,5.3c0.2,0.7,0.3,1.4,0.4,2c0.1-0.7,0.3-1.4,0.5-2.1l1.5-5.2h1.1l1.6,5.3c0.2,0.6,0.3,1.3,0.5,2.1
											c0.1-0.6,0.2-1.2,0.4-2l1.3-5.3h1.1L828.5,1186.6z"/>
										<path class="st32" d="M836.3,1186.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H836.3z"/>
										<path class="st32" d="M844.9,1183.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S844.9,1182.4,844.9,1183.4z M840,1183.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S840,1182.6,840,1183.4z"/>
										<path class="st32" d="M847.5,1186.6h-1v-9.1h1V1186.6z"/>
										<path class="st32" d="M852.3,1186.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S852.7,1186.7,852.3,1186.7z M852,1180.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S852.5,1180.9,852,1180.9z"/>
										<path class="st32" d="M860.6,1186.6h-1v-8.6h4.8v0.9h-3.8v3.1h3.6v0.9h-3.6V1186.6z"/>
										<path class="st32" d="M871.2,1183.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S871.2,1182.4,871.2,1183.4z M866.3,1183.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S866.3,1182.6,866.3,1183.4z"/>
										<path class="st32" d="M878.4,1183.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S878.4,1182.4,878.4,1183.4z M873.5,1183.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S873.5,1182.6,873.5,1183.4z"/>
										<path class="st32" d="M884.5,1185.8L884.5,1185.8c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L884.5,1185.8z M882.6,1185.9c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S882,1185.9,882.6,1185.9z"/>
										<path class="st32" d="M891.6,1184.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S891.6,1184.4,891.6,1184.9z"/>
									</g>
								</g>
								<g id="shopping_txt_16">
									<g>
										<path class="st32" d="M804,1201.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1201.6z"/>
										<path class="st32" d="M807.3,1198c0-1.7,0.3-2.9,1-3.8s1.6-1.3,2.9-1.3c0.4,0,0.8,0,1,0.1v0.8c-0.3-0.1-0.6-0.1-1-0.1
											c-0.9,0-1.6,0.3-2.1,0.9s-0.8,1.5-0.8,2.7h0.1c0.4-0.7,1.1-1,2-1c0.8,0,1.4,0.2,1.8,0.7s0.7,1.1,0.7,1.9c0,0.9-0.2,1.6-0.7,2.1
											s-1.1,0.8-2,0.8c-0.9,0-1.6-0.3-2.1-1S807.3,1199.1,807.3,1198z M810.2,1200.9c0.6,0,1-0.2,1.3-0.5s0.5-0.9,0.5-1.5
											c0-0.6-0.1-1-0.4-1.3s-0.7-0.5-1.3-0.5c-0.4,0-0.7,0.1-1,0.2s-0.5,0.3-0.7,0.6s-0.3,0.5-0.3,0.8c0,0.4,0.1,0.8,0.2,1.1
											s0.4,0.6,0.7,0.8S809.8,1200.9,810.2,1200.9z"/>
										<path class="st32" d="M814.4,1201c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1201.3,814.4,1201z"/>
										<path class="st32" d="M827.7,1201.6h-1.1l-4.7-7.2h0c0.1,0.8,0.1,1.6,0.1,2.3v4.9H821v-8.6h1.1l4.7,7.2h0c0-0.1,0-0.4-0.1-1
											s0-1,0-1.2v-4.9h0.9V1201.6z"/>
										<path class="st32" d="M835.4,1198.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S835.4,1197.4,835.4,1198.4z M830.5,1198.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S830.5,1197.6,830.5,1198.4z"/>
										<path class="st32" d="M840.1,1195.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S839.7,1195.1,840.1,1195.1z"/>
										<path class="st32" d="M844.1,1200.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S843.8,1200.9,844.1,1200.9z"/>
										<path class="st32" d="M850.7,1201.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H850.7z"/>
										<path class="st32" d="M856.6,1201.7c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S857.5,1201.7,856.6,1201.7z M856.5,1195.9c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S857,1195.9,856.5,1195.9z"/>
										<path class="st32" d="M866.5,1198.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S866.5,1197.4,866.5,1198.4z M861.6,1198.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S861.6,1197.6,861.6,1198.4z"/>
										<path class="st32" d="M868.1,1193.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S868.1,1193.7,868.1,1193.5z M869.2,1201.6h-1v-6.4h1V1201.6z"/>
										<path class="st32" d="M875.7,1201.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H875.7z"/>
										<path class="st32" d="M880.7,1200.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S880.4,1200.9,880.7,1200.9z"/>
										<path class="st32" d="M889.9,1201.6l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H889.9z"/>
										<path class="st32" d="M900.8,1201.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H900.8z M898.6,1200.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S898.2,1200.9,898.6,1200.9z"/>
										<path class="st32" d="M904.5,1201.6h-1v-9.1h1V1201.6z"/>
										<path class="st32" d="M907.5,1201.6h-1v-9.1h1V1201.6z"/>
									</g>
								</g>
								<g id="shopping_txt_17">
									<g>
										<path class="st32" d="M804,1216.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1216.6z"/>
										<path class="st32" d="M808.3,1216.6l3.6-7.7h-4.7v-0.9h5.7v0.8l-3.5,7.8H808.3z"/>
										<path class="st32" d="M814.4,1216c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1216.3,814.4,1216z"/>
										<path class="st32" d="M825.9,1208.1h1.1l-3.1,8.6h-1l-3.1-8.6h1.1l2,5.5c0.2,0.6,0.4,1.3,0.5,1.9c0.1-0.6,0.3-1.3,0.6-1.9
											L825.9,1208.1z"/>
										<path class="st32" d="M830.7,1216.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S831.1,1216.7,830.7,1216.7z M830.4,1210.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S830.9,1210.9,830.4,1210.9z"/>
										<path class="st32" d="M837.6,1210.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S837.2,1210.1,837.6,1210.1z"/>
										<path class="st32" d="M839.5,1208.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S839.5,1208.7,839.5,1208.5z M840.6,1216.6h-1v-6.4h1V1216.6z"/>
										<path class="st32" d="M846.7,1216.6h-4.7v-0.7l3.5-5h-3.3v-0.8h4.4v0.8l-3.5,4.9h3.5V1216.6z"/>
										<path class="st32" d="M853.8,1213.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S853.8,1212.4,853.8,1213.4z M848.9,1213.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S848.9,1212.6,848.9,1213.4z"/>
										<path class="st32" d="M859.9,1216.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H859.9z"/>
										<path class="st32" d="M873.6,1216.6h-1l-1.7-5.7c-0.1-0.3-0.2-0.6-0.3-1s-0.2-0.6-0.2-0.7c-0.1,0.5-0.2,1.1-0.4,1.7l-1.7,5.7h-1
											l-2.3-8.6h1.1l1.4,5.3c0.2,0.7,0.3,1.4,0.4,2c0.1-0.7,0.3-1.4,0.5-2.1l1.5-5.2h1.1l1.6,5.3c0.2,0.6,0.3,1.3,0.5,2.1
											c0.1-0.6,0.2-1.2,0.4-2l1.3-5.3h1.1L873.6,1216.6z"/>
										<path class="st32" d="M877,1208.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S877,1208.7,877,1208.5z M878.1,1216.6h-1v-6.4h1V1216.6z"/>
										<path class="st32" d="M883.1,1210.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S882.7,1210.1,883.1,1210.1z"/>
										<path class="st32" d="M887.7,1216.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S888.2,1216.7,887.7,1216.7z M887.5,1210.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S888,1210.9,887.5,1210.9z"/>
										<path class="st32" d="M892.7,1216.6h-1v-9.1h1V1216.6z"/>
										<path class="st32" d="M897.5,1216.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S897.9,1216.7,897.5,1216.7z M897.2,1210.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S897.8,1210.9,897.2,1210.9z"/>
										<path class="st32" d="M905.7,1214.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S905.7,1214.4,905.7,1214.9z"/>
										<path class="st32" d="M911.4,1214.9c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S911.4,1214.4,911.4,1214.9z"/>
										<path class="st32" d="M921.6,1216.6l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H921.6z M920.2,1213l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H920.2z"/>
										<path class="st32" d="M931.9,1216.6v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H931.9z"/>
										<path class="st32" d="M937.8,1216.7c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S938.7,1216.7,937.8,1216.7z M937.7,1210.9c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S938.3,1210.9,937.7,1210.9z"/>
										<path class="st32" d="M946.6,1216.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H946.6z"/>
										<path class="st32" d="M949.5,1208.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S949.5,1208.7,949.5,1208.5z M950.5,1216.6h-1v-6.4h1V1216.6z"/>
										<path class="st32" d="M954.7,1215.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S954.4,1215.9,954.7,1215.9z"/>
										<path class="st32" d="M961.2,1216.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H961.2z"/>
										<path class="st32" d="M966.9,1216.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S967.4,1216.7,966.9,1216.7z M966.7,1210.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S967.2,1210.9,966.7,1210.9z"/>
										<path class="st32" d="M974.9,1216.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H974.9z M972.7,1215.9c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S972.3,1215.9,972.7,1215.9z"/>
										<path class="st32" d="M979.7,1215.9c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
											S979.4,1215.9,979.7,1215.9z"/>
										<path class="st32" d="M984.6,1216.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S985,1216.7,984.6,1216.7z M984.3,1210.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S984.8,1210.9,984.3,1210.9z"/>
										<path class="st32" d="M991.5,1210.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S991.1,1210.1,991.5,1210.1z"/>
									</g>
								</g>
								<g id="shopping_txt_18">
									<g>
										<path class="st32" d="M804,1231.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1231.6z"/>
										<path class="st32" d="M810.1,1222.9c0.8,0,1.4,0.2,1.9,0.5s0.7,0.9,0.7,1.5c0,0.4-0.1,0.8-0.4,1.2s-0.7,0.7-1.3,0.9
											c0.7,0.3,1.2,0.7,1.5,1s0.4,0.8,0.4,1.3c0,0.7-0.2,1.3-0.7,1.7s-1.2,0.6-2,0.6c-0.9,0-1.6-0.2-2.1-0.6s-0.7-1-0.7-1.7
											c0-1,0.6-1.7,1.8-2.3c-0.5-0.3-0.9-0.6-1.2-1s-0.4-0.7-0.4-1.2c0-0.6,0.2-1.1,0.7-1.5S809.3,1222.9,810.1,1222.9z M808.2,1229.5
											c0,0.5,0.2,0.8,0.5,1.1s0.8,0.4,1.4,0.4c0.6,0,1-0.1,1.4-0.4s0.5-0.6,0.5-1.1c0-0.4-0.2-0.7-0.5-1s-0.8-0.6-1.6-0.9
											c-0.6,0.2-1,0.5-1.3,0.8S808.2,1229,808.2,1229.5z M810,1223.7c-0.5,0-0.9,0.1-1.1,0.4s-0.4,0.5-0.4,0.9c0,0.4,0.1,0.7,0.3,0.9
											s0.7,0.5,1.3,0.8c0.6-0.2,1-0.5,1.2-0.8s0.3-0.6,0.3-0.9c0-0.4-0.1-0.7-0.4-0.9S810.5,1223.7,810,1223.7z"/>
										<path class="st32" d="M814.4,1231c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1231.3,814.4,1231z"/>
										<path class="st32" d="M823.6,1231.6h-1v-7.7h-2.7v-0.9h6.4v0.9h-2.7V1231.6z"/>
										<path class="st32" d="M833,1228.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S833,1227.4,833,1228.4z M828.1,1228.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S828.1,1227.6,828.1,1228.4z"/>
										<path class="st32" d="M837.7,1231.7c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S838.5,1231.7,837.7,1231.7z M837.6,1225.9c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S838.1,1225.9,837.6,1225.9z"/>
										<path class="st32" d="M847.3,1225.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5
											s-1,0.6-1.8,0.6c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H847.3z M842.2,1232.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S842.2,1232.3,842.2,1232.7z M842.7,1227.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S842.7,1226.8,842.7,1227.3z"/>
										<path class="st32" d="M854.2,1228.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S854.2,1227.4,854.2,1228.4z M849.3,1228.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S849.3,1227.6,849.3,1228.4z"/>
										<path class="st32" d="M856.9,1231.6h-1v-9.1h1V1231.6z"/>
										<path class="st32" d="M861.8,1226h-1.6v5.7h-1v-5.7h-1.1v-0.4l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2
											l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4s-0.3,0.6-0.3,1.2v0.4h1.6V1226z"/>
									</g>
								</g>
								<g id="shopping_txt_19">
									<g>
										<path class="st32" d="M804,1246.6H803v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V1246.6z"/>
										<path class="st32" d="M812.8,1241.7c0,3.4-1.3,5-3.9,5c-0.5,0-0.8,0-1.1-0.1v-0.8c0.3,0.1,0.7,0.2,1.1,0.2
											c0.9,0,1.6-0.3,2.1-0.9s0.7-1.5,0.8-2.7h-0.1c-0.2,0.3-0.5,0.6-0.9,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.7
											s-0.7-1.1-0.7-1.9c0-0.9,0.2-1.6,0.7-2.1s1.2-0.8,2-0.8c0.6,0,1.1,0.1,1.5,0.4s0.8,0.7,1,1.3S812.8,1240.9,812.8,1241.7z
											 M810,1238.8c-0.6,0-1,0.2-1.3,0.5s-0.5,0.9-0.5,1.5c0,0.6,0.1,1,0.4,1.3s0.7,0.5,1.3,0.5c0.4,0,0.7-0.1,1-0.2s0.5-0.3,0.7-0.6
											s0.3-0.5,0.3-0.8c0-0.4-0.1-0.8-0.2-1.1s-0.4-0.6-0.7-0.8S810.3,1238.8,810,1238.8z"/>
										<path class="st32" d="M814.4,1246c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1246.3,814.4,1246z"/>
										<path class="st32" d="M823.6,1246.6h-1v-7.7h-2.7v-0.9h6.4v0.9h-2.7V1246.6z"/>
										<path class="st32" d="M831.9,1246.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H831.9z"/>
										<path class="st32" d="M837.5,1246.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S838,1246.7,837.5,1246.7z M837.3,1240.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S837.8,1240.9,837.3,1240.9z"/>
										<path class="st32" d="M848.5,1238.8c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C849.8,1239,849.1,1238.8,848.5,1238.8z"/>
										<path class="st32" d="M857.8,1243.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S857.8,1242.4,857.8,1243.4z M852.9,1243.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S852.9,1242.6,852.9,1243.4z"/>
										<path class="st32" d="M865,1243.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S865,1242.4,865,1243.4z M860.2,1243.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S860.2,1242.6,860.2,1243.4z"/>
										<path class="st32" d="M867.7,1246.6h-1v-9.1h1V1246.6z"/>
										<path class="st32" d="M872.5,1246.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S872.9,1246.7,872.5,1246.7z M872.2,1240.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S872.7,1240.9,872.2,1240.9z"/>
										<path class="st32" d="M879.4,1240.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S879,1240.1,879.4,1240.1z"/>
										<path class="st32" d="M884.7,1246.6v-8.6h1v8.6H884.7z"/>
										<path class="st32" d="M890.4,1246.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C891.6,1246.6,891.1,1246.7,890.4,1246.7z"/>
										<path class="st32" d="M896.3,1246.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S896.7,1246.7,896.3,1246.7z M896,1240.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S896.6,1240.9,896,1240.9z"/>
										<path class="st32" d="M904.6,1243.1v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H904.6z M904.6,1242.2h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V1242.2z"
											/>
										<path class="st32" d="M910.8,1238.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S910.8,1238.7,910.8,1238.5z M911.8,1246.6h-1v-6.4h1V1246.6z"/>
										<path class="st32" d="M918.3,1246.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H918.3z"/>
										<path class="st32" d="M922.2,1243.3c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C922.2,1242.5,922.2,1242.9,922.2,1243.3L922.2,1243.3z"/>
									</g>
								</g>
								<g id="shopping_txt_20">
									<g>
										<path class="st32" d="M806,1261.6h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V1261.6z"/>
										<path class="st32" d="M812.9,1257.3c0,1.5-0.2,2.6-0.7,3.3s-1.2,1.1-2.1,1.1c-0.9,0-1.6-0.4-2.1-1.1s-0.7-1.8-0.7-3.3
											c0-1.5,0.2-2.6,0.7-3.3s1.2-1.1,2.1-1.1c0.9,0,1.6,0.4,2.1,1.1S812.9,1255.9,812.9,1257.3z M808.2,1257.3c0,1.2,0.1,2.2,0.4,2.7
											s0.8,0.9,1.4,0.9c0.6,0,1.1-0.3,1.4-0.9s0.4-1.5,0.4-2.7s-0.1-2.1-0.4-2.7s-0.8-0.9-1.4-0.9c-0.6,0-1.1,0.3-1.4,0.8
											S808.2,1256.1,808.2,1257.3z"/>
										<path class="st32" d="M814.4,1261c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S814.4,1261.3,814.4,1261z"/>
										<path class="st32" d="M825.9,1253.1h1.1l-3.1,8.6h-1l-3.1-8.6h1.1l2,5.5c0.2,0.6,0.4,1.3,0.5,1.9c0.1-0.6,0.3-1.3,0.6-1.9
											L825.9,1253.1z"/>
										<path class="st32" d="M827.9,1253.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S827.9,1253.7,827.9,1253.5z M828.9,1261.6h-1v-6.4h1V1261.6z"/>
										<path class="st32" d="M833.6,1261.7c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C834.8,1261.6,834.2,1261.7,833.6,1261.7z"/>
										<path class="st32" d="M837.7,1258.3c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C837.7,1257.5,837.7,1257.9,837.7,1258.3L837.7,1258.3z"/>
										<path class="st32" d="M845.7,1261.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S846.2,1261.7,845.7,1261.7z M845.5,1255.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S846,1255.9,845.5,1255.9z"/>
										<path class="st32" d="M852.7,1255.1c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S852.3,1255.1,852.7,1255.1z"/>
										<path class="st32" d="M853.6,1255.2h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L853.6,1255.2z"/>
										<path class="st32" d="M868.9,1253.1h1.1l-3.1,8.6h-1l-3.1-8.6h1.1l2,5.5c0.2,0.6,0.4,1.3,0.5,1.9c0.1-0.6,0.3-1.3,0.6-1.9
											L868.9,1253.1z"/>
										<path class="st32" d="M870.9,1253.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S870.9,1253.7,870.9,1253.5z M871.9,1261.6h-1v-6.4h1V1261.6z"/>
										<path class="st32" d="M875,1261.6h-1v-9.1h1V1261.6z"/>
										<path class="st32" d="M878,1261.6h-1v-9.1h1V1261.6z"/>
										<path class="st32" d="M884,1261.6l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H884z M881.8,1260.9c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S881.4,1260.9,881.8,1260.9z"/>
										<path class="st32" d="M892,1255.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H892z M886.9,1262.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S886.9,1262.3,886.9,1262.7z M887.4,1257.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S887.4,1256.8,887.4,1257.3z"/>
										<path class="st32" d="M896,1261.7c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6H894c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S896.5,1261.7,896,1261.7z M895.8,1255.9c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S896.3,1255.9,895.8,1255.9z"/>
									</g>
								</g>
								<g id="shopping_txt_title">
									<g>
										<path class="st32" d="M805.8,959.3c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S805.8,958.8,805.8,959.3z"/>
										<path class="st32" d="M811.8,961.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H811.8z"/>
										<path class="st32" d="M820.3,958.4c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S820.3,957.4,820.3,958.4z M815.4,958.4
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S815.4,957.6,815.4,958.4z"/>
										<path class="st32" d="M825,961.7c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S825.8,961.7,825,961.7z
											 M824.8,955.9c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S825.4,955.9,824.8,955.9z"/>
										<path class="st32" d="M832.3,961.7c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S833.2,961.7,832.3,961.7z M832.2,955.9c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S832.8,955.9,832.2,955.9z"/>
										<path class="st32" d="M836.6,953.5c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S836.6,953.7,836.6,953.5z M837.7,961.6h-1v-6.4h1V961.6z"/>
										<path class="st32" d="M844.1,961.6v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H844.1z"/>
										<path class="st32" d="M852.4,955.2v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H852.4z M847.2,962.7c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S847.2,962.3,847.2,962.7z M847.8,957.3c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S847.8,956.8,847.8,957.3z"/>
									</g>
								</g>
							</g>
							<g id="Spa_x2F_Salon">
								<g id="spa_txt_1">
									<g>
										<path class="st32" d="M803.5,861.9h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V861.9z"/>
										<path class="st32" d="M807,861.2c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807,861.5,807,861.2z"/>
										<path class="st32" d="M817.3,854.1c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C818.6,854.2,817.9,854.1,817.3,854.1z"/>
										<path class="st32" d="M826.6,858.6c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S826.6,857.6,826.6,858.6z M821.7,858.6
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S821.7,857.8,821.7,858.6z"/>
										<path class="st32" d="M829.3,861.9h-1v-9.1h1V861.9z"/>
										<path class="st32" d="M832.3,861.9h-1v-9.1h1V861.9z"/>
										<path class="st32" d="M838.3,861.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H838.3z M836.1,861.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S835.7,861.2,836.1,861.2z"/>
										<path class="st32" d="M846.3,855.4v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H846.3z M841.2,862.9c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S841.2,862.5,841.2,862.9z M841.7,857.5c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S841.7,857,841.7,857.5z"/>
										<path class="st32" d="M850.3,862c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S850.8,862,850.3,862z M850.1,856.1c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
											S850.6,856.1,850.1,856.1z"/>
										<path class="st32" d="M862.4,859.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S862.4,859,862.4,859.6z"/>
										<path class="st32" d="M868,861.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H868z M865.8,861.2c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S865.4,861.2,865.8,861.2z"/>
										<path class="st32" d="M871.7,861.9h-1v-9.1h1V861.9z"/>
										<path class="st32" d="M879.3,858.6c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S879.3,857.6,879.3,858.6z M874.4,858.6
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S874.4,857.8,874.4,858.6z"/>
										<path class="st32" d="M885.4,861.9v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H885.4z"/>
									</g>
								</g>
								<g id="spa_txt_2">
									<g>
										<path class="st32" d="M805.5,876.9h-5.6V876l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V876.9z"/>
										<path class="st32" d="M807,876.2c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807,876.5,807,876.2z"/>
										<path class="st32" d="M813.6,868.3h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V868.3z M814.6,872h1.6c0.7,0,1.2-0.1,1.5-0.3s0.5-0.6,0.5-1.1
											c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V872z M814.6,872.8v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H814.6z"/>
										<path class="st32" d="M822.2,876.9h-1v-9.1h1V876.9z"/>
										<path class="st32" d="M825.2,870.4v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H825.2z"/>
										<path class="st32" d="M834.4,877c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S834.8,877,834.4,877z M834.1,871.1c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
											S834.6,871.1,834.1,871.1z"/>
										<path class="st32" d="M843.6,870.4v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H843.6z M838.5,877.9c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S838.5,877.5,838.5,877.9z M839,872.5c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S839,872,839,872.5z"/>
										<path class="st32" d="M847.9,870.3c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S847.5,870.3,847.9,870.3z"/>
										<path class="st32" d="M853.8,876.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H853.8z M851.6,876.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S851.2,876.2,851.6,876.2z"/>
										<path class="st32" d="M860.7,875.1c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S860.7,874.7,860.7,875.1z"/>
										<path class="st32" d="M866.4,875.1c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S866.4,874.7,866.4,875.1z"/>
										<path class="st32" d="M877.9,876.9h-1.1l-4.7-7.2h0c0.1,0.8,0.1,1.6,0.1,2.3v4.9h-0.9v-8.6h1.1l4.7,7.2h0c0-0.1,0-0.4-0.1-1
											s0-1,0-1.2v-4.9h0.9V876.9z"/>
										<path class="st32" d="M884.1,876.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H884.1z M881.9,876.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S881.5,876.2,881.9,876.2z"/>
										<path class="st32" d="M886.7,868.7c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S886.7,868.9,886.7,868.7z M887.8,876.9h-1v-6.4h1V876.9z"/>
										<path class="st32" d="M890.8,876.9h-1v-9.1h1V876.9z"/>
										<path class="st32" d="M901,874.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S901,874,901,874.6z"/>
										<path class="st32" d="M906.5,876.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H906.5z M904.3,876.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S904,876.2,904.3,876.2z"/>
										<path class="st32" d="M910.2,876.9h-1v-9.1h1V876.9z"/>
										<path class="st32" d="M917.8,873.6c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S917.8,872.6,917.8,873.6z M912.9,873.6
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S912.9,872.8,912.9,873.6z"/>
										<path class="st32" d="M923.9,876.9v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H923.9z"/>
										<path class="st32" d="M929.7,874.6c0-0.5,0.1-1,0.4-1.3s0.8-0.8,1.5-1.2c-0.3-0.4-0.6-0.7-0.7-0.8s-0.2-0.4-0.3-0.6
											s-0.1-0.4-0.1-0.6c0-0.6,0.2-1,0.6-1.4s0.9-0.5,1.6-0.5c0.6,0,1.1,0.2,1.5,0.5s0.5,0.8,0.5,1.4c0,0.4-0.1,0.8-0.4,1.2
											s-0.7,0.7-1.3,1.1l2.4,2.3c0.2-0.2,0.4-0.5,0.5-0.9s0.2-0.7,0.3-1.1h1c-0.3,1.1-0.7,2-1.2,2.5l1.8,1.7h-1.3l-1.1-1
											c-0.5,0.4-0.9,0.7-1.4,0.9s-1,0.3-1.6,0.3c-0.8,0-1.5-0.2-2-0.6S929.7,875.4,929.7,874.6z M932.3,876.1c0.9,0,1.7-0.3,2.3-0.9
											l-2.6-2.5c-0.4,0.3-0.7,0.5-0.9,0.7s-0.3,0.4-0.4,0.6s-0.1,0.4-0.1,0.7c0,0.5,0.2,0.8,0.5,1.1S931.8,876.1,932.3,876.1z
											 M931.4,870c0,0.3,0.1,0.5,0.2,0.8s0.4,0.5,0.7,0.9c0.5-0.3,0.9-0.6,1.1-0.8s0.3-0.5,0.3-0.9c0-0.3-0.1-0.5-0.3-0.7
											s-0.5-0.3-0.8-0.3c-0.3,0-0.6,0.1-0.8,0.3S931.4,869.7,931.4,870z"/>
										<path class="st32" d="M948.9,872.5c0,1.4-0.4,2.5-1.2,3.2s-1.9,1.1-3.3,1.1H942v-8.6h2.6c1.3,0,2.4,0.4,3.1,1.1
											S948.9,871.2,948.9,872.5z M947.8,872.5c0-1.1-0.3-2-0.8-2.5s-1.4-0.8-2.5-0.8H943v6.8h1.2c1.2,0,2.1-0.3,2.7-0.9
											S947.8,873.7,947.8,872.5z"/>
										<path class="st32" d="M954.6,876.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H954.6z M952.4,876.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S952,876.2,952.4,876.2z"/>
										<path class="st32" d="M956.3,870.4h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L956.3,870.4z"/>
										<path class="st32" d="M971.5,874.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S971.5,874,971.5,874.6z"/>
										<path class="st32" d="M976.1,877c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7H974c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9h0
											c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S976.9,877,976.1,877z
											 M975.9,871.1c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S976.5,871.1,975.9,871.1z"/>
										<path class="st32" d="M984.4,876.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H984.4z M982.2,876.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S981.8,876.2,982.2,876.2z"/>
									</g>
								</g>
								<g id="spa_txt_3">
									<g>
										<path class="st32" d="M805.2,885.3c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6h-0.8V887h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S805.2,884.6,805.2,885.3z"/>
										<path class="st32" d="M807,891.2c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807,891.5,807,891.2z"/>
										<path class="st32" d="M817.4,891.9l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H817.4z"/>
										<path class="st32" d="M832.4,889.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S832.4,889,832.4,889.6z"/>
										<path class="st32" d="M838,891.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H838z M835.8,891.2c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S835.4,891.2,835.8,891.2z"/>
										<path class="st32" d="M841.7,891.9h-1v-9.1h1V891.9z"/>
										<path class="st32" d="M849.3,888.6c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S849.3,887.6,849.3,888.6z M844.4,888.6
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S844.4,887.8,844.4,888.6z"/>
										<path class="st32" d="M855.4,891.9v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H855.4z"/>
										<path class="st32" d="M861.1,889.6c0-0.5,0.1-1,0.4-1.3s0.8-0.8,1.5-1.2c-0.3-0.4-0.6-0.7-0.7-0.8s-0.2-0.4-0.3-0.6
											s-0.1-0.4-0.1-0.6c0-0.6,0.2-1,0.6-1.4s0.9-0.5,1.6-0.5c0.6,0,1.1,0.2,1.5,0.5s0.5,0.8,0.5,1.4c0,0.4-0.1,0.8-0.4,1.2
											s-0.7,0.7-1.3,1.1l2.4,2.3c0.2-0.2,0.4-0.5,0.5-0.9s0.2-0.7,0.3-1.1h1c-0.3,1.1-0.7,2-1.2,2.5l1.8,1.7h-1.3l-1.1-1
											c-0.5,0.4-0.9,0.7-1.4,0.9s-1,0.3-1.6,0.3c-0.8,0-1.5-0.2-2-0.6S861.1,890.4,861.1,889.6z M863.7,891.1c0.9,0,1.7-0.3,2.3-0.9
											l-2.6-2.5c-0.4,0.3-0.7,0.5-0.9,0.7s-0.3,0.4-0.4,0.6s-0.1,0.4-0.1,0.7c0,0.5,0.2,0.8,0.5,1.1S863.2,891.1,863.7,891.1z
											 M862.8,885c0,0.3,0.1,0.5,0.2,0.8s0.4,0.5,0.7,0.9c0.5-0.3,0.9-0.6,1.1-0.8s0.3-0.5,0.3-0.9c0-0.3-0.1-0.5-0.3-0.7
											s-0.5-0.3-0.8-0.3c-0.3,0-0.6,0.1-0.8,0.3S862.8,884.7,862.8,885z"/>
										<path class="st32" d="M878.3,889.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S878.3,889,878.3,889.6z"/>
										<path class="st32" d="M882.9,892c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S883.7,892,882.9,892z
											 M882.8,886.1c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S883.3,886.1,882.8,886.1z"/>
										<path class="st32" d="M891.2,891.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H891.2z M889,891.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S888.6,891.2,889,891.2z"/>
									</g>
								</g>
								<g id="spa_txt_4">
									<g>
										<path class="st32" d="M805.9,904.9h-1.3v2h-0.9v-2h-4.2V904l4.1-5.8h1v5.8h1.3V904.9z M803.7,904v-2.8c0-0.6,0-1.2,0.1-1.9h0
											c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H803.7z"/>
										<path class="st32" d="M807,906.2c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807,906.5,807,906.2z"/>
										<path class="st32" d="M817.4,906.9l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H817.4z"/>
										<path class="st32" d="M824.2,898.7c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S824.2,898.9,824.2,898.7z M825.3,906.9h-1v-6.4h1V906.9z"/>
										<path class="st32" d="M831.7,906.9v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H831.7z"/>
										<path class="st32" d="M839.1,906L839.1,906c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L839.1,906z M837.1,906.2c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S836.5,906.2,837.1,906.2z"/>
										<path class="st32" d="M844.8,904.6c0-0.5,0.1-1,0.4-1.3s0.8-0.8,1.5-1.2c-0.3-0.4-0.6-0.7-0.7-0.8s-0.2-0.4-0.3-0.6
											s-0.1-0.4-0.1-0.6c0-0.6,0.2-1,0.6-1.4s0.9-0.5,1.6-0.5c0.6,0,1.1,0.2,1.5,0.5s0.5,0.8,0.5,1.4c0,0.4-0.1,0.8-0.4,1.2
											s-0.7,0.7-1.3,1.1l2.4,2.3c0.2-0.2,0.4-0.5,0.5-0.9s0.2-0.7,0.3-1.1h1c-0.3,1.1-0.7,2-1.2,2.5l1.8,1.7h-1.3l-1.1-1
											c-0.5,0.4-0.9,0.7-1.4,0.9s-1,0.3-1.6,0.3c-0.8,0-1.5-0.2-2-0.6S844.8,905.4,844.8,904.6z M847.5,906.1c0.9,0,1.7-0.3,2.3-0.9
											l-2.6-2.5c-0.4,0.3-0.7,0.5-0.9,0.7s-0.3,0.4-0.4,0.6s-0.1,0.4-0.1,0.7c0,0.5,0.2,0.8,0.5,1.1S847,906.1,847.5,906.1z
											 M846.6,900c0,0.3,0.1,0.5,0.2,0.8s0.4,0.5,0.7,0.9c0.5-0.3,0.9-0.6,1.1-0.8s0.3-0.5,0.3-0.9c0-0.3-0.1-0.5-0.3-0.7
											s-0.5-0.3-0.8-0.3c-0.3,0-0.6,0.1-0.8,0.3S846.6,899.7,846.6,900z"/>
										<path class="st32" d="M857.2,898.3h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V898.3z M858.2,902h1.6c0.7,0,1.2-0.1,1.5-0.3s0.5-0.6,0.5-1.1
											c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V902z M858.2,902.8v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H858.2z"/>
										<path class="st32" d="M870.4,903.6c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S870.4,902.6,870.4,903.6z M865.5,903.6
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S865.5,902.8,865.5,903.6z"/>
										<path class="st32" d="M876.4,906L876.4,906c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L876.4,906z M874.5,906.2c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S873.9,906.2,874.5,906.2z"/>
										<path class="st32" d="M878.4,900.4h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L878.4,900.4z"/>
										<path class="st32" d="M895.6,902.5c0,1.4-0.4,2.5-1.2,3.2s-1.9,1.1-3.3,1.1h-2.4v-8.6h2.6c1.3,0,2.4,0.4,3.1,1.1
											S895.6,901.2,895.6,902.5z M894.5,902.5c0-1.1-0.3-2-0.8-2.5s-1.4-0.8-2.5-0.8h-1.4v6.8h1.2c1.2,0,2.1-0.3,2.7-0.9
											S894.5,903.7,894.5,902.5z"/>
										<path class="st32" d="M901.3,906.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H901.3z M899.1,906.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S898.7,906.2,899.1,906.2z"/>
										<path class="st32" d="M903,900.4h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L903,900.4z"/>
										<path class="st32" d="M918.2,904.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S918.2,904,918.2,904.6z"/>
										<path class="st32" d="M922.8,907c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S923.6,907,922.8,907z
											 M922.6,901.1c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S923.2,901.1,922.6,901.1z"/>
										<path class="st32" d="M931.1,906.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H931.1z M928.9,906.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S928.5,906.2,928.9,906.2z"/>
									</g>
								</g>
								<g id="spa_txt_5">
									<g>
										<path class="st32" d="M802.5,916.6c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C801.6,916.7,802.1,916.6,802.5,916.6z"/>
										<path class="st32" d="M807,921.2c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S807,921.5,807,921.2z"/>
										<path class="st32" d="M819,921.9l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H819z M817.6,918.2l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H817.6z"/>
										<path class="st32" d="M826.3,915.4v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H826.3z M821.2,922.9c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S821.2,922.5,821.2,922.9z M821.7,917.5c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S821.7,917,821.7,917.5z"/>
										<path class="st32" d="M830.4,922c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S830.8,922,830.4,922z M830.1,916.1c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
											S830.6,916.1,830.1,916.1z"/>
										<path class="st32" d="M835.3,921.9h-1v-9.1h1V921.9z"/>
										<path class="st32" d="M840.1,922c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S840.6,922,840.1,922z M839.9,916.1c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
											S840.4,916.1,839.9,916.1z"/>
										<path class="st32" d="M848.3,920.1c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S848.3,919.7,848.3,920.1z"/>
										<path class="st32" d="M854,920.1c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S854,919.7,854,920.1z"/>
										<path class="st32" d="M859.8,918.3v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H859.8z M859.8,917.4h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V917.4z"/>
										<path class="st32" d="M868.8,922c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S869.3,922,868.8,922z M868.6,916.1c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
											S869.1,916.1,868.6,916.1z"/>
										<path class="st32" d="M881,921.9v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H881z"/>
										<path class="st32" d="M886.7,922c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S887.2,922,886.7,922z M886.5,916.1c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
											S887,916.1,886.5,916.1z"/>
										<path class="st32" d="M895.1,921L895.1,921c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L895.1,921z M893.2,921.2c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S892.6,921.2,893.2,921.2z"/>
										<path class="st32" d="M898,913.7c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S898,913.9,898,913.7z M899.1,921.9h-1v-6.4h1V921.9z"/>
										<path class="st32" d="M903.9,922c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S904.3,922,903.9,922z M903.6,916.1c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
											S904.1,916.1,903.6,916.1z"/>
										<path class="st32" d="M912,920.1c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S912,919.7,912,920.1z"/>
									</g>
								</g>
								<g id="spa_txt_title">
									<g>
										<path class="st32" d="M805.3,844.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S805.3,844,805.3,844.6z"/>
										<path class="st32" d="M809.9,847c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S810.7,847,809.9,847z
											 M809.7,841.1c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S810.3,841.1,809.7,841.1z"/>
										<path class="st32" d="M818.2,846.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H818.2z M816,846.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S815.6,846.2,816,846.2z"/>
										<path class="st32" d="M824.2,838.3l-3.2,8.6h-1l3.2-8.6H824.2z"/>
										<path class="st32" d="M830.3,844.6c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S830.3,844,830.3,844.6z"/>
										<path class="st32" d="M835.9,846.9l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H835.9z M833.7,846.2c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S833.3,846.2,833.7,846.2z"/>
										<path class="st32" d="M839.6,846.9h-1v-9.1h1V846.9z"/>
										<path class="st32" d="M847.2,843.6c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S847.2,842.6,847.2,843.6z M842.3,843.6
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S842.3,842.8,842.3,843.6z"/>
										<path class="st32" d="M853.3,846.9v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H853.3z"/>
									</g>
								</g>
							</g>
							<g id="Schools">
								<g id="school_txt_1">
									<g>
										<path class="st32" d="M803,701.1H802V695c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V701.1z"/>
										<path class="st32" d="M806.5,700.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,700.7,806.5,700.5z"/>
										<path class="st32" d="M818,698.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S818,698.2,818,698.8z"/>
										<path class="st32" d="M820.5,694.7v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H820.5z"/>
										<path class="st32" d="M835.1,701.1v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H835.1z"/>
										<path class="st32" d="M846.3,701.1v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H846.3z"/>
										<path class="st32" d="M849.2,692.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S849.2,693.1,849.2,692.9z M850.2,701.1h-1v-6.4h1V701.1z"/>
										<path class="st32" d="M854.4,700.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S854,700.4,854.4,700.4z"
											/>
										<path class="st32" d="M866.3,701.1h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V701.1z"/>
										<path class="st32" d="M868.4,692.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S868.4,693.1,868.4,692.9z M869.5,701.1h-1v-6.4h1V701.1z"/>
										<path class="st32" d="M872.5,701.1h-1V692h1V701.1z"/>
										<path class="st32" d="M875.5,701.1h-1V692h1V701.1z"/>
										<path class="st32" d="M876.6,694.7h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L876.6,694.7z"/>
										<path class="st32" d="M891.7,701.1h-4.8v-8.6h4.8v0.9h-3.8v2.8h3.6v0.9h-3.6v3.2h3.8V701.1z"/>
										<path class="st32" d="M894.4,701.1h-1V692h1V701.1z"/>
										<path class="st32" d="M899.2,701.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S899.6,701.2,899.2,701.2z M898.9,695.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S899.4,695.4,898.9,695.4z"/>
										<path class="st32" d="M911.4,701.1v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H911.4z"/>
										<path class="st32" d="M917.1,701.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6H915c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S917.5,701.2,917.1,701.2z M916.8,695.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S917.3,695.4,916.8,695.4z"/>
										<path class="st32" d="M925.5,701.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H925.5z"/>
										<path class="st32" d="M930.5,700.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S930.2,700.4,930.5,700.4
											z"/>
										<path class="st32" d="M936.6,701.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H936.6z M934.4,700.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S934.1,700.4,934.4,700.4z"/>
										<path class="st32" d="M942.3,694.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S941.9,694.5,942.3,694.5z"/>
										<path class="st32" d="M943.2,694.7h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L943.2,694.7z"/>
										<path class="st32" d="M958.4,698.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S958.4,698.2,958.4,698.8z"/>
										<path class="st32" d="M962.6,701.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C963.8,701.1,963.2,701.2,962.6,701.2z"/>
										<path class="st32" d="M970.1,701.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V692h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H970.1z"/>
										<path class="st32" d="M978.6,697.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S978.6,696.8,978.6,697.9z M973.7,697.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S973.7,697,973.7,697.9z"/>
										<path class="st32" d="M985.9,697.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S985.9,696.8,985.9,697.9z M981,697.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S981,697,981,697.9z"/>
										<path class="st32" d="M988.6,701.1h-1V692h1V701.1z"/>
									</g>
								</g>
								<g id="school_txt_2">
									<g>
										<path class="st32" d="M805,716.1h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V716.1z"/>
										<path class="st32" d="M806.5,715.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,715.7,806.5,715.5z"/>
										<path class="st32" d="M819.3,716.1h-1.2l-3.1-4.2l-0.9,0.8v3.4h-1v-8.6h1v4.2l3.9-4.2h1.2l-3.4,3.7L819.3,716.1z"/>
										<path class="st32" d="M820.3,707.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S820.3,708.1,820.3,707.9z M821.3,716.1h-1v-6.4h1V716.1z"/>
										<path class="st32" d="M827.8,716.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H827.8z"/>
										<path class="st32" d="M836,709.7v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H836z M830.9,717.2c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S830.9,716.8,830.9,717.2z M831.4,711.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S831.4,711.2,831.4,711.7z"/>
										<path class="st32" d="M838.1,707.5l0.1,0.1c-0.1,0.4-0.2,0.8-0.4,1.4s-0.4,1-0.6,1.4h-0.7c0.3-1.1,0.5-2.1,0.6-2.9H838.1z"/>
										<path class="st32" d="M843.5,714.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S843.5,713.9,843.5,714.3z"/>
										<path class="st32" d="M849.3,712.5v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H849.3z M849.3,711.7h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V711.7z"/>
										<path class="st32" d="M855.5,707.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S855.5,708.1,855.5,707.9z M856.6,716.1h-1v-6.4h1V716.1z"/>
										<path class="st32" d="M863,715.2L863,715.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V707h1v9.1h-0.8L863,715.2z M861.1,715.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S860.5,715.4,861.1,715.4z"/>
										<path class="st32" d="M871.3,709.7v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H871.3z M866.1,717.2c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S866.1,716.8,866.1,717.2z M866.7,711.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S866.7,711.2,866.7,711.7z"/>
										<path class="st32" d="M875.3,716.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S875.7,716.2,875.3,716.2z M875,710.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S875.5,710.4,875,710.4z"/>
										<path class="st32" d="M886.2,708.3c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C887.5,708.4,886.9,708.3,886.2,708.3z"/>
										<path class="st32" d="M894.4,716.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V707h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H894.4z"/>
										<path class="st32" d="M900.3,709.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S899.9,709.5,900.3,709.5z"/>
										<path class="st32" d="M902.2,707.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S902.2,708.1,902.2,707.9z M903.2,716.1h-1v-6.4h1V716.1z"/>
										<path class="st32" d="M909.4,714.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S909.4,713.9,909.4,714.3z"/>
										<path class="st32" d="M913.1,715.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S912.8,715.4,913.1,715.4
											z"/>
										<path class="st32" d="M915.2,707.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S915.2,708.1,915.2,707.9z M916.2,716.1h-1v-6.4h1V716.1z"/>
										<path class="st32" d="M922.2,716.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H922.2z M920,715.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S919.7,715.4,920,715.4z"/>
										<path class="st32" d="M929.4,716.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H929.4z"/>
										<path class="st32" d="M940.4,713.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S940.4,713.2,940.4,713.8z"/>
										<path class="st32" d="M944.6,716.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C945.8,716.1,945.2,716.2,944.6,716.2z"/>
										<path class="st32" d="M952.1,716.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V707h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H952.1z"/>
										<path class="st32" d="M960.7,712.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S960.7,711.8,960.7,712.9z M955.8,712.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S955.8,712,955.8,712.9z"/>
										<path class="st32" d="M967.9,712.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S967.9,711.8,967.9,712.9z M963,712.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S963,712,963,712.9z"/>
										<path class="st32" d="M970.6,716.1h-1V707h1V716.1z"/>
									</g>
								</g>
								<g id="school_txt_3">
									<g>
										<path class="st32" d="M804.7,724.5c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6h-0.8v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S804.7,723.9,804.7,724.5z"/>
										<path class="st32" d="M806.5,730.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,730.7,806.5,730.5z"/>
										<path class="st32" d="M816.8,723.3c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C818.1,723.4,817.4,723.3,816.8,723.3z"/>
										<path class="st32" d="M824.5,731.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H824.5z M822.3,730.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S821.9,730.4,822.3,730.4z"/>
										<path class="st32" d="M835.4,731.1v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H835.4z"/>
										<path class="st32" d="M841.4,724.6c0.8,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4s-0.2,1.9-0.7,2.5s-1.1,0.9-2,0.9
											c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1l-0.2,0.8h-0.7V722h1v2.2c0,0.5,0,0.9,0,1.3h0
											C839.8,724.9,840.5,724.6,841.4,724.6z M841.2,725.4c-0.7,0-1.1,0.2-1.4,0.6s-0.4,1-0.4,1.9s0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.4-1.1,0.4-1.9c0-0.8-0.1-1.5-0.4-1.9S841.8,725.4,841.2,725.4z"/>
										<path class="st32" d="M848.7,724.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S848.3,724.5,848.7,724.5z"/>
										<path class="st32" d="M850.5,722.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S850.5,723.1,850.5,722.9z M851.6,731.1h-1v-6.4h1V731.1z"/>
										<path class="st32" d="M858,730.2L858,730.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V722h1v9.1h-0.8L858,730.2z M856.1,730.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S855.5,730.4,856.1,730.4z"/>
										<path class="st32" d="M866.3,724.7v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H866.3z M861.2,732.2c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S861.2,731.8,861.2,732.2z M861.7,726.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S861.7,726.2,861.7,726.7z"/>
										<path class="st32" d="M870.3,731.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S870.7,731.2,870.3,731.2z M870,725.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S870.5,725.4,870,725.4z"/>
										<path class="st32" d="M884.1,731.1h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V731.1z"/>
										<path class="st32" d="M886.2,722.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S886.2,723.1,886.2,722.9z M887.3,731.1h-1v-6.4h1V731.1z"/>
										<path class="st32" d="M894.6,724.7v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H894.6z M889.5,732.2c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S889.5,731.8,889.5,732.2z M890,726.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S890,726.2,890,726.7z"/>
										<path class="st32" d="M900.3,731.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V722h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H900.3z"/>
										<path class="st32" d="M911.4,728.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S911.4,728.2,911.4,728.8z"/>
										<path class="st32" d="M915.5,731.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C916.7,731.1,916.2,731.2,915.5,731.2z"/>
										<path class="st32" d="M923.1,731.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V722h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H923.1z"/>
										<path class="st32" d="M931.6,727.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S931.6,726.8,931.6,727.9z M926.7,727.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S926.7,727,926.7,727.9z"/>
										<path class="st32" d="M938.8,727.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S938.8,726.8,938.8,727.9z M933.9,727.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S933.9,727,933.9,727.9z"/>
										<path class="st32" d="M941.5,731.1h-1V722h1V731.1z"/>
									</g>
								</g>
								<g id="school_txt_4">
									<g>
										<path class="st32" d="M805.4,744.1h-1.3v2h-0.9v-2H799v-0.8l4.1-5.8h1v5.8h1.3V744.1z M803.2,743.2v-2.8c0-0.6,0-1.2,0.1-1.9h0
											c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H803.2z"/>
										<path class="st32" d="M806.5,745.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,745.7,806.5,745.5z"/>
										<path class="st32" d="M819.6,746.1h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V746.1z"/>
										<path class="st32" d="M827.4,742.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S827.4,741.8,827.4,742.9z M822.5,742.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S822.5,742,822.5,742.9z"/>
										<path class="st32" d="M832.1,746.2c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7H830c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S832.9,746.2,832.1,746.2z M831.9,740.4c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S832.5,740.4,831.9,740.4z"/>
										<path class="st32" d="M839.1,746.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S839.6,746.2,839.1,746.2z M838.9,740.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S839.4,740.4,838.9,740.4z"/>
										<path class="st32" d="M848.4,746.1l-1.2-3.8c-0.1-0.2-0.2-0.8-0.4-1.6h0c-0.2,0.7-0.3,1.2-0.4,1.6l-1.2,3.8H844l-1.8-6.4h1
											c0.4,1.6,0.7,2.8,0.9,3.7s0.3,1.4,0.4,1.7h0c0-0.2,0.1-0.5,0.2-0.9s0.2-0.6,0.2-0.8l1.2-3.7h1.1l1.1,3.7
											c0.2,0.7,0.4,1.2,0.4,1.7h0c0-0.1,0.1-0.4,0.1-0.7s0.5-1.9,1.2-4.7h1l-1.8,6.4H848.4z"/>
										<path class="st32" d="M855.2,746.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S855.6,746.2,855.2,746.2z M854.9,740.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S855.4,740.4,854.9,740.4z"/>
										<path class="st32" d="M860.2,746.1h-1V737h1V746.1z"/>
										<path class="st32" d="M863.2,746.1h-1V737h1V746.1z"/>
										<path class="st32" d="M872.4,746.1l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H872.4z"/>
										<path class="st32" d="M879.2,737.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S879.2,738.1,879.2,737.9z M880.2,746.1h-1v-6.4h1V746.1z"/>
										<path class="st32" d="M886.7,745.2L886.7,745.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V737h1v9.1h-0.8L886.7,745.2z M884.7,745.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S884.1,745.4,884.7,745.4z"/>
										<path class="st32" d="M894,745.2L894,745.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V737h1v9.1h-0.8L894,745.2z M892.1,745.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S891.5,745.4,892.1,745.4z"/>
										<path class="st32" d="M898,746.1h-1V737h1V746.1z"/>
										<path class="st32" d="M902.7,746.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S903.2,746.2,902.7,746.2z M902.5,740.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S903,740.4,902.5,740.4z"/>
										<path class="st32" d="M914.9,743.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S914.9,743.2,914.9,743.8z"/>
										<path class="st32" d="M919,746.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C920.2,746.1,919.7,746.2,919,746.2z"/>
										<path class="st32" d="M926.6,746.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V737h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H926.6z"/>
										<path class="st32" d="M935.1,742.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S935.1,741.8,935.1,742.9z M930.2,742.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S930.2,742,930.2,742.9z"/>
										<path class="st32" d="M942.3,742.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S942.3,741.8,942.3,742.9z M937.4,742.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S937.4,742,937.4,742.9z"/>
										<path class="st32" d="M945,746.1h-1V737h1V746.1z"/>
									</g>
								</g>
								<g id="school_txt_5">
									<g>
										<path class="st32" d="M802,755.9c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C801.1,755.9,801.6,755.9,802,755.9z"/>
										<path class="st32" d="M806.5,760.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,760.7,806.5,760.5z"/>
										<path class="st32" d="M818.5,755c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2C817.5,752.5,818.5,753.3,818.5,755z
											 M814.1,756.9h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3s-0.9-0.4-1.7-0.4h-1.1V756.9z"/>
										<path class="st32" d="M823.1,754.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S822.7,754.5,823.1,754.5z"/>
										<path class="st32" d="M825,752.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S825,753.1,825,752.9z M826.1,761.1h-1v-6.4h1V761.1z"/>
										<path class="st32" d="M836.3,761.1v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H836.3z"/>
										<path class="st32" d="M842.2,754.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S841.8,754.5,842.2,754.5z"/>
										<path class="st32" d="M849.7,757.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S849.7,756.8,849.7,757.9z M844.8,757.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S844.8,757,844.8,757.9z"/>
										<path class="st32" d="M855.6,759.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S855.6,758.9,855.6,759.3z"/>
										<path class="st32" d="M859.9,761.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S860.3,761.2,859.9,761.2z M859.6,755.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S860.1,755.4,859.6,755.4z"/>
										<path class="st32" d="M872,758.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S872,758.2,872,758.8z"/>
										<path class="st32" d="M876.2,761.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C877.4,761.1,876.8,761.2,876.2,761.2z"/>
										<path class="st32" d="M883.7,761.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V752h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H883.7z"/>
										<path class="st32" d="M892.2,757.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S892.2,756.8,892.2,757.9z M887.3,757.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S887.3,757,887.3,757.9z"/>
										<path class="st32" d="M899.5,757.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S899.5,756.8,899.5,757.9z M894.6,757.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S894.6,757,894.6,757.9z"/>
										<path class="st32" d="M902.1,761.1h-1V752h1V761.1z"/>
										<path class="st32" d="M912.9,757.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S912.9,756.8,912.9,757.9z M908,757.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S908,757,908,757.9z"/>
										<path class="st32" d="M917.5,755.4h-1.6v5.7h-1v-5.7h-1.1V755l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2
											l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4s-0.3,0.6-0.3,1.2v0.4h1.6V755.4z"/>
										<path class="st32" d="M927.3,761.1l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H927.3z M925.9,757.5l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H925.9z"/>
										<path class="st32" d="M930.3,761.1h-1V752h1V761.1z"/>
										<path class="st32" d="M935.4,761.2c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S936.2,761.2,935.4,761.2z M935.2,755.4c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S935.8,755.4,935.2,755.4z"/>
										<path class="st32" d="M944.1,761.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V752h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H944.1z"/>
										<path class="st32" d="M951.1,761.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H951.1z M948.9,760.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S948.5,760.4,948.9,760.4z"/>
										<path class="st32" d="M956.7,754.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S956.3,754.5,956.7,754.5z"/>
										<path class="st32" d="M961.4,761.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S961.8,761.2,961.4,761.2z M961.1,755.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S961.6,755.4,961.1,755.4z"/>
										<path class="st32" d="M967.5,760.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S967.2,760.4,967.5,760.4
											z"/>
										<path class="st32" d="M971.7,760.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S971.4,760.4,971.7,760.4
											z"/>
										<path class="st32" d="M977.8,761.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H977.8z M975.6,760.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S975.3,760.4,975.6,760.4z"/>
									</g>
								</g>
								<g id="school_txt_6">
									<g>
										<path class="st32" d="M799.5,772.4c0-1.7,0.3-2.9,1-3.8s1.6-1.3,2.9-1.3c0.4,0,0.8,0,1,0.1v0.8c-0.3-0.1-0.6-0.1-1-0.1
											c-0.9,0-1.6,0.3-2.1,0.9s-0.8,1.5-0.8,2.7h0.1c0.4-0.7,1.1-1,2-1c0.8,0,1.4,0.2,1.8,0.7s0.7,1.1,0.7,1.9c0,0.9-0.2,1.6-0.7,2.1
											s-1.1,0.8-2,0.8c-0.9,0-1.6-0.3-2.1-1S799.5,773.6,799.5,772.4z M802.3,775.4c0.6,0,1-0.2,1.3-0.5s0.5-0.9,0.5-1.5
											c0-0.6-0.1-1-0.4-1.3s-0.7-0.5-1.3-0.5c-0.4,0-0.7,0.1-1,0.2s-0.5,0.3-0.7,0.6s-0.3,0.5-0.3,0.8c0,0.4,0.1,0.8,0.2,1.1
											s0.4,0.6,0.7,0.8S802,775.4,802.3,775.4z"/>
										<path class="st32" d="M806.5,775.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,775.7,806.5,775.5z"/>
										<path class="st32" d="M814.1,776.1h-1v-8.6h4.8v0.9h-3.8v3.1h3.6v0.9h-3.6V776.1z"/>
										<path class="st32" d="M820.1,769.7v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H820.1z"/>
										<path class="st32" d="M827.5,776.1h-1V767h1V776.1z"/>
										<path class="st32" d="M831.6,775.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S831.3,775.4,831.6,775.4
											z"/>
										<path class="st32" d="M839.3,772.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S839.3,771.8,839.3,772.9z M834.5,772.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S834.5,772,834.5,772.9z"/>
										<path class="st32" d="M845.4,776.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H845.4z"/>
										<path class="st32" d="M856.5,773.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S856.5,773.2,856.5,773.8z"/>
										<path class="st32" d="M860.7,776.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C861.9,776.1,861.3,776.2,860.7,776.2z"/>
										<path class="st32" d="M863.7,767.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S863.7,768.1,863.7,767.9z M864.8,776.1h-1v-6.4h1V776.1z"/>
										<path class="st32" d="M869.6,776.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S870,776.2,869.6,776.2z M869.3,770.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S869.8,770.4,869.3,770.4z"/>
										<path class="st32" d="M878,776.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H878z"/>
										<path class="st32" d="M883.5,776.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C884.7,776.1,884.2,776.2,883.5,776.2z"/>
										<path class="st32" d="M889.4,776.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S889.8,776.2,889.4,776.2z M889.1,770.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S889.6,770.4,889.1,770.4z"/>
										<path class="st32" d="M902.1,776.1l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H902.1z M900.7,772.5l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H900.7z"/>
										<path class="st32" d="M906.7,776.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C907.9,776.1,907.3,776.2,906.7,776.2z"/>
										<path class="st32" d="M913.8,776.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H913.8z M911.6,775.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S911.2,775.4,911.6,775.4z"/>
										<path class="st32" d="M920.9,775.2L920.9,775.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V767h1v9.1H921L920.9,775.2z M918.9,775.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S918.4,775.4,918.9,775.4z"/>
										<path class="st32" d="M926.6,776.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S927,776.2,926.6,776.2z M926.3,770.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S926.8,770.4,926.3,770.4z"/>
										<path class="st32" d="M938.8,776.1v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H938.8z"/>
										<path class="st32" d="M940.7,769.7h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1L944,777
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L940.7,769.7z"/>
										<path class="st32" d="M956.5,770c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2C955.4,767.5,956.5,768.3,956.5,770z
											 M952.1,771.9h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3s-0.9-0.4-1.7-0.4h-1.1V771.9z"/>
										<path class="st32" d="M961.1,769.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S960.7,769.5,961.1,769.5z"/>
										<path class="st32" d="M963,767.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S963,768.1,963,767.9z M964,776.1h-1v-6.4h1V776.1z"/>
										<path class="st32" d="M967.5,776.1l-2.4-6.4h1l1.4,3.8c0.3,0.9,0.5,1.5,0.6,1.7h0c0-0.2,0.2-0.6,0.4-1.3s0.7-2.1,1.5-4.3h1
											l-2.4,6.4H967.5z"/>
										<path class="st32" d="M976,776.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H976z M973.8,775.4c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S973.5,775.4,973.8,775.4z"/>
										<path class="st32" d="M980.8,775.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S980.5,775.4,980.8,775.4
											z"/>
										<path class="st32" d="M985.7,776.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S986.1,776.2,985.7,776.2z M985.4,770.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S986,770.4,985.4,770.4z"/>
										<path class="st32" d="M997.8,773.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S997.8,773.2,997.8,773.8z"/>
										<path class="st32" d="M1002,776.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C1003.2,776.1,1002.7,776.2,1002,776.2z"/>
										<path class="st32" d="M1009.5,776.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V767h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H1009.5z"/>
										<path class="st32" d="M1018.1,772.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S1018.1,771.8,1018.1,772.9z M1013.2,772.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S1013.2,772,1013.2,772.9z"/>
										<path class="st32" d="M1025.3,772.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S1025.3,771.8,1025.3,772.9z M1020.4,772.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S1020.4,772,1020.4,772.9z"/>
										<path class="st32" d="M1028,776.1h-1V767h1V776.1z"/>
									</g>
								</g>
								<g id="school_txt_7">
									<g>
										<path class="st32" d="M800.4,791.1l3.6-7.7h-4.7v-0.9h5.7v0.8l-3.5,7.8H800.4z"/>
										<path class="st32" d="M806.5,790.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,790.7,806.5,790.5z"/>
										<path class="st32" d="M813.1,782.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V782.5z M814.1,786.2h1.6c0.7,0,1.2-0.1,1.5-0.3
											s0.5-0.6,0.5-1.1c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V786.2z M814.1,787v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H814.1z"/>
										<path class="st32" d="M823.7,784.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S823.3,784.5,823.7,784.5z"/>
										<path class="st32" d="M825.6,782.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S825.6,783.1,825.6,782.9z M826.6,791.1h-1v-6.4h1V791.1z"/>
										<path class="st32" d="M833.1,790.2L833.1,790.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V782h1v9.1h-0.8L833.1,790.2z M831.1,790.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S830.5,790.4,831.1,790.4z"/>
										<path class="st32" d="M841.3,784.7v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H841.3z M836.2,792.2c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S836.2,791.8,836.2,792.2z M836.7,786.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S836.7,786.2,836.7,786.7z"/>
										<path class="st32" d="M845.3,791.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S845.8,791.2,845.3,791.2z M845.1,785.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S845.6,785.4,845.1,785.4z"/>
										<path class="st32" d="M854.6,791.1l-1.2-3.8c-0.1-0.2-0.2-0.8-0.4-1.6h0c-0.2,0.7-0.3,1.2-0.4,1.6l-1.2,3.8h-1.1l-1.8-6.4h1
											c0.4,1.6,0.7,2.8,0.9,3.7s0.3,1.4,0.4,1.7h0c0-0.2,0.1-0.5,0.2-0.9s0.2-0.6,0.2-0.8l1.2-3.7h1.1l1.1,3.7
											c0.2,0.7,0.4,1.2,0.4,1.7h0c0-0.1,0.1-0.4,0.1-0.7s0.5-1.9,1.2-4.7h1l-1.8,6.4H854.6z"/>
										<path class="st32" d="M862.6,791.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H862.6z M860.4,790.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S860,790.4,860.4,790.4z"/>
										<path class="st32" d="M864.3,784.7h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L864.3,784.7z"/>
										<path class="st32" d="M878.3,783.3c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C879.6,783.4,879,783.3,878.3,783.3z"/>
										<path class="st32" d="M886.5,791.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V782h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H886.5z"/>
										<path class="st32" d="M892.4,784.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S892,784.5,892.4,784.5z"/>
										<path class="st32" d="M894.3,782.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S894.3,783.1,894.3,782.9z M895.3,791.1h-1v-6.4h1V791.1z"/>
										<path class="st32" d="M901.5,789.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S901.5,788.9,901.5,789.3z"/>
										<path class="st32" d="M905.2,790.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S904.9,790.4,905.2,790.4
											z"/>
										<path class="st32" d="M907.3,782.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S907.3,783.1,907.3,782.9z M908.3,791.1h-1v-6.4h1V791.1z"/>
										<path class="st32" d="M914.3,791.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H914.3z M912.1,790.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S911.7,790.4,912.1,790.4z"/>
										<path class="st32" d="M921.4,791.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H921.4z"/>
										<path class="st32" d="M933.1,791.1l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H933.1z M931.7,787.5l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H931.7z"/>
										<path class="st32" d="M937.7,791.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C938.9,791.1,938.3,791.2,937.7,791.2z"/>
										<path class="st32" d="M944.8,791.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H944.8z M942.6,790.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S942.2,790.4,942.6,790.4z"/>
										<path class="st32" d="M951.9,790.2L951.9,790.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V782h1v9.1H952L951.9,790.2z M949.9,790.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S949.4,790.4,949.9,790.4z"/>
										<path class="st32" d="M957.6,791.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S958,791.2,957.6,791.2z M957.3,785.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S957.8,785.4,957.3,785.4z"/>
										<path class="st32" d="M969.8,791.1v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H969.8z"/>
										<path class="st32" d="M971.7,784.7h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1L975,792
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L971.7,784.7z"/>
									</g>
								</g>
								<g id="school_txt_8">
									<g>
										<path class="st32" d="M802.2,797.4c0.8,0,1.4,0.2,1.9,0.5s0.7,0.9,0.7,1.5c0,0.4-0.1,0.8-0.4,1.2s-0.7,0.7-1.3,0.9
											c0.7,0.3,1.2,0.7,1.5,1s0.4,0.8,0.4,1.3c0,0.7-0.2,1.3-0.7,1.7s-1.2,0.6-2,0.6c-0.9,0-1.6-0.2-2.1-0.6s-0.7-1-0.7-1.7
											c0-1,0.6-1.7,1.8-2.3c-0.5-0.3-0.9-0.6-1.2-1s-0.4-0.7-0.4-1.2c0-0.6,0.2-1.1,0.7-1.5S801.4,797.4,802.2,797.4z M800.3,803.9
											c0,0.5,0.2,0.8,0.5,1.1s0.8,0.4,1.4,0.4c0.6,0,1-0.1,1.4-0.4s0.5-0.6,0.5-1.1c0-0.4-0.2-0.7-0.5-1s-0.8-0.6-1.6-0.9
											c-0.6,0.2-1,0.5-1.3,0.8S800.3,803.5,800.3,803.9z M802.2,798.2c-0.5,0-0.9,0.1-1.1,0.4s-0.4,0.5-0.4,0.9c0,0.4,0.1,0.7,0.3,0.9
											s0.7,0.5,1.3,0.8c0.6-0.2,1-0.5,1.2-0.8s0.3-0.6,0.3-0.9c0-0.4-0.1-0.7-0.4-0.9S802.7,798.2,802.2,798.2z"/>
										<path class="st32" d="M806.5,805.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806.5,805.7,806.5,805.5z"/>
										<path class="st32" d="M815.8,806.1h-1v-7.7H812v-0.9h6.4v0.9h-2.7V806.1z"/>
										<path class="st32" d="M824,806.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V797h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H824z"/>
										<path class="st32" d="M829.7,806.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S830.1,806.2,829.7,806.2z M829.4,800.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S829.9,800.4,829.4,800.4z"/>
										<path class="st32" d="M840.7,801.6h2.9v4.2c-0.5,0.1-0.9,0.3-1.4,0.3s-1,0.1-1.6,0.1c-1.3,0-2.3-0.4-3-1.2s-1.1-1.9-1.1-3.2
											c0-0.9,0.2-1.7,0.5-2.3s0.9-1.2,1.5-1.5s1.5-0.5,2.4-0.5c0.9,0,1.8,0.2,2.6,0.5l-0.4,0.9c-0.8-0.3-1.5-0.5-2.2-0.5
											c-1,0-1.9,0.3-2.4,0.9s-0.9,1.5-0.9,2.6c0,1.2,0.3,2,0.8,2.6s1.4,0.9,2.5,0.9c0.6,0,1.2-0.1,1.7-0.2v-2.6h-1.9V801.6z"/>
										<path class="st32" d="M851.1,802.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S851.1,801.8,851.1,802.9z M846.2,802.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S846.2,802,846.2,802.9z"/>
										<path class="st32" d="M857.2,805.2L857.2,805.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V797h1v9.1h-0.8L857.2,805.2z M855.2,805.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S854.7,805.4,855.2,805.4z"/>
										<path class="st32" d="M864.5,805.2L864.5,805.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V797h1v9.1h-0.8L864.5,805.2z M862.6,805.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S862,805.4,862.6,805.4z"/>
										<path class="st32" d="M871.5,806.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H871.5z M869.3,805.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S868.9,805.4,869.3,805.4z"/>
										<path class="st32" d="M877.1,799.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S876.7,799.5,877.1,799.5z"/>
										<path class="st32" d="M883.5,805.2L883.5,805.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V797h1v9.1h-0.8L883.5,805.2z M881.5,805.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S880.9,805.4,881.5,805.4z"/>
										<path class="st32" d="M894.5,803.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S894.5,803.2,894.5,803.8z"/>
										<path class="st32" d="M898.7,806.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C899.9,806.1,899.4,806.2,898.7,806.2z"/>
										<path class="st32" d="M906.3,806.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V797h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H906.3z"/>
										<path class="st32" d="M914.8,802.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S914.8,801.8,914.8,802.9z M909.9,802.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S909.9,802,909.9,802.9z"/>
										<path class="st32" d="M922,802.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2s-0.4-1.1-0.4-1.8
											c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S922,801.8,922,802.9z M917.1,802.9c0,0.8,0.2,1.4,0.5,1.9
											s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1.1,0.2-1.4,0.6
											S917.1,802,917.1,802.9z"/>
										<path class="st32" d="M924.7,806.1h-1V797h1V806.1z"/>
									</g>
								</g>
								<g id="school_txt_title">
									<g>
										<path class="st32" d="M805.8,683.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S805.8,683.2,805.8,683.8z"/>
										<path class="st32" d="M810,686.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C811.1,686.1,810.6,686.2,810,686.2z"/>
										<path class="st32" d="M817.5,686.1v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V677h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H817.5z"/>
										<path class="st32" d="M826,682.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2s-0.4-1.1-0.4-1.8
											c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S826,681.8,826,682.9z M821.1,682.9c0,0.8,0.2,1.4,0.5,1.9
											s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1.1,0.2-1.4,0.6
											S821.1,682,821.1,682.9z"/>
										<path class="st32" d="M833.3,682.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S833.3,681.8,833.3,682.9z M828.4,682.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S828.4,682,828.4,682.9z"/>
										<path class="st32" d="M835.9,686.1h-1V677h1V686.1z"/>
										<path class="st32" d="M842.1,684.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S842.1,683.9,842.1,684.3z"/>
									</g>
								</g>
							</g>
							<g id="Hotel">
								<g id="hotel_txt_1">
									<g>
										<path class="st32" d="M802.4,570.1h-0.9V564c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V570.1z"/>
										<path class="st32" d="M806,569.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,569.8,806,569.5z"/>
										<path class="st32" d="M817.4,567.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S817.4,567.2,817.4,567.8z"/>
										<path class="st32" d="M822,570.2c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S822.8,570.2,822,570.2z
											 M821.9,564.4c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S822.4,564.4,821.9,564.4z"/>
										<path class="st32" d="M829.3,563.6c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S828.9,563.6,829.3,563.6z"/>
										<path class="st32" d="M831.2,561.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S831.2,562.2,831.2,561.9z M832.2,570.1h-1v-6.4h1V570.1z"/>
										<path class="st32" d="M838.7,570.1V566c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H838.7z"/>
										<path class="st32" d="M846.9,563.7v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H846.9z M841.8,571.2c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S841.8,570.8,841.8,571.2z M842.3,565.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S842.3,565.3,842.3,565.7z"/>
										<path class="st32" d="M854.9,570.1h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V570.1z"/>
										<path class="st32" d="M857,561.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S857,562.2,857,561.9z M858.1,570.1h-1v-6.4h1V570.1z"/>
										<path class="st32" d="M861.1,570.1h-1V561h1V570.1z"/>
										<path class="st32" d="M864.1,570.1h-1V561h1V570.1z"/>
										<path class="st32" d="M874.3,567.8c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S874.3,567.2,874.3,567.8z"/>
										<path class="st32" d="M876.8,563.7v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H876.8z"/>
										<path class="st32" d="M883.2,561.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S883.2,562.2,883.2,561.9z M884.2,570.1h-1v-6.4h1V570.1z"/>
										<path class="st32" d="M888.4,569.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V564l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S888.1,569.4,888.4,569.4z
											"/>
										<path class="st32" d="M893.3,570.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S893.7,570.2,893.3,570.2z M893,564.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S893.5,564.4,893,564.4z"/>
										<path class="st32" d="M901.4,568.4c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S901.4,567.9,901.4,568.4z"/>
									</g>
								</g>
								<g id="hotel_txt_2">
									<g>
										<path class="st32" d="M804.4,585.1h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V585.1z"/>
										<path class="st32" d="M806,584.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,584.8,806,584.5z"/>
										<path class="st32" d="M819.4,580.7c0,1.4-0.4,2.5-1.2,3.2s-1.9,1.1-3.3,1.1h-2.4v-8.6h2.6c1.3,0,2.4,0.4,3.1,1.1
											S819.4,579.4,819.4,580.7z M818.4,580.8c0-1.1-0.3-2-0.8-2.5s-1.4-0.8-2.5-0.8h-1.4v6.8h1.2c1.2,0,2.1-0.3,2.7-0.9
											S818.4,581.9,818.4,580.8z"/>
										<path class="st32" d="M826.7,581.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S826.7,580.9,826.7,581.9z M821.8,581.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S821.8,581.1,821.8,581.9z"/>
										<path class="st32" d="M829.3,578.7v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H829.3z"/>
										<path class="st32" d="M838.8,578.6c0.8,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4s-0.2,1.9-0.7,2.5s-1.1,0.9-2,0.9
											c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1l-0.2,0.8h-0.7V576h1v2.2c0,0.5,0,0.9,0,1.3h0
											C837.2,578.9,837.9,578.6,838.8,578.6z M838.6,579.4c-0.7,0-1.1,0.2-1.4,0.6s-0.4,1-0.4,1.9s0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.4-1.1,0.4-1.9c0-0.8-0.1-1.5-0.4-1.9S839.3,579.4,838.6,579.4z"/>
										<path class="st32" d="M844.1,585.1h-1V576h1V585.1z"/>
										<path class="st32" d="M848.9,585.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S849.3,585.2,848.9,585.2z M848.6,579.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S849.1,579.4,848.6,579.4z"/>
										<path class="st32" d="M855.7,585.1h-1v-7.7H852v-0.9h6.4v0.9h-2.7V585.1z"/>
										<path class="st32" d="M862.5,578.6c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S862.1,578.6,862.5,578.6z"/>
										<path class="st32" d="M867.2,585.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S867.6,585.2,867.2,585.2z M866.9,579.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S867.4,579.4,866.9,579.4z"/>
										<path class="st32" d="M873.9,585.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S874.3,585.2,873.9,585.2z M873.6,579.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S874.1,579.4,873.6,579.4z"/>
										<path class="st32" d="M884,578.6c0.8,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4s-0.2,1.9-0.7,2.5s-1.1,0.9-2,0.9c-0.4,0-0.8-0.1-1.1-0.2
											s-0.6-0.4-0.9-0.7h-0.1l-0.2,0.8H881V576h1v2.2c0,0.5,0,0.9,0,1.3h0C882.5,578.9,883.1,578.6,884,578.6z M883.9,579.4
											c-0.7,0-1.1,0.2-1.4,0.6s-0.4,1-0.4,1.9s0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7s0.4-1.1,0.4-1.9
											c0-0.8-0.1-1.5-0.4-1.9S884.5,579.4,883.9,579.4z"/>
										<path class="st32" d="M887.4,578.7h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L887.4,578.7z"/>
										<path class="st32" d="M904.2,585.1h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V585.1z"/>
										<path class="st32" d="M906.3,576.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S906.3,577.2,906.3,576.9z M907.4,585.1h-1v-6.4h1V585.1z"/>
										<path class="st32" d="M910.4,585.1h-1V576h1V585.1z"/>
										<path class="st32" d="M914.5,584.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V579l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S914.2,584.4,914.5,584.4z
											"/>
										<path class="st32" d="M922.3,581.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S922.3,580.9,922.3,581.9z M917.4,581.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S917.4,581.1,917.4,581.9z"/>
										<path class="st32" d="M928.4,585.1V581c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H928.4z"/>
									</g>
								</g>
								<g id="hotel_txt_3">
									<g>
										<path class="st32" d="M804.1,593.6c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6h-0.8v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S804.1,592.9,804.1,593.6z"/>
										<path class="st32" d="M806,599.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,599.8,806,599.5z"/>
										<path class="st32" d="M819.1,600.1h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V600.1z"/>
										<path class="st32" d="M820.3,593.7h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1v-0.8c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L820.3,593.7z"/>
										<path class="st32" d="M831.3,600.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H831.3z M829.1,599.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V597l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S828.7,599.4,829.1,599.4z"/>
										<path class="st32" d="M836.1,599.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V594l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8H835v3.8c0,0.4,0.1,0.7,0.3,0.9S835.8,599.4,836.1,599.4z"
											/>
										<path class="st32" d="M840.3,599.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V594l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S840,599.4,840.3,599.4z"
											/>
										<path class="st32" d="M851.2,594c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2C850.1,591.5,851.2,592.4,851.2,594z
											 M846.7,595.9h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3s-0.9-0.4-1.7-0.4h-1.1V595.9z"/>
										<path class="st32" d="M853.8,600.1h-1V591h1V600.1z"/>
										<path class="st32" d="M859.8,600.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H859.8z M857.6,599.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V597l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S857.2,599.4,857.6,599.4z"/>
										<path class="st32" d="M865.1,600.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C866.3,600.1,865.7,600.2,865.1,600.2z"/>
										<path class="st32" d="M871,600.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S871.4,600.2,871,600.2z M870.7,594.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S871.2,594.4,870.7,594.4z"/>
									</g>
								</g>
								<g id="hotel_txt_4">
									<g>
										<path class="st32" d="M804.8,613.1h-1.3v2h-0.9v-2h-4.2v-0.8l4.1-5.8h1v5.8h1.3V613.1z M802.6,612.3v-2.8c0-0.6,0-1.2,0.1-1.9h0
											c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H802.6z"/>
										<path class="st32" d="M806,614.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,614.8,806,614.5z"/>
										<path class="st32" d="M818,615.1l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H818z M816.6,611.5l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H816.6z"/>
										<path class="st32" d="M822.1,614.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V609l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S821.8,614.4,822.1,614.4z
											"/>
										<path class="st32" d="M825.2,615.1h-1V606h1V615.1z"/>
										<path class="st32" d="M831.2,615.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H831.2z M829,614.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V612l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S828.7,614.4,829,614.4z"/>
										<path class="st32" d="M838.4,615.1V611c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H838.4z"/>
										<path class="st32" d="M843.4,614.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V609l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S843.1,614.4,843.4,614.4z
											"/>
										<path class="st32" d="M849.5,615.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H849.5z M847.3,614.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V612l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S846.9,614.4,847.3,614.4z"/>
										<path class="st32" d="M859.3,615.1l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H859.3z"/>
										<path class="st32" d="M870.1,615.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H870.1z M867.9,614.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V612l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S867.6,614.4,867.9,614.4z"/>
										<path class="st32" d="M875.8,608.6c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S875.4,608.6,875.8,608.6z"/>
										<path class="st32" d="M880.7,608.6c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S880.3,608.6,880.7,608.6z"/>
										<path class="st32" d="M882.6,606.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S882.6,607.2,882.6,606.9z M883.6,615.1h-1v-6.4h1V615.1z"/>
										<path class="st32" d="M891.2,611.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S891.2,610.9,891.2,611.9z M886.4,611.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S886.4,611.1,886.4,611.9z"/>
										<path class="st32" d="M895,614.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V609l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8H894v3.8c0,0.4,0.1,0.7,0.3,0.9S894.7,614.4,895,614.4z"/>
										<path class="st32" d="M899.3,614.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V609l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S898.9,614.4,899.3,614.4z
											"/>
										<path class="st32" d="M910.1,615.1l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H910.1z M908.7,611.5l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H908.7z"/>
										<path class="st32" d="M913.1,615.1h-1V606h1V615.1z"/>
										<path class="st32" d="M918.2,615.2c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S919,615.2,918.2,615.2z M918,609.4c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S918.6,609.4,918,609.4z"/>
										<path class="st32" d="M926.9,615.1V611c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V606h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H926.9z"/>
										<path class="st32" d="M933.8,615.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H933.8z M931.6,614.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V612l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S931.3,614.4,931.6,614.4z"/>
										<path class="st32" d="M939.5,608.6c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S939.1,608.6,939.5,608.6z"/>
										<path class="st32" d="M944.2,615.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S944.6,615.2,944.2,615.2z M943.9,609.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S944.4,609.4,943.9,609.4z"/>
										<path class="st32" d="M950.3,614.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V609l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S949.9,614.4,950.3,614.4z
											"/>
										<path class="st32" d="M954.5,614.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V609l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S954.2,614.4,954.5,614.4z
											"/>
										<path class="st32" d="M960.6,615.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H960.6z M958.4,614.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V612l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S958,614.4,958.4,614.4z"/>
									</g>
								</g>
								<g id="hotel_txt_5">
									<g>
										<path class="st32" d="M801.5,624.9c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C800.6,624.9,801,624.9,801.5,624.9z"/>
										<path class="st32" d="M806,629.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,629.8,806,629.5z"/>
										<path class="st32" d="M813.6,626.5v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H813.6z M813.6,625.7h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V625.7z"/>
										<path class="st32" d="M822.6,630.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S823,630.2,822.6,630.2z M822.3,624.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S822.8,624.4,822.3,624.4z"/>
										<path class="st32" d="M830.7,628.4c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S830.7,627.9,830.7,628.4z"/>
										<path class="st32" d="M832.2,621.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S832.2,622.2,832.2,621.9z M833.3,630.1h-1v-6.4h1V630.1z"/>
										<path class="st32" d="M839.7,629.2L839.7,629.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5V621h1v9.1h-0.8L839.7,629.2z M837.8,629.4c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S837.2,629.4,837.8,629.4z"/>
										<path class="st32" d="M845.4,630.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S845.8,630.2,845.4,630.2z M845.1,624.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S845.7,624.4,845.1,624.4z"/>
										<path class="st32" d="M853.8,630.1V626c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H853.8z"/>
										<path class="st32" d="M859.4,630.2c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C860.6,630.1,860,630.2,859.4,630.2z"/>
										<path class="st32" d="M865.2,630.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S865.7,630.2,865.2,630.2z M865,624.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S865.5,624.4,865,624.4z"/>
										<path class="st32" d="M872.5,630.1v-8.6h1v8.6H872.5z"/>
										<path class="st32" d="M880.1,630.1V626c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H880.1z"/>
										<path class="st32" d="M887.5,630.1V626c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H887.5z"/>
									</g>
								</g>
								<g id="hotel_txt_6">
									<g>
										<path class="st32" d="M798.9,641.4c0-1.7,0.3-2.9,1-3.8s1.6-1.3,2.9-1.3c0.4,0,0.8,0,1,0.1v0.8c-0.3-0.1-0.6-0.1-1-0.1
											c-0.9,0-1.6,0.3-2.1,0.9s-0.8,1.5-0.8,2.7h0.1c0.4-0.7,1.1-1,2-1c0.8,0,1.4,0.2,1.8,0.7s0.7,1.1,0.7,1.9c0,0.9-0.2,1.6-0.7,2.1
											s-1.1,0.8-2,0.8c-0.9,0-1.6-0.3-2.1-1S798.9,642.6,798.9,641.4z M801.8,644.4c0.6,0,1-0.2,1.3-0.5s0.5-0.9,0.5-1.5
											c0-0.6-0.1-1-0.4-1.3s-0.7-0.5-1.3-0.5c-0.4,0-0.7,0.1-1,0.2s-0.5,0.3-0.7,0.6s-0.3,0.5-0.3,0.8c0,0.4,0.1,0.8,0.2,1.1
											s0.4,0.6,0.7,0.8S801.4,644.4,801.8,644.4z"/>
										<path class="st32" d="M806,644.5c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,644.8,806,644.5z"/>
										<path class="st32" d="M818,645.1l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H818z M816.6,641.5l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H816.6z"/>
										<path class="st32" d="M822.1,644.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V639l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S821.8,644.4,822.1,644.4z
											"/>
										<path class="st32" d="M825.2,645.1h-1V636h1V645.1z"/>
										<path class="st32" d="M831.2,645.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H831.2z M829,644.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V642l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S828.7,644.4,829,644.4z"/>
										<path class="st32" d="M838.4,645.1V641c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H838.4z"/>
										<path class="st32" d="M843.4,644.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V639l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S843.1,644.4,843.4,644.4z
											"/>
										<path class="st32" d="M849.5,645.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H849.5z M847.3,644.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V642l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S846.9,644.4,847.3,644.4z"/>
										<path class="st32" d="M860.9,645.1l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H860.9z M859.5,641.5l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H859.5z"/>
										<path class="st32" d="M863.9,645.1h-1V636h1V645.1z"/>
										<path class="st32" d="M869,645.2c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S869.8,645.2,869,645.2z
											 M868.8,639.4c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S869.4,639.4,868.8,639.4z"/>
										<path class="st32" d="M877.7,645.1V641c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V636h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H877.7z"/>
										<path class="st32" d="M884.7,645.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H884.7z M882.5,644.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V642l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S882.1,644.4,882.5,644.4z"/>
										<path class="st32" d="M890.3,638.6c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S889.9,638.6,890.3,638.6z"/>
										<path class="st32" d="M895,645.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S895.4,645.2,895,645.2z M894.7,639.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S895.2,639.4,894.7,639.4z"/>
										<path class="st32" d="M901.1,644.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V639l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S900.8,644.4,901.1,644.4z
											"/>
										<path class="st32" d="M905.3,644.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V639l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S905,644.4,905.3,644.4z"
											/>
										<path class="st32" d="M911.4,645.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H911.4z M909.2,644.4c0.6,0,1.1-0.2,1.4-0.5
											s0.5-0.8,0.5-1.4V642l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S908.9,644.4,909.2,644.4z"/>
										<path class="st32" d="M920.5,636.5l-3.2,8.6h-1l3.2-8.6H920.5z"/>
										<path class="st32" d="M931.6,645.1h-1.1l-4.7-7.2h0c0.1,0.8,0.1,1.6,0.1,2.3v4.9h-0.9v-8.6h1.1l4.7,7.2h0c0-0.1,0-0.4-0.1-1
											s0-1,0-1.2v-4.9h0.9V645.1z"/>
										<path class="st32" d="M939.4,641.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S939.4,640.9,939.4,641.9z M934.5,641.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S934.5,641.1,934.5,641.9z"/>
										<path class="st32" d="M944,638.6c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6s-0.6,0.9-0.6,1.6
											v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S943.6,638.6,944,638.6z"/>
										<path class="st32" d="M948.1,644.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V639l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8H947v3.8c0,0.4,0.1,0.7,0.3,0.9S947.7,644.4,948.1,644.4z"
											/>
										<path class="st32" d="M954.6,645.1V641c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											V636h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H954.6z"/>
										<path class="st32" d="M966.3,639c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2C965.2,636.5,966.3,637.4,966.3,639z
											 M961.9,640.9h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3s-0.9-0.4-1.7-0.4h-1.1V640.9z"/>
										<path class="st32" d="M973.5,641.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S973.5,640.9,973.5,641.9z M968.6,641.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S968.6,641.1,968.6,641.9z"/>
										<path class="st32" d="M975.1,636.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S975.1,637.2,975.1,636.9z M976.2,645.1h-1v-6.4h1V645.1z"/>
										<path class="st32" d="M982.6,645.1V641c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H982.6z"/>
										<path class="st32" d="M987.7,644.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9V639l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S987.3,644.4,987.7,644.4z
											"/>
										<path class="st32" d="M996.9,645.1l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H996.9z"/>
										<path class="st32" d="M1007.7,645.1l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5
											s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7
											c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H1007.7z M1005.5,644.4
											c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4V642l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8
											S1005.1,644.4,1005.5,644.4z"/>
										<path class="st32" d="M1011.4,645.1h-1V636h1V645.1z"/>
										<path class="st32" d="M1014.5,645.1h-1V636h1V645.1z"/>
									</g>
								</g>
								<g id="hotel_txt_title">
									<g>
										<path class="st32" d="M806.4,555.1h-1v-4h-4.5v4h-1v-8.6h1v3.6h4.5v-3.6h1V555.1z"/>
										<path class="st32" d="M814.2,551.9c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S814.2,550.9,814.2,551.9z M809.3,551.9
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S809.3,551.1,809.3,551.9z"/>
										<path class="st32" d="M817.9,554.4c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8H815V549l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S817.6,554.4,817.9,554.4z"
											/>
										<path class="st32" d="M822.8,555.2c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S823.2,555.2,822.8,555.2z M822.5,549.4c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S823.1,549.4,822.5,549.4z"/>
										<path class="st32" d="M827.8,555.1h-1V546h1V555.1z"/>
									</g>
								</g>
							</g>
							<g id="Dining">
								<g id="dining_txt_1">
									<path class="st53" d="M802.4,75h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8V75z
										 M806,74.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6c0,0.3-0.1,0.4-0.2,0.6
										s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,74.7,806,74.4z M818,75l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8L819,75H818z
										 M816.6,71.4l-1-2.7c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H816.6z M822.1,74.3c0.2,0,0.3,0,0.5,0
										s0.3-0.1,0.4-0.1V75c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9
										v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S821.8,74.3,822.1,74.3z M825.2,75h-1v-9.1h1V75z M831.3,75l-0.2-0.9h0
										c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4
										c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1
										c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6V75H831.3z M829.1,74.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6l-1,0
										c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S828.7,74.3,829.1,74.3z M838.4,75v-4.2c0-0.5-0.1-0.9-0.4-1.2
										s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8V75h-1v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3
										c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8V75H838.4z M843.4,74.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1V75c-0.1,0.1-0.3,0.1-0.5,0.1
										s-0.4,0-0.6,0c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9
										S843.1,74.3,843.4,74.3z M845.5,66.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
										s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S845.5,67.1,845.5,66.9z M846.6,75h-1v-6.4h1V75z M851.2,75.1
										c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8
										c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3
										v0.9C852.4,75,851.8,75.1,851.2,75.1z M862.4,75h-4.8v-8.6h4.8v0.9h-3.8v2.8h3.6V71h-3.6v3.2h3.8V75z M865.7,71.7l-2.2-3.1h1.1
										l1.7,2.5l1.7-2.5h1.1l-2.2,3.1l2.3,3.3H868l-1.8-2.6l-1.8,2.6h-1.1L865.7,71.7z M873.4,75.1c-0.4,0-0.8-0.1-1.1-0.2
										s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2
										c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S874.2,75.1,873.4,75.1z M873.3,69.3c-0.7,0-1.1,0.2-1.4,0.5
										s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8
										S873.8,69.3,873.3,69.3z M880.7,68.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
										s-0.6,0.9-0.6,1.6V75h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S880.3,68.5,880.7,68.5z M885.4,75.1c-0.9,0-1.7-0.3-2.2-0.9
										s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1V72h-4.4c0,0.8,0.2,1.3,0.6,1.7
										s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9c-0.3,0.1-0.7,0.3-1,0.3S885.8,75.1,885.4,75.1z M885.1,69.3c-0.5,0-0.9,0.2-1.2,0.5
										s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4S885.6,69.3,885.1,69.3z M893.5,73.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5
										c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6
										s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5
										c0.7,0,1.4,0.1,2,0.4l-0.3,0.8c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4
										s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5c0.8,0.3,1.3,0.6,1.5,0.8S893.5,72.8,893.5,73.3z M899.3,73.3c0,0.6-0.2,1.1-0.7,1.4
										s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7
										c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2
										s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6
										c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5c0.8,0.3,1.3,0.6,1.5,0.8S899.3,72.8,899.3,73.3z M908.9,72.7
										c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4s0.8,0.1,1.2,0.1
										c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5c-0.8-0.3-1.4-0.6-1.7-1
										s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9c-0.7-0.3-1.4-0.4-2.1-0.4
										c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5c0.9,0.3,1.5,0.7,1.9,1
										S908.9,72.2,908.9,72.7z M913.3,75.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
										c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1V72h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
										c-0.3,0.1-0.7,0.3-1,0.3S913.7,75.1,913.3,75.1z M913,69.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4
										S913.5,69.3,913,69.3z M921.2,75l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
										c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
										s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6V75H921.2z M919,74.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6l-1,0
										c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S918.6,74.3,919,74.3z M926.8,69.4h-1.6V75h-1v-5.7h-1.1v-0.4
										l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4
										s-0.3,0.6-0.3,1.2v0.4h1.6V69.4z M933.6,71.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
										s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S933.6,70.8,933.6,71.8z M928.7,71.8
										c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
										c-0.6,0-1.1,0.2-1.4,0.6S928.7,71,928.7,71.8z M940.8,71.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4
										s-0.8-0.7-1-1.2s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S940.8,70.8,940.8,71.8z
										 M935.9,71.8c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
										c-0.6,0-1.1,0.2-1.4,0.6S935.9,71,935.9,71.8z M946.9,74.2L946.9,74.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9
										s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1V75H947L946.9,74.2z M944.9,74.3
										c0.7,0,1.1-0.2,1.4-0.5s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9
										c0,0.8,0.2,1.4,0.5,1.9S944.4,74.3,944.9,74.3z M956.9,75l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1V75h-0.9v-8.6h1.5l2.7,7.1h0
										l2.7-7.1h1.5V75h-1v-5.5c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H956.9z M967.8,75l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2
										c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4
										c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6V75H967.8z
										 M965.6,74.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8
										S965.2,74.3,965.6,74.3z M973.4,68.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
										s-0.6,0.9-0.6,1.6V75h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S973,68.5,973.4,68.5z M976.4,71.7c0.2-0.2,0.4-0.6,0.8-0.9
										l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6V75h-1v-9.1h1v4.8C976.4,70.9,976.3,71.3,976.4,71.7L976.4,71.7z M984.4,75.1
										c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1V72h-4.4
										c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9c-0.3,0.1-0.7,0.3-1,0.3S984.8,75.1,984.4,75.1z M984.1,69.3
										c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4c0-0.6-0.1-1.1-0.4-1.4S984.6,69.3,984.1,69.3z M990.5,74.3c0.2,0,0.3,0,0.5,0
										s0.3-0.1,0.4-0.1V75c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9
										v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S990.2,74.3,990.5,74.3z"/>
								</g>
								<g id="dining_txt_2">
									<g>
										<path class="st32" d="M804.5,90h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V90z"/>
										<path class="st32" d="M806,89.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,89.7,806,89.4z"/>
										<path class="st32" d="M813.6,86.5V90h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H813.6z M813.6,85.6h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V85.6z"/>
										<path class="st32" d="M825.4,86.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S825.4,85.8,825.4,86.8z M820.5,86.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S820.5,86,820.5,86.8z"/>
										<path class="st32" d="M835.3,90v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6V90h-1v-4.2
											c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8V90h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8V90H835.3z"/>
										<path class="st32" d="M842.2,90l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6V90H842.2z M840,89.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6l-1,0
											c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S839.6,89.3,840,89.3z"/>
										<path class="st32" d="M849.3,90v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8V90h-1v-6.4
											h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8V90H849.3z"/>
										<path class="st32" d="M857.9,86.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S857.9,85.8,857.9,86.8z M853,86.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S853,86,853,86.8z"/>
										<path class="st32" d="M860.4,81.5l-0.2,3.1h-0.6l-0.2-3.1H860.4z"/>
										<path class="st32" d="M866.4,88.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S866.4,87.8,866.4,88.3z"/>
										<path class="st32" d="M875,90l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1V90h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5V90h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H875z"/>
										<path class="st32" d="M885.8,90l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6V90H885.8z M883.6,89.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S883.3,89.3,883.6,89.3z"/>
										<path class="st32" d="M891.1,90.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C892.3,90,891.8,90.1,891.1,90.1z"/>
										<path class="st32" d="M898.2,90l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6V90H898.2z M896,89.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6l-1,0
											c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S895.6,89.3,896,89.3z"/>
										<path class="st32" d="M903.9,83.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6V90h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S903.5,83.5,903.9,83.5z"/>
										<path class="st32" d="M911.4,86.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S911.4,85.8,911.4,86.8z M906.5,86.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S906.5,86,906.5,86.8z"/>
										<path class="st32" d="M917.5,90v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8V90h-1v-6.4
											h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8V90H917.5z"/>
										<path class="st32" d="M920.4,81.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S920.4,82.1,920.4,81.9z M921.4,90h-1v-6.4h1V90z"/>
										<path class="st32" d="M930.5,85.5h2.9v4.2c-0.5,0.1-0.9,0.3-1.4,0.3s-1,0.1-1.6,0.1c-1.3,0-2.3-0.4-3-1.2s-1.1-1.9-1.1-3.2
											c0-0.9,0.2-1.7,0.5-2.3s0.9-1.2,1.5-1.5s1.5-0.5,2.4-0.5c0.9,0,1.8,0.2,2.6,0.5l-0.4,0.9c-0.8-0.3-1.5-0.5-2.2-0.5
											c-1,0-1.9,0.3-2.4,0.9s-0.9,1.5-0.9,2.6c0,1.2,0.3,2,0.8,2.6s1.4,0.9,2.5,0.9c0.6,0,1.2-0.1,1.7-0.2v-2.6h-1.9V85.5z"/>
										<path class="st32" d="M938.3,83.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6V90h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S937.9,83.5,938.3,83.5z"/>
										<path class="st32" d="M940.2,81.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S940.2,82.1,940.2,81.9z M941.2,90h-1v-6.4h1V90z"/>
										<path class="st32" d="M944.3,90h-1v-9.1h1V90z"/>
										<path class="st32" d="M947.3,90h-1v-9.1h1V90z"/>
									</g>
								</g>
								<g id="dining_txt_3">
									<g>
										<path class="st32" d="M804.1,98.5c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6h-0.8v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S804.1,97.8,804.1,98.5z"/>
										<path class="st32" d="M806,104.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,104.7,806,104.4z"/>
										<path class="st32" d="M816.3,97.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C817.6,97.4,816.9,97.2,816.3,97.2z"/>
										<path class="st32" d="M824.4,105v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H824.4z"/>
										<path class="st32" d="M827.3,96.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S827.3,97.1,827.3,96.9z M828.4,105h-1v-6.4h1V105z"/>
										<path class="st32" d="M833.4,105.1c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S834.2,105.1,833.4,105.1z M833.3,99.3c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S833.8,99.3,833.3,99.3z"/>
										<path class="st32" d="M843.3,101.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S843.3,100.8,843.3,101.8z M838.4,101.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S838.4,101,838.4,101.8z"/>
										<path class="st32" d="M847.1,104.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S846.8,104.3,847.1,104.3
											z"/>
										<path class="st32" d="M850.2,105h-1v-9.1h1V105z"/>
										<path class="st32" d="M855,105.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S855.4,105.1,855,105.1z M854.7,99.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S855.2,99.3,854.7,99.3z"/>
										<path class="st32" d="M866.1,105l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H866.1z"/>
										<path class="st32" d="M875.7,105.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S876.1,105.1,875.7,105.1z M875.4,99.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S875.9,99.3,875.4,99.3z"/>
										<path class="st32" d="M881.3,101.7l-2.2-3.1h1.1l1.7,2.5l1.7-2.5h1.1l-2.2,3.1l2.3,3.3h-1.1l-1.8-2.6L880,105h-1.1L881.3,101.7z
											"/>
										<path class="st32" d="M885.9,96.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S885.9,97.1,885.9,96.9z M887,105h-1v-6.4h1V105z"/>
										<path class="st32" d="M891.6,105.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C892.8,105,892.2,105.1,891.6,105.1z"/>
										<path class="st32" d="M898.7,105l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L895,99c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H898.7z M896.5,104.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S896.1,104.3,896.5,104.3z"/>
										<path class="st32" d="M905.8,105v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H905.8z"/>
										<path class="st32" d="M915.8,100.5h2.9v4.2c-0.5,0.1-0.9,0.3-1.4,0.3s-1,0.1-1.6,0.1c-1.3,0-2.3-0.4-3-1.2s-1.1-1.9-1.1-3.2
											c0-0.9,0.2-1.7,0.5-2.3s0.9-1.2,1.5-1.5s1.5-0.5,2.4-0.5c0.9,0,1.8,0.2,2.6,0.5l-0.4,0.9c-0.8-0.3-1.5-0.5-2.2-0.5
											c-1,0-1.9,0.3-2.4,0.9s-0.9,1.5-0.9,2.6c0,1.2,0.3,2,0.8,2.6s1.4,0.9,2.5,0.9c0.6,0,1.2-0.1,1.7-0.2v-2.6h-1.9V100.5z"/>
										<path class="st32" d="M923.6,98.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S923.2,98.5,923.6,98.5z"/>
										<path class="st32" d="M925.4,96.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S925.4,97.1,925.4,96.9z M926.5,105h-1v-6.4h1V105z"/>
										<path class="st32" d="M929.5,105h-1v-9.1h1V105z"/>
										<path class="st32" d="M932.6,105h-1v-9.1h1V105z"/>
									</g>
								</g>
								<g id="dining_txt_4">
									<g>
										<path class="st32" d="M804.9,118h-1.3v2h-0.9v-2h-4.2v-0.8l4.1-5.8h1v5.8h1.3V118z M802.7,117.2v-2.8c0-0.6,0-1.2,0.1-1.9h0
											c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H802.7z"/>
										<path class="st32" d="M806,119.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,119.7,806,119.4z"/>
										<path class="st32" d="M812.6,120v-8.6h1v8.6H812.6z"/>
										<path class="st32" d="M818.4,120.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C819.5,120,819,120.1,818.4,120.1z"/>
										<path class="st32" d="M825.9,120v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H825.9z"/>
										<path class="st32" d="M828.8,111.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S828.8,112.1,828.8,111.9z M829.8,120h-1v-6.4h1V120z"/>
										<path class="st32" d="M834.9,113.5c0.8,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4s-0.2,1.9-0.7,2.5s-1.1,0.9-2,0.9
											c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1l-0.2,0.8h-0.7v-9.1h1v2.2c0,0.5,0,0.9,0,1.3h0
											C833.3,113.8,834,113.5,834.9,113.5z M834.8,114.3c-0.7,0-1.1,0.2-1.4,0.6s-0.4,1-0.4,1.9s0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.4-1.1,0.4-1.9c0-0.8-0.1-1.5-0.4-1.9S835.4,114.3,834.8,114.3z"/>
										<path class="st32" d="M843.2,120l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H843.2z M841,119.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S840.6,119.3,841,119.3z"/>
										<path class="st32" d="M850.3,120v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H850.3z"/>
										<path class="st32" d="M861.4,117.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S861.4,117.2,861.4,117.7z"/>
										<path class="st32" d="M865.1,119.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8H864v3.8c0,0.4,0.1,0.7,0.3,0.9S864.8,119.3,865.1,119.3z
											"/>
										<path class="st32" d="M869.9,120.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S870.4,120.1,869.9,120.1z M869.7,114.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S870.2,114.3,869.7,114.3z"/>
										<path class="st32" d="M877.9,120l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H877.9z M875.7,119.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S875.3,119.3,875.7,119.3z"/>
										<path class="st32" d="M881.6,116.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C881.6,115.9,881.6,116.3,881.6,116.7L881.6,116.7z"/>
										<path class="st32" d="M889.7,117.8c0-0.5,0.1-1,0.4-1.3s0.8-0.8,1.5-1.2c-0.3-0.4-0.6-0.7-0.7-0.8s-0.2-0.4-0.3-0.6
											s-0.1-0.4-0.1-0.6c0-0.6,0.2-1,0.6-1.4s0.9-0.5,1.6-0.5c0.6,0,1.1,0.2,1.5,0.5s0.5,0.8,0.5,1.4c0,0.4-0.1,0.8-0.4,1.2
											s-0.7,0.7-1.3,1.1l2.4,2.3c0.2-0.2,0.4-0.5,0.5-0.9s0.2-0.7,0.3-1.1h1c-0.3,1.1-0.7,2-1.2,2.5l1.8,1.7h-1.3l-1.1-1
											c-0.5,0.4-0.9,0.7-1.4,0.9s-1,0.3-1.6,0.3c-0.8,0-1.5-0.2-2-0.6S889.7,118.5,889.7,117.8z M892.4,119.3c0.9,0,1.7-0.3,2.3-0.9
											l-2.6-2.5c-0.4,0.3-0.7,0.5-0.9,0.7s-0.3,0.4-0.4,0.6s-0.1,0.4-0.1,0.7c0,0.5,0.2,0.8,0.5,1.1S891.9,119.3,892.4,119.3z
											 M891.5,113.2c0,0.3,0.1,0.5,0.2,0.8s0.4,0.5,0.7,0.9c0.5-0.3,0.9-0.6,1.1-0.8s0.3-0.5,0.3-0.9c0-0.3-0.1-0.5-0.3-0.7
											s-0.5-0.3-0.8-0.3c-0.3,0-0.6,0.1-0.8,0.3S891.5,112.8,891.5,113.2z"/>
										<path class="st32" d="M906.9,117.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S906.9,117.2,906.9,117.7z"/>
										<path class="st32" d="M909.4,113.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											H913l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H909.4z"/>
										<path class="st32" d="M920,118.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S920,117.8,920,118.3z"/>
										<path class="st32" d="M926,120v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1v-9.1
											h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H926z"/>
										<path class="st32" d="M928.9,111.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S928.9,112.1,928.9,111.9z M929.9,120h-1v-6.4h1V120z"/>
									</g>
								</g>
								<g id="dining_txt_5">
									<g>
										<path class="st32" d="M801.5,129.8c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C800.6,129.8,801.1,129.8,801.5,129.8z"/>
										<path class="st32" d="M806,134.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,134.7,806,134.4z"/>
										<path class="st32" d="M817.5,126.5h1.1l-3.1,8.6h-1l-3.1-8.6h1.1l2,5.5c0.2,0.6,0.4,1.3,0.5,1.9c0.1-0.6,0.3-1.3,0.6-1.9
											L817.5,126.5z"/>
										<path class="st32" d="M819.5,126.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S819.5,127.1,819.5,126.9z M820.6,135h-1v-6.4h1V135z"/>
										<path class="st32" d="M827,135v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1v-6.4
											h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H827z"/>
										<path class="st32" d="M834.4,135v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H834.4z"/>
										<path class="st32" d="M836.3,128.6h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1V137c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L836.3,128.6z"/>
										<path class="st32" d="M844.2,126.5l-0.2,3.1h-0.6l-0.2-3.1H844.2z"/>
										<path class="st32" d="M850.2,133.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S850.2,132.8,850.2,133.3z"/>
										<path class="st32" d="M862.5,130.7c0,1.4-0.3,2.4-1,3.2s-1.7,1.2-2.9,1.2c-1.3,0-2.2-0.4-2.9-1.2s-1-1.9-1-3.3
											c0-1.4,0.3-2.5,1-3.2c0.7-0.8,1.7-1.2,2.9-1.2c1.2,0,2.2,0.4,2.9,1.2C862.1,128.3,862.5,129.3,862.5,130.7z M855.7,130.7
											c0,1.2,0.2,2,0.7,2.6s1.2,0.9,2.2,0.9c0.9,0,1.7-0.3,2.2-0.9s0.7-1.5,0.7-2.6c0-1.2-0.2-2-0.7-2.6c-0.5-0.6-1.2-0.9-2.1-0.9
											c-0.9,0-1.7,0.3-2.2,0.9C855.9,128.7,855.7,129.6,855.7,130.7z"/>
										<path class="st32" d="M868.6,135v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H868.6z"/>
										<path class="st32" d="M882.3,135h-1l-1.7-5.7c-0.1-0.3-0.2-0.6-0.3-1c-0.1-0.4-0.2-0.6-0.2-0.7c-0.1,0.5-0.2,1.1-0.4,1.7
											l-1.7,5.7h-1l-2.3-8.6h1.1l1.4,5.3c0.2,0.7,0.3,1.4,0.4,2c0.1-0.7,0.3-1.4,0.5-2.1l1.5-5.2h1.1l1.6,5.3c0.2,0.6,0.3,1.3,0.5,2.1
											c0.1-0.6,0.2-1.2,0.4-2l1.3-5.3h1.1L882.3,135z"/>
										<path class="st32" d="M885.8,126.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S885.8,127.1,885.8,126.9z M886.8,135h-1v-6.4h1V135z"/>
										<path class="st32" d="M893.3,135v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H893.3z"/>
										<path class="st32" d="M900.6,134.2L900.6,134.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L900.6,134.2z M898.7,134.3c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S898.1,134.3,898.7,134.3z"/>
										<path class="st32" d="M908.8,135l-1.2-3.8c-0.1-0.2-0.2-0.8-0.4-1.6h0c-0.2,0.7-0.3,1.2-0.4,1.6l-1.2,3.8h-1.1l-1.8-6.4h1
											c0.4,1.6,0.7,2.8,0.9,3.7s0.3,1.4,0.4,1.7h0c0-0.2,0.1-0.5,0.2-0.9s0.2-0.6,0.2-0.8l1.2-3.7h1.1l1.1,3.7
											c0.2,0.7,0.4,1.2,0.4,1.7h0c0-0.1,0.1-0.4,0.1-0.7s0.5-1.9,1.2-4.7h1L910,135H908.8z"/>
										<path class="st32" d="M916.9,135l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H916.9z M914.7,134.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S914.3,134.3,914.7,134.3z"/>
										<path class="st32" d="M922.5,128.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S922.1,128.5,922.5,128.5z"/>
										<path class="st32" d="M928.9,134.2L928.9,134.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1H929L928.9,134.2z M926.9,134.3c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S926.3,134.3,926.9,134.3z"/>
									</g>
								</g>
								<g id="dining_txt_6">
									<g>
										<path class="st32" d="M798.9,147.4c0-1.7,0.3-2.9,1-3.8s1.6-1.3,2.9-1.3c0.4,0,0.8,0,1,0.1v0.8c-0.3-0.1-0.6-0.1-1-0.1
											c-0.9,0-1.6,0.3-2.1,0.9s-0.8,1.5-0.8,2.7h0.1c0.4-0.7,1.1-1,2-1c0.8,0,1.4,0.2,1.8,0.7s0.7,1.1,0.7,1.9c0,0.9-0.2,1.6-0.7,2.1
											s-1.1,0.8-2,0.8c-0.9,0-1.6-0.3-2.1-1S798.9,148.5,798.9,147.4z M801.8,150.3c0.6,0,1-0.2,1.3-0.5s0.5-0.9,0.5-1.5
											c0-0.6-0.1-1-0.4-1.3s-0.7-0.5-1.3-0.5c-0.4,0-0.7,0.1-1,0.2s-0.5,0.3-0.7,0.6s-0.3,0.5-0.3,0.8c0,0.4,0.1,0.8,0.2,1.1
											s0.4,0.6,0.7,0.8S801.4,150.3,801.8,150.3z"/>
										<path class="st32" d="M806,150.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,150.7,806,150.4z"/>
										<path class="st32" d="M817.4,148.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S817.4,148.2,817.4,148.7z"/>
										<path class="st32" d="M819.9,144.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H819.9z"/>
										<path class="st32" d="M830.5,149.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S830.5,148.8,830.5,149.3z"/>
										<path class="st32" d="M836.5,151v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H836.5z"/>
										<path class="st32" d="M839.4,142.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S839.4,143.1,839.4,142.9z M840.5,151h-1v-6.4h1V151z"/>
										<path class="st32" d="M852.5,151h-1.1l-4.7-7.2h0c0.1,0.8,0.1,1.6,0.1,2.3v4.9h-0.9v-8.6h1.1l4.7,7.2h0c0-0.1,0-0.4-0.1-1
											s0-1,0-1.2v-4.9h0.9V151z"/>
										<path class="st32" d="M858.6,151l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H858.6z M856.4,150.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S856.1,150.3,856.4,150.3z"/>
										<path class="st32" d="M869.5,151v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H869.5z"/>
										<path class="st32" d="M872.4,142.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S872.4,143.1,872.4,142.9z M873.5,151h-1v-6.4h1V151z"/>
									</g>
								</g>
								<g id="dining_txt_7">
									<g>
										<path class="st32" d="M799.9,166l3.6-7.7h-4.7v-0.9h5.7v0.8L801,166H799.9z"/>
										<path class="st32" d="M806,165.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,165.7,806,165.4z"/>
										<path class="st32" d="M816.3,158.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C817.6,158.4,816.9,158.2,816.3,158.2z"/>
										<path class="st32" d="M824,166l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H824z M821.8,165.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S821.4,165.3,821.8,165.3z"/>
										<path class="st32" d="M829.7,159.5c0.8,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4s-0.2,1.9-0.7,2.5s-1.1,0.9-2,0.9
											c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1l-0.2,0.8h-0.7v-9.1h1v2.2c0,0.5,0,0.9,0,1.3h0
											C828.1,159.8,828.8,159.5,829.7,159.5z M829.5,160.3c-0.7,0-1.1,0.2-1.4,0.6s-0.4,1-0.4,1.9s0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.4-1.1,0.4-1.9c0-0.8-0.1-1.5-0.4-1.9S830.1,160.3,829.5,160.3z"/>
										<path class="st32" d="M836.8,166.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S837.2,166.1,836.8,166.1z M836.5,160.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S837,160.3,836.5,160.3z"/>
										<path class="st32" d="M843.7,159.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S843.3,159.5,843.7,159.5z"/>
										<path class="st32" d="M850.1,166v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H850.1z"/>
										<path class="st32" d="M855.8,166.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S856.2,166.1,855.8,166.1z M855.5,160.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S856,160.3,855.5,160.3z"/>
										<path class="st32" d="M861.8,165.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S861.5,165.3,861.8,165.3
											z"/>
										<path class="st32" d="M872.1,163.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S872.1,163.2,872.1,163.7z"/>
										<path class="st32" d="M875.8,165.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S875.5,165.3,875.8,165.3
											z"/>
										<path class="st32" d="M880.7,166.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S881.1,166.1,880.7,166.1z M880.4,160.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S880.9,160.3,880.4,160.3z"/>
										<path class="st32" d="M888.6,166l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H888.6z M886.4,165.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S886,165.3,886.4,165.3z"/>
										<path class="st32" d="M892.3,162.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C892.3,161.9,892.3,162.3,892.3,162.7L892.3,162.7z"/>
										<path class="st32" d="M902,166v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1v-9.1
											h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H902z"/>
										<path class="st32" d="M910.6,162.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S910.6,161.8,910.6,162.8z M905.7,162.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S905.7,162,905.7,162.8z"/>
										<path class="st32" d="M913.2,159.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H913.2z"/>
										<path class="st32" d="M923.8,164.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S923.8,163.8,923.8,164.3z"/>
										<path class="st32" d="M928.1,166.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6H926c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S928.5,166.1,928.1,166.1z M927.8,160.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S928.3,160.3,927.8,160.3z"/>
									</g>
								</g>
								<g id="dining_txt_8">
									<g>
										<path class="st32" d="M801.7,172.3c0.8,0,1.4,0.2,1.9,0.5s0.7,0.9,0.7,1.5c0,0.4-0.1,0.8-0.4,1.2s-0.7,0.7-1.3,0.9
											c0.7,0.3,1.2,0.7,1.5,1s0.4,0.8,0.4,1.3c0,0.7-0.2,1.3-0.7,1.7s-1.2,0.6-2,0.6c-0.9,0-1.6-0.2-2.1-0.6s-0.7-1-0.7-1.7
											c0-1,0.6-1.7,1.8-2.3c-0.5-0.3-0.9-0.6-1.2-1s-0.4-0.7-0.4-1.2c0-0.6,0.2-1.1,0.7-1.5S800.9,172.3,801.7,172.3z M799.8,178.9
											c0,0.5,0.2,0.8,0.5,1.1s0.8,0.4,1.4,0.4c0.6,0,1-0.1,1.4-0.4s0.5-0.6,0.5-1.1c0-0.4-0.2-0.7-0.5-1s-0.8-0.6-1.6-0.9
											c-0.6,0.2-1,0.5-1.3,0.8S799.8,178.4,799.8,178.9z M801.7,173.1c-0.5,0-0.9,0.1-1.1,0.4s-0.4,0.5-0.4,0.9c0,0.4,0.1,0.7,0.3,0.9
											s0.7,0.5,1.3,0.8c0.6-0.2,1-0.5,1.2-0.8s0.3-0.6,0.3-0.9c0-0.4-0.1-0.7-0.4-0.9S802.1,173.1,801.7,173.1z"/>
										<path class="st32" d="M806,180.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,180.7,806,180.4z"/>
										<path class="st32" d="M817.5,172.5h1.1l-3.1,8.6h-1l-3.1-8.6h1.1l2,5.5c0.2,0.6,0.4,1.3,0.5,1.9c0.1-0.6,0.3-1.3,0.6-1.9
											L817.5,172.5z"/>
										<path class="st32" d="M819.5,172.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S819.5,173.1,819.5,172.9z M820.6,181h-1v-6.4h1V181z"/>
										<path class="st32" d="M827,180.2L827,180.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L827,180.2z M825,180.3c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S824.5,180.3,825,180.3z"/>
										<path class="st32" d="M833.9,181l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H833.9z M831.7,180.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S831.3,180.3,831.7,180.3z"/>
										<path class="st32" d="M837.6,181h-1v-9.1h1V181z"/>
										<path class="st32" d="M839.6,172.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S839.6,173.1,839.6,172.9z M840.7,181h-1v-6.4h1V181z"/>
										<path class="st32" d="M846.7,181l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L843,175c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H846.7z M844.5,180.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S844.1,180.3,844.5,180.3z"/>
										<path class="st32" d="M850.2,172.5l-0.2,3.1h-0.6l-0.2-3.1H850.2z"/>
										<path class="st32" d="M856.2,179.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S856.2,178.8,856.2,179.3z"/>
										<path class="st32" d="M865.9,178.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S865.9,178.2,865.9,178.7z"/>
										<path class="st32" d="M873,177.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2s-0.4-1.1-0.4-1.8
											c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S873,176.8,873,177.8z M868.1,177.8c0,0.8,0.2,1.4,0.5,1.9
											s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1.1,0.2-1.4,0.6
											S868.1,177,868.1,177.8z"/>
										<path class="st32" d="M875.6,174.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9H879c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H875.6z"/>
										<path class="st32" d="M884.2,180.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S883.8,180.3,884.2,180.3
											z"/>
										<path class="st32" d="M890.7,181v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H890.7z"/>
										<path class="st32" d="M896.4,181.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S896.8,181.1,896.4,181.1z M896.1,175.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S896.6,175.3,896.1,175.3z"/>
										<path class="st32" d="M903.4,174.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S902.9,174.5,903.4,174.5z"/>
										<path class="st32" d="M909.7,181v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H909.7z"/>
										<path class="st32" d="M920.8,178.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S920.8,178.2,920.8,178.7z"/>
										<path class="st32" d="M924.5,180.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S924.1,180.3,924.5,180.3
											z"/>
										<path class="st32" d="M929.3,181.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S929.8,181.1,929.3,181.1z M929.1,175.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S929.6,175.3,929.1,175.3z"/>
										<path class="st32" d="M937.3,181l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H937.3z M935.1,180.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S934.7,180.3,935.1,180.3z"/>
										<path class="st32" d="M941,177.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7H944l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1v4.8
											C941,176.9,941,177.3,941,177.7L941,177.7z"/>
										<path class="st32" d="M950.7,181v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H950.7z"/>
										<path class="st32" d="M959.2,177.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S959.2,176.8,959.2,177.8z M954.3,177.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S954.3,177,954.3,177.8z"/>
										<path class="st32" d="M961.9,174.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H961.9z"/>
										<path class="st32" d="M972.5,179.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S972.5,178.8,972.5,179.3z"/>
										<path class="st32" d="M976.7,181.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S977.2,181.1,976.7,181.1z M976.5,175.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S977,175.3,976.5,175.3z"/>
									</g>
								</g>
								<g id="dining_txt_9">
									<g>
										<path class="st32" d="M804.5,191.1c0,3.4-1.3,5-3.9,5c-0.5,0-0.8,0-1.1-0.1v-0.8c0.3,0.1,0.7,0.2,1.1,0.2c0.9,0,1.6-0.3,2.1-0.9
											s0.7-1.5,0.8-2.7h-0.1c-0.2,0.3-0.5,0.6-0.9,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.7s-0.7-1.1-0.7-1.9
											c0-0.9,0.2-1.6,0.7-2.1s1.2-0.8,2-0.8c0.6,0,1.1,0.1,1.5,0.4s0.8,0.7,1,1.3S804.5,190.3,804.5,191.1z M801.6,188.2
											c-0.6,0-1,0.2-1.3,0.5s-0.5,0.9-0.5,1.5c0,0.6,0.1,1,0.4,1.3s0.7,0.5,1.3,0.5c0.4,0,0.7-0.1,1-0.2s0.5-0.3,0.7-0.6
											s0.3-0.5,0.3-0.8c0-0.4-0.1-0.8-0.2-1.1s-0.4-0.6-0.7-0.8S801.9,188.2,801.6,188.2z"/>
										<path class="st32" d="M806,195.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S806,195.7,806,195.4z"/>
										<path class="st32" d="M818,196l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H818z M816.6,192.4l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H816.6z"/>
										<path class="st32" d="M821,192.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7H824l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1v4.8
											C821,191.9,821,192.3,821,192.7L821,192.7z"/>
										<path class="st32" d="M826.3,187.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S826.3,188.1,826.3,187.9z M827.3,196h-1v-6.4h1V196z"/>
										<path class="st32" d="M833.8,196v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H833.8z"/>
										<path class="st32" d="M840.7,196l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L837,190c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H840.7z M838.5,195.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S838.1,195.3,838.5,195.3z"/>
										<path class="st32" d="M851.5,193.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S851.5,193.2,851.5,193.7z"/>
										<path class="st32" d="M854,189.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H854z"/>
										<path class="st32" d="M864.6,194.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S864.6,193.8,864.6,194.3z"/>
										<path class="st32" d="M870.6,196v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H870.6z"/>
										<path class="st32" d="M873.5,187.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S873.5,188.1,873.5,187.9z M874.5,196h-1v-6.4h1V196z"/>
										<path class="st32" d="M879.4,193.8c0-0.5,0.1-1,0.4-1.3s0.8-0.8,1.5-1.2c-0.3-0.4-0.6-0.7-0.7-0.8s-0.2-0.4-0.3-0.6
											s-0.1-0.4-0.1-0.6c0-0.6,0.2-1,0.6-1.4s0.9-0.5,1.6-0.5c0.6,0,1.1,0.2,1.5,0.5s0.5,0.8,0.5,1.4c0,0.4-0.1,0.8-0.4,1.2
											s-0.7,0.7-1.3,1.1l2.4,2.3c0.2-0.2,0.4-0.5,0.5-0.9s0.2-0.7,0.3-1.1h1c-0.3,1.1-0.7,2-1.2,2.5l1.8,1.7h-1.3l-1.1-1
											c-0.5,0.4-0.9,0.7-1.4,0.9s-1,0.3-1.6,0.3c-0.8,0-1.5-0.2-2-0.6S879.4,194.5,879.4,193.8z M882,195.3c0.9,0,1.7-0.3,2.3-0.9
											l-2.6-2.5c-0.4,0.3-0.7,0.5-0.9,0.7s-0.3,0.4-0.4,0.6s-0.1,0.4-0.1,0.7c0,0.5,0.2,0.8,0.5,1.1S881.5,195.3,882,195.3z
											 M881.1,189.2c0,0.3,0.1,0.5,0.2,0.8s0.4,0.5,0.7,0.9c0.5-0.3,0.9-0.6,1.1-0.8s0.3-0.5,0.3-0.9c0-0.3-0.1-0.5-0.3-0.7
											s-0.5-0.3-0.8-0.3c-0.3,0-0.6,0.1-0.8,0.3S881.1,188.8,881.1,189.2z"/>
										<path class="st32" d="M895.5,191.5h2.9v4.2c-0.5,0.1-0.9,0.3-1.4,0.3s-1,0.1-1.6,0.1c-1.3,0-2.3-0.4-3-1.2s-1.1-1.9-1.1-3.2
											c0-0.9,0.2-1.7,0.5-2.3s0.9-1.2,1.5-1.5s1.5-0.5,2.4-0.5c0.9,0,1.8,0.2,2.6,0.5l-0.4,0.9c-0.8-0.3-1.5-0.5-2.2-0.5
											c-1,0-1.9,0.3-2.4,0.9s-0.9,1.5-0.9,2.6c0,1.2,0.3,2,0.8,2.6s1.4,0.9,2.5,0.9c0.6,0,1.2-0.1,1.7-0.2v-2.6h-1.9V191.5z"/>
										<path class="st32" d="M903.3,189.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S902.9,189.5,903.3,189.5z"/>
										<path class="st32" d="M905.2,187.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S905.2,188.1,905.2,187.9z M906.2,196h-1v-6.4h1V196z"/>
										<path class="st32" d="M909.2,196h-1v-9.1h1V196z"/>
										<path class="st32" d="M912.3,196h-1v-9.1h1V196z"/>
									</g>
								</g>
								<g id="dining_txt_10">
									<g>
										<path class="st32" d="M802.4,211h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V211z"/>
										<path class="st32" d="M811.4,206.7c0,1.5-0.2,2.6-0.7,3.3s-1.2,1.1-2.1,1.1c-0.9,0-1.6-0.4-2.1-1.1s-0.7-1.8-0.7-3.3
											c0-1.5,0.2-2.6,0.7-3.3s1.2-1.1,2.1-1.1c0.9,0,1.6,0.4,2.1,1.1S811.4,205.3,811.4,206.7z M806.7,206.7c0,1.2,0.1,2.2,0.4,2.7
											s0.8,0.9,1.4,0.9c0.6,0,1.1-0.3,1.4-0.9s0.4-1.5,0.4-2.7s-0.1-2.1-0.4-2.7s-0.8-0.9-1.4-0.9c-0.6,0-1.1,0.3-1.4,0.8
											S806.7,205.5,806.7,206.7z"/>
										<path class="st32" d="M812.9,210.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,210.7,812.9,210.4z"/>
										<path class="st32" d="M824.3,208.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S824.3,208.2,824.3,208.7z"/>
										<path class="st32" d="M828.8,204.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S828.4,204.5,828.8,204.5z"/>
										<path class="st32" d="M830.7,202.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S830.7,203.1,830.7,202.9z M831.8,211h-1v-6.4h1V211z"/>
										<path class="st32" d="M843.3,211h-1.2l-3.1-4.2l-0.9,0.8v3.4h-1v-8.6h1v4.2l3.9-4.2h1.2l-3.4,3.7L843.3,211z"/>
										<path class="st32" d="M847.2,204.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S846.8,204.5,847.2,204.5z"/>
										<path class="st32" d="M849.1,202.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S849.1,203.1,849.1,202.9z M850.2,211h-1v-6.4h1V211z"/>
										<path class="st32" d="M856.4,209.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S856.4,208.8,856.4,209.3z"/>
										<path class="st32" d="M862.4,211v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H862.4z"/>
										<path class="st32" d="M869.7,211v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H869.7z"/>
										<path class="st32" d="M876.6,211l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H876.6z M874.4,210.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S874.1,210.3,874.4,210.3z"/>
										<path class="st32" d="M887.5,202.5h1.1l-3.1,8.6h-1l-3.1-8.6h1.1l2,5.5c0.2,0.6,0.4,1.3,0.5,1.9c0.1-0.6,0.3-1.3,0.6-1.9
											L887.5,202.5z"/>
										<path class="st32" d="M889.5,202.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S889.5,203.1,889.5,202.9z M890.6,211h-1v-6.4h1V211z"/>
										<path class="st32" d="M893.6,211h-1v-9.1h1V211z"/>
										<path class="st32" d="M899.6,211l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H899.6z M897.5,210.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S897.1,210.3,897.5,210.3z"/>
										<path class="st32" d="M906.5,209.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S906.5,208.8,906.5,209.3z"/>
									</g>
								</g>
								<g id="dining_txt_11">
									<g>
										<path class="st32" d="M802.4,226h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V226z"/>
										<path class="st32" d="M809.3,226h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V226z"/>
										<path class="st32" d="M812.9,225.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,225.7,812.9,225.4z"/>
										<path class="st32" d="M819.5,217.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V217.5z M820.4,221.1h1.6c0.7,0,1.2-0.1,1.5-0.3
											s0.5-0.6,0.5-1.1c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V221.1z M820.4,222v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H820.4z"/>
										<path class="st32" d="M832.6,222.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S832.6,221.8,832.6,222.8z M827.7,222.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S827.7,222,827.7,222.8z"/>
										<path class="st32" d="M838.7,226v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H838.7z"/>
										<path class="st32" d="M844.4,226.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S844.8,226.1,844.4,226.1z M844.1,220.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S844.7,220.3,844.1,220.3z"/>
										<path class="st32" d="M852.8,226v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H852.8z"/>
										<path class="st32" d="M858.5,226.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S858.9,226.1,858.5,226.1z M858.2,220.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S858.7,220.3,858.2,220.3z"/>
										<path class="st32" d="M866.5,226l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H866.5z M864.3,225.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S863.9,225.3,864.3,225.3z"/>
										<path class="st32" d="M873.6,225.2L873.6,225.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L873.6,225.2z M871.6,225.3c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S871,225.3,871.6,225.3z"/>
										<path class="st32" d="M880.7,224.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S880.7,223.8,880.7,224.3z"/>
									</g>
								</g>
								<g id="dining_txt_12">
									<g>
										<path class="st32" d="M802.4,241h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V241z"/>
										<path class="st32" d="M811.3,241h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V241z"/>
										<path class="st32" d="M812.9,240.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,240.7,812.9,240.4z"/>
										<path class="st32" d="M826.9,241h-1l-1.7-5.7c-0.1-0.3-0.2-0.6-0.3-1s-0.2-0.6-0.2-0.7c-0.1,0.5-0.2,1.1-0.4,1.7l-1.7,5.7h-1
											l-2.3-8.6h1.1l1.4,5.3c0.2,0.7,0.3,1.4,0.4,2c0.1-0.7,0.3-1.4,0.5-2.1l1.5-5.2h1.1l1.6,5.3c0.2,0.6,0.3,1.3,0.5,2.1
											c0.1-0.6,0.2-1.2,0.4-2l1.3-5.3h1.1L826.9,241z"/>
										<path class="st32" d="M830.3,232.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S830.3,233.1,830.3,232.9z M831.4,241h-1v-6.4h1V241z"/>
										<path class="st32" d="M834.4,241h-1v-9.1h1V241z"/>
										<path class="st32" d="M840.9,240.2L840.9,240.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1H841L840.9,240.2z M838.9,240.3c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S838.3,240.3,838.9,240.3z"/>
										<path class="st32" d="M846.7,235.4h-1.6v5.7h-1v-5.7H843v-0.4l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2
											l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4s-0.3,0.6-0.3,1.2v0.4h1.6V235.4z M848.9,241h-1v-9.1h1V241z"/>
										<path class="st32" d="M856.5,237.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S856.5,236.8,856.5,237.8z M851.6,237.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S851.6,237,851.6,237.8z"/>
										<path class="st32" d="M859.1,234.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H859.1z"/>
										<path class="st32" d="M868.5,234.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S868.1,234.5,868.5,234.5z"/>
									</g>
								</g>
								<g id="dining_txt_13">
									<g>
										<path class="st32" d="M802.4,256h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V256z"/>
										<path class="st32" d="M811,249.5c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6H807v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S811,248.8,811,249.5z"/>
										<path class="st32" d="M812.9,255.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,255.7,812.9,255.4z"/>
										<path class="st32" d="M823.2,256l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0L824,256H823.2z"/>
										<path class="st32" d="M832.9,256.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S833.3,256.1,832.9,256.1z M832.6,250.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S833.1,250.3,832.6,250.3z"/>
										<path class="st32" d="M837.8,256h-1v-9.1h1V256z"/>
										<path class="st32" d="M840.9,256h-1v-9.1h1V256z"/>
										<path class="st32" d="M848.5,252.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S848.5,251.8,848.5,252.8z M843.6,252.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S843.6,252,843.6,252.8z"/>
										<path class="st32" d="M855.4,256l-1.2-3.8c-0.1-0.2-0.2-0.8-0.4-1.6h0c-0.2,0.7-0.3,1.2-0.4,1.6l-1.2,3.8H851l-1.8-6.4h1
											c0.4,1.6,0.7,2.8,0.9,3.7s0.3,1.4,0.4,1.7h0c0-0.2,0.1-0.5,0.2-0.9s0.2-0.6,0.2-0.8l1.2-3.7h1.1l1.1,3.7
											c0.2,0.7,0.4,1.2,0.4,1.7h0c0-0.1,0.1-0.4,0.1-0.7s0.5-1.9,1.2-4.7h1l-1.8,6.4H855.4z"/>
										<path class="st32" d="M866.6,256l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H866.6z"/>
										<path class="st32" d="M874.4,249.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											H878l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H874.4z"/>
										<path class="st32" d="M885,254.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S885,253.8,885,254.3z"/>
										<path class="st32" d="M891,256v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1v-9.1
											h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H891z"/>
										<path class="st32" d="M896.9,249.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S896.5,249.5,896.9,249.5z"/>
										<path class="st32" d="M904.4,252.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S904.4,251.8,904.4,252.8z M899.5,252.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S899.5,252,899.5,252.8z"/>
										<path class="st32" d="M911.6,252.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S911.6,251.8,911.6,252.8z M906.7,252.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S906.7,252,906.7,252.8z"/>
										<path class="st32" d="M921.5,256v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H921.5z"/>
									</g>
								</g>
								<g id="dining_txt_14">
									<g>
										<path class="st32" d="M802.4,271h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V271z"/>
										<path class="st32" d="M811.7,269h-1.3v2h-0.9v-2h-4.2v-0.8l4.1-5.8h1v5.8h1.3V269z M809.5,268.2v-2.8c0-0.6,0-1.2,0.1-1.9h0
											c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H809.5z"/>
										<path class="st32" d="M812.9,270.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,270.7,812.9,270.4z"/>
										<path class="st32" d="M818.2,273.3c-0.4,0-0.7-0.1-0.9-0.2v-0.8c0.3,0.1,0.6,0.1,0.9,0.1c0.4,0,0.7-0.1,0.9-0.4s0.3-0.6,0.3-1
											v-8.6h1v8.5c0,0.7-0.2,1.3-0.6,1.7S818.9,273.3,818.2,273.3z"/>
										<path class="st32" d="M826.5,271l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H826.5z M824.3,270.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S823.9,270.3,824.3,270.3z"/>
										<path class="st32" d="M833.3,269.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S833.3,268.8,833.3,269.3z"/>
										<path class="st32" d="M840.5,267.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S840.5,266.8,840.5,267.8z M835.6,267.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S835.6,267,835.6,267.8z"/>
										<path class="st32" d="M846.6,271v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H846.6z"/>
										<path class="st32" d="M850.3,262.5l0.1,0.1c-0.1,0.4-0.2,0.8-0.4,1.4s-0.4,1-0.6,1.4h-0.7c0.3-1.1,0.5-2.1,0.6-2.9H850.3z"/>
										<path class="st32" d="M855.7,269.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S855.7,268.8,855.7,269.3z"/>
										<path class="st32" d="M867.4,266.7c0,1.4-0.4,2.5-1.2,3.2s-1.9,1.1-3.3,1.1h-2.4v-8.6h2.6c1.3,0,2.4,0.4,3.1,1.1
											S867.4,265.3,867.4,266.7z M866.3,266.7c0-1.1-0.3-2-0.8-2.5s-1.4-0.8-2.5-0.8h-1.4v6.8h1.2c1.2,0,2.1-0.3,2.7-0.9
											S866.3,267.8,866.3,266.7z"/>
										<path class="st32" d="M871.9,271.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S872.3,271.1,871.9,271.1z M871.6,265.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S872.1,265.3,871.6,265.3z"/>
										<path class="st32" d="M876.9,271h-1v-9.1h1V271z"/>
										<path class="st32" d="M878.8,262.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S878.8,263.1,878.8,262.9z M879.9,271h-1v-6.4h1V271z"/>
									</g>
								</g>
								<g id="dining_txt_15">
									<g>
										<path class="st32" d="M802.4,287h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V287z"/>
										<path class="st32" d="M808.4,281.8c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C807.5,281.8,807.9,281.8,808.4,281.8z"/>
										<path class="st32" d="M812.9,286.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,286.7,812.9,286.4z"/>
										<path class="st32" d="M820.4,287h-1v-8.6h4.8v0.9h-3.8v3.1h3.6v0.9h-3.6V287z"/>
										<path class="st32" d="M825.4,278.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S825.4,279.1,825.4,278.9z M826.5,287h-1v-6.4h1V287z"/>
										<path class="st32" d="M831.5,280.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S831.1,280.5,831.5,280.5z"/>
										<path class="st32" d="M836.1,287.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S836.6,287.1,836.1,287.1z M835.9,281.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S836.4,281.3,835.9,281.3z"/>
										<path class="st32" d="M843.1,281.4h-1.6v5.7h-1v-5.7h-1.1v-0.4l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2
											l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4s-0.3,0.6-0.3,1.2v0.4h1.6V281.4z M845.2,287h-1v-9.1h1V287z"/>
										<path class="st32" d="M846.2,280.6h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1V289c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L846.2,280.6z"/>
										<path class="st32" d="M862.8,287h-1.2l-3.1-4.2l-0.9,0.8v3.4h-1v-8.6h1v4.2l3.9-4.2h1.2l-3.4,3.7L862.8,287z"/>
										<path class="st32" d="M863.7,278.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S863.7,279.1,863.7,278.9z M864.8,287h-1v-6.4h1V287z"/>
										<path class="st32" d="M868.9,286.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8H866v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S868.6,286.3,868.9,286.3z
											"/>
										<path class="st32" d="M873.6,287.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C874.8,287,874.3,287.1,873.6,287.1z"/>
										<path class="st32" d="M881.2,287v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H881.2z"/>
										<path class="st32" d="M886.9,287.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S887.3,287.1,886.9,287.1z M886.6,281.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S887.1,281.3,886.6,281.3z"/>
										<path class="st32" d="M895.3,287v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H895.3z"/>
										<path class="st32" d="M901,284.8c0-0.5,0.1-1,0.4-1.3s0.8-0.8,1.5-1.2c-0.3-0.4-0.6-0.7-0.7-0.8s-0.2-0.4-0.3-0.6
											s-0.1-0.4-0.1-0.6c0-0.6,0.2-1,0.6-1.4s0.9-0.5,1.6-0.5c0.6,0,1.1,0.2,1.5,0.5s0.5,0.8,0.5,1.4c0,0.4-0.1,0.8-0.4,1.2
											s-0.7,0.7-1.3,1.1l2.4,2.3c0.2-0.2,0.4-0.5,0.5-0.9s0.2-0.7,0.3-1.1h1c-0.3,1.1-0.7,2-1.2,2.5l1.8,1.7h-1.3l-1.1-1
											c-0.5,0.4-0.9,0.7-1.4,0.9s-1,0.3-1.6,0.3c-0.8,0-1.5-0.2-2-0.6S901,285.5,901,284.8z M903.7,286.3c0.9,0,1.7-0.3,2.3-0.9
											l-2.6-2.5c-0.4,0.3-0.7,0.5-0.9,0.7s-0.3,0.4-0.4,0.6s-0.1,0.4-0.1,0.7c0,0.5,0.2,0.8,0.5,1.1S903.2,286.3,903.7,286.3z
											 M902.8,280.2c0,0.3,0.1,0.5,0.2,0.8s0.4,0.5,0.7,0.9c0.5-0.3,0.9-0.6,1.1-0.8s0.3-0.5,0.3-0.9c0-0.3-0.1-0.5-0.3-0.7
											s-0.5-0.3-0.8-0.3c-0.3,0-0.6,0.1-0.8,0.3S902.8,279.8,902.8,280.2z"/>
										<path class="st32" d="M913.4,278.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V278.5z M914.4,282.1h1.6c0.7,0,1.2-0.1,1.5-0.3
											s0.5-0.6,0.5-1.1c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V282.1z M914.4,283v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H914.4z"/>
										<path class="st32" d="M925,287l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H925z M922.8,286.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S922.4,286.3,922.8,286.3z"/>
										<path class="st32" d="M930.6,280.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S930.2,280.5,930.6,280.5z"/>
									</g>
								</g>
								<g id="dining_txt_16">
									<g>
										<path class="st32" d="M802.4,303h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V303z"/>
										<path class="st32" d="M805.8,299.4c0-1.7,0.3-2.9,1-3.8s1.6-1.3,2.9-1.3c0.4,0,0.8,0,1,0.1v0.8c-0.3-0.1-0.6-0.1-1-0.1
											c-0.9,0-1.6,0.3-2.1,0.9s-0.8,1.5-0.8,2.7h0.1c0.4-0.7,1.1-1,2-1c0.8,0,1.4,0.2,1.8,0.7s0.7,1.1,0.7,1.9c0,0.9-0.2,1.6-0.7,2.1
											s-1.1,0.8-2,0.8c-0.9,0-1.6-0.3-2.1-1S805.8,300.5,805.8,299.4z M808.7,302.3c0.6,0,1-0.2,1.3-0.5s0.5-0.9,0.5-1.5
											c0-0.6-0.1-1-0.4-1.3s-0.7-0.5-1.3-0.5c-0.4,0-0.7,0.1-1,0.2s-0.5,0.3-0.7,0.6s-0.3,0.5-0.3,0.8c0,0.4,0.1,0.8,0.2,1.1
											s0.4,0.6,0.7,0.8S808.3,302.3,808.7,302.3z"/>
										<path class="st32" d="M812.9,302.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,302.7,812.9,302.4z"/>
										<path class="st32" d="M822.1,303h-1v-7.7h-2.7v-0.9h6.4v0.9h-2.7V303z"/>
										<path class="st32" d="M829.9,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H829.9z M827.7,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S827.3,302.3,827.7,302.3z"/>
										<path class="st32" d="M835.5,296.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S835.1,296.5,835.5,296.5z"/>
										<path class="st32" d="M841.5,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H841.5z M839.3,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S838.9,302.3,839.3,302.3z"/>
										<path class="st32" d="M848.6,303v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H848.6z"/>
										<path class="st32" d="M852.5,296.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H852.5z"/>
										<path class="st32" d="M867.1,303v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,0.9-0.4,1.6v3.6h-1
											v-4.2c0-0.5-0.1-0.9-0.3-1.2s-0.6-0.4-1-0.4c-0.6,0-1.1,0.2-1.3,0.5s-0.4,1-0.4,1.8v3.4h-1v-6.4h0.8l0.2,0.9h0
											c0.2-0.3,0.4-0.6,0.8-0.7s0.7-0.3,1.1-0.3c1,0,1.7,0.4,2,1.1h0c0.2-0.3,0.5-0.6,0.8-0.8s0.8-0.3,1.2-0.3c0.7,0,1.3,0.2,1.6,0.6
											s0.5,1,0.5,1.8v4.2H867.1z"/>
										<path class="st32" d="M874,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H874z M871.8,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S871.5,302.3,871.8,302.3z"/>
										<path class="st32" d="M878.8,302.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S878.5,302.3,878.8,302.3
											z"/>
										<path class="st32" d="M884.9,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H884.9z M882.7,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S882.4,302.3,882.7,302.3z"/>
										<path class="st32" d="M894.7,303l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0l-2.9,7.6H894.7z"/>
										<path class="st32" d="M904.3,303.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S904.8,303.1,904.3,303.1z M904.1,297.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S904.6,297.3,904.1,297.3z"/>
										<path class="st32" d="M909.9,299.7l-2.2-3.1h1.1l1.7,2.5l1.7-2.5h1.1l-2.2,3.1l2.3,3.3h-1.1l-1.8-2.6l-1.8,2.6h-1.1L909.9,299.7
											z"/>
										<path class="st32" d="M914.6,294.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S914.6,295.1,914.6,294.9z M915.6,303h-1v-6.4h1V303z"/>
										<path class="st32" d="M920.2,303.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C921.4,303,920.9,303.1,920.2,303.1z"/>
										<path class="st32" d="M927.3,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H927.3z M925.1,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S924.8,302.3,925.1,302.3z"/>
										<path class="st32" d="M934.5,303v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H934.5z"/>
										<path class="st32" d="M944.5,298.5h2.9v4.2c-0.5,0.1-0.9,0.3-1.4,0.3s-1,0.1-1.6,0.1c-1.3,0-2.3-0.4-3-1.2s-1.1-1.9-1.1-3.2
											c0-0.9,0.2-1.7,0.5-2.3s0.9-1.2,1.5-1.5s1.5-0.5,2.4-0.5c0.9,0,1.8,0.2,2.6,0.5l-0.4,0.9c-0.8-0.3-1.5-0.5-2.2-0.5
											c-1,0-1.9,0.3-2.4,0.9s-0.9,1.5-0.9,2.6c0,1.2,0.3,2,0.8,2.6s1.4,0.9,2.5,0.9c0.6,0,1.2-0.1,1.7-0.2v-2.6h-1.9V298.5z"/>
										<path class="st32" d="M952.2,296.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S951.8,296.5,952.2,296.5z"/>
										<path class="st32" d="M954.1,294.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S954.1,295.1,954.1,294.9z M955.1,303h-1v-6.4h1V303z"/>
										<path class="st32" d="M958.2,303h-1v-9.1h1V303z"/>
										<path class="st32" d="M961.2,303h-1v-9.1h1V303z"/>
										<path class="st32" d="M970.3,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H970.3z M968.2,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S967.8,302.3,968.2,302.3z"/>
										<path class="st32" d="M977.5,303v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H977.5z"/>
										<path class="st32" d="M984.8,302.2L984.8,302.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L984.8,302.2z M982.9,302.3c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S982.3,302.3,982.9,302.3z"/>
										<path class="st32" d="M993.7,303h-1v-7.7H990v-0.9h6.4v0.9h-2.7V303z"/>
										<path class="st32" d="M1000.3,303.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S1000.7,303.1,1000.3,303.1z M1000,297.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S1000.5,297.3,1000,297.3z"/>
										<path class="st32" d="M1006.6,303.1c-0.8,0-1.5-0.3-1.9-0.9s-0.7-1.4-0.7-2.4c0-1.1,0.2-1.9,0.7-2.5s1.1-0.9,2-0.9
											c0.9,0,1.6,0.3,2,1h0.1l0.1-0.9h0.8v9.3h-1v-2.7c0-0.4,0-0.7,0.1-1h-0.1C1008.1,302.8,1007.5,303.1,1006.6,303.1z M1006.7,302.3
											c0.6,0,1.1-0.2,1.4-0.5s0.5-0.9,0.5-1.8v-0.2c0-0.9-0.2-1.5-0.5-1.9s-0.8-0.6-1.4-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9
											s0.1,1.4,0.4,1.8S1006.1,302.3,1006.7,302.3z"/>
										<path class="st32" d="M1012.5,296.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H1012.5z"/>
										<path class="st32" d="M1018.9,294.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S1018.9,295.1,1018.9,294.9z M1020,303h-1v-6.4h1V303z"/>
										<path class="st32" d="M1023,303h-1v-9.1h1V303z"/>
										<path class="st32" d="M1029,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H1029z M1026.8,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S1026.4,302.3,1026.8,302.3z"/>
										<path class="st32" d="M1035,294.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V294.5z M1036,298.1h1.6c0.7,0,1.2-0.1,1.5-0.3s0.5-0.6,0.5-1.1
											c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V298.1z M1036,299v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H1036z"/>
										<path class="st32" d="M1046.6,303l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H1046.6z M1044.4,302.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S1044,302.3,1044.4,302.3z"/>
										<path class="st32" d="M1052.2,296.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S1051.8,296.5,1052.2,296.5z"/>
									</g>
								</g>
								<g id="dining_txt_17">
									<g>
										<path class="st32" d="M802.4,318h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V318z"/>
										<path class="st32" d="M806.8,318l3.6-7.7h-4.7v-0.9h5.7v0.8l-3.5,7.8H806.8z"/>
										<path class="st32" d="M812.9,317.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,317.7,812.9,317.4z"/>
										<path class="st32" d="M823.1,310.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C824.4,310.4,823.8,310.2,823.1,310.2z"/>
										<path class="st32" d="M832.4,314.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S832.4,313.8,832.4,314.8z M827.5,314.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S827.5,314,827.5,314.8z"/>
										<path class="st32" d="M835.1,318h-1v-9.1h1V318z"/>
										<path class="st32" d="M838.1,318h-1v-9.1h1V318z"/>
										<path class="st32" d="M842.9,318.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S843.3,318.1,842.9,318.1z M842.6,312.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S843.2,312.3,842.6,312.3z"/>
										<path class="st32" d="M849,317.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8H848v3.8c0,0.4,0.1,0.7,0.3,0.9S848.7,317.3,849,317.3z"
											/>
										<path class="st32" d="M853.2,317.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S852.9,317.3,853.2,317.3
											z"/>
										<path class="st32" d="M859.3,318l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H859.3z M857.2,317.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S856.8,317.3,857.2,317.3z"/>
									</g>
								</g>
								<g id="dining_txt_18">
									<g>
										<path class="st32" d="M802.4,333h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V333z"/>
										<path class="st32" d="M808.5,324.3c0.8,0,1.4,0.2,1.9,0.5s0.7,0.9,0.7,1.5c0,0.4-0.1,0.8-0.4,1.2s-0.7,0.7-1.3,0.9
											c0.7,0.3,1.2,0.7,1.5,1s0.4,0.8,0.4,1.3c0,0.7-0.2,1.3-0.7,1.7s-1.2,0.6-2,0.6c-0.9,0-1.6-0.2-2.1-0.6s-0.7-1-0.7-1.7
											c0-1,0.6-1.7,1.8-2.3c-0.5-0.3-0.9-0.6-1.2-1s-0.4-0.7-0.4-1.2c0-0.6,0.2-1.1,0.7-1.5S807.8,324.3,808.5,324.3z M806.7,330.9
											c0,0.5,0.2,0.8,0.5,1.1s0.8,0.4,1.4,0.4c0.6,0,1-0.1,1.4-0.4s0.5-0.6,0.5-1.1c0-0.4-0.2-0.7-0.5-1s-0.8-0.6-1.6-0.9
											c-0.6,0.2-1,0.5-1.3,0.8S806.7,330.4,806.7,330.9z M808.5,325.1c-0.5,0-0.9,0.1-1.1,0.4s-0.4,0.5-0.4,0.9c0,0.4,0.1,0.7,0.3,0.9
											s0.7,0.5,1.3,0.8c0.6-0.2,1-0.5,1.2-0.8s0.3-0.6,0.3-0.9c0-0.4-0.1-0.7-0.4-0.9S809,325.1,808.5,325.1z"/>
										<path class="st32" d="M812.9,332.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,332.7,812.9,332.4z"/>
										<path class="st32" d="M825.6,333h-1.2l-3.1-4.2l-0.9,0.8v3.4h-1v-8.6h1v4.2l3.9-4.2h1.2l-3.4,3.7L825.6,333z"/>
										<path class="st32" d="M832.2,329.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S832.2,328.8,832.2,329.8z M827.3,329.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S827.3,329,827.3,329.8z"/>
										<path class="st32" d="M838.3,333v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H838.3z"/>
										<path class="st32" d="M845.2,333l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H845.2z M843,332.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S842.7,332.3,843,332.3z"/>
										<path class="st32" d="M855,328.5h2.9v4.2c-0.5,0.1-0.9,0.3-1.4,0.3s-1,0.1-1.6,0.1c-1.3,0-2.3-0.4-3-1.2s-1.1-1.9-1.1-3.2
											c0-0.9,0.2-1.7,0.5-2.3s0.9-1.2,1.5-1.5s1.5-0.5,2.4-0.5c0.9,0,1.8,0.2,2.6,0.5l-0.4,0.9c-0.8-0.3-1.5-0.5-2.2-0.5
											c-1,0-1.9,0.3-2.4,0.9s-0.9,1.5-0.9,2.6c0,1.2,0.3,2,0.8,2.6s1.4,0.9,2.5,0.9c0.6,0,1.2-0.1,1.7-0.2v-2.6H855V328.5z"/>
										<path class="st32" d="M862.7,326.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S862.3,326.5,862.7,326.5z"/>
										<path class="st32" d="M864.6,324.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S864.6,325.1,864.6,324.9z M865.7,333h-1v-6.4h1V333z"/>
										<path class="st32" d="M868.7,333h-1v-9.1h1V333z"/>
										<path class="st32" d="M871.8,333h-1v-9.1h1V333z"/>
									</g>
								</g>
								<g id="dining_txt_19">
									<g>
										<path class="st32" d="M802.4,348h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V348z"/>
										<path class="st32" d="M811.3,343.1c0,3.4-1.3,5-3.9,5c-0.5,0-0.8,0-1.1-0.1v-0.8c0.3,0.1,0.7,0.2,1.1,0.2c0.9,0,1.6-0.3,2.1-0.9
											s0.7-1.5,0.8-2.7h-0.1c-0.2,0.3-0.5,0.6-0.9,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.7s-0.7-1.1-0.7-1.9
											c0-0.9,0.2-1.6,0.7-2.1s1.2-0.8,2-0.8c0.6,0,1.1,0.1,1.5,0.4s0.8,0.7,1,1.3S811.3,342.3,811.3,343.1z M808.4,340.2
											c-0.6,0-1,0.2-1.3,0.5s-0.5,0.9-0.5,1.5c0,0.6,0.1,1,0.4,1.3s0.7,0.5,1.3,0.5c0.4,0,0.7-0.1,1-0.2s0.5-0.3,0.7-0.6
											s0.3-0.5,0.3-0.8c0-0.4-0.1-0.8-0.2-1.1s-0.4-0.6-0.7-0.8S808.8,340.2,808.4,340.2z"/>
										<path class="st32" d="M812.9,347.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,347.7,812.9,347.4z"/>
										<path class="st32" d="M823.2,348l-2.9-7.6h0c0.1,0.6,0.1,1.3,0.1,2.1v5.4h-0.9v-8.6h1.5l2.7,7.1h0l2.7-7.1h1.5v8.6h-1v-5.5
											c0-0.6,0-1.3,0.1-2.1h0L824,348H823.2z"/>
										<path class="st32" d="M834.1,348l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H834.1z M831.9,347.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S831.5,347.3,831.9,347.3z"/>
										<path class="st32" d="M839.7,341.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S839.3,341.5,839.7,341.5z"/>
										<path class="st32" d="M842.7,348h-1v-9.1h1V348z"/>
										<path class="st32" d="M850.3,344.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S850.3,343.8,850.3,344.8z M845.4,344.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S845.4,344,845.4,344.8z"/>
										<path class="st32" d="M857.2,348l-1.2-3.8c-0.1-0.2-0.2-0.8-0.4-1.6h0c-0.2,0.7-0.3,1.2-0.4,1.6L854,348h-1.1l-1.8-6.4h1
											c0.4,1.6,0.7,2.8,0.9,3.7s0.3,1.4,0.4,1.7h0c0-0.2,0.1-0.5,0.2-0.9s0.2-0.6,0.2-0.8l1.2-3.7h1.1l1.1,3.7
											c0.2,0.7,0.4,1.2,0.4,1.7h0c0-0.1,0.1-0.4,0.1-0.7s0.5-1.9,1.2-4.7h1l-1.8,6.4H857.2z"/>
										<path class="st32" d="M862.2,339.5l-0.2,3.1h-0.6l-0.2-3.1H862.2z"/>
										<path class="st32" d="M868.1,346.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S868.1,345.8,868.1,346.3z"/>
										<path class="st32" d="M875.6,348h-1v-7.7h-2.7v-0.9h6.4v0.9h-2.7V348z"/>
										<path class="st32" d="M883.4,348l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H883.4z M881.2,347.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S880.8,347.3,881.2,347.3z"/>
										<path class="st32" d="M887.5,348l-2.4-6.4h1l1.4,3.8c0.3,0.9,0.5,1.5,0.6,1.7h0c0-0.2,0.2-0.6,0.4-1.3s0.7-2.1,1.5-4.3h1
											l-2.4,6.4H887.5z"/>
										<path class="st32" d="M894.9,348.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S895.3,348.1,894.9,348.1z M894.6,342.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S895.1,342.3,894.6,342.3z"/>
										<path class="st32" d="M901.8,341.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S901.4,341.5,901.8,341.5z"/>
										<path class="st32" d="M908.2,348v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H908.2z"/>
									</g>
								</g>
								<g id="dining_txt_20">
									<g>
										<path class="st32" d="M804.5,363h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V363z"/>
										<path class="st32" d="M811.4,358.7c0,1.5-0.2,2.6-0.7,3.3s-1.2,1.1-2.1,1.1c-0.9,0-1.6-0.4-2.1-1.1s-0.7-1.8-0.7-3.3
											c0-1.5,0.2-2.6,0.7-3.3s1.2-1.1,2.1-1.1c0.9,0,1.6,0.4,2.1,1.1S811.4,357.3,811.4,358.7z M806.7,358.7c0,1.2,0.1,2.2,0.4,2.7
											s0.8,0.9,1.4,0.9c0.6,0,1.1-0.3,1.4-0.9s0.4-1.5,0.4-2.7s-0.1-2.1-0.4-2.7s-0.8-0.9-1.4-0.9c-0.6,0-1.1,0.3-1.4,0.8
											S806.7,357.5,806.7,358.7z"/>
										<path class="st32" d="M812.9,362.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,362.7,812.9,362.4z"/>
										<path class="st32" d="M824.9,356.9c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2
											C823.8,354.5,824.9,355.3,824.9,356.9z M820.4,358.8h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3
											s-0.9-0.4-1.7-0.4h-1.1V358.8z"/>
										<path class="st32" d="M826.4,354.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S826.4,355.1,826.4,354.9z M827.5,363h-1v-6.4h1V363z"/>
										<path class="st32" d="M834,363v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1v-6.4
											h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H834z"/>
										<path class="st32" d="M837.9,359.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C837.9,358.9,837.9,359.3,837.9,359.7L837.9,359.7z"/>
										<path class="st32" d="M846.2,356.5c0.8,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4s-0.2,1.9-0.7,2.5s-1.1,0.9-2,0.9
											c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1l-0.2,0.8h-0.7v-9.1h1v2.2c0,0.5,0,0.9,0,1.3h0
											C844.7,356.8,845.3,356.5,846.2,356.5z M846.1,357.3c-0.7,0-1.1,0.2-1.4,0.6s-0.4,1-0.4,1.9s0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.4-1.1,0.4-1.9c0-0.8-0.1-1.5-0.4-1.9S846.7,357.3,846.1,357.3z"/>
										<path class="st32" d="M853.3,363.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S853.7,363.1,853.3,363.1z M853,357.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S853.5,357.3,853,357.3z"/>
										<path class="st32" d="M860.2,356.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S859.8,356.5,860.2,356.5z"/>
										<path class="st32" d="M865.1,356.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S864.7,356.5,865.1,356.5z"/>
										<path class="st32" d="M866.1,356.6h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1V365c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L866.1,356.6z"/>
									</g>
								</g>
								<g id="dining_txt_21">
									<g>
										<path class="st32" d="M804.5,378h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V378z"/>
										<path class="st32" d="M809.3,378h-0.9v-6.1c0-0.5,0-1,0-1.4c-0.1,0.1-0.2,0.2-0.3,0.3s-0.6,0.5-1.4,1.1l-0.5-0.7l2.3-1.8h0.8
											V378z"/>
										<path class="st32" d="M812.9,377.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,377.7,812.9,377.4z"/>
										<path class="st32" d="M826.9,373.7c0,1.4-0.3,2.4-1,3.2s-1.7,1.2-2.9,1.2c-1.3,0-2.2-0.4-2.9-1.2s-1-1.9-1-3.3
											c0-1.4,0.3-2.5,1-3.2s1.7-1.2,2.9-1.2c1.2,0,2.2,0.4,2.9,1.2S826.9,372.3,826.9,373.7z M820.1,373.7c0,1.2,0.2,2,0.7,2.6
											s1.2,0.9,2.2,0.9c0.9,0,1.7-0.3,2.2-0.9s0.7-1.5,0.7-2.6c0-1.2-0.2-2-0.7-2.6s-1.2-0.9-2.1-0.9c-0.9,0-1.7,0.3-2.2,0.9
											S820.1,372.6,820.1,373.7z"/>
										<path class="st32" d="M832.6,378l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H832.6z M830.4,377.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S830,377.3,830.4,377.3z"/>
										<path class="st32" d="M836.3,374.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C836.3,373.9,836.3,374.3,836.3,374.7L836.3,374.7z"/>
										<path class="st32" d="M849.7,375.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S849.7,375.2,849.7,375.7z"/>
										<path class="st32" d="M853.4,377.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S853.1,377.3,853.4,377.3
											z"/>
										<path class="st32" d="M858.3,378.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S858.7,378.1,858.3,378.1z M858,372.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S858.5,372.3,858,372.3z"/>
										<path class="st32" d="M866.2,378l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H866.2z M864,377.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S863.7,377.3,864,377.3z"/>
										<path class="st32" d="M869.9,374.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C869.9,373.9,869.9,374.3,869.9,374.7L869.9,374.7z"/>
										<path class="st32" d="M879.7,378v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H879.7z"/>
										<path class="st32" d="M888.2,374.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S888.2,373.8,888.2,374.8z M883.3,374.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S883.3,374,883.3,374.8z"/>
										<path class="st32" d="M890.8,371.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H890.8z"/>
										<path class="st32" d="M901.4,376.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S901.4,375.8,901.4,376.3z"/>
										<path class="st32" d="M905.7,378.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S906.1,378.1,905.7,378.1z M905.4,372.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S905.9,372.3,905.4,372.3z"/>
									</g>
								</g>
								<g id="dining_txt_22">
									<g>
										<path class="st32" d="M804.5,393h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V393z"/>
										<path class="st32" d="M811.3,393h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V393z"/>
										<path class="st32" d="M812.9,392.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,392.7,812.9,392.4z"/>
										<path class="st32" d="M820.4,389.5v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H820.4z M820.4,388.6h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V388.6z"/>
										<path class="st32" d="M827.6,386.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9H831c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H827.6z"/>
										<path class="st32" d="M836.2,392.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S835.8,392.3,836.2,392.3
											z"/>
										<path class="st32" d="M842.7,393v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H842.7z"/>
										<path class="st32" d="M846.5,384.5l0.1,0.1c-0.1,0.4-0.2,0.8-0.4,1.4s-0.4,1-0.6,1.4h-0.7c0.3-1.1,0.5-2.1,0.6-2.9H846.5z"/>
										<path class="st32" d="M851.9,391.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S851.9,390.8,851.9,391.3z"/>
										<path class="st32" d="M860.4,385.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C861.7,385.4,861,385.2,860.4,385.2z"/>
										<path class="st32" d="M868.5,393v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H868.5z"/>
										<path class="st32" d="M874.4,386.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S874,386.5,874.4,386.5z"/>
										<path class="st32" d="M876.3,384.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S876.3,385.1,876.3,384.9z M877.4,393h-1v-6.4h1V393z"/>
										<path class="st32" d="M883.6,391.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S883.6,390.8,883.6,391.3z"/>
										<path class="st32" d="M893.3,390.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S893.3,390.2,893.3,390.7z"/>
										<path class="st32" d="M896.9,392.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8H894v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S896.6,392.3,896.9,392.3z
											"/>
										<path class="st32" d="M901.8,393.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S902.3,393.1,901.8,393.1z M901.6,387.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S902.1,387.3,901.6,387.3z"/>
										<path class="st32" d="M909.8,393l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H909.8z M907.6,392.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S907.2,392.3,907.6,392.3z"/>
										<path class="st32" d="M913.5,389.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C913.5,388.9,913.5,389.3,913.5,389.7L913.5,389.7z"/>
										<path class="st32" d="M923.2,393v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H923.2z"/>
										<path class="st32" d="M931.7,389.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S931.7,388.8,931.7,389.8z M926.8,389.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S926.8,389,926.8,389.8z"/>
										<path class="st32" d="M934.3,386.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H934.3z"/>
										<path class="st32" d="M944.9,391.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S944.9,390.8,944.9,391.3z"/>
										<path class="st32" d="M949.2,393.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S949.7,393.1,949.2,393.1z M949,387.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S949.5,387.3,949,387.3z"/>
									</g>
								</g>
								<g id="dining_txt_23">
									<g>
										<path class="st32" d="M804.5,408h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V408z"/>
										<path class="st32" d="M811,401.5c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6H807v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S811,400.8,811,401.5z"/>
										<path class="st32" d="M812.9,407.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,407.7,812.9,407.4z"/>
										<path class="st32" d="M824.3,405.7c0,0.8-0.3,1.3-0.8,1.8s-1.3,0.6-2.2,0.6c-1,0-1.8-0.1-2.3-0.4v-1c0.4,0.1,0.7,0.3,1.1,0.4
											s0.8,0.1,1.2,0.1c0.7,0,1.2-0.1,1.5-0.4s0.5-0.6,0.5-1.1c0-0.3-0.1-0.5-0.2-0.7s-0.3-0.4-0.6-0.5s-0.7-0.3-1.3-0.5
											c-0.8-0.3-1.4-0.6-1.7-1s-0.5-0.9-0.5-1.5c0-0.7,0.2-1.2,0.7-1.6s1.2-0.6,2-0.6c0.9,0,1.6,0.2,2.3,0.5l-0.3,0.9
											c-0.7-0.3-1.4-0.4-2.1-0.4c-0.5,0-0.9,0.1-1.2,0.3s-0.4,0.5-0.4,0.9c0,0.3,0.1,0.5,0.2,0.7s0.3,0.4,0.6,0.5s0.7,0.3,1.2,0.5
											c0.9,0.3,1.5,0.7,1.9,1S824.3,405.2,824.3,405.7z"/>
										<path class="st32" d="M829.8,408l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H829.8z M827.6,407.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S827.3,407.3,827.6,407.3z"/>
										<path class="st32" d="M837.8,401.6v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H837.8z M832.7,409.1c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S832.7,408.7,832.7,409.1z M833.2,403.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S833.2,403.2,833.2,403.7z"/>
										<path class="st32" d="M841.9,408.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S842.3,408.1,841.9,408.1z M841.6,402.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S842.1,402.3,841.6,402.3z"/>
										<path class="st32" d="M856.6,408h-1l-1.7-5.7c-0.1-0.3-0.2-0.6-0.3-1s-0.2-0.6-0.2-0.7c-0.1,0.5-0.2,1.1-0.4,1.7l-1.7,5.7h-1
											l-2.3-8.6h1.1l1.4,5.3c0.2,0.7,0.3,1.4,0.4,2c0.1-0.7,0.3-1.4,0.5-2.1l1.5-5.2h1.1l1.6,5.3c0.2,0.6,0.3,1.3,0.5,2.1
											c0.1-0.6,0.2-1.2,0.4-2l1.3-5.3h1.1L856.6,408z"/>
										<path class="st32" d="M865.6,404.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S865.6,403.8,865.6,404.8z M860.7,404.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S860.7,404,860.7,404.8z"/>
										<path class="st32" d="M872.9,404.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S872.9,403.8,872.9,404.8z M868,404.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S868,404,868,404.8z"/>
										<path class="st32" d="M879,407.2L879,407.2c-0.5,0.7-1.2,1-2.1,1c-0.8,0-1.5-0.3-2-0.9s-0.7-1.4-0.7-2.4s0.2-1.9,0.7-2.5
											s1.1-0.9,2-0.9c0.9,0,1.5,0.3,2,0.9h0.1l0-0.5l0-0.5v-2.6h1v9.1h-0.8L879,407.2z M877,407.3c0.7,0,1.1-0.2,1.4-0.5
											s0.4-0.9,0.4-1.7v-0.2c0-0.9-0.2-1.6-0.5-1.9s-0.8-0.6-1.5-0.6c-0.6,0-1,0.2-1.3,0.7s-0.5,1.1-0.5,1.9c0,0.8,0.2,1.4,0.5,1.9
											S876.4,407.3,877,407.3z"/>
										<path class="st32" d="M884.8,402.4h-1.6v5.7h-1v-5.7h-1.1v-0.4l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2
											l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4s-0.3,0.6-0.3,1.2v0.4h1.6V402.4z M885.9,399.9c0-0.2,0.1-0.4,0.2-0.5
											s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2
											S885.9,400.1,885.9,399.9z M887,408h-1v-6.4h1V408z"/>
										<path class="st32" d="M892,401.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6s-0.6,0.9-0.6,1.6
											v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S891.6,401.5,892,401.5z"/>
										<path class="st32" d="M896.7,408.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S897.1,408.1,896.7,408.1z M896.4,402.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S896.9,402.3,896.4,402.3z"/>
										<path class="st32" d="M906.6,408h-1v-7.7h-2.7v-0.9h6.4v0.9h-2.7V408z"/>
										<path class="st32" d="M914.4,408l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H914.4z M912.2,407.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S911.8,407.3,912.2,407.3z"/>
										<path class="st32" d="M918.5,408l-2.4-6.4h1l1.4,3.8c0.3,0.9,0.5,1.5,0.6,1.7h0c0-0.2,0.2-0.6,0.4-1.3s0.7-2.1,1.5-4.3h1
											l-2.4,6.4H918.5z"/>
										<path class="st32" d="M925.8,408.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S926.3,408.1,925.8,408.1z M925.6,402.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S926.1,402.3,925.6,402.3z"/>
										<path class="st32" d="M932.8,401.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S932.4,401.5,932.8,401.5z"/>
										<path class="st32" d="M939.1,408v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H939.1z"/>
										<path class="st32" d="M950.8,408l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H950.8z M949.4,404.4l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H949.4z"/>
										<path class="st32" d="M953.8,408h-1v-9.1h1V408z"/>
										<path class="st32" d="M958.9,408.1c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S959.7,408.1,958.9,408.1z M958.7,402.3c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S959.3,402.3,958.7,402.3z"/>
										<path class="st32" d="M967.6,408v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H967.6z"/>
										<path class="st32" d="M974.5,408l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H974.5z M972.3,407.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S972,407.3,972.3,407.3z"/>
										<path class="st32" d="M980.2,401.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S979.8,401.5,980.2,401.5z"/>
										<path class="st32" d="M984.9,408.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S985.3,408.1,984.9,408.1z M984.6,402.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S985.1,402.3,984.6,402.3z"/>
										<path class="st32" d="M991,407.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8H988v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S990.6,407.3,991,407.3z"
											/>
										<path class="st32" d="M995.2,407.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S994.9,407.3,995.2,407.3
											z"/>
										<path class="st32" d="M1001.3,408l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H1001.3z M999.1,407.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S998.7,407.3,999.1,407.3z"/>
									</g>
								</g>
								<g id="dining_txt_24">
									<g>
										<path class="st32" d="M804.5,423h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V423z"/>
										<path class="st32" d="M811.7,421h-1.3v2h-0.9v-2h-4.2v-0.8l4.1-5.8h1v5.8h1.3V421z M809.5,420.2v-2.8c0-0.6,0-1.2,0.1-1.9h0
											c-0.2,0.4-0.4,0.7-0.5,0.9l-2.7,3.8H809.5z"/>
										<path class="st32" d="M812.9,422.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,422.7,812.9,422.4z"/>
										<path class="st32" d="M823.1,415.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C824.4,415.4,823.8,415.2,823.1,415.2z"/>
										<path class="st32" d="M830.8,423l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H830.8z M828.6,422.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S828.2,422.3,828.6,422.3z"/>
										<path class="st32" d="M834.5,423h-1v-9.1h1V423z"/>
										<path class="st32" d="M836.5,414.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S836.5,415.1,836.5,414.9z M837.6,423h-1v-6.4h1V423z"/>
										<path class="st32" d="M842.5,417.4h-1.6v5.7h-1v-5.7h-1.1v-0.4l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2
											l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4s-0.3,0.6-0.3,1.2v0.4h1.6V417.4z"/>
										<path class="st32" d="M849.2,419.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S849.2,418.8,849.2,419.8z M844.3,419.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S844.3,419,844.3,419.8z"/>
										<path class="st32" d="M853.9,416.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S853.5,416.5,853.9,416.5z"/>
										<path class="st32" d="M860.2,423v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H860.2z"/>
										<path class="st32" d="M863.1,414.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S863.1,415.1,863.1,414.9z M864.2,423h-1v-6.4h1V423z"/>
										<path class="st32" d="M870.2,423l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H870.2z M868,422.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S867.6,422.3,868,422.3z"/>
										<path class="st32" d="M881.6,416.9c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2
											C880.5,414.5,881.6,415.3,881.6,416.9z M877.2,418.8h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3
											s-0.9-0.4-1.7-0.4h-1.1V418.8z"/>
										<path class="st32" d="M883.2,414.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S883.2,415.1,883.2,414.9z M884.2,423h-1v-6.4h1V423z"/>
										<path class="st32" d="M890.4,423h-4.7v-0.7l3.5-5h-3.3v-0.8h4.4v0.8l-3.5,4.9h3.5V423z"/>
										<path class="st32" d="M896,423h-4.7v-0.7l3.5-5h-3.3v-0.8h4.4v0.8l-3.5,4.9h3.5V423z"/>
										<path class="st32" d="M901.5,423l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H901.5z M899.3,422.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S898.9,422.3,899.3,422.3z"/>
										<path class="st32" d="M913.6,423h-1.2l-3.1-4.2l-0.9,0.8v3.4h-1v-8.6h1v4.2l3.9-4.2h1.2l-3.4,3.7L913.6,423z"/>
										<path class="st32" d="M914.6,414.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S914.6,415.1,914.6,414.9z M915.6,423h-1v-6.4h1V423z"/>
										<path class="st32" d="M919.8,422.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S919.5,422.3,919.8,422.3
											z"/>
										<path class="st32" d="M924.5,423.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C925.7,423,925.2,423.1,924.5,423.1z"/>
										<path class="st32" d="M932,423v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1v-9.1
											h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H932z"/>
										<path class="st32" d="M937.7,423.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S938.2,423.1,937.7,423.1z M937.5,417.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S938,417.3,937.5,417.3z"/>
										<path class="st32" d="M946.1,423v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H946.1z"/>
									</g>
								</g>
								<g id="dining_txt_25">
									<g>
										<path class="st32" d="M804.5,438h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V438z"/>
										<path class="st32" d="M808.4,432.8c0.9,0,1.6,0.2,2.1,0.7s0.8,1.1,0.8,1.8c0,0.9-0.3,1.6-0.8,2.1s-1.3,0.8-2.3,0.8
											c-1,0-1.7-0.2-2.2-0.5v-0.9c0.3,0.2,0.6,0.3,1,0.4s0.8,0.1,1.2,0.1c0.7,0,1.2-0.2,1.6-0.5s0.6-0.8,0.6-1.4
											c0-1.2-0.7-1.8-2.2-1.8c-0.4,0-0.9,0.1-1.5,0.2l-0.5-0.3l0.3-4h4.3v0.9h-3.4l-0.2,2.6C807.5,432.8,807.9,432.8,808.4,432.8z"/>
										<path class="st32" d="M812.9,437.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,437.7,812.9,437.4z"/>
										<path class="st32" d="M822.1,438h-1v-7.7h-2.7v-0.9h6.4v0.9h-2.7V438z"/>
										<path class="st32" d="M830.3,438v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H830.3z"/>
										<path class="st32" d="M836,438.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6H834c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S836.5,438.1,836,438.1z M835.8,432.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S836.3,432.3,835.8,432.3z"/>
										<path class="st32" d="M847,430.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C848.3,430.4,847.6,430.2,847,430.2z"/>
										<path class="st32" d="M855.1,438v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H855.1z"/>
										<path class="st32" d="M860.8,438.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S861.2,438.1,860.8,438.1z M860.5,432.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S861.1,432.3,860.5,432.3z"/>
										<path class="st32" d="M867.5,438.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S868,438.1,867.5,438.1z M867.3,432.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S867.8,432.3,867.3,432.3z"/>
										<path class="st32" d="M875.7,436.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S875.7,435.8,875.7,436.3z"/>
										<path class="st32" d="M880,438.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S880.4,438.1,880,438.1z M879.7,432.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S880.2,432.3,879.7,432.3z"/>
										<path class="st32" d="M886.6,438.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C887.8,438,887.2,438.1,886.6,438.1z"/>
										<path class="st32" d="M893.7,438l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L890,432c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H893.7z M891.5,437.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S891.1,437.3,891.5,437.3z"/>
										<path class="st32" d="M897.4,434.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C897.4,433.9,897.3,434.3,897.4,434.7L897.4,434.7z"/>
										<path class="st32" d="M905.4,438.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S905.8,438.1,905.4,438.1z M905.2,432.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S905.7,432.3,905.2,432.3z"/>
										<path class="st32" d="M913.7,438h-1v-8.6h4.8v0.9h-3.8v3.1h3.6v0.9h-3.6V438z"/>
										<path class="st32" d="M922.7,438l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L919,432c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H922.7z M920.5,437.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S920.1,437.3,920.5,437.3z"/>
										<path class="st32" d="M928,438.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C929.2,438,928.6,438.1,928,438.1z"/>
										<path class="st32" d="M933.2,437.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S932.9,437.3,933.2,437.3
											z"/>
										<path class="st32" d="M940.9,434.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S940.9,433.8,940.9,434.8z M936,434.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S936,434,936,434.8z"/>
										<path class="st32" d="M945.5,431.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S945.1,431.5,945.5,431.5z"/>
										<path class="st32" d="M946.5,431.6h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1V440c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L946.5,431.6z"/>
									</g>
								</g>
								<g id="dining_txt_26">
									<g>
										<path class="st32" d="M804.5,453h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V453z"/>
										<path class="st32" d="M805.8,449.4c0-1.7,0.3-2.9,1-3.8s1.6-1.3,2.9-1.3c0.4,0,0.8,0,1,0.1v0.8c-0.3-0.1-0.6-0.1-1-0.1
											c-0.9,0-1.6,0.3-2.1,0.9s-0.8,1.5-0.8,2.7h0.1c0.4-0.7,1.1-1,2-1c0.8,0,1.4,0.2,1.8,0.7s0.7,1.1,0.7,1.9c0,0.9-0.2,1.6-0.7,2.1
											s-1.1,0.8-2,0.8c-0.9,0-1.6-0.3-2.1-1S805.8,450.5,805.8,449.4z M808.7,452.3c0.6,0,1-0.2,1.3-0.5s0.5-0.9,0.5-1.5
											c0-0.6-0.1-1-0.4-1.3s-0.7-0.5-1.3-0.5c-0.4,0-0.7,0.1-1,0.2s-0.5,0.3-0.7,0.6s-0.3,0.5-0.3,0.8c0,0.4,0.1,0.8,0.2,1.1
											s0.4,0.6,0.7,0.8S808.3,452.3,808.7,452.3z"/>
										<path class="st32" d="M812.9,452.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,452.7,812.9,452.4z"/>
										<path class="st32" d="M820.4,449.5v3.6h-1v-8.6h2.3c1.1,0,1.8,0.2,2.3,0.6s0.8,1,0.8,1.8c0,1.1-0.6,1.9-1.7,2.3l2.3,3.8h-1.2
											l-2.1-3.6H820.4z M820.4,448.6h1.4c0.7,0,1.2-0.1,1.5-0.4s0.5-0.7,0.5-1.3c0-0.6-0.2-1-0.5-1.2s-0.9-0.4-1.6-0.4h-1.3V448.6z"/>
										<path class="st32" d="M830.7,453l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L827,447c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H830.7z M828.5,452.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S828.1,452.3,828.5,452.3z"/>
										<path class="st32" d="M832.4,446.6h1l1.4,3.7c0.3,0.8,0.5,1.4,0.6,1.8h0c0.1-0.2,0.2-0.5,0.3-1s0.7-2,1.6-4.5h1l-2.8,7.3
											c-0.3,0.7-0.6,1.2-1,1.5s-0.8,0.5-1.3,0.5c-0.3,0-0.6,0-0.9-0.1V455c0.2,0,0.5,0.1,0.7,0.1c0.7,0,1.1-0.4,1.4-1.1l0.4-0.9
											L832.4,446.6z"/>
										<path class="st32" d="M840.2,444.5l0.1,0.1c-0.1,0.4-0.2,0.8-0.4,1.4s-0.4,1-0.6,1.4h-0.7c0.3-1.1,0.5-2.1,0.6-2.9H840.2z"/>
										<path class="st32" d="M845.6,451.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S845.6,450.8,845.6,451.3z"/>
										<path class="st32" d="M854.3,453l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H854.3z M852.1,452.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S851.7,452.3,852.1,452.3z"/>
										<path class="st32" d="M859.1,452.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8H858v3.8c0,0.4,0.1,0.7,0.3,0.9S858.8,452.3,859.1,452.3z
											"/>
										<path class="st32" d="M870.7,453h-1.2l-3.1-4.2l-0.9,0.8v3.4h-1v-8.6h1v4.2l3.9-4.2h1.2l-3.4,3.7L870.7,453z"/>
										<path class="st32" d="M871.6,444.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S871.6,445.1,871.6,444.9z M872.7,453h-1v-6.4h1V453z"/>
										<path class="st32" d="M875.7,453h-1v-9.1h1V453z"/>
										<path class="st32" d="M878.8,453h-1v-9.1h1V453z"/>
										<path class="st32" d="M883.5,453.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S884,453.1,883.5,453.1z M883.3,447.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S883.8,447.3,883.3,447.3z"/>
										<path class="st32" d="M890.5,446.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S890.1,446.5,890.5,446.5z"/>
										<path class="st32" d="M899.4,445.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C900.7,445.4,900,445.2,899.4,445.2z"/>
										<path class="st32" d="M906.1,446.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S905.7,446.5,906.1,446.5z"/>
										<path class="st32" d="M910.8,453.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S911.2,453.1,910.8,453.1z M910.5,447.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S911,447.3,910.5,447.3z"/>
										<path class="st32" d="M917.5,453.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S917.9,453.1,917.5,453.1z M917.2,447.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S917.7,447.3,917.2,447.3z"/>
										<path class="st32" d="M922.5,449.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C922.5,448.9,922.4,449.3,922.5,449.7L922.5,449.7z"/>
									</g>
								</g>
								<g id="dining_txt_27">
									<g>
										<path class="st32" d="M804.5,468h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V468z"/>
										<path class="st32" d="M806.8,468l3.6-7.7h-4.7v-0.9h5.7v0.8l-3.5,7.8H806.8z"/>
										<path class="st32" d="M812.9,467.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,467.7,812.9,467.4z"/>
										<path class="st32" d="M824.9,461.9c0,0.9-0.3,1.5-0.9,2s-1.4,0.7-2.5,0.7h-1v3.4h-1v-8.6h2.2
											C823.8,459.5,824.9,460.3,824.9,461.9z M820.4,463.8h0.9c0.9,0,1.5-0.1,1.9-0.4s0.6-0.7,0.6-1.4c0-0.6-0.2-1-0.6-1.3
											s-0.9-0.4-1.7-0.4h-1.1V463.8z"/>
										<path class="st32" d="M826.4,467.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S826.4,467.7,826.4,467.4z"/>
										<path class="st32" d="M830.9,468h-1v-8.6h4.8v0.9h-3.8v3.1h3.6v0.9h-3.6V468z"/>
										<path class="st32" d="M835.8,467.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S835.8,467.7,835.8,467.4z"/>
										<path class="st32" d="M846,460.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6s1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C847.3,460.4,846.7,460.2,846,460.2z"/>
										<path class="st32" d="M854.2,468v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H854.2z"/>
										<path class="st32" d="M861.1,468l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H861.1z M858.9,467.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S858.5,467.3,858.9,467.3z"/>
										<path class="st32" d="M868.2,468v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H868.2z"/>
										<path class="st32" d="M876.5,461.6v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H876.5z M871.3,469.1c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S871.3,468.7,871.3,469.1z M871.9,463.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S871.9,463.2,871.9,463.7z"/>
										<path class="st32" d="M878.6,459.5l-0.2,3.1h-0.6l-0.2-3.1H878.6z"/>
										<path class="st32" d="M884.6,466.3c0,0.6-0.2,1.1-0.7,1.4s-1.1,0.5-1.9,0.5c-0.9,0-1.5-0.1-2-0.4v-0.9c0.3,0.2,0.6,0.3,1,0.4
											s0.7,0.1,1,0.1c0.5,0,0.9-0.1,1.2-0.2s0.4-0.4,0.4-0.7c0-0.2-0.1-0.5-0.3-0.6s-0.6-0.4-1.3-0.6c-0.6-0.2-1-0.4-1.3-0.6
											s-0.4-0.4-0.6-0.6s-0.2-0.5-0.2-0.8c0-0.5,0.2-0.9,0.6-1.2s1-0.5,1.8-0.5c0.7,0,1.4,0.1,2,0.4l-0.3,0.8
											c-0.6-0.3-1.2-0.4-1.8-0.4c-0.5,0-0.8,0.1-1,0.2s-0.4,0.3-0.4,0.6c0,0.2,0,0.3,0.1,0.4s0.2,0.2,0.4,0.3s0.6,0.3,1.1,0.5
											c0.8,0.3,1.3,0.6,1.5,0.8S884.6,465.8,884.6,466.3z"/>
									</g>
								</g>
								<g id="dining_txt_28">
									<g>
										<path class="st32" d="M804.5,483h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V483z"/>
										<path class="st32" d="M808.5,474.3c0.8,0,1.4,0.2,1.9,0.5s0.7,0.9,0.7,1.5c0,0.4-0.1,0.8-0.4,1.2s-0.7,0.7-1.3,0.9
											c0.7,0.3,1.2,0.7,1.5,1s0.4,0.8,0.4,1.3c0,0.7-0.2,1.3-0.7,1.7s-1.2,0.6-2,0.6c-0.9,0-1.6-0.2-2.1-0.6s-0.7-1-0.7-1.7
											c0-1,0.6-1.7,1.8-2.3c-0.5-0.3-0.9-0.6-1.2-1s-0.4-0.7-0.4-1.2c0-0.6,0.2-1.1,0.7-1.5S807.8,474.3,808.5,474.3z M806.7,480.9
											c0,0.5,0.2,0.8,0.5,1.1s0.8,0.4,1.4,0.4c0.6,0,1-0.1,1.4-0.4s0.5-0.6,0.5-1.1c0-0.4-0.2-0.7-0.5-1s-0.8-0.6-1.6-0.9
											c-0.6,0.2-1,0.5-1.3,0.8S806.7,480.4,806.7,480.9z M808.5,475.1c-0.5,0-0.9,0.1-1.1,0.4s-0.4,0.5-0.4,0.9c0,0.4,0.1,0.7,0.3,0.9
											s0.7,0.5,1.3,0.8c0.6-0.2,1-0.5,1.2-0.8s0.3-0.6,0.3-0.9c0-0.4-0.1-0.7-0.4-0.9S809,475.1,808.5,475.1z"/>
										<path class="st32" d="M812.9,482.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,482.7,812.9,482.4z"/>
										<path class="st32" d="M819.5,474.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V474.5z M820.4,478.1h1.6c0.7,0,1.2-0.1,1.5-0.3
											s0.5-0.6,0.5-1.1c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V478.1z M820.4,479v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H820.4z"/>
										<path class="st32" d="M828,476.6v4.2c0,0.5,0.1,0.9,0.4,1.2s0.6,0.4,1.1,0.4c0.7,0,1.2-0.2,1.5-0.6s0.5-1,0.5-1.8v-3.4h1v6.4
											h-0.8l-0.1-0.9h-0.1c-0.2,0.3-0.5,0.6-0.8,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.6s-0.6-1-0.6-1.8v-4.2H828z"/>
										<path class="st32" d="M837,483.1c-0.9,0-1.6-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1.1,0.3-1.9,0.8-2.5s1.3-0.9,2.2-0.9
											c0.3,0,0.6,0,0.9,0.1s0.6,0.1,0.7,0.2l-0.3,0.8c-0.2-0.1-0.4-0.2-0.7-0.2s-0.5-0.1-0.7-0.1c-1.3,0-2,0.8-2,2.5
											c0,0.8,0.2,1.4,0.5,1.8s0.8,0.6,1.4,0.6c0.5,0,1.1-0.1,1.6-0.3v0.9C838.2,483,837.7,483.1,837,483.1z"/>
										<path class="st32" d="M844.1,483l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H844.1z M841.9,482.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S841.5,482.3,841.9,482.3z"/>
										<path class="st32" d="M856.9,478.7c0,1.4-0.4,2.5-1.2,3.2s-1.9,1.1-3.3,1.1h-2.4v-8.6h2.6c1.3,0,2.4,0.4,3.1,1.1
											S856.9,477.3,856.9,478.7z M855.9,478.7c0-1.1-0.3-2-0.8-2.5s-1.4-0.8-2.5-0.8h-1.4v6.8h1.2c1.2,0,2.1-0.3,2.7-0.9
											S855.9,479.8,855.9,478.7z"/>
										<path class="st32" d="M858.6,474.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S858.6,475.1,858.6,474.9z M859.7,483h-1v-6.4h1V483z"/>
										<path class="st32" d="M865,474.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V474.5z M866,478.1h1.6c0.7,0,1.2-0.1,1.5-0.3s0.5-0.6,0.5-1.1
											c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3H866V478.1z M866,479v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H866z"/>
										<path class="st32" d="M875.3,483.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S875.8,483.1,875.3,483.1z M875.1,477.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S875.6,477.3,875.1,477.3z"/>
										<path class="st32" d="M882.3,483.1c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S883.2,483.1,882.3,483.1z M882.2,477.3c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S882.8,477.3,882.2,477.3z"/>
										<path class="st32" d="M889.7,483.1c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8
											l0.1,0.9h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5
											S890.5,483.1,889.7,483.1z M889.6,477.3c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6
											c0.6,0,1-0.2,1.3-0.7s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S890.1,477.3,889.6,477.3z"/>
										<path class="st32" d="M899.6,479.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S899.6,478.8,899.6,479.8z M894.7,479.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S894.7,479,894.7,479.8z"/>
									</g>
								</g>
								<g id="dining_txt_29">
									<g>
										<path class="st32" d="M804.5,498h-5.6v-0.8l2.3-2.3c0.7-0.7,1.1-1.2,1.4-1.5s0.4-0.6,0.5-0.9s0.2-0.6,0.2-0.9
											c0-0.5-0.1-0.8-0.4-1.1s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.8-0.7,1.6-1,2.6-1
											c0.8,0,1.4,0.2,1.9,0.6s0.7,1,0.7,1.7c0,0.5-0.2,1.1-0.5,1.6s-0.9,1.2-1.7,2l-1.9,1.8v0h4.4V498z"/>
										<path class="st32" d="M811.3,493.1c0,3.4-1.3,5-3.9,5c-0.5,0-0.8,0-1.1-0.1v-0.8c0.3,0.1,0.7,0.2,1.1,0.2c0.9,0,1.6-0.3,2.1-0.9
											s0.7-1.5,0.8-2.7h-0.1c-0.2,0.3-0.5,0.6-0.9,0.7s-0.8,0.3-1.2,0.3c-0.8,0-1.4-0.2-1.8-0.7s-0.7-1.1-0.7-1.9
											c0-0.9,0.2-1.6,0.7-2.1s1.2-0.8,2-0.8c0.6,0,1.1,0.1,1.5,0.4s0.8,0.7,1,1.3S811.3,492.3,811.3,493.1z M808.4,490.2
											c-0.6,0-1,0.2-1.3,0.5s-0.5,0.9-0.5,1.5c0,0.6,0.1,1,0.4,1.3s0.7,0.5,1.3,0.5c0.4,0,0.7-0.1,1-0.2s0.5-0.3,0.7-0.6
											s0.3-0.5,0.3-0.8c0-0.4-0.1-0.8-0.2-1.1s-0.4-0.6-0.7-0.8S808.8,490.2,808.4,490.2z"/>
										<path class="st32" d="M812.9,497.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,497.7,812.9,497.4z"/>
										<path class="st32" d="M819.5,489.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V489.5z M820.4,493.1h1.6c0.7,0,1.2-0.1,1.5-0.3
											s0.5-0.6,0.5-1.1c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3h-1.5V493.1z M820.4,494v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H820.4z"/>
										<path class="st32" d="M829.8,498.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S830.2,498.1,829.8,498.1z M829.5,492.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S830,492.3,829.5,492.3z"/>
										<path class="st32" d="M838.2,498v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H838.2z"/>
										<path class="st32" d="M841.1,489.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S841.1,490.1,841.1,489.9z M842.2,498h-1v-6.4h1V498z"/>
										<path class="st32" d="M848.6,498v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H848.6z"/>
										<path class="st32" d="M855.5,498l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H855.5z M853.3,497.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S852.9,497.3,853.3,497.3z"/>
										<path class="st32" d="M862.6,498v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H862.6z"/>
										<path class="st32" d="M869.6,498l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H869.6z M867.4,497.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S867,497.3,867.4,497.3z"/>
										<path class="st32" d="M880.9,498l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H880.9z M879.6,494.4l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H879.6z"/>
										<path class="st32" d="M884,498h-1v-9.1h1V498z"/>
										<path class="st32" d="M889,498.1c-0.4,0-0.8-0.1-1.1-0.2s-0.6-0.4-0.9-0.7h-0.1c0,0.4,0.1,0.7,0.1,1.1v2.6h-1v-9.3h0.8l0.1,0.9
											h0c0.2-0.4,0.5-0.6,0.9-0.8s0.7-0.2,1.1-0.2c0.9,0,1.5,0.3,2,0.9s0.7,1.4,0.7,2.4c0,1.1-0.2,1.9-0.7,2.5S889.9,498.1,889,498.1z
											 M888.9,492.3c-0.7,0-1.1,0.2-1.4,0.5s-0.4,0.9-0.5,1.7v0.2c0,0.9,0.2,1.5,0.5,1.9s0.8,0.6,1.4,0.6c0.6,0,1-0.2,1.3-0.7
											s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.8S889.5,492.3,888.9,492.3z"/>
										<path class="st32" d="M897.8,498v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H897.8z"/>
										<path class="st32" d="M904.7,498l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L901,492c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H904.7z M902.5,497.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S902.1,497.3,902.5,497.3z"/>
										<path class="st32" d="M910.4,491.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S910,491.5,910.4,491.5z"/>
										<path class="st32" d="M915,498.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6H913c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S915.5,498.1,915,498.1z M914.8,492.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S915.3,492.3,914.8,492.3z"/>
										<path class="st32" d="M921.1,497.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S920.8,497.3,921.1,497.3
											z"/>
										<path class="st32" d="M925.4,497.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S925.1,497.3,925.4,497.3
											z"/>
										<path class="st32" d="M931.5,498l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5l-0.3-0.7c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H931.5z M929.3,497.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S928.9,497.3,929.3,497.3z"/>
									</g>
								</g>
								<g id="dining_txt_30">
									<g>
										<path class="st32" d="M804.1,507.5c0,0.5-0.2,1-0.5,1.3s-0.7,0.6-1.3,0.7v0c0.7,0.1,1.2,0.3,1.5,0.7s0.5,0.8,0.5,1.4
											c0,0.8-0.3,1.4-0.8,1.9s-1.4,0.7-2.4,0.7c-0.5,0-0.9,0-1.2-0.1s-0.7-0.2-1.1-0.4v-0.9c0.4,0.2,0.8,0.3,1.2,0.4s0.8,0.1,1.2,0.1
											c1.5,0,2.2-0.6,2.2-1.7c0-1-0.8-1.6-2.4-1.6h-0.8v-0.8h0.9c0.7,0,1.2-0.1,1.6-0.4s0.6-0.7,0.6-1.2c0-0.4-0.1-0.7-0.4-1
											s-0.7-0.4-1.2-0.4c-0.4,0-0.7,0.1-1.1,0.2s-0.7,0.3-1.1,0.6l-0.5-0.7c0.4-0.3,0.8-0.5,1.2-0.7s0.9-0.2,1.5-0.2
											c0.8,0,1.5,0.2,1.9,0.6S804.1,506.8,804.1,507.5z"/>
										<path class="st32" d="M811.4,509.7c0,1.5-0.2,2.6-0.7,3.3s-1.2,1.1-2.1,1.1c-0.9,0-1.6-0.4-2.1-1.1s-0.7-1.8-0.7-3.3
											c0-1.5,0.2-2.6,0.7-3.3s1.2-1.1,2.1-1.1c0.9,0,1.6,0.4,2.1,1.1S811.4,508.3,811.4,509.7z M806.7,509.7c0,1.2,0.1,2.2,0.4,2.7
											c0.3,0.6,0.8,0.9,1.4,0.9c0.6,0,1.1-0.3,1.4-0.9c0.3-0.6,0.4-1.5,0.4-2.7s-0.1-2.1-0.4-2.7s-0.8-0.9-1.4-0.9
											c-0.6,0-1.1,0.3-1.4,0.8S806.7,508.5,806.7,509.7z"/>
										<path class="st32" d="M812.9,513.4c0-0.3,0.1-0.5,0.2-0.6s0.3-0.2,0.5-0.2c0.2,0,0.4,0.1,0.5,0.2s0.2,0.3,0.2,0.6
											c0,0.3-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.2c-0.2,0-0.4-0.1-0.5-0.2S812.9,513.7,812.9,513.4z"/>
										<path class="st32" d="M824.8,514l-1.1-2.7h-3.4l-1.1,2.7h-1l3.4-8.6h0.8l3.4,8.6H824.8z M823.5,510.4l-1-2.7
											c-0.1-0.3-0.3-0.7-0.4-1.2c-0.1,0.4-0.2,0.8-0.4,1.2l-1,2.7H823.5z"/>
										<path class="st32" d="M831.3,514v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H831.3z"/>
										<path class="st32" d="M839.8,510.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S839.8,509.8,839.8,510.8z M834.9,510.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S834.9,510,834.9,510.8z"/>
										<path class="st32" d="M843.6,513.3c0.2,0,0.3,0,0.5,0s0.3-0.1,0.4-0.1v0.7c-0.1,0.1-0.3,0.1-0.5,0.1s-0.4,0-0.6,0
											c-1.2,0-1.9-0.7-1.9-2v-3.8h-0.9v-0.5l0.9-0.4l0.4-1.4h0.6v1.5h1.9v0.8h-1.9v3.8c0,0.4,0.1,0.7,0.3,0.9S843.3,513.3,843.6,513.3
											z"/>
										<path class="st32" d="M850.1,514v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.6s-0.5,1-0.5,1.8v3.4h-1
											v-9.1h1v2.8c0,0.3,0,0.6,0,0.8h0.1c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.8,0.6s0.6,1,0.6,1.8v4.2H850.1z"/>
										<path class="st32" d="M855.8,514.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S856.3,514.1,855.8,514.1z M855.6,508.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S856.1,508.3,855.6,508.3z"/>
										<path class="st32" d="M862.8,507.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S862.4,507.5,862.8,507.5z"/>
										<path class="st32" d="M868,505.5h2.4c1.1,0,2,0.2,2.5,0.5s0.8,0.9,0.8,1.6c0,0.5-0.1,0.9-0.4,1.3s-0.7,0.5-1.2,0.6v0.1
											c1.3,0.2,2,0.9,2,2.1c0,0.8-0.3,1.4-0.8,1.8s-1.2,0.6-2.2,0.6h-3V505.5z M869,509.1h1.6c0.7,0,1.2-0.1,1.5-0.3s0.5-0.6,0.5-1.1
											c0-0.5-0.2-0.8-0.5-1s-0.9-0.3-1.6-0.3H869V509.1z M869,510v3.2h1.8c0.7,0,1.2-0.1,1.6-0.4s0.5-0.7,0.5-1.3
											c0-0.5-0.2-0.9-0.5-1.2s-0.9-0.4-1.6-0.4H869z"/>
										<path class="st32" d="M878.6,507.5c0.3,0,0.5,0,0.8,0.1l-0.1,0.9c-0.3-0.1-0.5-0.1-0.7-0.1c-0.5,0-1,0.2-1.3,0.6
											s-0.6,0.9-0.6,1.6v3.4h-1v-6.4h0.8l0.1,1.2h0c0.2-0.4,0.5-0.7,0.9-1S878.2,507.5,878.6,507.5z"/>
										<path class="st32" d="M886.1,510.8c0,1-0.3,1.9-0.8,2.5s-1.3,0.9-2.2,0.9c-0.6,0-1.1-0.1-1.5-0.4s-0.8-0.7-1-1.2
											s-0.4-1.1-0.4-1.8c0-1,0.3-1.9,0.8-2.4s1.2-0.9,2.2-0.9c0.9,0,1.6,0.3,2.1,0.9S886.1,509.8,886.1,510.8z M881.2,510.8
											c0,0.8,0.2,1.4,0.5,1.9s0.8,0.6,1.4,0.6s1.1-0.2,1.5-0.6s0.5-1.1,0.5-1.9c0-0.8-0.2-1.4-0.5-1.9s-0.8-0.6-1.5-0.6
											c-0.6,0-1.1,0.2-1.4,0.6S881.2,510,881.2,510.8z"/>
										<path class="st32" d="M888.7,510.7c0.2-0.2,0.4-0.6,0.8-0.9l2.1-2.2h1.2l-2.6,2.7l2.8,3.7h-1.2l-2.3-3l-0.7,0.6v2.4h-1v-9.1h1
											v4.8C888.7,509.9,888.7,510.3,888.7,510.7L888.7,510.7z"/>
										<path class="st32" d="M896.8,514.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S897.2,514.1,896.8,514.1z M896.5,508.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S897,508.3,896.5,508.3z"/>
										<path class="st32" d="M905.2,514v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8v3.4h-1
											v-6.4h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8v4.2H905.2z"/>
										<path class="st32" d="M916.2,514h-4.8v-8.6h4.8v0.9h-3.8v2.8h3.6v0.9h-3.6v3.2h3.8V514z"/>
										<path class="st32" d="M923.2,507.6v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6c0.1-0.2,0.3-0.3,0.6-0.5
											c-0.3-0.1-0.6-0.4-0.8-0.7s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H923.2z M918.1,515.1
											c0,0.3,0.1,0.6,0.4,0.8s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2
											c-0.4,0-0.8,0.1-1,0.3S918.1,514.7,918.1,515.1z M918.6,509.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4
											c0-1-0.5-1.4-1.4-1.4c-0.5,0-0.8,0.1-1.1,0.4S918.6,509.2,918.6,509.7z"/>
										<path class="st32" d="M929.8,507.6v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6c0.1-0.2,0.3-0.3,0.6-0.5
											c-0.3-0.1-0.6-0.4-0.8-0.7s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H929.8z M924.7,515.1
											c0,0.3,0.1,0.6,0.4,0.8s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2
											c-0.4,0-0.8,0.1-1,0.3S924.7,514.7,924.7,515.1z M925.2,509.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4
											c0-1-0.5-1.4-1.4-1.4c-0.5,0-0.8,0.1-1.1,0.4S925.2,509.2,925.2,509.7z"/>
										<path class="st32" d="M938.1,506.2c-0.9,0-1.7,0.3-2.2,0.9s-0.8,1.5-0.8,2.6c0,1.1,0.3,2,0.8,2.6c0.5,0.6,1.3,0.9,2.2,0.9
											c0.6,0,1.3-0.1,2-0.3v0.9c-0.6,0.2-1.3,0.3-2.2,0.3c-1.3,0-2.2-0.4-2.9-1.1s-1-1.9-1-3.3c0-0.9,0.2-1.7,0.5-2.3s0.8-1.2,1.4-1.5
											s1.4-0.5,2.2-0.5c0.9,0,1.7,0.2,2.4,0.5l-0.4,0.9C939.3,506.4,938.7,506.2,938.1,506.2z"/>
										<path class="st32" d="M945.8,514l-0.2-0.9h0c-0.3,0.4-0.6,0.7-1,0.8s-0.7,0.2-1.2,0.2c-0.6,0-1.1-0.2-1.5-0.5s-0.5-0.8-0.5-1.4
											c0-1.3,1-2,3.1-2l1.1,0v-0.4c0-0.5-0.1-0.9-0.3-1.1s-0.6-0.4-1-0.4c-0.5,0-1.1,0.2-1.8,0.5L942,508c0.3-0.2,0.7-0.3,1-0.4
											s0.8-0.1,1.1-0.1c0.8,0,1.3,0.2,1.7,0.5s0.6,0.9,0.6,1.6v4.4H945.8z M943.6,513.3c0.6,0,1.1-0.2,1.4-0.5s0.5-0.8,0.5-1.4v-0.6
											l-1,0c-0.8,0-1.3,0.1-1.7,0.4s-0.5,0.5-0.5,1c0,0.4,0.1,0.6,0.3,0.8S943.2,513.3,943.6,513.3z"/>
										<path class="st32" d="M951.4,508.4h-1.6v5.7h-1v-5.7h-1.1v-0.4l1.1-0.4v-0.4c0-1.6,0.7-2.4,2.1-2.4c0.3,0,0.7,0.1,1.2,0.2
											l-0.3,0.8c-0.4-0.1-0.7-0.2-1-0.2c-0.4,0-0.6,0.1-0.8,0.4s-0.3,0.6-0.3,1.2v0.4h1.6V508.4z"/>
										<path class="st32" d="M955.3,514.1c-0.9,0-1.7-0.3-2.2-0.9s-0.8-1.4-0.8-2.4c0-1,0.3-1.9,0.8-2.5s1.2-0.9,2.1-0.9
											c0.8,0,1.4,0.3,1.9,0.8s0.7,1.2,0.7,2.1v0.6h-4.4c0,0.8,0.2,1.3,0.6,1.7s0.9,0.6,1.5,0.6c0.7,0,1.4-0.1,2.1-0.4v0.9
											c-0.3,0.1-0.7,0.3-1,0.3S955.7,514.1,955.3,514.1z M955,508.3c-0.5,0-0.9,0.2-1.2,0.5s-0.5,0.8-0.5,1.4h3.4
											c0-0.6-0.1-1.1-0.4-1.4S955.5,508.3,955,508.3z"/>
									</g>
								</g>
								<g id="dining_txt_title">
									<g>
										<path class="st32" d="M806.3,56.7c0,1.4-0.4,2.5-1.2,3.2s-1.9,1.1-3.3,1.1h-2.4v-8.6h2.6c1.3,0,2.4,0.4,3.1,1.1
											S806.3,55.3,806.3,56.7z M805.2,56.7c0-1.1-0.3-2-0.8-2.5s-1.4-0.8-2.5-0.8h-1.4v6.8h1.2c1.2,0,2.1-0.3,2.7-0.9
											S805.2,57.8,805.2,56.7z"/>
										<path class="st32" d="M807.9,52.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S807.9,53.1,807.9,52.9z M809,61h-1v-6.4h1V61z"/>
										<path class="st32" d="M815.4,61v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8V61h-1v-6.4
											h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8V61H815.4z"/>
										<path class="st32" d="M818.3,52.9c0-0.2,0.1-0.4,0.2-0.5s0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2s0.2,0.3,0.2,0.5
											s-0.1,0.4-0.2,0.5s-0.2,0.2-0.4,0.2c-0.2,0-0.3-0.1-0.4-0.2S818.3,53.1,818.3,52.9z M819.4,61h-1v-6.4h1V61z"/>
										<path class="st32" d="M825.9,61v-4.2c0-0.5-0.1-0.9-0.4-1.2s-0.6-0.4-1.1-0.4c-0.7,0-1.2,0.2-1.5,0.5s-0.5,1-0.5,1.8V61h-1v-6.4
											h0.8l0.2,0.9h0c0.2-0.3,0.5-0.6,0.8-0.7s0.8-0.3,1.2-0.3c0.8,0,1.4,0.2,1.7,0.6s0.6,1,0.6,1.8V61H825.9z"/>
										<path class="st32" d="M834.1,54.6v0.6l-1.2,0.1c0.1,0.1,0.2,0.3,0.3,0.5s0.1,0.5,0.1,0.7c0,0.6-0.2,1.1-0.6,1.5s-1,0.6-1.8,0.6
											c-0.2,0-0.4,0-0.5,0c-0.4,0.2-0.6,0.5-0.6,0.8c0,0.2,0.1,0.3,0.2,0.4s0.4,0.1,0.7,0.1h1.1c0.7,0,1.2,0.1,1.6,0.4
											s0.6,0.7,0.6,1.3c0,0.7-0.3,1.3-0.9,1.6s-1.4,0.6-2.5,0.6c-0.8,0-1.5-0.2-1.9-0.5s-0.7-0.8-0.7-1.3c0-0.4,0.1-0.7,0.4-1
											s0.6-0.5,1.1-0.6c-0.2-0.1-0.3-0.2-0.4-0.3s-0.2-0.3-0.2-0.5c0-0.2,0.1-0.4,0.2-0.6s0.3-0.3,0.6-0.5c-0.3-0.1-0.6-0.4-0.8-0.7
											s-0.3-0.7-0.3-1.1c0-0.7,0.2-1.2,0.6-1.6s1-0.6,1.8-0.6c0.3,0,0.6,0,0.9,0.1H834.1z M829,62.1c0,0.3,0.1,0.6,0.4,0.8
											s0.7,0.3,1.3,0.3c0.8,0,1.4-0.1,1.8-0.4s0.6-0.6,0.6-1c0-0.3-0.1-0.6-0.3-0.7s-0.6-0.2-1.2-0.2h-1.2c-0.4,0-0.8,0.1-1,0.3
											S829,61.7,829,62.1z M829.5,56.7c0,0.4,0.1,0.8,0.4,1s0.6,0.3,1.1,0.3c0.9,0,1.4-0.5,1.4-1.4c0-1-0.5-1.4-1.4-1.4
											c-0.5,0-0.8,0.1-1.1,0.4S829.5,56.2,829.5,56.7z"/>
									</g>
								</g>
							</g>
							
							
							<!-------------- CLICK BOXES --------------->
							<g>
								<rect id="box-dining" x="777.6" y="47.8" class="st6" width="279.6" height="475.1" opacity="0" fill-opacity="0"/>
							</g>
							<g>
								<rect id="box-hotel" x="773.6" y="539.9" class="st6" width="256.1" height="112.3" opacity="0" fill-opacity="0"/>
							</g>
							<g>
								<rect id="box-schools" x="773.3" y="667.8" class="st6" width="270.3" height="146.1" opacity="0" fill-opacity="0"/>
							</g>
							<g>
								<rect  id="box-spa" x="772" y="829.8" class="st6" width="261.9" height="100.6" opacity="0" fill-opacity="0"/>
							</g>
							<g >
								<rect id="box-shopping" x="772.7" y="946.8" class="st6" width="237.9" height="329.1" opacity="0" fill-opacity="0"/>
							</g>
	
						</svg>
				
					</figure>
				</div>
			</div>
		


								

				

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>



	</body>

</html> <!-- end of site. what a ride! -->                                                                                                                                   