<?php
/*
 Template Name: Property Details
*/
?>
<?php
include_once( get_template_directory() . '/MLS-DB-Atlanta.php' );
$data 		= 	new db();

header('Cache-Control: max-age=900');
?>
<?php

// #### REDIRECT TO 404 if property does not exist 

$parts = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
$path_parts= explode('/', $parts[path]);
$mlsid = $path_parts[2];


$query = "SELECT * FROM ".table_name." WHERE MLSNumber = '".$mlsid."'" ;	
if (!mysql_num_rows(mysql_query($query))) {
	//Matrix_Unique_ID does not exist, redirect to 404 using javascript
	echo '<script>window.location="/404"</script>';
	exit();
} 
 	
?>

<?php 
//Get Property Page Title from Query String
function assignPageTitle(){
	
 	
 	$parts2 = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
	$path_parts2= explode('/', $parts2[path]);

 	$mlsid = $path_parts2[2];

 	$query = "SELECT * FROM ".table_name." WHERE MLSNumber = '".$mlsid."'" ;	
 	if (!mysql_query($query)) {
	 	return "The Manor Golf & Country Club";
 	} else {
 		//Property exists, return address as page title
	 	$urlproperty = mysql_fetch_array(mysql_query($query));
	 	//return $urlproperty["ADDRESS"]." ".substr($urlproperty["CITY"].", ".$urlproperty['STATE']." ".$urlproperty['ZIPCODE'], 0, 40). " | The Manor Golf & Country Club";	
	 	//$new_address = $urlproperty["StreetNumber"] . " " . $urlproperty["StreetName"] . " " . $urlproperty["StreetSuffix"];
	 	//return $new_address.", ".substr($urlproperty["City"].", ".$urlproperty'StateOrProvince'].", ".$$urlproperty['PostalCode'], 0, 40) . " | The Manor Golf & Country Club";	
	 	
 	};
 	
}
add_filter('wp_title', 'assignPageTitle', 100);


//Add Facebook Meta content to head
function insert_fb_in_head() {
		$parts = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
		$path_parts= explode('/', $parts[path]);
		$title = urldecode($path_parts[3]);
        echo '<meta property="og:title" content="'.$title.' | The Manor Golf & Country Club"/>';
        echo '<meta property="og:type" content="article"/>';
        echo '<meta property="og:description" content="Check out this property at The Manor Golf & Country Club website."/>';
        echo '<meta property="og:site_name" content="The Manor Golf & Country Club"/>';
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

function get_current_fbURL() {
     $current_url  = 'http';
     $server_https = $_SERVER["HTTPS"];
     $server_name  = $_SERVER["SERVER_NAME"];
     $server_port  = $_SERVER["SERVER_PORT"];
     $request_uri  = $_SERVER["REQUEST_URI"];
     if ($server_https == "on") $current_url .= "s";
     $current_url .= "://";
     if ($server_port != "80") $current_url .= $server_name . ":" . $server_port . $request_uri;
     else $current_url .= $server_name . $request_uri;
     return $current_url;
} 
$fblink="https://www.facebook.com/sharer.php?u=".urlencode(get_current_fbURL());
?><?php get_header(); ?>

<?php



//Grab MLS sys id from URL
$parts = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
$path_parts= explode('/', $parts[path]);
//Get MLS #
$id = $path_parts[2];
//Get MLS Title
$title = str_replace ("-"," ",$path_parts[3]);

$queryString = "select * from mls_properties_atlanta where MLSNumber='$id'";
$runQuery = mysql_query ($queryString);
$mlsData = mysql_fetch_array ($runQuery);

//Search for homes nearby withing 10% of prize in same zone
$zoneCode = $mlsData['list_19'];
$propertyType = $mlsData['list_1'];
$highPrice = $mlsData['list_137']+($mlsData['list_137'] * 0.15);
$lowPrice = $mlsData['list_137']-($mlsData['list_137'] * 0.15);
$currentProperty = $mlsData['MLSNumber'];
$zonequeryString = "select * from mls_properties_atlanta where list_19='$zoneCode' AND list_1='$propertyType' AND list_137 <= '$highPrice' AND list_137 >= '$lowPrice' AND MLSNumber != '$currentProperty' ORDER BY list_137 DESC LIMIT 3";

$runzoneQuery = mysql_query ($zonequeryString);
$zoneData = mysql_fetch_array ($runzoneQuery);


$num_similarlistings = mysql_num_rows($runzoneQuery);

$locationstring_unfiltered = str_replace($mlsData['list_17'], "", str_replace(" TE"," Terrace", str_replace(" CONC"," Concourse",str_replace(" PW"," Pkwy",$mlsData['list_881'])))).", ".$mlsData['list_922'].", ".$mlsData['list_924']." ".$mlsData['list_10'];
$locationstring_filtered = str_replace("."," ", $locationstring_unfiltered);
$locationstring = urlencode($locationstring_filtered);






$title 		= $mlsData["list_881"]." ".substr($mlsData["list_922"].", ".$mlsData['list_924']." ".$mlsData['list_10'], 0, 40);	
$site_title = str_replace(",","",$title);
$site_title = str_replace("#","",$site_title);
$site_title = str_replace(".","",$site_title);
$site_title	= preg_replace ('/\s+/','-',$site_title);

$URLtitle 		= $mlsData["list_881"].", ".substr($mlsData["list_922"].", ".$mlsData['list_924'].", ".$mlsData['list_10'], 0, 40);	
$urlsite_title = str_replace(",","",$URLtitle);
$urlsite_title = str_replace("#","",$urlsite_title);
$urlsite_title = str_replace(".","",$urlsite_title);
$urlsite_title	= preg_replace ('/\s+/','-',$urlsite_title);

?><div id="content">

				<div id="inner-content" class="wrap cf defaultwrapper">

					<div class="address">
							<h1><?php 
									$new_address = $mlsData["StreetNumber"] . " " . $mlsData["StreetName"] . " " . $mlsData["StreetSuffix"];

									echo $new_address;  ?></h1>
									<?php 
										
										$mapinfo = pods( 'listingstatus' ); 
									    $params = array('limit' => 400, 'where' => 'mls_id.meta_value = "' . $mlsData["MLSNumber"] . '"');  
										$mapinfo->find($params);
										while ( $mapinfo->fetch() ) { 
										
											if ($mapinfo->field('lot_number')   ) { 	?>
											
												<div class="lotdetail-information">Lot <?php echo $mapinfo->field('lot_number'); ?> - <?php echo $mapinfo->field('lot_status'); ?></div>
												
											<?php	
											}
												
										}
						
									?>
									
						</div>
						
						<div class="top-property-menu cf">
							<div class="left-side">
								<!--<a href="#" id="link-photos">Photos</a>-->

								<?php/* if ($mlsData["VIRTUALTOURLINK"]) {?><a href="<?php echo $mlsData["VIRTUALTOURLINK"]; ?>" id="link-videos" target="_blank">Videos</a><?php } */?>
								
								<?php echo $mlsData["BedsTotal"]; ?> bedrooms, <?php echo $mlsData["BathsFull"]; ?> full and <?php echo $mlsData["BathsHalf"]; ?> half bathrooms | $<?php echo number_format($mlsData["ListPrice"]); ?> | MLSID <?php echo $mlsData["MLSNumber"]; ?>  

							</div>
							

							<div class="right-side">
								<a href="/find-your-home/" id="link-return">< Return to Listings</a>
							</div>
						</div>
							
						<div id="main" class="m-all t-all d-2of3 cf" role="main">
							
							
							
							<div class="property-left-side">
								<div id="property-slideshow">
									
									<div id="imgslider2" class="flexslider">
					  <ul class="slides">
					    <?php 
							           		//Get MLS Image
										   $data->retImage3($mlsData['Matrix_Unique_ID']); 
							           ?>
					  </ul>
					</div>

<div class="property-thumbnails">
			    <div id="carouselslider2" class="flexslider" style="max-height: 144px;">
					  <ul class="slides">
					    <?php 
						           		//Get MLS Image
									   $data->retImage3($mlsData['Matrix_Unique_ID']); 
						           ?>
				  </ul>
				</div>
		    </div>


								</div>
								<div class="details">
									
									
									
									
											<?php echo $mlsData["PublicRemarksConsumerView"]; ?>
									
									
								</div>
							</div><!-- end property-left-side -->
							
							
							
						</div> <!--end main -->

						<div class="m-all t-all d-1of3 last-col cf">
							<div class="property-right-side">
								<div class="top-info">
									<div>
								<div class="m-all t-1of2 d-1of2 cf"><img id="agent" src="<?php echo get_template_directory_uri(); ?>/library/images/agent.jpg"/></div>
												<div class="m-all t-1of2 d-1of2 last-col cf">

													<h2 class="agentname">Carol Dick</h2>
													<div class="address">5750 Windward Pkwy, Suite 300<br />
														Alpharetta, Georgia 30005</div> 
													<div class="phones">
														+1 678.394.3700<br />
														+1 770.824.9070  
													</div>
													<div class="afhagent">ENGEL & V&Ouml;LKERS North Point</div>
								                </div>
										</div>


										<div class="request-info cf">
									
									<div class="m-all t-all d-all cf"><?php echo do_shortcode( "[contact-form-7 id='2392' title='prop-details-page-form_copy']" ) ?></div>
								
								</div>

								<div class="info-links cf">
									<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
									<script type="text/javascript">
										stLight.options({
											publisher:'e38d8263-b71a-4ffc-b033-77cf299b45c6'
										
										});
									</script>
									<a href="/contact/" class="info-link-2">Contact Us</a>
									<span class="st_sharethis" st_url="<?php echo site_url()."/property/".$mlsData['Matrix_Unique_ID']."/".trim($urlsite_title); ?>" st_title="<?php echo $title;?>" displayText=""></span>
									
									<a href="<?php echo site_url(); ?>/print/<?php echo $mlsData['Matrix_Unique_ID']; ?>/<?php echo trim($site_title); ?>" target="_blank" class="info-link-3">Print Brochure</a>
									<a href="/premier-neighborhoods/schools/" class="info-link-4">School Info</a>
									<a href="/mortgage-calculator/" class="info-link-5">Mortgage Calculator</a>
									<!--<a href="#" class="info-link-6">Save Property</a>-->
								</div>
								
							</div><!-- end property-right-side-->
						</div>
						
						
						
						

				</div>

			</div>
			
	<script>
		jQuery(document).ready(function(){
			jQuery("#input_2_5").val("<?php echo $mlsData["ADDRESS"];?>");
		});
	</script>
<?php get_footer(); ?>