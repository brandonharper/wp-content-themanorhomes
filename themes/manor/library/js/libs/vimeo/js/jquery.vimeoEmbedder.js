/*!
 *  vimeoEmbedder
 *  @version 1.0
 *  @date August 10, 2013
 *  @author Jason Safir http://jasonsafir.com
 *  Project: https://github.com/jasonsafir/vimeoEmbedder
 *
 *  Extend the customization and styling configuration of the Vimeo video player using jQuery.
 */

/* Usage and Options: */ 
$( document ).ready(function() {
		var vimeoID = "136224804"; // specify Vimeo video ID to be played
		var width = "100%"; // define WIDTH of Vimeo video canvas
		var height = "auto";	 // define HEIGHT of Vimeo video canvas
		var image = "/wp-content/uploads/2015/10/hero-bg.jpg"; // path of your video placeholder image
		var imageArray = ['/wp-content/uploads/2016/02/homepage-2.jpg', '/wp-content/uploads/2016/02/homepage-1.jpg'];
		var randomImage = imageArray [Math.floor(Math.random() * imageArray .length)];
		var vimeoColor = "36acbc"; // specify the color of the video controls
		var playButtonColor= "36acbc"; // set the background color of play button
		var vimeoBgColor = "rgba(255, 255, 255, 0)"; // set the background color of Vimeo player

		$('#vimeoEmbedder').html("<div id=\"video\" vimeo-color="+ vimeoColor +" vimeo-id="+ vimeoID +"><div class=\"video-container\" style=\"width:"+ width +"; height:"+ height +"; background-color:"+ vimeoBgColor +";\"> <img alt=\"Play\" src="+ randomImage +" class=\"placeholder\"></div></div>"); // Get the HTML contents of the video player

});

vimeoEmbedder = function(){

	$("#video").each(function(){
		
		var videoObj = $(this),
		videoContainer = $(".video-container", this),
		placeholderImg = videoObj.find(".placeholder"),
		videoColour = "aaaaaa";
	
		if((placeholderImg.attr("height") / placeholderImg.attr("width")) > .57) $(".video-container", this).addClass("fourbythree");
		
		if(videoObj.attr("vimeo-color")) videoColour = videoObj.attr("vimeo-color");
		
		var videoHTML = '<iframe src="http://player.vimeo.com/video/' + videoObj.attr("vimeo-id") + '?title=0&amp;byline=0&amp;portrait=0&amp;color=' + videoColour + '&amp;autoplay=1&amp;api=1" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
			
		$(".play, .placeholder", this).on("click", function(e){
			videoContainer.empty().append(videoHTML);
		});
	
	});
	
	if($("body").hasClass("home")) $("html").addClass("hideScroll");
	else $("html").removeClass("hideScroll");

}

$(document).ready(function() { vimeoEmbedder(); }); // Initiate the plugin