<?php get_header(); ?>


			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							
							<h1 style="margin-left: 24px;">Blog</h1>

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" class="thespace" >
								 

											

								<header class="article-header">

									<a href="<?php the_permalink() ?>">

										<?php
										// Must be inside a loop.

										if ( has_post_thumbnail() ) {
											the_post_thumbnail('full', array( 'class'	=> "featured-image-post attachment-post-thumbnail"));
										}
										else {
											echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/thumbnail-default.jpg" />';
										}
											?>
										</a>

									
									<p class="byline entry-meta vcard">
                                                                        <?php printf( __( '', 'bonestheme' ).' %1$s %2$s',
                       								/* the time the post was published */
                       								'<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time> ',
                       								/* the author of the post */
                       								' '
                    							); ?>
									</p>

									<h1 class="h2 entry-title" ><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

								</header>

								<section class="entry-content cf" >
									<?php the_content(); ?>
									<hr/>
								</section>

								<footer class="article-footer cf">
									<p class="footer-comment-count">
										<?php /* comments_number( __( '<span>0</span> Comments ', 'bonestheme' ), __( '<span>1</span> Comment ', 'bonestheme' ), __( '<span>%</span> Comments', 'bonestheme' ) );*/?>
									
										<?php
          echo getCrunchifyPostViews(get_the_ID());
?>	
									</p>


                 				<p class="footer-category"> </p>

                  <?php the_tags( '<p class="footer-tags tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>
                 

								</footer>

							</article>

							<?php endwhile; ?>

									<?php bones_page_navi(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>


						</main>

					<?php get_sidebar(); ?>

				</div>

			</div>


<?php get_footer(); ?>