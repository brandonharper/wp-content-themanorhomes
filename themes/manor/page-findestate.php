<?php
/*
 Template Name: Find an estate details
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
<?php

include_once( get_template_directory() . '/MLS-DB-Atlanta.php' );
$data 		= 	new db();

?>
<?php

	$data 			=   new db();
	$_SESSION['viewtype']	= 		$viewtype		= 	($_REQUEST ["view_type"]== '') ? $_SESSION['viewtype'] : $_REQUEST ["view_type"] ;

	//$result = "SELECT * from mls_properties_atlanta WHERE SubdComplex LIKE 'The Manor' AND PropertyType LIKE 'Residential Detached'";
	$result = "SELECT * from mls_properties_atlanta WHERE SubdComplex LIKE 'The Manor' ";
	
	$current_page 	= 	site_url()."/".get_page_uri($page_id);

	$tbl_name="mls_properties_atlanta";		//your table name
	// How many adjacent pages should be shown on each side?
	$adjacents = 2;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	
	
	
	
	global $user_ID; if( $user_ID ) : 
		if( current_user_can('level_10') ) : 
			//Shows SQL Query 
			//echo "<br />---------------- PRINTING QUERY RESULT------------------<Br />";
			//echo "Result is: ".$result;
 	//echo "<br />---------------- PRINTING QUERY RESULT------------------<Br />";
	//echo "Result is: ".$result;
	//Print_r ($_SESSION);
  endif;
	endif;
 
 		$query = $result;		
	$total_pages = mysql_num_rows(mysql_query($query));
	//echo $total_pages;
	
	/* Setup vars for query. */
	$targetpage = $current_page; 	//your file name  (the name of this file)
	
	$limit = 100;  
						//how many items to show per page
	
	
	$page = (get_query_var('page')) ? get_query_var('page') : 1; 

	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
		
		
	/* Get data. */
	$sql = $result." LIMIT $start, $limit";
	$execute = mysql_query ($sql);

	
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage/$prev\">&lt;&nbsp;Previous Page&nbsp;</a>";
		else
			//$pagination.= "<span class=\"disabled\">&lt;&nbsp;PREVIOUS&nbsp;</span>";	
			$pagination.= "";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
				else
					$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
					else
						$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage/$lpm1\">&nbsp;$lpm1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/$lastpage\">&nbsp;$lastpage&nbsp;</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage/1\">&nbsp;1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/2\">&nbsp;2&nbsp;</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
					else
						$pagination.= "<a href=\"$targetpage/$counter\">&nbsp;$counter&nbsp;</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage/$lpm1\">&nbsp;$lpm1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/$lastpage\">&nbsp;$lastpage&nbsp;</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage/1\">&nbsp;1&nbsp;</a>";
				$pagination.= "<a href=\"$targetpage/2\">&nbsp;2&nbsp;</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">&nbsp;$counter&nbsp;</span>";
					else
						$pagination.= "<a href=\"$targetpage/$counter\"> &nbsp;$counter &nbsp;</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
		{
			$pagination.= "<a href=\"$targetpage/$next\">&nbsp;Next Page&nbsp;></a>";
			}
		else
		{
				$pagination.= "<span class=\"disabled\">&nbsp;Next Page&nbsp;></span>";
		}
		$pagination.= "</div>\n";		
		
		
		
	}

	
?>
<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<meta name="viewport" content="width=device-width,initial-scale=1" />

		




		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
		
		<!-- Galleria -->
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/galleria/galleria-1.4.2.min.js"></script>
 <style>
	 
	 .find-an-estate {
		 width: 1692px;
		 margin: 40px auto;
	 }
	  
	body.page-template-page-findestate-php {
		background: none;
		background-color: white;
	}
	
	.page-template-page-findestate-php .display-grid-view li {
		width: 810px;
		display: inline-block;
		float: none;
	}
	
	.page-template-page-findestate-php .display-grid-view li:nth-child(odd) { margin-right: 50px; }
	
	.page-template-page-findestate-php .display-grid-view .property {
		width: 100%;
		padding-bottom: 20px;
		border-bottom: 1px solid $brown;
		margin-bottom: 20px;
	}
	
	.page-template-page-findestate-php .display-grid-view .property .property-image {
		
		width: 100%;
	}
	.page-template-page-findestate-php .display-grid-view .property .property-image img {
		width: 810px;
		height: 540px;
		margin-bottom: 0px;
	}
	.page-template-page-findestate-php .display-grid-view .property .property-details {
		width:  100%;
		height: 120px;
	
		font-size: 12px;
		
		line-height: 1.6;
	}
	.page-template-page-findestate-php .display-grid-view .property .property-details .detail-top {
		
	}
	.page-template-page-findestate-php .display-grid-view .property .property-details .detail-top .left a { /* address */
		width: 50%;
		float: left;
		font-family: Georgia, serif;
		font-size: 30px;
		line-height: 1.2;
		
		color: #0f6748;
		margin-bottom: 10px;
	}
	.page-template-page-findestate-php .display-grid-view .property .property-details .detail-top .right  { /* price */
		width: 50%;
		float: right;
		text-align: right;
		padding-top: 40px;
		font-family: Georgia, serif;
		font-size: 30px;
		line-height: 1.2;
	
		color: #0f6748;
		margin-bottom: 10px;
	}
	.page-template-page-findestate-php .display-grid-view .property .property-details .detail-bottom .left{ /* address */
		width: 50%;
		float: left;
		font-family: sans-serif;
		font-size: 25px;
		line-height: 1.2;
		
		color: #636466;
		margin-bottom: 10px;
	}
	.page-template-page-findestate-php .display-grid-view .property .property-details .detail-bottom .right  { /* price */
		width: 50%;
		float: right;
		text-align: right;
		
		font-family: sans-serif;
		font-size: 22px;
		line-height: 1.2;
		font-weight: 300;
		color: #636466;
		margin-bottom: 10px;
	}

	

	
	
  </style>

  <script>
	 
	  
	  
	  
	  jQuery(document).ready(function($) {
	  	
					

		});
	  </script>
	</head>
	
	<body <?php body_class(); ?>>
	
		


						

							
		<section class="entry-content cf" itemprop="articleBody">
			
			<div class="find-an-estate">
				<div class="find-container">
					<ul class="display-grid-view cf">
					<?php while ($mlsData = mysql_fetch_array($execute)) { ?>
					<?php 
						
						$new_address = $mlsData["StreetNumber"] . " " . $mlsData["StreetName"] . " " . $mlsData["StreetSuffix"];
						
						$title 		= $new_address.", <Br />".substr($mlsData["City"].", ".$mlsData['StateOrProvince']." ".$mlsData['PostalCode'], 0, 40);	
	
		              	
		            	$site_title = str_replace(",","",$title);
		            	$site_title = str_replace("#","",$site_title);
		            	$site_title = str_replace(".","",$site_title);
		             	$site_title	= preg_replace ('/\s+/','-',$site_title);
		             	
		             	$URLtitle 		= $$new_address.", ".substr($mlsData["City"].", ".$mlsData['StateOrProvince'].", ".$mlsData['PostalCode'], 0, 40);	
	
		             	$urlsite_title = str_replace(",","",$URLtitle);
		            	$urlsite_title = str_replace("#","",$urlsite_title);
		            	$urlsite_title = str_replace(".","",$urlsite_title);
		             	$urlsite_title	= preg_replace ('/\s+/','-',$urlsite_title);
					?>
					<li>
						<div class="property">
							
							<div class="cf">
								<div class="property-image">
									<a href="<?php echo site_url(); ?>/find-estate-details/?id=<?php echo $mlsData['Matrix_Unique_ID']; ?>"><img src="<?php echo $data->get_MLSimage($mlsData['Matrix_Unique_ID']);?>"/></a>
								</div>
								<div class="property-details">
									
									<div class="detail-top">
										<div class="left">
											<a href="<?php echo site_url(); ?>/find-estate-details/?id=<?php echo $mlsData['Matrix_Unique_ID']; ?>"><?php if (!$viewtype || $viewtype == 'list') { ?><?php echo $new_address.", ".$mlsData["City"].", ".$mlsData['StateOrProvince']." ".$mlsData['PostalCode'];  ?><?php } else if ($viewtype == 'grid') {?><?php echo $mlsData["ADDRESS"]."<br /> ".$mlsData["CITY"].", ".$mlsData["STATE"]." ".$mlsData["ZIPCODE"];  ?><?php } ?></a>
										</div>
									    <div class="right">
										    $<?php echo number_format($mlsData["ListPrice"])?>
									    </div>
									</div>
									<div class="detail-bottom">
										<div class="left">
											ID: <?php echo $mlsData["MLSNumber"];?>
										</div>
									    <div class="right">
										    <?php echo ($mlsData["BedsTotal"] ? $mlsData["BedsTotal"] : "0")?> Bedrooms | <?php echo ($mlsData["BathsFull"] ? $mlsData["BathsFull"] : "0")?> Full Baths | <?php echo ($mlsData["BathsHalf"] ? $mlsData["BathsHalf"] : "0")?> Half Baths
									    </div>
									</div>
									
									
										
										
								</div>
								
							
							
							</div>
							
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
			
			
			
			
		</section>


								

							

									

				

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>



	</body>

</html> <!-- end of site. what a ride! -->                                                                                                                                   