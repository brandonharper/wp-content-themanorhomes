<?php
/*
 Template Name: Property Print
*/
?>
<?php get_header(); ?>

<?php

	include_once( get_template_directory() . '/MLS-DB-Atlanta.php' );
	$data 		= 	new db();
	
	//Grab MLS sys id from URL
	$parts = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
	$path_parts= explode('/', $parts[path]);
	//Get MLS #
	$id = $path_parts[2];
	//Get MLS Title
	$title = str_replace ("-"," ",$path_parts[3]);
	
	$queryString = "select * from mls_properties_atlanta where Matrix_Unique_ID='$id'";
	$runQuery = mysql_query ($queryString);
	$mlsData = mysql_fetch_array ($runQuery);
		
		$new_address = $mlsData["StreetNumber"] . " " . $mlsData["StreetName"] . " " . $mlsData["StreetSuffix"];
	$locationstring = $new_address ." ".$mlsData['City']." ".$mlsData['State'];
	
	?><div id="content">

				<div id="inner-content" class="wrap cf defaultwrapper">

						<div id="main" class="m-all t-all d-all cf defaultpages" role="main">
							
								<div id="property-details" class="clearfix">
									<div id="inner" class="clearfix">																		
																			
										<img src="/wp-content/uploads/2014/10/web-site-logo.png" style="margin: 0 auto; width: 400px; display: block;"/>
										<div id="property-left" class="clearfix">
											
											<div class="clearfix">	
										           <img src="<?php $data->get_MLSimage($mlsData['Matrix_Unique_ID']); ?>" width="734" height="550" style="float: left;"/>
										           <div id="property-right" >
											
											<div class="property-contact clearfix" >
												<div class="top-info">
													<img id="agent" src="<?php echo get_template_directory_uri(); ?>/library/images/agent.jpg"/>
													<h2>Carol Dick</h2>
													5750 Windward Pkwy, Suite 300<br />
														Alpharetta, Georgia 30005 
													<div class="phones">
														+1 678.394.3700<br />
														+1 770.824.9070  
													</div>
													ENGEL & V&Ouml;LKERS North Point
												</div>	
											</div>
	
											<div class="print-showing-today">Schedule A Showing Today</div>
										</div><!-- end #property-right -->					
											</div>    <!-- end <div class="clearfix">	 -->  
										           
										   
																						
											<div style="width: 555px; float: left;">
												<div class="section-title">Details for <?php echo $new_address;?></div>
												<div class="price">$<?php echo number_format($mlsData["ListPrice"]); ?></div>
												
												<div class="specs">
													Bedrooms: <?php echo $mlsData["BedsTotal"]; ?><br />
													Full Baths: <?php echo $mlsData["BathsFull"]; ?> <br />
													Half Baths: <?php echo $mlsData["BathsHalf"]; ?> <br />
													Property Type: <?php echo $mlsData["PropertyType"]; ?> <br />
													Year Built: <?php echo $mlsData["YearBuilt"]; ?><br />
													MLS ID: <?php echo $mlsData["MLSNumber"]; ?> <br />
													Subdivision: The Manor Golf and Country Club<br />
													Schools: Summit Hill, Cambridge
													
												</div>
											 

											</div>
											<div style="width: 420px; float: left; margin-left: 45px;">
												<h3>PROPERTY MAP</h3>
												<div class="printmap">
													<!--<iframe width="400" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $locationstring; ?>&amp;sspn=34.313287,86.044922&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $locationstring; ?>&amp;z=16&amp;output=embed"></iframe>-->
													<img width="400" height="400" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $locationstring; ?>&zoom=15&size=400x400&sensor=false&markers=color:red|<?php echo $locationstring; ?>" />
																							
												
													</div><!-- end #map -->	
												</div><div class="clearfix"></div>								
								
			
											
										</div>
										<!-- end #property-left -->
										
														
								</div><!-- end #inner -->
								
	
							</div> <!-- end #property-detail -->					
							
						</div>

				</div>

			</div>
			
<?php get_footer(); ?>