<?php get_header(); ?>



			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<article id="post-not-found" class="hentry cf">

							<header class="article-header">

								<h1 class="white-header-text-center"><?php _e( '404 Error', 'bonestheme' ); ?></h1>

							</header>

							<section class="entry-content">

								<p style="color:#ffffff;font-size: 1.988em;line-height: 1.371;text-align: center;font-weight: 300;padding:5%0;">Sorry, this page doesn’t exist or some other error has occured. <br/>
									Go back to the previous page or start again at the homepage.</p>

							</section>

							<section class="returnhome" style="text-align:center;">

								
										<a href="www.themanorhomes.com" class="btn" >Return to Home Page</a>


									

							</section>

							<footer class="article-footer">

									<p><?php // _e( 'This is the 404.php template.', 'bonestheme' ); ?></p>

							</footer>

						</article>

					</main>

				</div>

			</div>

<?php get_footer(); ?>